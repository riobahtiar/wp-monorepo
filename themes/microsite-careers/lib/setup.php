<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
	// Enable features from Soil when plugin is activated
	// https://roots.io/plugins/soil/

	// Make theme available for translation
	// Community translations can be found at https://github.com/roots/sage-translations
	load_theme_textdomain( 'sage', get_template_directory() . '/lang' );

	// Register wp_nav_menu() menus
	// http://codex.wordpress.org/Function_Reference/register_nav_menus
	register_nav_menus(
		[
			'primary_navigation' => __( 'Primary Navigation', 'sage' ),
		]
	);

	// Use main stylesheet for visual editor
	// To add custom styles edit /assets/styles/layouts/_tinymce.scss
	add_editor_style( Assets\asset_path( 'styles/main.css' ) );
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\\setup' );

/**
 * Register sidebars
 */
function widgets_init() {
	register_sidebar(
		[
			'name'          => __( 'Primary', 'sage' ),
			'id'            => 'sidebar-primary',
			'before_widget' => '<section class="widget %1$s %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		]
	);

	register_sidebar(
		[
			'name'          => __( 'Footer', 'sage' ),
			'id'            => 'sidebar-footer',
			'before_widget' => '<section class="widget %1$s %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		]
	);
}
add_action( 'widgets_init', __NAMESPACE__ . '\\widgets_init' );

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
	static $display;

	isset( $display ) || $display = ! in_array(
		true, [
			// The sidebar will NOT be displayed if ANY of the following return true.
			// @link https://codex.wordpress.org/Conditional_Tags
			is_404(),
			is_front_page(),
			is_page_template(
				[
					'template-custom.php',
					'template-about.php',
					'template-careers.php',
					'template-careers_v2.php',
					'template-careers_v3.php',
					'template-function.php',
					'template-function_v2.php',
					'template-function_v3.php',
					'template-nakama.php',
					'template-explore.php',
				]
			),
			is_page(),
		]
	);

	return apply_filters( 'sage/display_sidebar', $display );
}

/**
 * Theme assets
 */
function assets() {
	wp_enqueue_style( 'font-montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:400,600,800' );
	wp_enqueue_style( 'FontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );

	wp_enqueue_style( 'sage/css', Assets\asset_path( 'styles/main.css' ), false, null );

	// Load careers css
	if ( is_page_template( 'template-careers.php' ) ) {
		wp_enqueue_style( 'careers', Assets\asset_path( 'styles/careers.css' ), false, null );
	}
	if ( is_page_template( 'template-careers_v2.php' ) ) {
		wp_enqueue_style( 'careers_v2', Assets\asset_path( 'styles/careers_v2.css' ), false, null );
	}
	if ( is_page_template( 'template-careers_v3.php' ) ) {
		wp_enqueue_style( 'careers_v3', Assets\asset_path( 'styles/careers_v2.css' ), false, null );
	}

	// Load function css
	if ( is_page_template( 'template-function.php' ) ) {
		wp_enqueue_style( 'function-page', Assets\asset_path( 'styles/function.css' ), false, null );
	}
	if ( is_page_template( 'template-function_v2.php' ) ) {
		wp_enqueue_style( 'function-page_v2', Assets\asset_path( 'styles/function_v2.css' ), false, null );
	}
	if ( is_page_template( 'template-function_v3.php' ) ) {
		wp_enqueue_style( 'function-page_v3', Assets\asset_path( 'styles/function_v2.css' ), false, null );
	}

	// Load About
	if ( is_page_template( 'template-about.php' ) ) {
		wp_enqueue_style( 'about-page', Assets\asset_path( 'styles/about.css' ), false, null );
	}

	// Load Nakama Css
	if ( is_page_template( 'template-nakama.php' ) ) {
		wp_enqueue_style( 'nakama-page', Assets\asset_path( 'styles/nakama.css' ), false, null );
	}

	// Explore page
	if ( is_page_template( 'template-workplace.php' ) ) {
		wp_enqueue_style( 'workplace-page', Assets\asset_path( 'styles/workplace.css' ), false, null );
	}

	// Blogs page
	if ( is_page_template( 'template-blog.php' ) ) {
		wp_enqueue_style( 'blogs-page', Assets\asset_path( 'styles/blogs.css' ), false, null );
	}

	// Our Community page
	if ( is_page_template( 'template-community.php' ) ) {
		wp_enqueue_style( 'community-page', Assets\asset_path( 'styles/community.css' ), false, null );
	}

	// Front Page
	if ( is_home() ) {
		wp_enqueue_style( 'fp-style', Assets\asset_path( 'styles/front-page.css' ), false, null );
	}

	wp_enqueue_script( 'sage/js', Assets\asset_path( 'scripts/main.js' ), [ 'jquery' ], null, true );
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100 );