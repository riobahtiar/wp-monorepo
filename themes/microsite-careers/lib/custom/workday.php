<?php
/**
 * Workday SOAP API Request
 * Author: Rhoma Cahyanti
*/

namespace WorkdayAPI;

class workday {
	/*
	* Initialize Variables
	*/
	private $client;
	private $config = [];
	private $oasis  = 'http://docs.oasis-open.org/wss/2004/01';

	public $joblist;

	/* 
	* Construct 
	*/
	public function __construct() {
		
	}

	/* 
	* Connect to Workday SOAP API
	*/
	public function connect() {
		ini_set( 'soap.wsdl_cache_enabled', '1' );
		ini_set( 'soap.wsdl_cache_ttl', '86400' );

		// Config
		$this->config['wsdl']          = 'https://wd3-impl-services1.workday.com/ccx/service/tokopedia2/Recruiting/v31.0?wsdl';
		$this->config['namespace']     = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
		$this->config['username']      = 'All_Emp_User@tokopedia2';
		$this->config['password']      = 'Password#123';
		$this->config['password_type'] = 'PasswordText';
		$this->config['soap_options']  = array(
			'uri'                => 'http://schemas.xmlsoap.org/soap/envelope/',
			'style'              => SOAP_RPC,
			'use'                => SOAP_ENCODED,
			'soap_version'       => SOAP_1_1,
			'cache_wsdl'         => WSDL_CACHE_NONE,
			'connection_timeout' => 15,
			'trace'              => true,
			'encoding'           => 'UTF-8',
			'exceptions'         => true,
			'stream_context'=>stream_context_create(
				array('http'=>
					array(
						'protocol_version'=>'1.0',
						'header' => 'Connection: Close'
					)
				)
			)
		);

		// Header
		$header = '
		<SOAP-ENV:Header xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
			<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="' . $this->oasis . '/oasis-200401-wss-wssecurity-secext-1.0.xsd">
				<wsse:UsernameToken>
				<wsse:Username>' . $this->config['username'] . '</wsse:Username>
				<wsse:Password Type="' . $this->oasis . '/oasis-200401-wss-username-token-profile-1.0#PasswordText">'
				. $this->config['password'] . '</wsse:Password>
				</wsse:UsernameToken>
			</wsse:Security>
		</SOAP-ENV:Header>';

		// New SOAP Client
		$this->client = new \SoapClient( $this->config['wsdl'], $this->config['soap_options'] );

		$wss_username_token_header = new \SoapHeader(
			$this->oasis . '/oasis-200401-wss-wssecurity-secext-1.0.xsd',
			'Security',
			new \SoapVar( $header, XSD_ANYXML ),
			true
		);

		$this->client->__setSoapHeaders( $wss_username_token_header );
	}

	/*
	* Get All Workday SOAP API Operations
	*/
	public function get_soap_api_functions() {
		$this->connect();
		$resp = $this->client->__getFunctions();

		return $resp;
	}

	/*
	* Request Job Postings
	*/
	public function get_job_posting_request() {
		$this->connect();
		$request = array(
			'Get_Job_Postings_Request' => array(
				'Request_Criteria' => array(
					'Show_Only_Active_Job_Postings' => true,
				),
			),
		);

		$resp = $this->client->__soapCall( 'Get_Job_Postings', $request );
		
		return $resp;
	}

	/*
	* Request Job Posting by ID
	*/
	public function get_job_posting_item_request( $id ) {
		$this->connect();
		$request = array(
			'Get_Job_Postings_Request' => array(
				'Request_Reference' => array(
					'Job_Posting_Reference' => array(
						'ID' => $id,
					),
				),
			),
		);

		$resp = $this->client->__soapCall( 'Get_Job_Postings', $request );
		
		return $resp;
	}

	/*
	* Request Job Requisitions
	*/ 
	public function get_job_requisitions_request( $count ) {
		$this->connect();
		$request = array(
			'Get_Job_Requisitions_Request' => array(
				'Response_Filter' => array(
					'Count' => $count,
				),
			),
		);

		$resp = $this->client->__soapCall( 'Get_Job_Requisitions', $request );

		return $resp;
	}

	/*
	* Request Put Applicant
	*/
	public function put_applicant_request() {
		$this->connect();
		$request = array(
			'Put_Applicant_Request' => array(
				'Applicant_Data' => array(
					'Personal_Data' => array(
						'Name_Data'    => array(
							'Legal_Name_Data' => array(
								'Name_Detail_Data' => array(
									'Country_Reference' => array(
										'ID' => 'ID',
									),
									'First_Name'        => 'TestFirstName2',
									'Last_Name'         => 'TestLastName2',
								),
							),
						),
						'Contact_Data' => array(
							'Email_Address_Data' => array(
								'Email_Address' => 'test2@tokopedia.com',
								'Usage_Data'    => array(
									'Public'    => 1,
									'Type_Data' => array(
										'Primary'        => 1,
										'Type_Reference' => array(
											'ID' => array(
												'_'    => 'Work',
												'type' => 'Communication_Usage_Type_ID',
											),
										),
									),
								),
							),
						),
					),
				),
			),
		);

		$resp = $this->client->__soapCall( 'Put_Applicant', $request );

		return $resp;
	}
	
	/*
	* Get Job Family
	*/
	public function get_job_family( $job_family ) {
		$list_job_family_group = array(
			'Business'    => array(
				'BU-Bussiness Dev.',
				'BU-Community',
				'BU-Corp. Banking',
				'BU-Management',
			),
			'Finance'     => array(
				'FA-Analytics',
				'FA-Bussiness Finance',
				'FA-Corporate Finance & Investor Relation',
				'FA-Escrow & Transaction',
				'FA-Finance Controller',
				'FA-Internal Control',
				'FA-Legal',
				'FA-Management',
				'FA-Procurement',
			),
			'Marketing'   => array(
				'Job Families',
				'MA-Communication',
				'MA-Creative',
				'MA-Internet Marketing',
				'MA-Management',
				'MA-Media Planner',
				'MA-Social Media',
			),
			'Operational' => array(
				'OP-Back Office',
				'OP-Call Center',
				'OP-Management',
				'OP-Ops Logistic',
				'OP-Ops Social Media',
				'OP-Ops Strategic',
				'OP-Ops Support',
				'OP-Resolution',
				'OP-SIM',
			),
			'People'      => array(
				'PE-Assistant',
				'PE-Management',
				'PE-Office Management',
				'PE-Organization & Data',
				'PE-Payroll & Comben',
				'PE-People Dev.',
				'PE-Recruitment',
			),
			'Product'     => array(
				'PO-Management',
				'PO-Product Design',
				'PO-Product Owner',
			),
			'Technology'  => array(
				'TE-IT Infrastructure',
				'TE-IT Security',
				'TE-Management',
				'TE-Quality Assurance',
				'TE-Risk Management',
				'TE-Software Engineer',
			),
		);

		foreach ( $list_job_family_group as $key => $value ) {
			foreach ( $value as $job_families ) {
				if ( $job_family == $job_families ) {
					$job_family_name = $key;
					return $job_family_name;
				}
			}
		}
	}

	/*
	* Get All Job (Active and External Job) and Store in new Array
	*/
	public function get_jobs( $per_page = 900 ) {
		$jobs_transient_name 	= 'workday_job_posting_perpage_' . $per_page;
		$jobs_transient			= get_transient( $jobs_transient_name );
		
		if ( false ===  $jobs_transient ) {
			$resp     = $this->get_job_posting_request();
			$all_jobs = json_decode( json_encode( $resp ), true );
			// $total_jobs	= $all_jobs['Response_Results']['Total_Results'];

			$i = 0;
			foreach ( $all_jobs['Response_Data']['Job_Posting'] as $job ) {
				// get only external job posting
				if( array_key_exists( 'External_Job_Path', $job['Job_Posting_Data'] ) && array_key_exists( 'External_Job_Path', $job['Job_Posting_Data'] ) ) {
					$this->joblist[ $i ]['id']                       = $job['Job_Posting_Reference']['ID'][0]['_'];
					$this->joblist[ $i ]['shortcode']                = $job['Job_Posting_Reference']['ID'][0]['_'];
					$this->joblist[ $i ]['requisition_id']           = array_key_exists( 'Job_Requisition_ID', $job['Job_Posting_Data'] ) ? $job['Job_Posting_Data']['Job_Requisition_ID'] : '';
					$this->joblist[ $i ]['title']                    = $job['Job_Posting_Data']['Job_Posting_Title'];
					$this->joblist[ $i ]['description']              = $job['Job_Posting_Data']['Job_Posting_Description'];
					$this->joblist[ $i ]['department']               = $this->get_job_family( $job['Job_Posting_Data']['Job_Family_Reference']['ID'][1]['_'] );
					$this->joblist[ $i ]['function']                 = sanitize_title( $this->get_job_family( $job['Job_Posting_Data']['Job_Family_Reference']['ID'][1]['_'] ) );
					$this->joblist[ $i ]['type']                     = $job['Job_Posting_Data']['Job_Type_Reference']['ID'][1]['_'];
					$this->joblist[ $i ]['time_type']                = $job['Job_Posting_Data']['Time_Type_Reference']['ID'][1]['_'];
					$this->joblist[ $i ]['location']['country']      = strpos( $job['Job_Posting_Data']['Job_Posting_Location_Data']['Primary_Location_Reference']['ID'][1]['_'], 'Indonesia' ) ? 'Indonesia' : 'India';
					$this->joblist[ $i ]['location']['country_code'] = $this->joblist[ $i ]['location']['country'] == 'Indonesia' ? 'ID' : 'IND';
					$this->joblist[ $i ]['location']['region']       = $this->joblist[ $i ]['location']['country'] == 'Indonesia' ? 'Jakarta' : 'India';
					$this->joblist[ $i ]['location']['region_code']  = $this->joblist[ $i ]['location']['country'] == 'Indonesia' ? 'Jakarta' : '';
					$this->joblist[ $i ]['location']['city']         = $this->joblist[ $i ]['location']['country'] == 'Indonesia' ? 'Jakarta' : '';
					$this->joblist[ $i ]['location']['zip_code']     = $this->joblist[ $i ]['location']['country'] == 'Indonesia' ? '' : '';
					$this->joblist[ $i ]['location']['office']       = $this->joblist[ $i ]['location']['country'] == 'Indonesia' ? 'Jakarta Head Office' : 'India Office';
					$this->joblist[ $i ]['location']['telecomuting'] = $this->joblist[ $i ]['location']['country'] == 'Indonesia' ? '' : '';
					$this->joblist[ $i ]['external_job_path']        = $job['Job_Posting_Data']['External_Job_Path'];
					$this->joblist[ $i ]['external_apply_url']       = $job['Job_Posting_Data']['External_Apply_URL'];
					$this->joblist[ $i ]['slug']                     = sanitize_title( $this->joblist[ $i ]['title'] . ' for ' . $this->joblist[ $i ]['department'] . '-in-' . $this->joblist[ $i ]['location']['country'] );
					$this->joblist[ $i ]['plain_description']        = sanitize_text_field( $this->joblist[ $i ]['description'] );
					$i++;
				}
			}
			set_transient( $jobs_transient_name, json_encode ( $this->joblist ) );
		}
		else {
			$this->joblist = json_decode( $jobs_transient, true );
		}

		// echo "Response time: " . ( microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'] ). ' seconds';
		return $this->joblist;
	}

	/*
	* Get Job Item (by Shortcode/ID)
	*/
	public function get_job_item( $shortcode ) {
		if ( $shortcode == '' or $shortcode == null ) {
			$job_item = array(
				'code'    => 'rest_no_route',
				'message' => 'Sorry, No route was found matching the URL and request method.',
				'data'    => array( 'code' => 404 ),
			);
		} else {
			$uniq_name     = 'workday_job_';
			$transient_job = get_transient( $uniq_name . $shortcode );

			if ( $transient_job != '' && $transient_job != 0 ) {
				$job_item = $transient_job;
			} else {
				$resp = $this->get_job_posting_item_request( $shortcode );
				$job  = json_decode( json_encode( $resp ), true );
				
				$job_item['id']                       = $job['Job_Posting_Reference']['ID'][0]['_'];
				$job_item['shortcode']                = $job['Job_Posting_Reference']['ID'][0]['_'];
				$job_item['requisition_id']           = array_key_exists( 'Job_Requisition_ID', $job['Job_Posting_Data'] ) ? $job['Job_Posting_Data']['Job_Requisition_ID'] : '';
				$job_item['title']                    = $job['Job_Posting_Data']['Job_Posting_Title'];
				$job_item['description']              = $job['Job_Posting_Data']['Job_Posting_Description'];
				$job_item['department']               = $this->get_job_family( $job['Job_Posting_Data']['Job_Family_Reference']['ID'][1]['_'] );
				$job_item['function']                 = sanitize_title( $this->get_job_family( $job['Job_Posting_Data']['Job_Family_Reference']['ID'][1]['_'] ) );
				$job_item['type']                     = $job['Job_Posting_Data']['Job_Type_Reference']['ID'][1]['_'];
				$job_item['time_type']                = $job['Job_Posting_Data']['Time_Type_Reference']['ID'][1]['_'];
				$job_item['location']['country']      = strpos( $job['Job_Posting_Data']['Job_Posting_Location_Data']['Primary_Location_Reference']['ID'][1]['_'], 'Indonesia' ) ? 'Indonesia' : 'India';
				$job_item['location']['country_code'] = $job_item['location']['country'] == 'Indonesia' ? 'ID' : 'IND';
				$job_item['location']['region']       = $job_item['location']['country'] == 'Indonesia' ? 'Jakarta' : 'India';
				$job_item['location']['region_code']  = $job_item['location']['country'] == 'Indonesia' ? 'Jakarta' : '';
				$job_item['location']['city']         = $job_item['location']['country'] == 'Indonesia' ? 'Jakarta' : '';
				$job_item['location']['zip_code']     = $job_item['location']['country'] == 'Indonesia' ? '' : '';
				$job_item['location']['office']       = $job_item['location']['country'] == 'Indonesia' ? 'Jakarta Head Office' : 'India Office';
				$job_item['location']['telecomuting'] = $job_item['location']['country'] == 'Indonesia' ? '' : '';
				$job_item['external_job_path']        = array_key_exists( 'External_Job_Path', $job['Job_Posting_Data'] ) ? $job['Job_Posting_Data']['External_Job_Path'] : '';
				$job_item['external_apply_url']       = array_key_exists( 'External_Apply_URL', $job['Job_Posting_Data'] ) ? $job['Job_Posting_Data']['External_Apply_URL'] : '';
				$job_item['slug']                     = sanitize_title( $job_item['title'] . ' for ' . $job_item['department'] . '-in-' . $job_item['location']['country'] );
				$job_item['plain_description']        = sanitize_text_field( $job_item['description'] );

				set_transient( $uniq_name . $shortcode, $job_item, 6 * HOUR_IN_SECONDS );
			}
		}
		return $job_item;
	}

	/*
	* Get All Job (Grouped by Function)
	*/
	public function get_grouped_jobs( $view = false ) {
		$jobsv     = $this->get_jobs();
		$op        = - 1;
		$opv       = - 1;
		$arr_jobs  = array();
		$data      = array();
		$functions = array( 'product', 'technology', 'people', 'business', 'finance', 'operational', 'marketing', 'internship' );

		if ( $view == true ) {
			foreach ( $jobsv as $jobs ) {
				$data[] = array(
					'id'                 => $jobs['id'],
					'shortcode'          => $jobs['shortcode'],
					'requisition_id'     => $jobs['requisition_id'],
					'title'              => $jobs['title'],
					'description'        => $jobs['description'],
					'department'         => $jobs['department'],
					'function'           => sanitize_title( $jobs['department'] ),
					'type'               => $jobs['type'],
					'time_type'          => $jobs['time_type'],
					'location'           => $jobs['location'],
					'external_job_path'  => $jobs['external_job_path'],
					'external_apply_url' => $jobs['external_apply_url'],
					'slug'               => $jobs['slug'],
					'plain_description'  => sanitize_text_field( $jobs['description'] ),
				);
			}

			foreach ( $data as $key => $value ) {
				$sort[ $key ] = $value['title'];
			}

			array_multisort( $sort, SORT_ASC, $data );

			$arr_jobs['jobs'] = $data;

			return $arr_jobs;
		} else {
			foreach ( $jobsv as $jobs ) {
				$data[] = array(
					'id'                 => $jobs['id'],
					'shortcode'          => $jobs['shortcode'],
					'requisition_id'     => $jobs['requisition_id'],
					'title'              => $jobs['title'],
					'description'        => $jobs['description'],
					'department'         => $jobs['department'],
					'function'           => sanitize_title( $jobs['department'] ),
					'type'               => $jobs['type'],
					'time_type'          => $jobs['time_type'],
					'location'           => $jobs['location'],
					'external_job_path'  => $jobs['external_job_path'],
					'external_apply_url' => $jobs['external_apply_url'],
					'slug'               => $jobs['slug'],
					'plain_description'  => sanitize_text_field( $jobs['description'] ),
				);
			}

			$arr_jobs = $data;

			foreach ( $arr_jobs as $job ) {
				$opv ++;
				$department[ $arr_jobs[ $opv ]['function'] ][] = $job;
			}

			return (array) $department;
		}
	}

	/*
	* Search Job by Input String
	*/
	public function search_job( $string ) {
		echo " Search string: " . $string . "</br>";
		$jobs = $this->get_jobs();
		$job_found = array();

		foreach( $jobs as $job ) {
			if ( strpos( strtolower( $job['title'] ), strtolower( $string ) ) !== false ) {
				$job_found[] = $job;
			}
		}
		return $job_found;
	} 

	/*
	* Get Selected Job from Slug
	*/
	public function get_job_slug( $slug = '', $type = false ) {
		$job_listing = $this->get_grouped_jobs( true )['jobs'];
		$arr         = array_keys( array_column( $job_listing, 'slug' ), $slug );

		if ( ! empty( $arr ) ) {
			if ( $type == true ) {
				if ( $slug == '' or $slug == null ) {
					$job = array(
						'code'    => 'rest_no_route',
						'message' => 'Sorry, No route was found matching the URL and request method.',
						'data'    => array( 'code' => 404 ),
					);
				}
			} 
			else {
				$uniq_name = 'workday_job_';
				$ts_job    = get_transient( $uniq_name . $slug );
	
				if ( $ts_job != '' && $ts_job != 0 ) {
					// echo "Job retrieve from transient!";
					$selected_job = json_decode( json_encode( $ts_job ), true );
				} else {
					foreach( $job_listing as $job ) {
						if ( $job['slug'] == $slug ) {
							$selected_job = $job;
						}
					}
					// echo "Job retrieve from workday api!";
					set_transient( $uniq_name . $slug, json_encode( $selected_job ) );
				}
			}
			return (array) $selected_job;
		} 
	}
}

// Schedule Func Cron Job Event to Get Jobs
if ( ! wp_next_scheduled( 'cron_workdayapi_twicedaily_hook' ) ) {
	wp_schedule_event( time(), 'twicedaily', 'cron_workdayapi_twicedaily_hook' );
}

add_action( 'cron_workdayapi_twicedaily_hook', 'hit_workday' );

// Get Jobs from Workday API (Scheduled by Cron Job)
function hit_workday() {
	$transient_name = 'workday_job_posting_perpage_900';

	delete_transient( $transient_name );

	$wws  = new workday;

	$jobs = $wws->get_jobs();

	set_transient( $transient_name, $jobs );

	wp_mail( 'rhoma.cahyanti@tokopedia.com', 'Careers Get Jobs [Automatic email Cron Job ' . date( 'H:i:s' ) . ']', 'Automatic scheduled get jobs from Workday Done. Run on: ' . date( 'H:i:s' ) . ' - ' . date( 'd F Y' ) );

}
