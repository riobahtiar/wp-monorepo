<?php

/**
 * Remove unused wp features
 */

// Disable wp_embed
function disable_embeds_code_init() {
	add_filter( 'embed_oembed_discover', '__return_false' );
	remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
}
add_action( 'init', 'disable_embeds_code_init', 9999 );

/* Remove Wp Emoji Release */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Limit Text
 *
 * @param $text
 * @param $limit
 *
 * @return string
 */
function limit_text( $text, $limit ) {
	if ( str_word_count( $text, 0 ) > $limit ) {
		$words = str_word_count( $text, 2 );
		$pos   = array_keys( $words );
		$text  = substr( $text, 0, $pos[ $limit ] ) . '...';
	}

	return $text;
}

/**
 * Generate Sitemap XML
 */
if ( ! wp_next_scheduled( 'sitemap_hook' ) ) {
	wp_schedule_event( time(), 'daily', 'sitemap_hook' );
}

add_action( 'sitemap_hook', 'create_sitemap' );

function create_sitemap() {
	$wk   = new Careers\workable;
	$cat  = json_decode( $wk->get_jobs( 150 ), true );
	$dept = $wk->get_grouped_jobs( false );
	$jobs = $wk->get_grouped_jobs( true );

	$sitemap  = '<?xml version="1.0" encoding="UTF-8"?>';
	$sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

	//Home
	$sitemap .= '<url>' .
		'<loc>' . 'https://www.tokopedia.com/careers/' . '</loc>' .
		'<changefreq>monthly</changefreq>' .
	 '</url>';

	//Function
	foreach ( $dept as $key => $value ) {
		$sitemap .= '<url>' .
			'<loc>' . 'https://www.tokopedia.com/careers/function/' . $key . '/' . '</loc>' .
			'<changefreq>monthly</changefreq>' .
		 '</url>';
	}

	//Jobs
	$sitemap .= '<url>' .
		'<loc>' . 'https://www.tokopedia.com/careers/jobs/' . '</loc>' .
		'<changefreq>monthly</changefreq>' .
	 '</url>';

	//Job
	foreach ( $jobs['jobs'] as $job ) {
		$sitemap .= '<url>' .
		'<loc>' . 'https://www.tokopedia.com/careers/job/' . $job['slug'] . '/' . '</loc>' .
		'<lastmod>' . date( 'Y-m-d', strtotime( $job['created_at'] ) ) . '</lastmod>' .
		'<changefreq>monthly</changefreq>' .
		'</url>';
	}

	$sitemap .= '</urlset>';

	$fp = fopen( get_template_directory() . '/sitemap.xml', 'w' );

	fwrite( $fp, $sitemap );
	fclose( $fp );
}
//add_action( 'init', 'create_sitemap' );

/**
 * Generate Sitemap Index XML
 */
if ( ! wp_next_scheduled( 'sitemap_index_hook' ) ) {
	wp_schedule_event( time(), 'daily', 'sitemap_index_hook' );
}

add_action( 'sitemap_index_hook', 'create_sitemap_index' );

function create_sitemap_index() {
	$wk   = new Careers\workable;
	$cat  = json_decode( $wk->get_jobs( 150 ), true );
	$dept = $wk->get_grouped_jobs( false );
	$jobs = $wk->get_grouped_jobs( true );

	$sitemap  = '<?xml version="1.0" encoding="UTF-8"?>';
	$sitemap .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

	//Home
	$sitemap .= '<sitemap>' .
		'<loc>' . 'https://www.tokopedia.com/careers/' . '</loc>' .
		'<changefreq>monthly</changefreq>' .
	 '</sitemap>';

	//Function
	foreach ( $dept as $key => $value ) {
		$sitemap .= '<sitemap>' .
			'<loc>' . 'https://www.tokopedia.com/careers/function/' . $key . '/' . '</loc>' .
			'<changefreq>monthly</changefreq>' .
		 '</sitemap>';
	}

	//Jobs
	$sitemap .= '<sitemap>' .
		'<loc>' . 'https://www.tokopedia.com/careers/jobs/' . '</loc>' .
		'<changefreq>monthly</changefreq>' .
	 '</sitemap>';

	//Job
	foreach ( $jobs['jobs'] as $job ) {
		$sitemap .= '<sitemap>' .
		'<loc>' . 'https://www.tokopedia.com/careers/job/' . $job['slug'] . '/' . '</loc>' .
		'<lastmod>' . date( 'Y-m-d', strtotime( $job['created_at'] ) ) . '</lastmod>' .
		'<changefreq>monthly</changefreq>' .
		'</sitemap>';
	}

	$sitemap .= '</sitemapindex>';

	$fp = fopen( get_template_directory() . '/sitemap_index.xml', 'w' );

	fwrite( $fp, $sitemap );
	fclose( $fp );
}
//add_action( 'init', 'create_sitemap_index' );
