<?php

/*===================================
Get job list from workable
=====================================*/
function get_jobs() {
	$url = 'https://www.tokopedia.com/careers/ws-tkpd/wp/v2/jobs?list_view=true&per_page=150';

	$cget_jobs = curl_init();
	curl_setopt( $cget_jobs, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $cget_jobs, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $cget_jobs, CURLOPT_URL, $url );
	$jobs = curl_exec( $cget_jobs );
	curl_close( $cget_jobs );

	$jobs    = json_decode( $jobs, true );
	$joblist = array();

	foreach ( $jobs as $job ) {
		$i = 0;
		foreach ( $job as $job_detail ) {
			$slug        = $job_detail['slug'];
			$title       = $job_detail['title'];
			$shortcode   = $job_detail['shortcode'];
			$description = sanitize_text_field( $job_detail['plain_description'] );

			$joblist[ $shortcode ] = $title;
			$i++;
		}
	}

	return $joblist;
}

/*===================================
Load job list as select choices
=====================================*/
function acf_load_job_title_workable_field_choices( $field ) {

	// reset choices
	$field['choices'] = array();

	$choices = get_jobs();

	// loop through array and add to field 'choices'
	if ( is_array( $choices ) ) {
		foreach ( $choices as $key => $value ) {
			$field['choices'][ $key ] = $value;
		}
	}

	// return the field
	return $field;
}

add_filter( 'acf/load_field/name=list_job_workable', 'acf_load_job_title_workable_field_choices' );

/*===================================
Load department list
=====================================*/
function acf_load_department_list_field_choices( $field ) {

	// reset choices
	$field['choices'] = array();

	$choices = array(
		'business'    => 'Business',
		'finance'     => 'Finance',
		'product'     => 'Product',
		'operational' => 'Operational',
		'people'      => 'People',
		'technology'  => 'Technology',
		'internship'  => 'Internship',
	);

	// loop through array and add to field 'choices'
	if ( is_array( $choices ) ) {
		foreach ( $choices as $key => $value ) {
			$field['choices'][ $key ] = $value;
		}
	}

	// return the field
	return $field;
}

add_filter( 'acf/load_field/name=department_name', 'acf_load_department_list_field_choices' );
