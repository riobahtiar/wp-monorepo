<?php
/**
 * Custom SEO for Dynamic Pages
 */

if (
	is_front_page() || is_home() ||
	is_page( [ 'job', 'join', 'function' ] ) ||
	is_page_template( [ 'template-custom.php', 'template-about.php', 'template-careers.php', 'template-careers_v2.php', 'template-careers_v3.php', 'template-function.php', 'template-function_v2.php', 'template-function_v3.php' ] )
) {
	// Remove existing Title
	remove_action( 'wp_head', '_wp_render_title_tag', 1 );
	// Remove existing Title
	remove_action( 'wp_head', 'rel_canonical', 1 );
}
