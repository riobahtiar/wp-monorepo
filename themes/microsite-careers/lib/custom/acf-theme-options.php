<?php
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
		array(
			'page_title' => 'Careers Options',
			'menu_title' => 'Options',
			'menu_slug'  => 'acf-options',
			'capability' => 'edit_posts',
			'icon_url'   => 'dashicons-universal-access-alt',
			'redirect'   => false,
		)
	);
}
