<?php
/**
 * +++++ Rest API Workable and Workday Mapping +++++
 */

 /**
 * @todo: Modify url base from wp-json to 'ws-tkpd'
 */
add_filter( 'rest_url_prefix', 'tkpd_api_slug' );

function tkpd_api_slug( $slug ) {
	return 'ws-tkpd';
}

/**
* @todo: Add New endpoint to 'ws-tkpd'
*/
function init_tkpd_router() {
	// @todo: -> Register wp/v2/list-jobs
	register_rest_route(
		'wp/v2', '/jobs', array(
			'methods'  => WP_REST_Server::READABLE,
			'callback' => 'wp_get_jobs',
			'args'     => wp_get_data_args(),
		)
	);

	register_rest_route(
		'wp/v3', '/jobs', array(
			'methods'  => WP_REST_Server::READABLE,
			'callback' => 'wp_get_jobs_workday',
			'args'     => wp_get_data_args(),
		)
	);
}

add_action( 'rest_api_init', 'init_tkpd_router' );


/**
 * @todo: push content to wp/v2/list-jobs
 */
function wp_get_jobs( $request ) {
	$wk           = new Careers\workable;
	$get_jobs     = $wk->get_jobs( $request['per_page'] );
	$grouped_jobs = $wk->get_grouped_jobs( $request['list_view'] );

	return rest_ensure_response( $grouped_jobs );
}

function wp_get_jobs_workday( $request ) {
	$wws 			= new WorkdayAPI\workday;

	if( $request['search'] != '' ) {
		$jobs = $wws->search_job( $request['search'] );
	} else {
		// $all_jobs   	= $wws->get_jobs( $request['per_page'] );
		$jobs	= $wws->get_grouped_jobs( $request['list_view'] );
	}

	return rest_ensure_response( $jobs );
}

/**
 * @todo: Args for our rest api.
 */
function wp_get_data_args() {
	$args               = array();
	$args['per_page']   = array(
		'description' => esc_html__( 'The filter parameter is used to filter the collection of jobs per page', 'careers' ),
		'type'        => 'string',
	);
	$args['search']   = array(
		'description' => esc_html__( 'The filter parameter is used to filter the collection of jobs by string input', 'careers' ),
		'type'        => 'string',
	);
	$args['department'] = array(
		'description' => esc_html__( 'The filter parameter is used to filter the collection of jobs by Department', 'careers' ),
		'type'        => 'string',
	);
	$args['list_view']  = array(
		'description' => esc_html__( 'The filter parameter is used to filter the collection of jobs by View', 'careers' ),
		'type'        => 'string',
	);
	return $args;
}
