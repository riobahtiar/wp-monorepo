<?php
/**
 * +++++ Custom Page Router +++++
 * - Never expose to public this api.
 * - This is Production API
 */


/**
 * FUNC: Job Info Router
 * @var [type]
 */
add_action( 'init', 'careers_router_init' );
function careers_router_init() {
	// Page job
	add_rewrite_rule(
		'job/(.+?)/?$',
		'index.php?pagename=job&slug=$matches[1]',
		'top'
	);
	// Page Apply Form
	add_rewrite_rule(
		'join/(.+?)/?$',
		'index.php?pagename=join&slug=$matches[1]',
		'top'
	);
	// Page Function Job
	add_rewrite_rule(
		'function/(.+?)/?$',
		'index.php?pagename=function&slug=$matches[1]',
		'top'
	);
	add_rewrite_tag( '%slug%', '([^&]+)' );
}

/**
 * FUNC: Register Param
 *
 * @param  [string] $query_slug [description]
 *
 * @return [type]          [description]
 */
