<?php
/**
 * +++++ Workable Authenticator +++++
 * - Never expose to public this api.
 * - This is Production API
 */

// Declare ns workble
namespace Careers;

class workable {
	// workable
	private $workable_token = 'd64c494f75cb7122dcfc4c18dd405d12da3a917eec02368441af27b08649058d';
	public $workable_uri    = 'https://tokopedia.workable.com';
	public $jobslist;

	public function get_jobs( $per_page = 500 ) {
		$uniq_name = 'workable_get_perpage_';
		$value     = get_transient( $uniq_name . $per_page );
		$headers   = array(
			'Content-Type: application/json',
			'Authorization: Bearer ' . $this->workable_token,
		);

		if ( $value != '' && $value != 0 ) {
			$this->jobslist = $value;
		} else {
			$cget_jobs = curl_init();
			curl_setopt( $cget_jobs, CURLOPT_URL, $this->workable_uri . '/spi/v3/jobs?state=published&include_fields=description&limit=' . $per_page );
			curl_setopt( $cget_jobs, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $cget_jobs, CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $cget_jobs, CURLOPT_HTTPHEADER, $headers );
			$this->jobslist = curl_exec( $cget_jobs );
			curl_close( $cget_jobs );
			set_transient( $uniq_name . $per_page, $this->jobslist );
		}

		return $this->jobslist;
	}

	public function get_grouped_jobs( $view = false ) {
		$jobsv    = json_decode( $this->jobslist, true );
		$op       = - 1;
		$opv      = - 1;
		$arr_jobs = array();
		$data     = array();

		//exclude internal jobs
		$arr_dept = array( 'Product', 'Technology', 'People', 'Business', 'Finance', 'Operational', 'Internship' );

		if ( $view == true ) {
			foreach ( $jobsv['jobs'] as $jobv ) {
				$op ++;
				foreach ( $arr_dept as $dept ) {
					if ( $jobsv['jobs'][ $op ]['department'] == $dept ) {

						$data[] = array(
							'id'                => $jobsv['jobs'][ $op ]['id'],
							'title'             => $jobsv['jobs'][ $op ]['title'],
							'full_title'        => $jobsv['jobs'][ $op ]['full_title'],
							'shortcode'         => $jobsv['jobs'][ $op ]['shortcode'],
							'code'              => $jobsv['jobs'][ $op ]['code'],
							'state'             => $jobsv['jobs'][ $op ]['state'],
							'department'        => $jobsv['jobs'][ $op ]['department'],
							'url'               => $jobsv['jobs'][ $op ]['url'],
							'application_url'   => $jobsv['jobs'][ $op ]['application_url'],
							'shortlink'         => $jobsv['jobs'][ $op ]['shortlink'],
							'location'          => $jobsv['jobs'][ $op ]['location'],
							'created_at'        => $jobsv['jobs'][ $op ]['created_at'],
							'description'       => $jobsv['jobs'][ $op ]['description'],
							'function'          => sanitize_title( $jobsv['jobs'][ $op ]['department'] ),
							'unique_class'      => 'item-' . sanitize_title( $jobsv['jobs'][ $op ]['department'] . ' ' . $jobsv['jobs'][ $op ]['shortcode'] ),
							'slug'              => sanitize_title( $jobsv['jobs'][ $op ]['title'] . ' for ' . $jobsv['jobs'][ $op ]['department'] . ' in ' . $jobsv['jobs'][ $op ]['location']['region'] . '-' . $jobsv['jobs'][ $op ]['location']['country'] ),
							'plain_description' => sanitize_text_field( $jobsv['jobs'][ $op ]['description'] ),
						);
					}
				}
			}

			foreach ( $data as $key => $value ) {
				$sort[ $key ] = $value['title'];
			}

			array_multisort( $sort, SORT_ASC, $data );

			$arr_jobs['jobs'] = $data;

			return $arr_jobs;
		} else {
			foreach ( $jobsv['jobs'] as $jobv ) {
				$op ++;
				foreach ( $arr_dept as $dept ) {
					if ( $jobsv['jobs'][ $op ]['department'] == $dept ) {

						$data[] = array(
							'id'                => $jobsv['jobs'][ $op ]['id'],
							'title'             => $jobsv['jobs'][ $op ]['title'],
							'full_title'        => $jobsv['jobs'][ $op ]['full_title'],
							'shortcode'         => $jobsv['jobs'][ $op ]['shortcode'],
							'code'              => $jobsv['jobs'][ $op ]['code'],
							'state'             => $jobsv['jobs'][ $op ]['state'],
							'department'        => $jobsv['jobs'][ $op ]['department'],
							'url'               => $jobsv['jobs'][ $op ]['url'],
							'application_url'   => $jobsv['jobs'][ $op ]['application_url'],
							'shortlink'         => $jobsv['jobs'][ $op ]['shortlink'],
							'location'          => $jobsv['jobs'][ $op ]['location'],
							'created_at'        => $jobsv['jobs'][ $op ]['created_at'],
							'description'       => $jobsv['jobs'][ $op ]['description'],
							'function'          => sanitize_title( $jobsv['jobs'][ $op ]['department'] ),
							'unique_class'      => 'item-' . sanitize_title( $jobsv['jobs'][ $op ]['department'] . ' ' . $jobsv['jobs'][ $op ]['shortcode'] ),
							'slug'              => sanitize_title( $jobsv['jobs'][ $op ]['title'] . ' for ' . $jobsv['jobs'][ $op ]['department'] . ' in ' . $jobsv['jobs'][ $op ]['location']['region'] . '-' . $jobsv['jobs'][ $op ]['location']['country'] ),
							'plain_description' => sanitize_text_field( $jobsv['jobs'][ $op ]['description'] ),
						);
					}
				}
			}

			foreach ( $data as $key => $value ) {
				$sort[ $key ] = $value['title'];
			}

			array_multisort( $sort, SORT_ASC, $data );

			$arr_jobs['jobs'] = $data;

			foreach ( $arr_jobs['jobs'] as $job ) {
				$opv ++;
				$department[ $arr_jobs['jobs'][ $opv ]['function'] ][] = $job;
			}

			return (array) $department;
		}
	}

	public function get_job_item( $shortcode = '' ) {
		if ( $shortcode == '' or $shortcode == null ) {
			$job = array(
				'code'    => 'rest_no_route',
				'message' => 'Sorry, No route was found matching the URL and request method.',
				'data'    => array( 'code' => 404 ),
			);
		} else {
			$uniq_name = 'workable_job_';
			$ts_job    = get_transient( $uniq_name . $shortcode );
			$headers   = array(
				'Content-Type: application/json',
				'Authorization: Bearer ' . $this->workable_token,
			);
			if ( $ts_job != '' && $ts_job != 0 ) {
				$job = $ts_job;
			} else {
				$cget_jobs = curl_init();
				curl_setopt( $cget_jobs, CURLOPT_URL, $this->workable_uri . '/spi/v3/jobs/' . $shortcode );
				curl_setopt( $cget_jobs, CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $cget_jobs, CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $cget_jobs, CURLOPT_HTTPHEADER, $headers );
				$job = curl_exec( $cget_jobs );
				curl_close( $cget_jobs );
				set_transient( $uniq_name . $shortcode, $job, 3 * HOUR_IN_SECONDS );
			}
		}

		return $job;
	}

	public function get_job_slug( $slug = '', $type = false ) {
		$job_listing = $this->get_grouped_jobs( true )['jobs'];
		$arr         = array_keys( array_column( $job_listing, 'slug' ), $slug );

		if ( ! empty( $arr ) ) {
			$shortcode = $job_listing[ $arr[0] ]['shortcode'];

			if ( $type == true ) {
				if ( $shortcode == '' or $shortcode == null ) {
					$job = array(
						'code'    => 'rest_no_route',
						'message' => 'Sorry, No route was found matching the URL and request method.',
						'data'    => array( 'code' => 404 ),
					);
				} else {
					$uniq_name = 'wb_job_';
					$ts_job    = get_transient( $uniq_name . $shortcode );
					$headers   = array(
						'Content-Type: application/json',
						'Authorization: Bearer ' . $this->workable_token,
					);
					if ( $ts_job != '' && $ts_job != 0 ) {
						$selected_job = $ts_job;
					} else {
						$cget_jobs = curl_init();
						curl_setopt( $cget_jobs, CURLOPT_URL, $this->workable_uri . '/spi/v3/jobs/' . $shortcode );
						curl_setopt( $cget_jobs, CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $cget_jobs, CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $cget_jobs, CURLOPT_HTTPHEADER, $headers );
						$selected_job = (array) json_decode( curl_exec( $cget_jobs ), true );
						curl_close( $cget_jobs );
						set_transient( $uniq_name . $shortcode, $selected_job, 3 * HOUR_IN_SECONDS );
					}
				}
			} else {
				$selected_job = $job_listing[ $arr[0] ];
			}

			return (array) $selected_job;
		}
	}

	public function get_job_application_form( $slug = '' ) {
		$job_listing = $this->get_grouped_jobs( true )['jobs'];
		$arr         = array_keys( array_column( $job_listing, 'slug' ), $slug );
		$shortcode   = $job_listing[ $arr[0] ]['shortcode'];
		$uniq_name   = 'fa_job_';

		$fa_job = $ts_job = get_transient( $uniq_name . $shortcode );

		$headers = array(
			'Content-Type: application/json',
			'Authorization: Bearer ' . $this->workable_token,
		);

		if ( $fa_job != '' && $fa_job != 0 ) {
			$selected_job = $fa_job;
		} else {
			$cget_jobs = curl_init();
			curl_setopt( $cget_jobs, CURLOPT_URL, $this->workable_uri . '/spi/v3/jobs/' . $shortcode . '/application_form' );
			curl_setopt( $cget_jobs, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $cget_jobs, CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $cget_jobs, CURLOPT_HTTPHEADER, $headers );
			$selected_job = (array) json_decode( curl_exec( $cget_jobs ), true );
			curl_close( $cget_jobs );
			set_transient( $uniq_name . $shortcode, $selected_job, 3 * HOUR_IN_SECONDS );
		}

		return json_encode( $selected_job );
	}

	public function insert_candidate( $shortcode = null, $data = null ) {
		$authentication = array(
			'Content-Type: application/json',
			'Authorization: Bearer ' . $this->workable_token,
			'Accept: application/json',
		);
		if ( $shortcode != null or $data != null ) {
			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $this->workable_uri . '/spi/v3/jobs/' . $shortcode . '/candidates' );
			// curl_setopt($ch, CURLOPT_FAILONERROR, true);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $authentication );
			curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
			curl_setopt( $ch, CURLOPT_REFERER, 'wp-careers' );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
			$result = curl_exec( $ch );
			// var_dump(curl_getinfo($ch));
			// echo curl_errno($ch) . '<br/>';
			if ( $result == false ) {
				$result = curl_error( $ch );
			}
			// echo curl_error($ch) . '<br/>';
			curl_close( $ch );
		} else {
			$result = array(
				'code'    => 'rest_no_route',
				'message' => 'Sorry, No route was found matching the URL and request method.',
				'data'    => array(
					'code' => 404,
				),
			);
		}

		return $result;
	}


	// end of class Careers
}


// Schedule Func Cron Job Event to Scheduled Get Jobs


if ( ! wp_next_scheduled( 'careers_cron_twicedaily_hook' ) ) {
	wp_schedule_event( time(), 'twicedaily', 'careers_cron_twicedaily_hook' );
}

add_action( 'careers_cron_twicedaily_hook', 'hit_workable' );


function hit_workable() {
	// Vars
	$trs_name = 'workable_get_perpage_99';

	// Clean Up Transient
	delete_transient( $trs_name );

	// Set Transient

	$headers = array(
		'Content-Type: application/json',
		'Authorization: Bearer d64c494f75cb7122dcfc4c18dd405d12da3a917eec02368441af27b08649058d',
	);

	$cget_jobs = curl_init();
	curl_setopt( $cget_jobs, CURLOPT_URL, 'https://tokopedia.workable.com/spi/v3/jobs?state=published&include_fields=description&limit=99' );
	curl_setopt( $cget_jobs, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $cget_jobs, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $cget_jobs, CURLOPT_HTTPHEADER, $headers );
	$jobslist = curl_exec( $cget_jobs );
	curl_close( $cget_jobs );
	set_transient( $trs_name, $jobslist );
	wp_mail( 'rio.bahtiar@tokopedia.com', 'Careers Get Jobs [Automatic email Cron Job ' . date( 'H:i:s' ) . ']', 'Automatic scheduled get jobs from Workable Done. Run on: ' . date( 'H:i:s' ) . ' - ' . date( 'd F Y' ) );

}
