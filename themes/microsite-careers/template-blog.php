<?php
	/**
	 * Template Name: Blog Listing
	 */
	$workable      = new Careers\workable;
	$abcde         = $workable->get_jobs( 99 );
	$raw           = $workable->get_grouped_jobs( true );
	$the_shortcode = '';
	// Send Jobs Data to Javascript
	wp_localize_script( 'sage/js', 'jobsData', json_encode( $raw ) );
?>

<div class="breadcrumb-wrapper margin">
	<div class="container">
		<div class="careers-breadcrumb careers-breadcrumb--padding">
			<a href="../">
				<div>Career</div>
			</a>
			<a href="./">
				<div>Blog</div>
			</a>
		</div>
	</div>
</div>

<section class="section-ibanner">
    <div class="ibanner">
        <div class="container">
            <div class="ibanner-content">
                <h3 class="ibanner-content__title">
                    Blog
                </h3>
                <div class="ibanner-content__line"></div>
                <p class="ibanner-content__desc">
                    Our Nakama are passionate people. 
                    We enjoy learning as much as we enjoy sharing.
                    What better way to learn and share than to organize an activity 
                    that will engage you with other Nakama across departments?
                </p>
            </div>
        </div>
    </div>
    <div class="section-ibanner__l"></div>
    <div class="section-ibanner__r"></div>

    <section class="section-blog">
        <div class="blog">
            <div class="container">
                <div class="row">
                    <a href="https://medium.com/tokopedia-engineering">
                        <div class="blog-list">
                            <div class="blog-list-image">
                                <img src="https://ecs7.tokopedia.net/assets/images/careers/tech-blog--new.svg" alt="" class="blog-list-image__img">
                            </div>
                            <h2 class="blog-list__text">
                                Tech Blog
                            </h2>
                        </div>
                    </a>
                    <a href="https://medium.com/tokopedia-product-team">
                        <div class="blog-list">
                            <div class="blog-list-image">
                                <img src="https://ecs7.tokopedia.net/assets/images/careers/product-blog.svg" alt="" class="blog-list-image__img">
                            </div>
                            <h2 class="blog-list__text">
                                Product Blog
                            </h2>
                        </div>
                    </a>
                    <a href="https://medium.com/tokopedia-data-squad">
                        <div class="blog-list">
                            <div class="blog-list-image">
                                <img src="https://ecs7.tokopedia.net/assets/images/careers/data-blog.svg" alt="" class="blog-list-image__img">
                            </div>
                            <h2 class="blog-list__text">
                                Data Blog
                            </h2>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</section>
