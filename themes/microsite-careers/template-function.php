<?php
/**
 * Template Name:  Career Function Jobs
 */

// Pass data to JS
wp_localize_script( 'sage/js', 'jobsDataGroup', json_encode( $jobs ) );
wp_localize_script( 'sage/js', 'theSlug', $slug );

?>

<?php
	$showTooltip = false;
	$showTooltip = $slug == 'internship' ? false : true;
?>

<?php if ( $slugExists ) : ?>

	<section id="msrc" class="section_mobile-search">
		<div class="m-searchbar clearfix">
			<div class="m-searchbar-arrow">
				<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-back-mobile.png"
					 alt=""
					 class="m-searchbar-arrow__arrow"
				>
			</div>
			<div class="m-searchbar-lens">
				<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-search-mobile.png"
					 alt=""
					 class="m-searchbar-lens__lens"
				>
			</div>
			<!-- <form id="search-job" action="#" method="GET"> -->
			<input name="query" type="search" class="search-bar__thebar search-bar__thebar--m"
				   placeholder="Type your dream jobs...">
			<!-- </form> -->
		</div>
		<ul class="search-bar-thebar-ul search-bar-thebar-ul--m">
			<p class="m-searchcopy__copy">Search Suggestions</p>
			<!-- populated via JS -->
		</ul>
	</section>

	<section class="section_mobile-location clearfix">
		<div class="m-location clearfix">
			<div class="m-location-copy">
				<h2 class="m-location-copy__copy">
					Choose your prefered place
				</h2>
			</div>
			<ul class="search-bar-theloc-ul--m">
				<li class="search-bar-theloc__li">
					<div class="search-bar-theloc__liwrapper">
						<span class="search-bar-theloc__liloc">All Locations</span>
						<div class="pull-right">
						</div>
					</div>
				</li>
				<!-- populated via JS -->
			</ul>
		</div>
	</section>
	<section class="section_banner <?php echo $banner; ?>">
		<div class="container">
			<!-- Title -->
			<div class="row">
				<div class="col-xs-12">
					<div class="search-title text-center txt--white">
						<h1 class="search-title__hero fw--smb ls--2">
							<?php echo $slug . ' team'; ?>
						</h1>
						<h2 class="search-title__small fs--14 text-center">
							<?php echo $copyText; ?>
						</h2>
					</div>
				</div>
			</div>
		</div>
	</section>

	<form action="<?php echo get_bloginfo( 'url' ); ?>/jobs" method="get">
		<section class="section_search">
			<div class="search-wrap">
				<div class="container">

					<!-- Search Bar -->
					<div class="search-bar clearfix">

						<div class="col-search-60">
							<div class="search-bar-wrapbar clearfix">
								<div class="search-bar-thebar clearfix">
									<div class="search-bar-thebar-image">
										<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-jobsnew.png"
											 alt="Search Jobs"
											 class="search-bar__icon"
										>
										<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-jobsnew-scroll.png"
											 alt="Search Jobs"
											 class="search-bar__icon--scroll"
										>
									</div>
									<div class="search-bar-thebar-group">
										<label for="query"
											   class="search-bar__label txt--black txt--primary fs--12 fw--smb">I want
											to join as</label>
										<input name="query" type="text" class="search-bar__thebar"
											   placeholder="Search job in <?php echo $slug; ?>"
											   autocomplete="off"></input>
									</div>
									<ul class="search-bar-thebar-ul">
										<!-- populated via JS -->
									</ul>
								</div>

								<button type="submit" class="btn-tkpd btn-search--mobile btn-tkpd--large btn-tkpd--orange ripple-effect ripple-main">
									<span class="btn-tkpd__text">Search</span>
								</button>

								<div class="search-bar-theloc">
									<div class="search-bar-thebar-image">
										<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-location-on.png"
											 alt="Search Jobs Location"
											 class="search-bar__icon"
										>
										<img src="https://ecs7.tokopedia.net/assets/images/careers/icon-lokasi-scroll.png"
											 alt="Search Jobs Location"
											 class="search-bar__icon--scroll"
										>
									</div>
									<div class="search-bar-thebar-group">
										<label for="country"
											   class="search-bar__label txt--black txt--primary fs--12 fw--smb">Location</label>
										<input type="text" class="search-bar__theloc" placeholder="All Locations"
											   readonly="true" value=""></input>
										<input name="country" type="hidden" class="search-bar__theloc--true"
											   placeholder="All Locations" readonly="true" value=""></input>
									</div>
									<i class="fa fa-angle-down search-bar__thelocarrow" aria-hidden="true"></i>
									<!-- <i class="fa fa-map-marker search-bar__thelocmarker" aria-hidden="true"></i> -->
									<!-- <i class="fa fa-angle-down search-bar__thelocarrow" aria-hidden="true"></i> -->
									<ul class="search-bar-theloc-ul">
										<li class="search-bar-theloc__li">
											<div class="search-bar-theloc__liwrapper">
												<span class="search-bar-theloc__liloc">All Locations</span>
												<div class="pull-right">

												</div>
											</div>
										</li>
										<!-- populated via JS -->
									</ul>
								</div>
							</div>
						</div>

						<div class="col-search-40">
							<div class="buttons-wrap clearfix">
								<div class="col-search-btn">
									<button type="submit" class="btn-tkpd btn-search btn-tkpd--large btn-tkpd--orange ripple-effect ripple-main">
										<span class="btn-tkpd__text">Search</span>
									</button>
								</div>
							</div>
						</div>
						<input type="hidden" name="function" value="<?php echo $slug; ?>">
					</div>
				</div>
			</div>
		</section>
	</form>

	<section class="section_crumbs">
		<div class="container">
			<div class="crumbs-wrap clearfix">
				<nav class="crumbs-breadcrumbs">
					<a href="<?php echo get_bloginfo( 'url' ); ?>"><span class="crumbs--link">Careers</span></a>
					<i class="fa fa-angle-right" aria-hidden="true"></i>
					<span class="crumbs--capitalize txt--black txt--secondary"><?php echo $slug . ' team'; ?></span>
				</nav>
				<div class="crumbs-count txt--black txt--secondary">
					Showing <?php echo sizeof( $jobs[ strtolower( $slug ) ] ); ?> Jobs in <span><?php echo $slug; ?></span> Team
				</div>
			</div>
		</div>
	</section>

	<section id="job-list">
		<div class="container">
			<!-- Business Function -->
			<div class="list-function 
			<?php
			if ( $slug && sizeof( $jobs[ strtolower( $slug ) ] ) ) {
				echo 'active';
			}
			?>
			" id="<?php echo strtolower( $slug ); ?>">
				<div class="row">
					<div class="list-function-divs clearfix">

						<?php
						if ( $slug && sizeof( $jobs[ strtolower( $slug ) ] ) ) :
							$divs = $jobs[ strtolower( $slug ) ];
							?>

							<?php foreach ( $divs as $job ) : ?>
								<div class="col-sm-6 col-md-4 col-xs-12">
									<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
										<h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
										<p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
										<div class="list-div-highlight--mobile clearfix">
											<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
											<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
										</div>
										<div class="list-div-highlight clearfix">
											<div class="list-div-helper list-div-helper--function"
												<?php if ( $showTooltip ) : ?>
													data-tooltip="Function"
													data-tooltip-position="top"
												<?php endif; ?>
											>
													<?php echo $job['department']; ?>
											</div>
											<div class="list-div-helper list-div-helper--location"
												 data-tooltip="Location"
												 data-tooltip-position="top"
											>
													<?php echo $job['location']['country']; ?>
											</div>
										</div>
										<div class="list-div-desc clearfix">
											<div class="list-div-action">
												<div class="list-div-action-btn">
													<a href="<?php echo '../../job/' . $job['slug']; ?>"
													   class="btn-tkpd btn-tkpd--large btn-tkpd--green list-div-action-btn__action ripple-effect ripple-main">
													   <span class="btn-tkpd__text">See Details</span>
													</a>
												</div>
												<div class="list-div-action-btn">
													<a href="<?php echo '../../join/' . $job['slug']; ?>"
													   class="visible-xs list-div-action-btn__action btn-tkpd btn-tkpd--sgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
														<span class="btn-tkpd__text">Apply Now</span>
													</a>
													<a href="<?php echo '../../join/' . $job['slug']; ?>"
													   class="hidden-xs list-div-action-btn__action btn-tkpd btn-tkpd--lgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
														<span class="btn-tkpd__text">Apply Now</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>

						<?php endif; ?>
					</div>
				</div> <!-- ./row -->
			</div>
		</div> <!-- ./container -->
	</section>
	<?php
else :
	echo 'Function Not Found';
endif;
?>

<!-- Schema -->
<script type="application/ld+json">
	{
	 "@context": "http://schema.org",
	 "@type": "BreadcrumbList",
	 "itemListElement":
	 [
	  {
	   "@type": "ListItem",
	   "position": 1,
	   "item":
	   {
		"@id": "<?php echo get_bloginfo( 'url' ); ?>",
		"name": "Careers"
		}
	  }
	 ]
	}

</script>


