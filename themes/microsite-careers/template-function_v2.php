<?php
/**
 * Template Name:  Career Function Jobs V2
 */

$workable      = new Careers\workable;
$abcde         = $workable->get_jobs( 99 );
$raw           = $workable->get_grouped_jobs( true );
$the_shortcode = '';
// Send Jobs Data to Javascript
wp_localize_script( 'sage/js', 'jobsData', json_encode( $raw ) );
// Pass data to JS
wp_localize_script( 'sage/js', 'jobsDataGroup', json_encode( $jobs ) );
wp_localize_script( 'sage/js', 'theSlug', $slug );

?>

<?php
	$showTooltip = false;
	$showTooltip = $slug == 'internship' ? false : true;
?>

<?php if ( $slugExists ) : ?>
    <div class="breadcrumb-wrapper margin">
        <div class="container">
            <div class="careers-breadcrumb careers-breadcrumb--padding">
                <a href="<?php echo get_bloginfo( "url" ) ?>">
                    <div>Careers</div>
                </a>
                <a href="<?php echo get_bloginfo( "url" ) . "/jobs/"  ?>">
                    <div>Jobs</div>
                </a>
                <a href="<?php echo get_bloginfo( "url" ) . "/function/" . strtolower( $slug ); ?>">
                    <div><?php echo ucwords( $slug ); ?></div>
                </a>
            </div>
        </div>
    </div>

    <!-- <div class="dummy-background">

    </div> -->
    <!-- <div class="dummy-left">

	</div>
	<div class="dummy-right">

    </div> -->

	<section id="job-list">
		<div class="container">
			<div class="list-function-header clearfix">
				<img src="<?php echo $functionBanner; ?>" alt="business" class="list-function-header__img">
				<div class="list-function-header-text">
				<h3 class="list-function-header__h3"><?php echo ucwords( $slug ); ?></h3>
				<p class="list-function-header__p"><?php echo $copyText; ?></p>
				</div>
			</div>
			<!-- Business Function -->
			<div class="list-function 
			<?php
			if ( $slug && sizeof( $jobs[ strtolower( $slug ) ] ) ) {
				echo 'active';
			}
			?>
			" id="<?php echo strtolower( $slug ); ?>">
				<div class="row">
					<div class="list-function-divs clearfix">

						<?php
						if ( $slug && sizeof( $jobs[ strtolower( $slug ) ] ) ) :
							$divs = $jobs[ strtolower( $slug ) ];
							?>

							<?php foreach ( $divs as $job ): ?>
                                <div class="col-sm-6 col-md-4 col-xs-12">
                                    <div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
                                        <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . "..."; ?></h4>
                                        <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . "..."; ?></p>
                                        <div class="list-div-highlight--mobile clearfix">
                                            <div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
                                            <div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
                                        </div>
                                        <div class="list-div-highlight clearfix">
                                            <div class="list-div-helper list-div-helper--function"
                                                <?php if( $showTooltip ) : ?>
                                                    data-tooltip="Function"
                                                    data-tooltip-position="top"
                                                <?php endif;?>
                                            >
                                                    <?php echo $job['department']; ?>
                                            </div>
                                            <div class="list-div-helper list-div-helper--location"
                                                 data-tooltip="Location"
                                                 data-tooltip-position="top"
                                            >
                                                    <?php echo $job['location']['country']; ?>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="list-div-desc clearfix">
                                            <div class="list-div-action">
                                                <div class="list-div-action-btn">
                                                    <a href="<?php echo '../../join/'.$job['slug']; ?>" class="" rel="nofollow" title="Apply Now">
                                                        <button class="list-btn-apply unf-btn unf-btn--no-shadow unf-btn--small full-width">Apply<div class="text">Apply</div></button>
                                                    </a>
                                                    </div> 
                                                    <div class="list-div-action-btn">
                                                    <a href="<?php echo '../../job/'.$job['slug']; ?>" class="">
                                                    <button class="list-btn-details unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php endforeach; ?>

						<?php endif; ?>
					</div>
				</div> <!-- ./row -->
			</div>
		</div> <!-- ./container -->
	</section>
	<?php
else :
	echo 'Function Not Found';
endif;
?>

<!-- Schema -->
<script type="application/ld+json">
	{
	 "@context": "http://schema.org",
	 "@type": "BreadcrumbList",
	 "itemListElement":
	 [
	  {
	   "@type": "ListItem",
	   "position": 1,
	   "item":
	   {
		"@id": "<?php echo get_bloginfo( 'url' ); ?>",
		"name": "Careers"
		}
	  }
	 ]
	}

</script>


