<?php
/**
 * Page Job
 *
 */


$workable      = new Careers\workable;
$abcde         = $workable->get_jobs( 99 );
$raw           = $workable->get_grouped_jobs( true );
$the_shortcode = '';
// Send Jobs Data to Javascript
wp_localize_script( 'sage/js', 'jobsData', json_encode( $raw ) );

if ( $job['department'] == 'Business' ) {
	$desktopBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174726/bg_business.png';
	$mobileBanner   = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150918/bg_business_mobile.jpg';
	$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/business.png';
}

if ( $job['department'] == 'Finance' ) {
	$desktopBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174728/bg_finance.png';
	$mobileBanner   = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150919/bg_finance_mobile.jpg';
	$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/finance.png';
}

if ( $job['department'] == 'Operational' ) {
	$desktopBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174733/bg_operational.png';
	$mobileBanner   = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150922/bg_operational_mobile.jpg';
	$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/operational.png';
}

if ( $job['department'] == 'People' ) {
	$desktopBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174729/bg_HR_people.png';
	$mobileBanner   = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150920/bg_HR_People_mobile.jpg';
	$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/people.png';
}

if ( $job['department'] == 'Product' ) {
	$desktopBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174734/bg_product1.png';
	$mobileBanner   = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150922/bg_product_mobile.jpg';
	$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/product.png';
}

if ( $job['department'] == 'Technology' ) {
	$desktopBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174735/bg_tech.png';
	$mobileBanner   = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150923/bg_tech_mobile.jpg';
	$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/technology.png';
}

if ( $job['department'] == 'Internship' ) {
	$desktopBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174730/bg_intern.png';
	$mobileBanner   = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150921/bg_intern_mobile.jpg';
	$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/internship.png';
}

?>
<script type="application/ld+json">
{
 "@context": "http://schema.org",
 "@type": "BreadcrumbList",
 "itemListElement":
 [
  {
   "@type": "ListItem",
   "position": 1,
   "item":
   {
	"@id": "<?php echo get_bloginfo( 'url' ); ?>",
	"name": "Careers"
	}
  },
  {
   "@type": "ListItem",
  "position": 2,
  "item":
   {
	 "@id": "<?php echo get_bloginfo( 'url' ) . '/jobs?function=' . strtolower( $job['department'] ); ?>",
	 "name": "<?php echo $job['department']; ?>"
   }
 },
 {
  "@type": "ListItem",
 "position": 3,
 "item":
  {
	"@id": "<?php echo $job['title']; ?>",
	"name": "<?php echo $job['title']; ?>"
  }
 }
 ]
}


</script>

<div class="detail-job">
	<div class="breadcrumb-wrapper margin breadcrumb-wrapper--transparent">
        <div class="container">
            <div class="careers-breadcrumb careers-breadcrumb--padding">
                <a href="<?php echo get_bloginfo( "url" ) ?>">
                    <div>Career</div>
                </a>
                <a href="<?php echo get_bloginfo( "url" ) . "/jobs/"  ?>">
                    <div>Jobs</div>
                </a>
                <a href="<?php echo get_bloginfo( "url" ) . "/function/" . strtolower( $job["department"] ) ?>">
                    <div><?php echo $job["department"] ?></div>
                </a>
                <a href="./">
                    <div><?php echo $job["title"] ?></div>
                </a>
            </div>
        </div>
    </div>
	<div class="container">
		<div class="white-container">         
			<div class="">
				<div class="side-left">
					<div class="show-on-mobile">
						<h1><?php echo $job['title']; ?></h1>
						<div class="detail-job-sub-container">
							<div class="detail-job-sub" data-tooltip="Function" data-tooltip-position="top">
								<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/group-19.svg">
								<?php echo $job['department']; ?>
							</div>
							<div class="detail-job-sub" data-tooltip="Location" data-tooltip-position="top">
								<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/sen-percent-loc.svg">
								<?php echo $job['location']['country']; ?>
							</div>
						</div>
					</div>
					<hr/>
					<h3>Description</h3>
					<p>
						<?php
						echo $job_item->description;
						?>
					</p>
					<h3>Requirements</h3>
					<p>
						<?php
						if ( strpos( $job_item->requirements, '<ul>' ) !== 0 ) {
							?>
					<ul>
							<?php
						}
						?>
						<?php
						echo $job_item->requirements;
						?>
						<?php
						if ( strpos( $job_item->requirements, '<ul>' ) !== 0 ) {
							?>
						<ul>
							<?php
						}
						?>
					</p>
						<?php if ( $job_item->benefits != '' ) { ?>
												<h3>Benefits</h3>
												<p>
							<?php
}
						echo $job_item->benefits;
?>
												</p>
				</div>
				<div class="side-share-width">
					<div class="side-share-container">
						<img width="124" src="<?php echo $functionBanner; ?>" />
						<h2><?php echo $job['title']; ?></h2>

						<div class="detail-job-sub-container-right">
							<div class="detail-job-sub">
								<div class="sub-icon-container">
									<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/group-19.svg">
								</div>
								<?php echo $job['department']; ?>
							</div>
							<div class="detail-job-sub">
								<div class="sub-icon-container">
									<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/sen-percent-loc.svg">
								</div>
								<?php echo $job['location']['country']; ?>
							</div>
						</div>
						<div style="clear:both"></div>
						<a href="<?php echo get_bloginfo( 'url' ) . '/join/' . $slug; ?>">
							<button class="unf-btn unf-btn--small full-width">Apply Now<div class="text">Apply Now</div></button>
						</a>
						<div class="text-center apply-text">Not what you had in mind? <a
									href="<?php echo get_bloginfo( 'url' ) . '/function/' . strtolower( $job['department'] ); ?>"><br>See Related Jobs</a></div>
						<!-- <div class="apply-divider">
							<span>OR</span>
						</div> -->
						<hr/>
						<!-- <div class="apply-share-text">Know someone who would be perfect for this role? let them
							know:
						</div> -->
						<div class="share-title">Share</div>

						<div style="text-align:left">
							<div class="addthis_toolbox">
								<div class="custom_images">
									<a class="addthis_button_facebook" >
										<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/facebook.svg" border="0" alt="Share to Facebook">
									</a>
									<a class="addthis_button_twitter " >
										<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/twitter.svg" border="0" alt="Share to Twitter">
									</a>
									<a class="addthis_button_google_plusone_share">
										<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/googleplus.svg" border="0" alt="Share to Link">
									</a>
									<a class="addthis_button_whatsapp" >
										<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/whatsapp.svg" border="0" alt="Share to WhatsApp">
									</a>
									
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- mobile white card -->


	</div>

	<div class="job-apply-mobile">
		<div class="container">
			<div class="apply-bottom-mobile">
				<div style="clear:both"></div>
				<div class="text-center apply-text">Not what you had in mind? <a
							href="<?php echo get_bloginfo( 'url' ) . '/function/' . strtolower( $job['department'] ); ?>"><br>See Related Jobs</a></div>
				<!-- <div class="apply-divider">
					<span>OR</span>
				</div> -->
				<hr/>
				<!-- <div class="apply-share-text">Know someone who would be perfect for this role? let them know:</div> -->
				<div class="share-title">Share</div>
				<div style="text-align:center">
					<div class="addthis_toolbox">
						<div class="custom_images">
							<a class="addthis_button_facebook" >
								<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/facebook.svg" border="0" alt="Share to Facebook">
							</a>
							<a class="addthis_button_twitter " >
								<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/twitter.svg" border="0" alt="Share to Twitter">
							</a>
							<a class="addthis_button_google_plusone_share">
								<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/googleplus.svg" border="0" alt="Share to Link">
							</a>
							<a class="addthis_button_whatsapp" >
								<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/whatsapp.svg" border="0" alt="Share to WhatsApp">
							</a>
							
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="container job-apply-btn">
			<a href="<?php echo get_bloginfo( 'url' ) . '/join/' . $slug; ?>" rel="nofollow" title="Apply Now">
				<!-- <div class="text-center job-apply-btn fixed btn--big btn-tkpd btn--green">Apply Now</div> -->
				<button class="unf-btn  unf-btn--small full-width">Apply Now<div class="text">Apply Now</div></button>
			</a>
		</div>
	  

	</div>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a3ee5468ebdecce"></script>
<script>
	(function ($) {
		$(document).ready(function () {
			var sideTop = $(".side-share-container").offset().top;
			// $(document).on('scroll', function () {
			//     if ($(document).scrollTop() > sideTop) {
			//         $(".side-share-container").css({
			//             position: "fixed",
			//             marginTop: "64px",
			//             top: "0",
			//             width: $(".side-share-width").width()
			//         });
			//     }
			//     if ($(document).scrollTop() < sideTop) {
			//         $(".side-share-container").css({
			//             position: "static",
			//             width: "auto",
			//             marginTop: "0px"
			//         });
			//     }
			// });
			if ($(window).width() < 993) {
				$(document).on('scroll', function () {
					// console.log($(document).scrollTop(), $(".footer").offset().top - $(window).height());
					if ($(document).scrollTop() > $(".footer").offset().top - $(window).height()) {
						$(".footer").css("margin-top", "0");
						$(".job-apply-btn").removeClass("fixed");
					}
					else {
						$(".footer").css("margin-top", "71px");
						$(".job-apply-btn").addClass("fixed");
					}
				}).trigger("scroll");
			}
			if ($(window).width() < 481) {
				$(".detail-job-banner").each(function () {
					$(this).css("background-image", "URL('" + $(this).attr("data-background-mobile") + "')");
				});
			}


			$('*[data-tooltip]').on('mouseenter', function () {
				var pos = $(this).position();
				var width = $(this).outerWidth();
				var position = "top";
				if ($(this).attr('data-tooltip-position')) {
					position = $(this).attr('data-tooltip-position');
				}
				if ($(this).attr('data-tooltip-color')) {
					var color = $(this).attr('data-tooltip-color');
				}
				var new_tooltip = $('<div class="tkpd-tooltip" data-tooltip-position=' + position + ' data-tooltip-color=' + color + '>' + $(this).attr('data-tooltip') + '</div>').insertAfter($(this));

				//if tooltip has more width then the object
				if (new_tooltip.innerWidth() > $(this).innerWidth()) {
					var left = -(new_tooltip.innerWidth() - $(this).innerWidth()) / 2;
					var top = -new_tooltip.innerHeight() - 10;
					if (position == "left") {
						left = -new_tooltip.innerWidth() - 10;
						if (new_tooltip.innerHeight() < $(this).innerHeight()) {
							top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
						}
						if (new_tooltip.innerHeight() > $(this).innerHeight()) {
							top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
						}
					}
					if (position == "bottom") {
						top = new_tooltip.innerHeight() + 20;
					}
					if (position == "right") {
						left = $(this).innerWidth() + 10;
						if (new_tooltip.innerHeight() < $(this).innerHeight()) {
							top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
						}
						if (new_tooltip.innerHeight() > $(this).innerHeight()) {
							top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
						}
					}
				}
				if (new_tooltip.innerWidth() < $(this).innerWidth()) {
					var left = ($(this).innerWidth() - new_tooltip.innerWidth()) / 2;
					var top = -new_tooltip.innerHeight() - 10;
				}
				new_tooltip.css({
					top: pos.top + top + "px",
					left: pos.left + left + 1 + "px"
				});
			});

			$('*[data-tooltip]').on('mouseleave', function () {
				$(this).next().remove();
			});

		});
	})(jQuery);
</script>
