<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

$werk = new Careers\workable;

if ( empty( $_POST['dataform'] ) or empty( $_POST['sc'] ) ) {
	wp_redirect( home_url() );
	exit();
}

$data = $_POST['dataform'];
$sc   = $_POST['sc'];

$firstname = $data['candidate']['firstname'];
$lastname  = $data['candidate']['lastname'];
$phone     = $data['candidate']['phone'];
$email     = $data['candidate']['email'];
$response  = [];
if ( $firstname == '' ) {
	$response['status']  = 'fail';
	$response['message'] = 'Empty Firstname';
	$response['form']    = 'firstname';
} elseif ( $lastname == '' ) {
	$response['status']  = 'fail';
	$response['message'] = 'Empty Lastname';
	$response['form']    = 'lastname';
} elseif ( $email == '' ) {
	$response['status']  = 'fail';
	$response['message'] = 'Empty Email';
	$response['form']    = 'email';
} elseif ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
	$response['status']  = 'fail';
	$response['message'] = 'Invalid Email';
	$response['form']    = 'email';
} elseif ( $phone == '' ) {
	$response['status']  = 'fail';
	$response['message'] = 'Empty Phone';
	$response['form']    = 'phone';
} elseif ( ! is_numeric( $phone ) ) {
	$response['status']  = 'fail';
	$response['message'] = 'Invalid Phone';
	$response['form']    = 'phone';
} elseif ( array_key_exists( 'summary', $data['candidate'] ) ) {
	if ( $data['candidate']['summary'] == '' ) {
		$response['status']  = 'fail';
		$response['message'] = 'Empty Summary';
		$response['form']    = 'summary';
	} else {
		$res                 = $werk->insert_candidate( $sc, json_encode( $data ) );
		$response['status']  = 'success';
		$response['message'] = $res;

	}
} elseif ( ! array_key_exists( 'summary', $data['candidate'] ) ) {
	$res                 = $werk->insert_candidate( $sc, json_encode( $data ) );
	$response['status']  = 'success';
	$response['message'] = $res;
}
echo json_encode( $response );

