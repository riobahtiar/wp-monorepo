<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

/* =================
 * Variable Init
 * ================= */
$wws  = new WorkdayAPI\workday;
// $cat  = $wws->get_jobs();
$jobs = $wws->get_grouped_jobs( false );
// $raw  = $wws->get_grouped_jobs( true );
$slug = '';

//wp_localize_script( 'sage/js', 'jobsDataGroup', json_encode( $raw ) );
$isBusiness  = $isFinance = $isProduct = $isOperational = $isPeople = $isTechnology = $isInternship = false;
$countryCode = array(
	'ID' => 'Indonesia',
	'IN' => 'India',
);

if ( get_query_var( 'slug' ) != null ) {
	$slug       = sanitize_title( get_query_var( 'slug' ) );
	$slugExists = true;
}

//wp_localize_script( 'sage/js', 'theSlug', $slug );

/* ======================
 * Banner Handler
 * ====================== */

switch ( strtolower( $slug ) ) {
	case 'business':
		$banner         = 'list-jumbotron--business';
		$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/business.png';
		break;
	case 'finance':
		$banner         = 'list-jumbotron--finance';
		$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/finance.png';
		break;
	case 'product':
		$banner         = 'list-jumbotron--product';
		$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/product.png';
		break;
	case 'operational':
		$banner         = 'list-jumbotron--operational';
		$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/operational.png';
		break;
	case 'marketing':
		$banner         = 'list-jumbotron--operational';
		$functionBanner = 'https://ecs7.tokopedia.net/assets/images/careers/Marketing.png';
		break;
	case 'people':
		$banner         = 'list-jumbotron--people';
		$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/people.png';
		break;
	case 'technology':
		$banner         = 'list-jumbotron--technology';
		$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/technology.png';
		break;
	case 'internship':
		$banner         = 'list-jumbotron--internship';
		$functionBanner = 'https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/internship.png';
		break;
	default:
		$banner = '';
}

/* ======================
* Copy Handler
* ====================== */

switch ( strtolower( $slug ) ) {
	case 'business':
		$copyText = 'Promote and promote. In the Business division, you get to choose from a wide range of teams to join, from Creative to PR Event. Everybody brings something to the table, are you willing to contribute?';
		break;
	case 'finance':
		$copyText = "Having a keen eye on finance won't go unnoticed at Tokopedia. If you are skilled at budgeting, investor relations-managing, or have a general talent for numbers, look no further!";
		break;
	case 'product':
		$copyText = 'Design, build, and beautify the visual of the product';
		break;
	case 'operational':
		$copyText = "Tokopedia is equal parts harmony among seller and buyer. It needs work to maintain that balance, and the operational department is the thread that keeps it together. Assist and solve customer's concerns professionally by joining this team.";
		break;
	case 'people':
		$copyText = 'Manage, coordinate, and collaborate with the Nakamas to create a healthy workplace';
		break;
	case 'technology':
		$copyText = "Tech department holds a significant role in Tokopedia. And spoiler alert: maintaining the performance of website and app all day every day is just a small part of it. Work behind the scenes of Tokopedia to increase the company's digital reliability.";
		break;
	case 'internship':
		$copyText = 'Never too early to start! Gain a first-hand working experience in e-commerce by joining our internship program as a college student or fresh graduate. Who knows what kind of trails you will blaze during your time here?';
		break;
	default:
		$copyText = '';
}


// Passing data to Header
$the_title       = ucfirst( $slug ) . ' Jobs | Tokopedia Careers';
$the_description = $copyText;
$the_slug        = 'function/' . $slug . '/';

?>

<!doctype html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebSite" prefix="og: http://ogp.me/ns#">
<?php include_once 'templates/head.php'; ?>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TN8CNF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--[if IE]>
<div class="alert alert-warning">
	<?php
	_e(
		'You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
    browser</a> to improve your experience.', 'sage'
	);
	?>
</div>
<![endif]-->
<?php
do_action( 'get_header' );
get_template_part( 'templates/header' );
?>
<?php if ( isset( $jobs[ strtolower( $slug ) ] ) ) : ?>
<main class="main-careers">
	<?php include Wrapper\template_path(); ?>
</main><!-- /.main -->
	<?php if ( Setup\display_sidebar() ) : ?>
	<aside class="sidebar">
		<?php include Wrapper\sidebar_path(); ?>
	</aside><!-- /.sidebar -->
		<?php
	endif;
else :

	global $wp_query;

	$wp_query->set_404();
	status_header( '404' );
	get_template_part( '404' );
endif;
?>

<?php
do_action( 'get_footer' );
get_template_part( 'templates/footer' );
wp_footer();
?>
</body>
</html>
