<main class="main-404">
        <div class="wrapper-404 hidden-xs">
            <div class="the-404">
                <div class="img-wrapper">
                    <div class="the404-img">
                        <div class="astro-img">
                            <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-astro.svg" alt="">
                        </div>
                        <div class="rocket-img">
                            <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-apolo.svg" alt="">
                        </div>
                        <div class="small-circle">
                            <!-- <img src="https://ecs7.tokopedia.net/assets/images/careers/stroke-5.svg" alt=""> -->
                        </div>
                        <div class="middle-circle">
                            <!-- <img src="https://ecs7.tokopedia.net/assets/images/careers/stroke-3.svg" alt=""> -->
                        </div>
                        <div class="big-circle">
                            <!-- <img src="https://ecs7.tokopedia.net/assets/images/careers/stroke-1.svg" alt=""> -->
                        </div>

                        <div class="planetleft-img">
                            <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-spaceleft.svg" alt="">
                        </div>
                        <div class="planetmiddle-img">
                            <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-space-1.svg" alt="">
                        </div>
                        <div class="planetright-img">
                            <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-spaceright.svg" alt="">
                        </div>

                        <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-404.svg" alt="" class="hero-img">
                    </div>
                </div>

                <div class="the404-copy text-center">
                    <h1 class="the404-copy__hero">Whoops..</h1>
                    <h2 class="the404-copy__sub">Job not available or has been disabled</h2>
                </div>

                <div class="text-center">
                    <a href="/jobs" class="btn-tkpd btn--green btn--big">Back to Job List</a>
                </div>

            </div>
        </div>

        <div class="wrapper-404 visible-xs">
            <div class="the-404">
                <div class="img-wrapper">
                    <div class="the404-img">
                        <div class="astro-img">
                            <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-astro.svg" alt="">
                        </div>

                        <div style="margin: auto;">
                            <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-404.svg" alt="" class="hero-img">
                        </div>
                    </div>
                </div>

                <div class="the404-copy text-center">
                    <h1 class="the404-copy__hero">Whoops..</h1>
                    <h2 class="the404-copy__sub">Job not available or has been disabled</h2>
                </div>

                <div class="text-center">
                    <a href="/jobs" class="btn-tkpd btn--green btn--big">Back to Job List</a>
                </div>

            </div>
        </div>

</main>
