<?php
/**
 * Template Name: Careers Listing V3
 */

/* =================
 * Variable Init
 * ================= */
$wws         = new WorkdayAPI\workday;
$jobs        = $wws->get_grouped_jobs( false );
$isBusiness  = $isFinance = $isProduct = $isOperational = $isMarketing = $isPeople = $isTechnology = $isInternship = false;
$countryCode = array(
	'ID' => 'Indonesia',
	'IN' => 'India',
);

/* ================
 * Localize script
 * ================ */

wp_localize_script( 'sage/js', 'jobsData', json_encode( $jobs ) );
wp_localize_script( 'sage/js', 'baseUrl', get_bloginfo( 'url' ) );

/* ========================
 * Fetch Param if available
 * ======================== */
$searchTerm = isset( $_GET['query'] ) ? sanitize_text_field( $_GET['query'] ) : '';
$country    = isset( $_GET['country'] ) ? sanitize_text_field( $_GET['country'] ) : '';
$function   = isset( $_GET['function'] ) ? sanitize_text_field( $_GET['function'] ) : 'all';

switch ( $function ) {
	case 'business':
		$isBusiness = true;
		break;
	case 'finance':
		$isFinance = true;
		break;
	case 'product':
		$isProduct = true;
		break;
	case 'operational':
		$isOperational = true;
		break;
	case 'marketing':
		$isMarketing = true;
		break;
	case 'people':
		$isPeople = true;
		break;
	case 'technology':
		$isTechnology = true;
		break;
	case 'internship':
		$isInternship = true;
		break;
	default:
		$isBusiness = $isFinance = $isProduct = $isOperational = $isMarketing = $isPeople = $isTechnology = $isInternship = true;
		break;
}

/* ===================
 * Total jobs
 * =================== */
$all_jobs = 0;
foreach( $jobs as $job_func ) {
	foreach( $job_func as $job ) {
		$all_jobs++;
	}
}

$totalJobs = $function === 'all' ? $all_jobs : sizeof( $jobs[ $function ] );

/* ======================
 * Server side searching
 * ====================== */
function searchJob( $job ) {
	if ( $GLOBALS['searchTerm'] ) :
		if ( $GLOBALS['country'] ) :
			return ( strpos( strtolower( $job['title'] ), strtolower( $GLOBALS['searchTerm'] ) ) !== false ) &&
			( strpos( strtolower( $job['location']['country_code'] ), strtolower( $GLOBALS['country'] ) ) !== false );
	  endif;
		return ( strpos( strtolower( $job['title'] ), strtolower( $GLOBALS['searchTerm'] ) ) !== false );
  endif;
	return ( strpos( strtolower( $job['location']['country_code'] ), strtolower( $GLOBALS['country'] ) ) !== false );
}

if ( $searchTerm || $country ) :
	// reset total jobs
	$totalJobs = 0;

	// reassign each job function
	foreach ( $jobs as $func => $value ) :
		$jobs[ $func ] = array_filter( $jobs[ $func ], 'searchJob' );
		$totalJobs    += sizeof( $jobs[ $func ] );
  endforeach;
endif;

$displayDefault = '';
// If Job not found
if ( $totalJobs == 0 ) {
	$displayDefault = 'style="display: block;"';
}

/* ======================
 * Banner Handler
 * ====================== */

switch ( $function ) {
	case 'business':
		 $banner = 'list-jumbotron--business';
		break;
	case 'finance':
		 $banner = 'list-jumbotron--finance';
		break;
	case 'product':
		 $banner = 'list-jumbotron--product';
		break;
	case 'operational':
		 $banner = 'list-jumbotron--operational';
		break;
	case 'marketing':
		 $banner = 'list-jumbotron--operational';
	 	break;
	case 'people':
		 $banner = 'list-jumbotron--people';
		break;
	case 'technology':
		 $banner = 'list-jumbotron--technology';
		break;
	case 'internship':
		 $banner = 'list-jumbotron--internship';
		break;
	default:
		 $banner = '';
}
?>

<div class="breadcrumb-wrapper margin">
    <div class="container">
        <div class="careers-breadcrumb careers-breadcrumb--nopadding">
            <a href="../">
                <div>Career</div>
            </a>
            <a href="./">
                <div>Jobs</div>
            </a>
        </div>
    </div>
</div>
<!-- <div class="dummy-background">

</div> -->
<!-- <div class="dummy-left">

</div>
<div class="dummy-right">

</div> -->
<section class="list-jumbotron <?php echo $banner; ?>">
  <div class="leaf-left">

  </div>
  <div class="leaf-right">

  </div>
  <div class="container">
	<div class="list-jumbotron-text">
	  <h1 class="list-jumbotron-text__h1">My Vacancy</h1>
	  <div class="green-line-small"></div>
	  <h2 class="list-jumbotron-text__h2">Explore your passion and opportunities and grow with us.</h2>
	</div>
  </div>
</section>

<section id="discovery">
  <div class="container">
	<div class="list-discovery">
	  <div class="list-search clearfix">
		<form id="search-form">
		  <div class="list-select">
			<div class="relative">
			  <i class="list-search__icon"></i>
			  <input type="text" class="list-search__input" placeholder="Type Your Keywords" value="<?php echo $searchTerm ? $searchTerm : ''; ?>"/>
			  <!-- <input type="submit" class="visible-xs list-search__btn btn-tkpd btn-tkpd--large btn-tkpd--orange ripple-effect ripple-main" value="Search"/> -->
			  <!-- <button class="visible-xs unf-btn unf-btn--no-shadow unf-btn--small unf-btn--transaction">Search<div class="text">Search</div></button> -->
			</div>
			<div class="list-select-input list-filter" data-filter="location" data-value="<?php echo $country ? $country : 'all'; ?>">
			  <i class="list-select-input__icon list-select-input__icon--location"></i>
			  <span class="list-select-input__text list-select-input__location"><?php echo $country ? $countryCode[ $country ] : 'Location'; ?></span>
			  <i class="fa fa-angle-down list-select-input__arrow" aria-hidden="true"></i>
			  <div class="list-select-options">
				<ul>
				  <li data-value="all"><span class="all-location">All locations</span></li>
				  <li data-value="ID"><span class="text-country">Indonesia </span><div class="pull-left"><span class="flag-indonesia"></span></div></li>
				  <li data-value="IN"><span class="text-country">India</span><div class="pull-left"><span class="flag-india"></span></div></li>
				</ul>
			  </div>
			</div>
			<div class="search-btn-wrapper">
			  <!-- <span class="btn-tkpd__text">Search</span> -->
			  <button class="jobs-search-btn unf-btn unf-btn--no-shadow unf-btn--small unf-btn--transaction">Search<div class="text">Search</div></button>
			</div>
		  </div>
		</form>
	  </div>
	  <div class="list-category">
		<!-- <span class="list-category__label">Job category:</span> -->
		<span class="list-category__item 
		<?php
		if ( $function == 'business' ) {
			echo 'active';}
		?>
		" data-function="business">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Business
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'finance' ) {
			echo 'active';}
		?>
		" data-function="finance">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Finance
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'operational' ) {
			echo 'active';}
		?>
		" data-function="operational">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Operational
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'marketing' ) {
			echo 'active';}
		?>
		" data-function="marketing">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Marketing
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'people' ) {
			echo 'active';}
		?>
		" data-function="people">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> People
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'product' ) {
			echo 'active';}
		?>
		" data-function="product">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Product
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'technology' ) {
			echo 'active';}
		?>
		" data-function="technology">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Technology
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'internship' ) {
			echo 'active';}
		?>
		" data-function="internship">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Internship
		</span>
	  </div>
	  <div class="list-category-slick">
		<div class="list-category__item 
		<?php
		if ( $function == 'business' ) {
			echo 'active';}
		?>
		" data-function="business">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Business
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'finance' ) {
			echo 'active';}
		?>
		" data-function="finance">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Finance
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'operational' ) {
			echo 'active';}
		?>
		" data-function="operational">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Operational
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'marketing' ) {
			echo 'active';}
		?>
		" data-function="marketing">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Marketing
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'people' ) {
			echo 'active';}
		?>
		" data-function="people">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> People
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'product' ) {
			echo 'active';}
		?>
		" data-function="product">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Product
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'technology' ) {
			echo 'active';}
		?>
		" data-function="technology">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Technology
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'internship' ) {
			echo 'active';}
		?>
		" data-function="internship">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Internship
		</div>
	  </div>
	  <div class="list-filter-more">
		<span class="list-filter-more__text">More filter</span>
		<i class="fa fa-angle-down list-filter-more__icon"></i>
	  </div>
	</div>
	<!-- <div class="list-helper">
	  <div class="list-helper-label pull-left">
		Showing <span class="list-helper-label__count"><?php echo $totalJobs; ?></span> matching jobs <span class="list-helper-label__speed"></span>
	  </div>
	</div> -->
  </div>
</section>

<section id="job-list">
  <div class="container">
	<!-- Business Function -->
	<?php if ( array_key_exists( 'business', $jobs ) ) : ?>
	<div class="list-function 
		<?php
		if ( $isBusiness && sizeof( $jobs['business'] ) ) {
			echo 'active';}
		?>
	" id="business">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/business.png" alt="business" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/business"><h3 class="list-function-header__h3">Business</h3></a>
		  <p class="list-function-header__p">Foresee business opportunities and strategize business plan to create a valuable marketing actions</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isBusiness && sizeof( $jobs['business'] ) ) :
			$divs = $jobs['business'];
			?>


			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
			<div class="col-sm-6 col-md-4 col-xs-12 col-container">
			  <div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
				<h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 35 ? $job['title'] : substr( $job['title'], 0, 35 ) . '...'; ?></h4>
				<p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
				<div class="list-div-highlight--mobile clearfix">
				  <div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				  <div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
				</div>
				<div class="list-div-highlight clearfix">
				  <div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><span><?php echo $job['department']; ?><span></div>
				  <div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><span><?php echo $job['location']['country']; ?></span></div>
				</div>
				<hr/>
				<div class="list-div-desc clearfix">
				  <div class="list-div-action">
					<div class="list-div-action-btn">
					<?php $apply_link = 'https://tokopedia2.wd3.myworkdayjobs-impl.com/en-US/Search/job/' . str_replace( ' ', '-', $job['location']['office'] ) . '/' . str_replace( ' ', '-', $job['title'] ) . '_' . $job['requisition_id'] . '/apply'; ?>
					  <a href="<?php echo $job['external_apply_url']; ?>" class="" rel="nofollow" title="Apply Now">
						<button class="unf-btn unf-btn--no-shadow unf-btn--small full-width">Register<div class="text">Register</div></button>
					  </a>
					</div> 
					<div class="list-div-action-btn">
					  <a href="<?php echo '../job/' . $job['slug']; ?>" class="">
					   <button class="unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>
					  </a>
					</div>
				   
				  </div>
				</div>
			  </div>
			</div>
			<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center wrap-line">
			<hr/>
			<a href="<?php echo bloginfo( 'url' ); ?>/function/business" class="see-more-link" data-function="business">
			 <button class="unf-btn unf-btn--small unf-btn--invert">See <?php echo sizeof( $divs ) - 6; ?> More<div class="text">See <?php echo sizeof( $divs ) - 6; ?> More</div></button>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div> <!-- ./function -->
	<?php endif; ?>
	<!-- ./Business Function -->
   

	<!-- Finance Function -->
	<?php if ( array_key_exists( 'finance', $jobs ) ) : ?>
	<div class="list-function 
		<?php
		if ( $isFinance && sizeof( $jobs['finance'] ) ) {
			echo 'active';}
		?>
	" id="finance">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/finance.png" alt="finance" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/finance"><h3 class="list-function-header__h3">Finance</h3></a>
		  <p class="list-function-header__p">Plan, control, and monitor the financial purpose</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isFinance && sizeof( $jobs['finance'] ) ) :
			$divs = $jobs['finance'];
			?>
			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <hr/>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
				  <?php $apply_link = 'https://tokopedia2.wd3.myworkdayjobs-impl.com/en-US/Search/job/' . str_replace( ' ', '-', $job['location']['office'] ) . '/' . str_replace( ' ', '-', $job['title'] ) . '_' . $job['requisition_id'] . '/apply'; ?>
					<a href="<?php echo $job['external_apply_url']; ?>" class="" rel="nofollow" title="Apply Now">
					  <button class="unf-btn unf-btn--no-shadow unf-btn--small full-width">Register<div class="text">Register</div></button>
					</a>
				  </div> 
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="">
					  <button class="unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center wrap-line">
			<hr/>
			<a href="<?php echo bloginfo( 'url' ); ?>/function/finance" class="see-more-link" data-function="finance">
			  <button class="unf-btn unf-btn--small unf-btn--invert">See <?php echo sizeof( $divs ) - 6; ?> More<div class="text">See <?php echo sizeof( $divs ) - 6; ?> More</div></button>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<?php endif; ?>
	<!-- ./Finance Function -->

	<!-- Operational Function -->
	<?php if ( array_key_exists( 'operational', $jobs ) ) : ?>
	<div class="list-function 
		<?php
		if ( $isOperational && sizeof( $jobs['operational'] ) ) {
			echo 'active';}
		?>
	" id="operational">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/operational.png" alt="operational" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/operational"><h3 class="list-function-header__h3">Operational</h3></a>
		  <p class="list-function-header__p">Monitor, assist, and solve customer’s problem professionally through providing valuable guidance</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isOperational && sizeof( $jobs['operational'] ) ) :
			$divs = $jobs['operational'];
			?>

			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <hr/>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
				  <?php $apply_link = 'https://tokopedia2.wd3.myworkdayjobs-impl.com/en-US/Search/job/' . str_replace( ' ', '-', $job['location']['office'] ) . '/' . str_replace( ' ', '-', $job['title'] ) . '_' . $job['requisition_id'] . '/apply'; ?>
					<a href="<?php echo $job['external_apply_url']; ?>" class="" rel="nofollow" title="Apply Now">
					  <button class="unf-btn unf-btn--no-shadow unf-btn--small full-width">Register<div class="text">Register</div></button>
					</a>
				  </div> 
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="">
					  <button class="unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>
		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center wrap-line">
			<hr/>
			<a href="<?php echo bloginfo( 'url' ); ?>/function/operational" class="see-more-link" data-function="operational">
			 <button class="unf-btn unf-btn--small unf-btn--invert">See <?php echo sizeof( $divs ) - 6; ?> More<div class="text">See <?php echo sizeof( $divs ) - 6; ?> More</div></button>
			</a>
		  </div>
		<?php endif; ?>
	  </div> <!-- ./ row -->
	</div>
	<?php endif; ?>
	<!-- ./Operational Function -->

	<!-- Marketing Function -->
	<?php if ( array_key_exists( 'marketing', $jobs ) ) : ?>
	<div class="list-function 
		<?php
		if ( $isMarketing && sizeof( $jobs['marketing'] ) ) {
			echo 'active';}
		?>
	" id="marketing">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/Marketing.png" alt="marketing" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/marketing"><h3 class="list-function-header__h3">Marketing</h3></a>
		  <p class="list-function-header__p">Monitor, assist, and solve customer’s problem professionally through providing valuable guidance</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isMarketing && sizeof( $jobs['marketing'] ) ) :
			$divs = $jobs['marketing'];
			?>

			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <hr/>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
				  <?php $apply_link = 'https://tokopedia2.wd3.myworkdayjobs-impl.com/en-US/Search/job/' . str_replace( ' ', '-', $job['location']['office'] ) . '/' . str_replace( ' ', '-', $job['title'] ) . '_' . $job['requisition_id'] . '/apply'; ?>
					<a href="<?php echo $job['external_apply_url']; ?>" class="" rel="nofollow" title="Apply Now">
					  <button class="unf-btn unf-btn--no-shadow unf-btn--small full-width">Register<div class="text">Register</div></button>
					</a>
				  </div> 
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="">
					  <button class="unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>
		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center wrap-line">
			<hr/>
			<a href="<?php echo bloginfo( 'url' ); ?>/function/marketing" class="see-more-link" data-function="marketing">
			 <button class="unf-btn unf-btn--small unf-btn--invert">See <?php echo sizeof( $divs ) - 6; ?> More<div class="text">See <?php echo sizeof( $divs ) - 6; ?> More</div></button>
			</a>
		  </div>
		<?php endif; ?>
	  </div> <!-- ./ row -->
	</div>
	<?php endif; ?>
	<!-- ./Marketing Function -->

	<!-- Product Function -->
	<?php if ( array_key_exists( 'product', $jobs ) ) : ?>
	<div class="list-function 
		<?php
		if ( $isProduct && sizeof( $jobs['product'] ) ) {
			echo 'active';}
		?>
	" id="product">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/product.png" alt="product" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/product"><h3 class="list-function-header__h3">Product</h3></a>
		  <p class="list-function-header__p">Design, build, and beautify the visual of the product</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isProduct && sizeof( $jobs['product'] ) ) :
			$divs = $jobs['product'];
			?>


			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
			<div class="col-sm-6 col-md-4 col-xs-12">
			  <div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
				<h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
				<p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
				<div class="list-div-highlight--mobile clearfix">
				  <div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				  <div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
				</div>
				<div class="list-div-highlight clearfix">
				  <div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				  <div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
				</div>
				<hr/>
				<div class="list-div-desc clearfix">
				  <div class="list-div-action">
					<div class="list-div-action-btn">
					<?php $apply_link = 'https://tokopedia2.wd3.myworkdayjobs-impl.com/en-US/Search/job/' . str_replace( ' ', '-', $job['location']['office'] ) . '/' . str_replace( ' ', '-', $job['title'] ) . '_' . $job['requisition_id'] . '/apply'; ?>
					  <a href="<?php echo $job['external_apply_url']; ?>" class="" rel="nofollow" title="Apply Now">
						<button class="unf-btn unf-btn--no-shadow unf-btn--small full-width">Register<div class="text">Register</div></button>
					  </a>
					</div> 
					<div class="list-div-action-btn">
					  <a href="<?php echo '../job/' . $job['slug']; ?>" class="">
						<button class="unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>
					  </a>
					</div>
				  </div>
				</div>
			  </div>
			</div>
			<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center wrap-line">
			<hr/>
			<a href="<?php echo bloginfo( 'url' ); ?>/function/product" class="see-more-link" data-function="product">
			  <button class="unf-btn unf-btn--small unf-btn--invert">See <?php echo sizeof( $divs ) - 6; ?> More<div class="text">See <?php echo sizeof( $divs ) - 6; ?> More</div></button>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<?php endif; ?>
	<!-- ./Product Function -->

	<!-- People Function -->
	<?php if ( array_key_exists( 'people', $jobs ) ) : ?>
	<div class="list-function 
		<?php
		if ( $isPeople && sizeof( $jobs['people'] ) ) {
			echo 'active';}
		?>
	" id="people">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/people.png" alt="people" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/people"><h3 class="list-function-header__h3">People</h3></a>
		  <p class="list-function-header__p">Manage, coordinate, and collaborate with the Nakamas to create a healthy workplace</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isPeople && sizeof( $jobs['people'] ) ) :
			$divs = $jobs['people'];
			?>


			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <hr/>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
				  <?php $apply_link = 'https://tokopedia2.wd3.myworkdayjobs-impl.com/en-US/Search/job/' . str_replace( ' ', '-', $job['location']['office'] ) . '/' . str_replace( ' ', '-', $job['title'] ) . '_' . $job['requisition_id'] . '/apply'; ?>
					<a href="<?php echo $job['external_apply_url']; ?>" class="" rel="nofollow" title="Apply Now">
					  <button class="unf-btn unf-btn--no-shadow unf-btn--small full-width">Register<div class="text">Register</div></button>
					</a>
				  </div> 
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="">
					  <button class="unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center wrap-line">
			<hr/>
			<a href="<?php echo bloginfo( 'url' ); ?>/function/people" class="see-more-link" data-function="people">
			  <button class="unf-btn unf-btn--small unf-btn--invert">See <?php echo sizeof( $divs ) - 6; ?> More<div class="text">See <?php echo sizeof( $divs ) - 6; ?> More</div></button>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<?php endif; ?>
	<!-- ./People Function -->

	<!-- Tech Function -->
	<?php if ( array_key_exists( 'technology', $jobs ) ) : ?>
	<div class="list-function 
		<?php
		if ( $isTechnology && sizeof( $jobs['technology'] ) ) {
			echo 'active';}
		?>
	" id="technology">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/technology.png" alt="tech" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/technology"><h3 class="list-function-header__h3">Technology</h3></a>
		  <p class="list-function-header__p">Build and maintain the architecture of the product</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isTechnology && sizeof( $jobs['technology'] ) ) :
			$divs = $jobs['technology'];
			?>

			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <hr/>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
				  <?php $apply_link = 'https://tokopedia2.wd3.myworkdayjobs-impl.com/en-US/Search/job/' . str_replace( ' ', '-', $job['location']['office'] ) . '/' . str_replace( ' ', '-', $job['title'] ) . '_' . $job['requisition_id'] . '/apply'; ?>
					<a href="<?php echo $job['external_apply_url']; ?>" class="" rel="nofollow" title="Apply Now">
					  <button class="unf-btn unf-btn--no-shadow unf-btn--small full-width">Register<div class="text">Register</div></button>
					</a>
				  </div> 
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="">
					  <button class="unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center wrap-line">
			<hr/>
			<a href="<?php echo bloginfo( 'url' ); ?>/function/technology" class="see-more-link" data-function="technology">
			  <button class="unf-btn unf-btn--small unf-btn--invert">See <?php echo sizeof( $divs ) - 6; ?> More<div class="text">See <?php echo sizeof( $divs ) - 6; ?> More</div></button>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<?php endif; ?>
	<!-- ./Tech Function -->

	<!-- Intern -->
	<?php if ( array_key_exists( 'internship', $jobs ) ) : ?>
	<div class="list-function 
		<?php
		if ( $isInternship && sizeof( $jobs['internship'] ) ) {
			echo 'active';}
		?>
	" id="internship">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/microsite-production/careers/img/careers-v2/internship.png" alt="intern" class="list-function-header__img"/>
		<div class="list-function-header-text">
		  <a href="../function/internship"><h3 class="list-function-header__h3">Internship</h3></a>
		  <p class="list-function-header__p">Learn and experience through project contribution</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isInternship && sizeof( $jobs['internship'] ) ) :
			$divs = $jobs['internship'];
			?>

			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <hr/>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="" rel="nofollow" title="Apply Now">
					  <button class="unf-btn unf-btn--no-shadow unf-btn--small full-width">Register<div class="text">Register</div></button>
					</a>
				  </div> 
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="">
					  <button class="unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center wrap-line">
			<hr/>
			<a href="<?php echo bloginfo( 'url' ); ?>/function/internship" class="see-more-link" data-function="internship">
			  <button class="unf-btn unf-btn--small unf-btn--invert">See <?php echo sizeof( $divs ) - 6; ?> More<div class="text">See <?php echo sizeof( $divs ) - 6; ?> More</div></button>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<?php endif; ?>
	<!-- ./Internship -->
  </div>
</section>

<section class="section_default" <?php echo $displayDefault; ?>>
  <div class="container ct-error">
	  <div class="ct-cell">
		  <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-oops.png" alt="">
		  <h2 class="error__thanks">Oops, We can't find the job</h2>

		  <div class="error-copy">
			  <h3 class="error__notify">
				  <a href="<?php echo get_bloginfo( 'url' ); ?>/jobs">Try searching other jobs</a>
			  </h3>
		  </div>
	  </div>
  </div>
</section>
