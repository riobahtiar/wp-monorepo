<?php
/**
* Template Name:  Career Nakama
*/
?>

<section class="images-section">
	<div class="images-wrapper">
		<div class="images"></div>
	</div>
</section>

<script>
(function($) {

	var imagePiece;
	console.log(document.querySelectorAll('.images-wrapper')[0]);

	function startImage(src) {
		document.querySelectorAll('.images-wrapper')[0].style.height = window.innerHeight+'px';
		imagePiece = new imageComponent(src);
		// generateWrapper.start();
	}

	function randomImage(start, end) {
		return Math.floor(Math.random() * end) + start;
	}

	function randomizer(start, end) {
		var num = Math.floor(Math.random() * end) + start;
		return num *= Math.floor(Math.random()*2) == 1 ? 1 : -1;
	}

	function imageComponent(src) {
		var ti; //theImage
		var mult; //translate Multiplier
		var dom; //dom to append to
		var rot; //default Y rotation
		var ty;

		var ww; //Window width
		var wh;
		
		rot = randomizer(0, 25);
		ty = randomImage(3, 10);
		var tyc = ty;
		dom = document.querySelectorAll('.images')[0];
		mult = 0;
		ti = new Image();
		ti.src = src;

		ww = window.innerWidth;
		wh = window.innerHeight;

		// Init Transform Styling
		ti.style.transform = 'translateX(-200px) translateY('+randomImage(0, wh)+'px)';

		dom.appendChild(ti);

		ti.onload = function() {
			var defTrans = 'translateX('+ww+'px) translateY('+randomImage(0, wh)+'px) perspective(400px) translateZ(0)';

			ti.style.transform = 'translateX(-200px)';
			// ti.style.transition = 'transform '+ty+'s linear';
			ti.style.transition = 'transform '+randomImage(3, 10)+'s linear';
			// ti.style.transform = defTrans;
			

			ti.timeout = setTimeout(function(){
				ti.style.transform = defTrans;
				console.log('timeout trigger');
			}, 1000);
			ti.interval = setInterval(function() {
				// mult += 1;
				// ti.style.transform = defTrans+' translateY('+randomizer(100, 500)+'px)';
				var matrix = new WebKitCSSMatrix(window.getComputedStyle(ti).transform);
				if(matrix.m41 >= ww) {
					clearInterval(ti.interval);
					ti.parentNode.removeChild(ti);
				}
			}, 10);
			ti.interval2 = setInterval(function(){
				// ti.style.transform = defTrans+' translateY('+randomizer(100, 500)+'px)';
				if(tyc <= 0 ) {
					clearInterval(ti.interval2);
					clearInterval(ti.invertal);
					console.log(tyc);
				}
				tyc--;
			}, 500);
		};
	}

	$.getJSON('https://ecs7.tokopedia.net/assets/careers/json/images.json', function(data){
		var rando;
		var shownImage = [];
		var count = 0;
		
		// Generate Unique
		while( count < 1000 ) {
			rando = randomImage(0, data.data.length);
			if(shownImage.indexOf(rando) == -1) {
				shownImage.push(rando);
				count++;
			}
		}
		
		// Render
		var imageCounter = 0;
		var sLen = shownImage.length;
		console.log(sLen);
		var abc = setInterval(function(){
			// console.log(data.data[sLen].picture);
			console.log(sLen);
			startImage(data.data[shownImage[sLen-1]].picture);
			sLen--;
			if(sLen <= 0) {
				// clearInterval(abc);
				sLen = shownImage.length;
			}
		}, 2000);
	});

	// $('.images-section').width($(window).width() * 3);
	// $('.images').width($(window).width());
		
	})(jQuery);
</script>
