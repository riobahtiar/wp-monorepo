<?php
    $referer = esc_html(base64_decode($_GET["job_ref"]));
    $err = esc_html(base64_decode($_GET["err"]));
    $app_url = esc_html(base64_decode($_GET["app_url"]));
?>

<!-- 
<?php

if ( isset( $_SERVER['HTTP_REFERER'] ) ) {
	if ( ! strpos( $_SERVER['HTTP_REFERER'], 'tokopedia.com/careers' ) !== false ) {
		?>
<script>
window.location.replace("<?php echo home_url(); ?>");
</script>
		<?php
	}
} else {
	?>
<script>
window.location.replace("<?php echo home_url(); ?>");
</script>
	<?php
}
?>
 -->

<style>
	.ct-error {
		display: table;
		width: 100%;
		height: 100%;
		vertical-align: middle;
		text-align: center;
	}

	.ct-cell {
		display: table-cell; 
		vertical-align: middle;
	}

	.error__thanks {
		padding: 20px 0;
		font-size: 20px;
		font-weight: 600;
		color: rgba(0, 0, 0, 0.7);
		
	}

	.error__notify {
		font-size: 14px;
		color: rgba(0, 0, 0, 0.54);
	}

	.error__notify p {
		font-weight: 600;
		margin-top: 2px;
	}

	.error__notify--bold {
		font-weight: 700;
	}

	.error-copy {
		margin-bottom: 40px;
	}

	.error__or {
		font-size: 14px;
		margin: 25px 0;
		color: rgba(0, 0, 0, 0.7);
	}

	.error__appurl {
		text-decoration: underline;
		font-size: 16px;
	}

	
</style>

<div class="container ct-error">
	<div class="ct-cell">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-oops.png" alt="">
		<h2 class="error__thanks">Oops, Your application cannot be processed</h2>

		<div class="error-copy">
			<h3 class="error__notify">
				We are very sorry for this inconvenience. 
				<p><?php echo $err; ?></p>       
			</h3>
		</div>
		<div class="text-center">
			<a href="<?php echo bloginfo( 'url' ) . '/job/' . $referer; ?>" class="btn-tkpd btn--medium btn--green" style="width: 200px;">Back to Job Details</a>
		</div>
		<div class="text-center error__or">
			OR
		</div>
		<div class="text-center">
			<a href="<?php echo $app_url; ?>" class="error__appurl">Apply via Workable</a>
		</div>
	</div>
</div>
