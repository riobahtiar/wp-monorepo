
<head>
  <?php
  /*===================================
  Default value
  =====================================*/
  $robots    = 'index, follow';
  $canonical = 'https://www.tokopedia.com/careers/' . basename( get_permalink() . '/' );
  $og_image  = 'https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2017/12/tokopedia-one.jpg';

  /*===================================
  Page Home
  =====================================*/
  if ( is_front_page() || is_home() ) {
	  $canonical = 'https://www.tokopedia.com/careers/';
	  if ( have_rows( 'front_page', 'option' ) ) :
		  while ( have_rows( 'front_page', 'option' ) ) :
			  the_row();

			  $the_title              = get_sub_field( 'title' ) ?: $the_title;
			  $the_description        = get_sub_field( 'meta_description' ) ?: $the_description;
			  $index_by_search_engine = get_sub_field( 'index_by_search_engine' );
			  $canonical              = get_sub_field( 'canonical' ) ?: $canonical;
			  $og_image               = get_sub_field( 'og_image' ) ?: $og_image;
			  $robots                 = ( $index_by_search_engine == 1 ? 'index, follow' : 'noindex, nofollow' );

		endwhile;
	endif;
	}

	/*===================================
	Page About
	=====================================*/
	if ( is_page( 'about' ) ) {
		if ( have_rows( 'about_us', 'option' ) ) :
			while ( have_rows( 'about_us', 'option' ) ) :
				the_row();

				$the_title              = get_sub_field( 'title' ) ?: $the_title;
				$the_description        = get_sub_field( 'meta_description' ) ?: $the_description;
				$index_by_search_engine = get_sub_field( 'index_by_search_engine' );
				$canonical              = get_sub_field( 'canonical' ) ?: $canonical;
				$og_image               = get_sub_field( 'og_image' ) ?: $og_image;
				$robots                 = ( $index_by_search_engine == 1 ? 'index, follow' : 'noindex, nofollow' );

		  endwhile;
	  endif;
	}

	/*===================================
	Page Jobs
	=====================================*/
	if ( is_page( 'jobs' ) ) {
		if ( have_rows( 'jobs_page', 'option' ) ) :
			while ( have_rows( 'jobs_page', 'option' ) ) :
				the_row();

				$the_title              = get_sub_field( 'title' ) ?: $the_title;
				$the_description        = get_sub_field( 'meta_description' ) ?: $the_description;
				$index_by_search_engine = get_sub_field( 'index_by_search_engine' );
				$canonical              = get_sub_field( 'canonical' ) ?: $canonical;
				$og_image               = get_sub_field( 'og_image' ) ?: $og_image;
				$robots                 = ( $index_by_search_engine == 1 ? 'index, follow' : 'noindex, nofollow' );

		  endwhile;
	  endif;
	}


	/*===================================
	Page Job (Specific Job)
	=====================================*/
	if ( is_page( 'job' ) ) {
		$canonical = get_bloginfo( 'url' ) . '/' . $the_slug;
		if ( have_rows( 'meta_content_job', 'option' ) ) {
			$i      = 0;
			$robots = 'index, follow';

			while ( have_rows( 'meta_content_job', 'option' ) ) {
				the_row();
				$i++;
				//$job_workable = get_sub_field_object( 'job_title_workable' );
				$job_title_workable = get_sub_field_object( 'list_job_workable' );
				$value              = $job_title_workable['value'];
				$shortcode          = '';

				foreach ( $job_title_workable['choices'] as $k => $v ) {
					if ( $value == $k ) {
						$shortcode = $k;

					}
				}

				if ( $the_shortcode == $shortcode ) {

					$the_title              = get_sub_field( 'job_title' ) . ' Job | ' . get_bloginfo( 'name' );
					$the_description        = get_sub_field( 'meta_description' ) ?: $the_description;
					$index_by_search_engine = get_sub_field( 'index_by_search_engine' );
					$canonical              = get_sub_field( 'canonical' ) ?: $canonical;
					$og_image               = get_sub_field( 'og_image' ) ?: $og_image;
					$robots                 = ( $index_by_search_engine == 1 ? 'index, follow' : 'noindex, nofollow' );

				}
			}
		}
	}



	/*===================================
	Page Job Function
	=====================================*/
	if ( is_page( 'function' ) ) {
		if ( have_rows( 'job_function_page', 'option' ) ) :
			while ( have_rows( 'job_function_page', 'option' ) ) :
				the_row();

				$department_name        = get_sub_field_object( 'department_name' );
				$value                  = $department_name['value'];
				$custom_department_name = get_sub_field( 'custom_department_name' );
				$canonical              = 'https://www.tokopedia.com/careers/' . basename( get_permalink() . '/' . $slug );

				if ( $value == $slug ) {
					$the_title              = $custom_department_name . ' Jobs | Tokopedia Careers';
					$the_slug               = 'function/' . $value . '/';
					$the_description        = get_sub_field( 'meta_description' ) ?: $the_description;
					$index_by_search_engine = get_sub_field( 'index_by_search_engine' );
					$canonical              = get_sub_field( 'canonical' ) ?: $canonical;
					$og_image               = get_sub_field( 'og_image' ) ?: $og_image;
					$robots                 = ( $index_by_search_engine == 1 ? 'index, follow' : 'noindex, nofollow' );
				}

		  endwhile;
	  endif;
	}

	/*===================================
	No Index: Page Join, Success, Blog
	=====================================*/
	if ( is_page( 'join' ) || is_page( 'success' ) || is_page( 'blog' ) || is_page( 'error' ) ) {
		$robots = 'noindex, follow';
	} else {
		$robots = 'index, follow';
	}

	/*===================================
	Fallback if empty
	=====================================*/

	$the_title       = ( isset( $the_title ) ) ? $the_title : get_the_title();
	$the_description = ( isset( $the_title ) ) ? $the_title : get_the_title();

	?>
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="robots" content="<?php echo $robots; ?>" />
	<!-- Search Engine -->
	<title><?php echo $the_title; ?></title>
	<meta name="description" content="<?php echo $the_description; ?>" />
	<meta name="image" content="<?php echo $og_image; ?>" />
	<link rel="canonical" href="<?php echo $canonical; ?>" />
	<!-- Schema.org for Google -->
	<meta itemprop="name" content="<?php echo $the_title; ?>" />
	<meta itemprop="description" content="<?php echo $the_description; ?>" />
	<meta itemprop="image" content="https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2017/12/tokopedia-one.jpg" />
	<!-- Open Graph general (Facebook, Pinterest & Google+) -->
	<meta property="og:type" content="website" />
	<meta property="og:title" content="<?php echo $the_title; ?>" />
	<meta property="og:description" content="<?php echo $the_description; ?>" />
	<meta property="og:image" content="<?php echo $og_image; ?>" />
	<meta property="og:image:secure_url" content="<?php echo $og_image; ?>" />
	<meta property="og:url" content="<?php echo $canonical; ?>" />
	<meta property="og:site_name" content="<?php echo $the_title; ?>" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:locale:alternate" content="id_ID" />
	<meta property="og:locale:alternate" content="en_GB" />
	<meta property="fb:admins" content="386081579611" />
	<meta property="fb:app_id" content="2085658451663443" />
	<meta property="article:publisher" content="https://www.facebook.com/tokopedia/" />
	<meta property="article:author" content="https://www.facebook.com/tokopedia/" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@tokopedia" />
	<meta name="twitter:site:id" content="@tokopedia" />
	<meta name="twitter:creator" content="@tokopedia" />
	<meta name="twitter:description" content="<?php echo $the_description; ?>" />
	<meta name="twitter:image:src"
		  content="<?php echo $og_image; ?>" />
	<script type='application/ld+json'>
	{
	  "@context":"http://schema.org",
	  "@type":"WebSite",
	  "@id":"#website",
	  "url":"https://www.tokopedia.com/careers/",
	  "name":"Tokopedia Careers","potentialAction":
	  {
		"@type":"SearchAction",
		"target":"https://www.tokopedia.com/blog/jobs/?query={search_term_string}",
		"query-input":"required name=search_term_string"
	  }
	}
	</script>
	<script type='application/ld+json'>
	{
	  "@context":"http://schema.org",
	  "@type":"Organization",
	  "url":"https://www.tokopedia.com/careers/",
	  "sameAs":
	  [
		"https://www.facebook.com/tokopedia/",
		"https://www.instagram.com/tokopedia/",
		"https://www.linkedin.com/company/pt--tokopedia",
		"https://www.youtube.com/user/tokopedia",
		"https://twitter.com/tokopedia"
	  ],
	  "@id":"#organization",
	  "name":"PT. Tokopedia",
	  "logo":"https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2015/08/tokopedia.png"
	}
	</script>
	<?php wp_head(); ?>
	<!-- Fresh Marketer Tracking Code for https://www.tokopedia.com/careers/ -->
	<!-- <script type='text/javascript'>
		(function() {    window.zarget=true;    var protocol = ('https:' == document.location.protocol ? 'https:' : 'http:');    var scriptTag = document.createElement('script');    scriptTag.type = 'text/javascript';    scriptTag.async = true;    scriptTag.src = protocol +'//cdn.freshmarketer.com/179700/477681.js';    var s = document.getElementsByTagName('script')[0];    s.parentNode.insertBefore(scriptTag, s);})();function zargetTimeout() {    window.zarget = false;}window.zargetTimer = setTimeout(zargetTimeout, 3000);
	</script> -->

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TZNNXTG');</script>
	<!-- End Google Tag Manager -->

</head>

<noscript>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Warning!! </a>
				<p class="navbar-text text-danger" style="padding: 5px; color: black;">
					For full functionality of this site it is necessary to <b style="color: red;">Enable JavaScript</b>.
					Here are the <a href="https://www.enable-javascript.com/" target="_blank">
						instructions how to enable JavaScript in your web browser</a>.</p>
				<hr>
				<p class="navbar-text text-danger" style="padding: 5px; color: black;">
					<b style="color: red;">Website ini membutuhkan JavaScript untuk bekerja secara optimal.</b>
					<a href="https://www.enable-javascript.com/id/" target="_blank">
						Klik disini untuk cara mengaktifkan javascript</a>.</p>
			</div>
		</div>
	</nav>
</noscript>
