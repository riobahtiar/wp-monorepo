<?php
$useragent = $_SERVER['HTTP_USER_AGENT'];
echo( '<script> var nref = "theme";</script>' );
if ( preg_match( '/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent ) || preg_match( '/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr( $useragent, 0, 4 ) ) ) {
	echo( '<script> var dref="mobile";</script>' );
} else {
	echo( '<script> var dref="desktop";</script>' );
}
if ( ! isset( $_GET['flag_app'] ) ) {
	?>
<header id="header">
	<div class="container">
		<div class="top-navigation-logo hidden-xs hidden-sm">
			<a href="<?php echo home_url(); ?>" class="site-logo">
				<img src="https://ecs7.tokopedia.net/assets/images/careers/site-logo.svg"
					alt="Tokopedia Logo"
					class="site__logo">
			</a>
		</div>
		<div class="bridge-widget">
			<a href="#" class="bw-pivot">
				<img src="https://ecs7.tokopedia.net/assets/images/careers/pivot-new.svg"
						alt=""
						width="24">
			</a>
			<div class="bw-container">
				<div class="bw-item">
					<a href="https://www.tokopedia.com/">
						<div class="bw-icon__wrapper">
							<div class="bw-icon bw-icon-toped"></div>
						</div>
						<p class="bw-icon__text">Jual Beli Online</p>
						<span class="clear-b"></span>
					</a>
				</div>
				<div class="bw-item">
					<a href="https://www.tokopedia.com/official-store/">
						<div class="bw-icon__wrapper">
							<div class="bw-icon bw-icon-ofstore"></div>
						</div>
						<p class="bw-icon__text">Official Store</p>
						<span class="clear-b"></span>
					</a>
				</div>
				<div class="bw-item">
					<a href="https://www.tokopedia.com/pulsa/">
						<div class="bw-icon__wrapper">
							<div class="bw-icon bw-icon-pulsa"></div>
						</div>
						<p class="bw-icon__text">Produk Digital</p>
						<span class="clear-b"></span>
					</a>
				</div>
				<div class="bw-item">
					<a href="https://tiket.tokopedia.com/kereta-api/">
						<div class="bw-icon__wrapper">
							<div class="bw-icon bw-icon-tiket"></div>
						</div>
						<p class="bw-icon__text">Tiket Kereta</p>
						<span class="clear-b"></span>
					</a>
				</div>
				<div class="bw-item">
					<a href="https://tokopedia.com/berbagi/">
						<div class="bw-icon__wrapper">
							<div class="bw-icon bw-icon-donasi"></div>
						</div>
						<p class="bw-icon__text">Donasi</p>
						<span class="clear-b"></span>
					</a>
				</div>
				<div class="bw-item">
					<a href="https://www.tokopedia.com/bantuan/">
						<div class="bw-icon__wrapper">
							<div class="bw-icon bw-icon-bantuan"></div>
						</div>
						<p class="bw-icon__text">Bantuan</p>
						<span class="clear-b"></span>
					</a>
				</div>
			</div>
		</div>
		<?php
			$full      = home_url( $wp->request );
			$vacancy   = strpos( $full, '/jobs' ) || strpos( $full, '/function' ) || strpos( $full, '/job' );
			$workplace = strpos( $full, '/our-workplace' );
			$aboutus   = $full == home_url();
			$community = strpos( $full, '/our-community' );
			$blog      = strpos( $full, '/blog' );
		?>
		<div class="top-navigation-menu clearfix">
			<ul class="clearfix">
				<li class="nav-aboutus">
					<a href="<?php echo home_url() . '/'; ?>" 
										<?php
										if ( $aboutus ) {
											echo 'class="active"';}
										?>
					>
						About Us
					</a>
				</li>
				<li class="nav-workplace">
					<a href="<?php echo home_url() . '/our-workplace/'; ?>" 
										<?php
										if ( $workplace ) {
											echo 'class="active"';}
										?>
					>
						Our Workplace
					</a>
				</li>
				<li class="nav-community">
					<a href="<?php echo home_url() . '/our-community/'; ?>" 
										<?php
										if ( $community ) {
											echo 'class="active"';}
										?>
					>
						Our Community
					</a>
				</li>
				<li>
					<a href="<?php echo home_url() . '/jobs/'; ?>" 
										<?php
										if ( $vacancy ) {
											echo 'class="active"';}
										?>
					>
						Vacancy
					</a>
				</li>
				<li class="nav-blog">
					<a href="<?php echo home_url() . '/blog/'; ?>" 
										<?php
										if ( $blog ) {
											echo 'class="active"';}
										?>
					>
						Blog
					</a>
				</li>
			</ul>
		</div>
		<!--FOR MOBILE -->
		<div class="topnav-mobile clearfix">
			<div id="burger-menu" class="visible-xs visible-sm">
				<span></span>
			</div>
			<?php if( !is_home() && !is_page_template('template-careers_v2.php') && !is_page_template('template-careers_v3.php')):?>
				<div class="top-nav-search">
					<!-- <img src="https://ecs7.tokopedia.net/assets/images/careers/search-luv-new.svg" class="top-nav-search__search">
					<img src="https://ecs7.tokopedia.net/assets/images/careers/search-close.svg" class="top-nav-search__close"> -->
					<a class="search-icon">
						<span class="circle"></span>
						<span class="handle"></span>
					</a>
				</div>
			<?php endif; ?>
			<div class="top-navigation-logo visible-xs visible-sm">
				<a href="<?php echo home_url(); ?>" class="site-logo">
					<img src="https://ecs7.tokopedia.net/assets/images/careers/site-logo.svg"
						alt="Tokopedia Logo"
						class="site__logo">
				</a>
			</div>
		</div>
		<div class="site-overlay--ham"></div>
		<!-- <?php dynamic_sidebar( 'topnav-widget' ); ?> -->
	</div>
</header>

	<?php
}
?>
<div class="site-overlay"></div>

<?php if( !is_home() && !is_page_template('template-careers_v3.php') && !is_page_template('template-careers_v2.php') ): ?>
	<!-- SEARCH -->
	<section id="msrc" class="section_mobile-search">
		<div class="m-searchbar clearfix">
			<div class="m-searchbar-arrow">
				<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-back-mobile.png" alt="" class="m-searchbar-arrow__arrow">
			</div>
			<div class="m-searchbar-lens">
				<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-search-mobile.png" alt="" class="m-searchbar-lens__lens">
			</div>
			<!-- <form id="search-job" action="#" method="GET"> -->
			<input name="query" type="search" class="search-bar__thebar search-bar__thebar--m" placeholder="Type your dream jobs...">
			<!-- </form> -->
		</div>
		<ul class="search-bar-thebar-ul search-bar-thebar-ul--m">
			<p class="m-searchcopy__copy">Search Suggestions</p>
			<!-- populated via JS -->
		</ul>
	</section>

	<section class="section_mobile-location clearfix">
		<div class="m-location clearfix">
			<div class="m-location-copy">
				<h2 class="m-location-copy__copy">
					Choose your prefered place
				</h2>
			</div>
			<ul class="search-bar-theloc-ul--m">
				<li class="search-bar-theloc__li">
					<div class="search-bar-theloc__liwrapper">
						<span class="search-bar-theloc__liloc">All Locations</span>
						<div class="pull-right">
						</div>
					</div>
				</li>
				<!-- populated via JS -->
			</ul>
		</div>
	</section>

	<form action="<?php echo home_url(); ?>/jobs" method="get">
		<section class="section_search fixed" style="z-index: 1000;">
			<div class="section_search-header">
				<h1 class="section_search-header__title">
					Find Your Dream Job in Tokopedia!
				</h1>
				<h3 class="section_search-header__copy">
					Join Our Merry Band of Nakama Sailing the Seven Seas! 
				</h3>
			</div>
			<div class="search-wrap">
				<div class="container">
					<!-- Search Bar -->
					<div class="search-bar clearfix">

						<div class="col-search-60">
							<div class="search-bar-wrapbar clearfix">
								<div class="search-bar-thebar clearfix">
									<!-- <div class="search-bar-thebar-image">
										<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-lup.svg" alt="Search Jobs" class="search-bar__icon">
										<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-jobsnew-scroll.svg" alt="Search Jobs" class="search-bar__icon--scroll">
									</div> -->
									<!-- <div class="search-bar-thebar-group">
										<label for="query" class="search-bar__label txt--black txt--primary fs--12 fw--smb">I want to join as</label>
									</div> -->
									<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-lup.svg" alt="Search Jobs" class="search-bar__icon">
									<input name="query" type="text" class="search-bar__thebar" placeholder="Type your dream jobs" autocomplete="off"></input>
									<ul class="search-bar-thebar-ul">
										<!-- populated via JS -->
									</ul>
								</div>

								<div class="search-bar-theloc">
									<!-- <div class="search-bar-thebar-image">
										<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-location-on.svg" alt="Search Jobs Location" class="search-bar__icon">
										<img src="https://ecs7.tokopedia.net/assets/images/careers/icon-lokasi-scroll.svg" alt="Search Jobs Location" class="search-bar__icon--scroll">
									</div> -->
									<!-- <div class="search-bar-thebar-group">
										<label for="country" class="search-bar__label txt--black txt--primary fs--12 fw--smb">Location</label>
										<input type="text" class="search-bar__theloc" placeholder="All Locations" readonly="true" value=""></input>
										<input name="country" type="hidden" class="search-bar__theloc--true" placeholder="All Locations" readonly="true" value=""></input>
									</div> -->

									<img src="https://ecs7.tokopedia.net/assets/images/careers/sen-percent.svg" alt="Search Jobs Location" class="search-bar__icon">
									<input type="text" class="search-bar__theloc" placeholder="All Locations" readonly="true" value=""></input>
									<input name="country" type="hidden" class="search-bar__theloc--true" placeholder="All Locations" readonly="true" value=""></input>

									<i class="fa fa-angle-down search-bar__thelocarrow" aria-hidden="true"></i>
									<ul class="search-bar-theloc-ul">
										<li class="search-bar-theloc__li">
											<div class="search-bar-theloc__liwrapper">
												<span class="search-bar-theloc__liloc">All Locations</span>
												<div class="pull-right">

												</div>
											</div>
										</li>
			
									</ul>
								</div>
							</div>
						</div>

						<div class="col-search-40">
							<div class="search-or text-center fw--600">OR</div>
							<div class="search-or-line"></div>
							<div class="buttons-wrap clearfix">
								<button type="submit" class="btn-search unf-btn unf-btn--transaction unf-btn--small ripple-effect ripple-main">
									Search<div class="text">Search</div>
								</button>
								<a href="../jobs">
									<button class="btn-view unf-btn unf-btn--small ripple-effect ripple-main">
										View All Jobs<div class="text">View All Jobs</div>
									</button>
								</a>
								<!-- <div class="col-search-btn">
								</div> -->
								<!-- <div class="col-view-btn">
								</div> -->
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
	</form>
	<!-- /SEARCH -->
<?php endif;?>
<div class="mobile-animation-wrapper"> <!-- open div for body animation on mobile-->
