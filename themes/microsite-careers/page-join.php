<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<div class="container">
	<div class="apply">
		<h1>Join Tokopedia as <a
					href="<?php echo get_bloginfo( 'url' ) . '/job/' . $slug; ?>"><span><?php echo $job['title']; ?></span></a>
		</h1>
		<p>First, tell us a bit about yourself. Fields with asterisk (*) are required</p>
		<div class="row">
			<div class="stepper-width"></div>
			<div class="col-md-11" style="margin-left:25px">
				<form class="tkpd-form" style="margin-bottom:65px">
					<div class="row header">
						<div class="col-md-12">
							<h2>Personal Information</h2>
						</div>
					</div>
					<div class="row required">
						<div class="col-md-6">
							<div class="tkpd-input">
								<label>Name *</label>
								<input type="text" placeholder="First Name" required name="c-firstname">
							</div>
						</div>
						<div class="col-md-6">
							<div class="tkpd-input">
								<label>&nbsp;</label>
								<input type="text" placeholder="Last Name" required name="c-lastname">
							</div>
						</div>
					</div>
					<div class="row required">
						<div class="col-md-6">
							<div class="tkpd-input">
								<label>Email Address *</label>
								<input type="email" placeholder="Input your email address" disabled required
									   name="c-email">
							</div>
						</div>
					</div>
					<div class="row required">
						<div class="col-md-6">
							<div class="tkpd-input">
								<label>Phone Number *</label>
								<input type="tel" placeholder="Input your phone number" disabled required
									   name="c-phone">
							</div>
						</div>
					</div>
					<div class="row header">
						<div class="col-md-12">
							<h2>Professional Information</h2>
						</div>
					</div>
			<?php
			foreach ( $arr->form_fields as $key ) {
				if ( $key->key == 'resume' ) {
					?>
							<div class="row 
							<?php
							if ( $key->required ) {
								echo 'required'; }
							?>
								">

								<div class="col-md-6">

									<div class="tkpd-input">

										<label>Resume *</label>

										<input type="file" disabled id="resume" data-buttonText="Upload Resume"
											   accept=".pdf, .doc, .docx">

										<span class="info" style="display:block">Accepted file formats: pdf, doc, docx (3 MB max)</span>

									</div>

								</div>

							</div>
							<?php
				}

				if ( $key->key == 'avatar' ) {
					?>
							<div class="row 
							<?php
							if ( $key->required ) {
								echo 'required'; }
							?>
								">

								<div class="col-md-6">

									<div class="tkpd-input">

										<label>Photo</label>

										<input type="file" disabled id="avatar" data-buttonText="Upload Photo"
											   accept=".jpg, .jpeg, .png">

										<span class="info" style="display:block">Accepted file formats: jpg, jpeg, png (3 MB max)</span>

									</div>

								</div>

							</div>
							<?php
				}

				if ( $key->key == 'education' ) {
					?>
							<div class="row education-row 
							<?php
							if ( $key->required ) {
								echo 'required'; }
							?>
								">
								<div class="col-md-12">
									<div>
										<label> Do you provide last education information in your resume?</label>
										<div class="education clone">
											<div class="col-md-8">
												<div class="education-delete"><i class="fa fa-times"
																				 aria-hidden="true"></i></div>
												<div class="row">
													<div class="col-md-8">
														<span class="info" style="display:block">School Name</span>
														<input type="text" placeholder="Input school name"
															   style="margin-bottom:6px" required name="c-school">
													</div>
												</div>
												<div class="row">
													<div class="col-md-8">
														<span class="info" style="display:block">Field of Study</span>
														<input type="text" placeholder="Input field of study"
															   style="margin-bottom:6px" required name="c-study">
													</div>
												</div>
												<div class="row">
													<div class="col-md-8">
														<span class="info" style="display:block">Degree</span>
														<input type="text" placeholder="Input degree"
															   style="margin-bottom:6px" required name="c-degree">
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<span class="info" style="display:block">From</span>
														<input style="margin-bottom:6px" data-provide="datepicker"
															   required name="c-from">
													</div>
													<div class="col-md-4">
														<span class="info" style="display:block">To</span>
														<input style="margin-bottom:6px" data-provide="datepicker"
															   required name="c-to">
													</div>
												</div>
											</div>
										</div>
										<div>
											<div class="col-md-2">
												<input type="radio" id="radio-edu-yes" name="radio-edu" value="yes"
													   disabled><label for="radio-edu-yes" style="display:inline-block">Yes</label>
											</div>
											<div class="col-md-2">
												<input type="radio" id="radio-edu-no" name="radio-edu" value="no"
													   disabled><label for="radio-edu-no" style="display:inline-block">No</label>
											</div>
											<div style="clear:both"></div>
										</div>
										<div style="clear:both"></div>
										<span class="info" style="display:inline-block;margin-top:0;color:#42b549;"
											  data-add-education="true">+ Add more education</span>
									</div>
								</div>
							</div>
							<?php
				}

				if ( $key->key == 'experience' ) {
					?>
							<div class="row experience-row 
							<?php
							if ( $key->required ) {
								echo 'required'; }
							?>
								">

								<div class="col-md-12">

									<div>
										<label>Do you provide work experience information in your resume?</label>
										<div class="experience clone">
											<div class="col-md-8">
												<div class="experience-delete"><i class="fa fa-times"
																				  aria-hidden="true"></i></div>
												<div class="row">
													<div class="col-md-8">
														<span class="info" style="display:block">Company</span>
														<input type="text" placeholder="Input Company"
															   style="margin-bottom:6px" disabled required
															   name="c-company">
													</div>
												</div>
												<div class="row">
													<div class="col-md-8">
														<span class="info" style="display:block">Position</span>
														<input type="text" placeholder="Input position"
															   style="margin-bottom:6px" disabled required
															   name="c-position">
													</div>
												</div>
												<div class="row">
													<div class="col-md-10">
														<span class="info" style="display:block">Summary</span>
														<textarea type="text"
																  placeholder="Tell us about the most valuable experiences that you had"
																  style="margin-bottom:6px" rows=4 disabled required
																  name="c-summary"></textarea>
													</div>
												</div>
												<div class="row">
													<div class="col-md-8">
														<span class="info" style="display:block">Industry</span>
														<input type="text" placeholder="Input industry"
															   style="margin-bottom:6px" disabled required
															   name="c-industry">
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<span class="info" style="display:block">From</span>
														<input style="margin-bottom:6px" data-provide="datepicker"
															   disabled required name="c-from">
													</div>
													<div class="col-md-4">
														<span class="info" style="display:block">To</span>
														<input style="margin-bottom:6px" data-provide="datepicker"
															   disabled required name="c-to">
													</div>
													<div class="col-md-4 cwh">
														<input type="checkbox" id="currently-work-here" disabled
															   required name="c-cwh"> <span class="info"> Currently Work Here </span>
													</div>
												</div>
											</div>
										</div>
										<div>
											<div class="col-md-2">
												<input type="radio" id="radio-exp-yes" name="radio-exp" value="yes"
													   disabled><label for="radio-exp-yes" style="display:inline-block">Yes</label>
											</div>
											<div class="col-md-2">
												<input type="radio" id="radio-exp-no" name="radio-exp" value="no"
													   disabled><label for="radio-exp-no" style="display:inline-block">No</label>
											</div>
											<div style="clear:both"></div>
										</div>
										<div style="clear:both"></div>
										<span class="info" style="display:inline-block;margin-top:0;color:#42b549"
											  data-add-experience="true">+ Add more experience</span>
									</div>
								</div>
							</div>
						<?php
				}
				?>


						<?php
						if ( $key->key == 'summary' ) {
							?>
							<div class="row 
							<?php
							if ( $key->required ) {
								echo 'required'; }
							?>
								">

								<div class="col-md-8">

									<div class="tkpd-input">

										<label>Summary *</label>

										<textarea placeholder="" style="margin-bottom:6px" rows=4 disabled required
												  name="c-summary"></textarea>

									</div>

								</div>

							</div>
							<?php

						}
			}
			?>

			<?php
			if ( $arr->questions ) {
				foreach ( $arr->questions as $key ) {
					?>
				<div class="row 
					<?php
					if ( $key->required ) {
								echo 'required'; }
					?>
								">
				<div class="col-md-6">
							<div class="tkpd-input">
								<label><?php echo $key->body; ?> *</label>
								  <?php
									if ( $key->type == 'boolean' ) {
										?>

										<div>
											<div class="col-md-4">
												<input type="radio" id="ans-yes-<?php echo $key->id; ?>" value="true"
													   disabled name="ans-radio-<?php echo $key->id; ?>" data-key="<?php echo $key->id; ?>"><label for="ans-yes-<?php echo $key->id; ?>" style="display:inline-block">Yes</label>
											</div>
											<div class="col-md-4">
												<input type="radio" id="ans-no-<?php echo $key->id; ?>" value="false"
													   disabled name="ans-radio-<?php echo $key->id; ?>" data-key="<?php echo $key->id; ?>"><label for="ans-no-<?php echo $key->id; ?>" style="display:inline-block">No</label>
											</div>
											<div style="clear:both"></div>
										</div>

										<?php
									} else {
										?>
								<textarea placeholder="" style="margin-bottom:6px" rows=4 disabled required
												  name="answer" data-key="<?php echo $key->id; ?>"></textarea>

										<?php
									}
									?>
							</div>
						</div>
				</div>
					<?php
				}
			}
			?>


					<input type="submit" class="form-button" disabled value='Submit'
						   style="width:auto; padding:10px 20px; border:0">
				</form>
			</div>
		</div>
	</div>
</div>
</div>

<script>
	(function ($) {
		$(document).ready(function () {
			//PREPENDING STEPPER
			$(".tkpd-form > .row:not(.header)").prepend("<div class='stepper'></div><div class='stepper-point'></div>");
			$(".tkpd-form > .row.header:nth-child(1)").prepend("<div class='stepper active'></div><div class='stepper-point big active'></div>");
			$(".tkpd-form > .row.header:not(:nth-child(1))").prepend("<div class='stepper'></div><div class='stepper-point big'></div>");
			$(".tkpd-form > .row:nth-child(4)").children(".stepper").addClass("quite-long");

			//STEPPER ACTIVE LOGIC
			$("input[name^='ans-radio']").change(function(){
				$(this).parents(".row").children(".stepper, .stepper-point").addClass("active");
					$(this).parents(".row").next().find("input, textarea").removeAttr("disabled");
					$(this).parent().parent().parent().next().find(".form-button").removeClass("disabled");
					$(this).parents(".row").nextUntil(".row.required").children(".stepper, .stepper-point").addClass("active");
					if ($(".stepper-point:not('.active')").length == 0) {
						//ACTIVATE SUBMIT BUTTON HERE
						$("input[type=submit]").removeAttr("disabled");
					}
			});
			$(".tkpd-form .row input, .tkpd-form .row textarea").on("keyup", function () {
				var success = [];
				for (var i = 0; i < $(this).parent().parent().parent().find("input").length; i++) {
					if ($(this).parent().parent().parent().find("input").eq(i).val() != "" && $(this).parent().parent().parent().find("textarea").eq(i).val() != "") {
						success.push("true");
					}
					else {
						success.push("false");
					}
				}
				if ($.inArray("false", success) == -1) {
					//PROCEED TO NEXT STEP
					if ($(this).parents(".row").index() == $(".tkpd-form .row:not('.experience-row, .education-row')").length - 1) {
						//ACTIVATE SUBMIT BUTTON HERE
						$("input[type=submit]").removeAttr("disabled");
					}
					$(this).parents(".row").children(".stepper, .stepper-point").addClass("active");
					$(this).parents(".row").next().find("input, textarea").removeAttr("disabled");
					$(this).parent().parent().parent().next().find(".form-button").removeClass("disabled");
					$(this).parents(".row").nextUntil(".row.required").children(".stepper, .stepper-point").addClass("active");
					if ($(".stepper-point:not('.active')").length == 0) {
						//ACTIVATE SUBMIT BUTTON HERE
						$("input[type=submit]").removeAttr("disabled");
					}
					if ($(this).parent().parent().parent().next().hasClass("header")) {
						$(this).parents(".row").next().children(".stepper, .stepper-point").addClass("active");
						$(this).parents(".row").nextUntil(".row.required").addBack().find("input:not([type=submit]), textarea").removeAttr("disabled");
						$(this).parents(".row").nextUntil(".row.required").addBack().find(".form-button").removeClass("disabled");
						$(this).parents(".row").next().find(".form-button").removeClass("disabled");
						$(this).parents(".row").next().next().find(".form-button").removeClass("disabled");
						if ($(".stepper-point:not('.active')").length == 0) {
							//ACTIVATE SUBMIT BUTTON HERE
							$("input[type=submit]").removeAttr("disabled");
						}
						if (!$(this).parents(".row").next().next().hasClass("required")) {
							$(this).parents(".row").nextUntil(".row.required").children(".stepper, .stepper-point").addClass("active");
						}
					}
				} else {
					$(this).parent().parent().prevAll(".stepper, .stepper-point").removeClass("active");
					$("input[type=submit]").attr("disabled", "true");
				}
			});

			//FORM JS STUFF
			$.extend($.expr[':'], {
				'containsi': function (elem, i, match, array) {
					return (elem.textContent || elem.innerText || '').toLowerCase()
						.indexOf((match[3] || "").toLowerCase()) >= 0;
				}
			});

			$('.tkpd-input .prepend').prev().css("padding-left", function () {
				return $(this).next().width() + 20 + "px";
			});

			$('.tkpd-input.select input[readonly="true"]').click(function () {
				$(this).next().css("display", "block");
				$(this).next().css("opacity", "1");
			});

			$('.tkpd-input.select input').on('input', function () {
				$(this).next().css("display", "block");
				$(this).next().css("opacity", "1");
				$(this).next().children('.option').css('display', 'none');
				$(this).next().children('.option:containsi(' + $(this).val() + ')').css('display', 'block');
			});

			$('.tkpd-input.select .icon').click(function () {
				$(this).next().next().next().css("display", "block");
				$(this).next().next().next().css("opacity", "1");
			});

			$('.option').click(function () {
				var value = $(this).text();
				// console.log(value);
				$(this).parent().prev().val(value.trim());
				$(this).parent().css("display", "none");
				$(this).parent().css("opacity", "0");
			});

			$(document).mouseup(function (e) {
				var container = $(".tkpd-input.select .select");

				// if the target of the click isn't the container nor a descendant of the container
				if (!container.is(e.target) && container.has(e.target).length === 0) {
					container.css("display", "none");
					container.css("opacity", "0");
				}
			});

			$('input[type="file"]').each(function (i, obj) {
				$('<div data-file="' + $(this).attr("id") + '" class="form-button disabled">' + $(this).attr("data-buttonText") + '</div>').insertAfter($(this));
			});

			$("*[data-file]").click(function () {
				$("input[id=" + $(this).attr("data-file") + "]").trigger("click");
			});

			$("#resume").on("change", function () {
				if ($(this).val() != "") {
					// $(this).next().html($(this).val().split('\\').pop());
					$(this).parents(".row").children(".stepper, .stepper-point").addClass("active");
					// $(this).parents(".row").next().children(".stepper, .stepper-point").addClass("active");
					$(this).parents(".row").nextUntil(".row.required").children(".stepper, .stepper-point").addClass("active");
					$(this).parents(".row").next().find("input, textarea").removeAttr("disabled");
					$(this).parents(".row").next().find(".form-button").removeClass("disabled");
					$(this).parents(".row").next().next().find("input").removeAttr("disabled");
					$(this).parents(".row").next().next().find(".form-button").removeClass("disabled");
					if ($(".stepper-point:not('.active')").length == 0) {
						//ACTIVATE SUBMIT BUTTON HERE
						$("input[type=submit]").removeAttr("disabled");
					}
				}
			});

			$("#avatar").on("change", function () {
				if ($(this).val() != "") {
					// $(this).next().html($(this).val().split('\\').pop());
					$(this).parents(".row").children(".stepper, .stepper-point").addClass("active");
					$(this).parents(".row").next().children(".stepper, .stepper-point").addClass("active");
					$(this).parents(".row").nextUntil(".row.required").children(".stepper, .stepper-point").addClass("active");
					$(this).parents(".row").next().find("input, textarea").removeAttr("disabled");
					$(this).parents(".row").next().find(".form-button").removeClass("disabled");
					$(this).parents(".row").next().next().find("input").removeAttr("disabled");
					$(this).parents(".row").next().next().find(".form-button").removeClass("disabled");
					if ($(".stepper-point:not('.active')").length == 0) {
						//ACTIVATE SUBMIT BUTTON HERE
						$("input[type=submit]").removeAttr("disabled");
					}
				}
			});

			$("#portfolio").on("change", function () {
				if ($(this).val() != "") {
					$(this).next().html($(this).val().split('\\').pop());
					// $(this).parents(".row").children(".stepper, .stepper-point").addClass("active");
					if ($(this).parents(".row").index() == $(".tkpd-form .row:not('.experience-row, .education-row')").length - 1) {
						//ACTIVATE SUBMIT BUTTON HERE
						$("input[type=submit]").removeAttr("disabled");
					}
				}
			});

			$(".removeresume").click(function (event) {
				// console.log($(this).parent());
				$(this).parent().prev().val("");
				$(this).parent().html("Choose Resume");
			});

			$(':checkbox.fade-next').change(function () {
				if (this.checked) {
					$(this).next().next().fadeIn();
					// if($(this).parent().find(".theChip__text").length != 0){
					//   $(this).parents(".row").children(".stepper, .stepper-point").addClass("active");
					// }
				} else {
					$(this).next().next().fadeOut();
					// if($(':checkbox:checked').length == 0){
					//   $(this).parents(".row").children(".stepper, .stepper-point").removeClass("active");
					// }
				}
			});

			$(':radio.fade-experience').change(function () {
				if (this.checked) {
					$(".experience.experience:not('.clone'), *[data-add-experience]").hide();
				} else {
					$(".experience:not('.clone'), *[data-add-experience]").show();
				}
			});

			$(':radio.fade-education').change(function () {
				if (this.checked) {
					$(".education.education:not('.clone'), *[data-add-education]").hide();
				} else {
					$(".education:not('.clone'), *[data-add-education]").show();
				}
			});

			//CHIPS CODE
			$('.theInput').outerWidth(385);
			$('.inputContainer').on('keyup', '.theInput', function (e) {
				if (e.keyCode == 32 || e.keyCode == 13) {
					if ($(this).val() && $(this).val().trim() != "") {
						var totalWidth = 0;
						var inputWidth = $('.theInput').outerWidth();
						var defaultInput = '<input type="text" class="theInput">';
						var val = $(this).val();
						var chip = '<span class="theChip"><span class="theChip__text">' + val + '</span><span class="theChip__delete fa fa-times"></span></span>';

						$(this).parent().append(chip);
						$(this).parent().append(defaultInput);
						$('.theInput').focus();
						$('.theInput').outerWidth(inputWidth - $(this).prev().outerWidth());
						if ($('.theInput').outerWidth() < 40) {
							$('.theInput').outerWidth(385);
						}
						// $(this).parents(".row").children(".stepper, .stepper-point").addClass("active");
						$(this).remove();

					}
				}
			});
			$('.inputContainer').on('click', '.theChip__delete', function () {
				if ($(".theChip__text").length == 1) {
					// $(this).parents(".row").children(".stepper, .stepper-point").removeClass("active");
				}
				var chipWidth = $(this).outerWidth();
				var inputWidth = $('.theInput').outerWidth();
				$('.theInput').outerWidth(inputWidth + chipWidth);
				if ($('.theInput').outerWidth() >= 385) {
					$('.theInput').outerWidth(385)
				}
				$('.theInput').outerWidth()
				$(this).parent().remove();
			})
			$('.inputContainer').on('keydown', '.theInput', function (e) {
				if (e.keyCode == 8) {
					if (!$(this).val()) {
						var prevVal = $(this).prev().text();
						e.preventDefault();
						$(this).val(prevVal);
						$(this).select();
						$(this).prev().remove();
						if ($(".theChip__text").length == 0) {
							// $(this).parents(".row").children(".stepper, .stepper-point").removeClass("active");
						}
					}
				}
			});

			//APPEND experience
			var j = 0;
			$("*[data-add-experience]").css("cursor", "pointer");
			$("*[data-add-experience]").click(function () {
				$(".experience.clone").clone().insertBefore($(this).prev()).removeClass("clone").find("label").remove();
				$(".experience:not(.clone)").find("input[type='checkbox'][id='" + $(".experience.clone").find("input[type='checkbox']").attr("id") + "']").attr("id", "currently-work-here-" + j);
				$('input[type="checkbox"][id="currently-work-here-' + j + '"]').each(function (i, obj) {
					$('<label for="' + $(this).attr('id') + '" class="tkpd-switch-wrap"><div class="tkpd-switch-track"><div class="tkpd-switch-thumb"></div></div></label>').insertAfter($(this));
				});
				$(".experience.clone").find("input[type='checkbox']").attr("id", "currently-work-here-" + j + 1);
				j++;
			});

			//DELETE experience
			$(document).on('click', '.experience-delete', function () {
				$(this).parent().remove();
			});

			//APPEND education
			$("*[data-add-education]").css("cursor", "pointer");
			$("*[data-add-education]").click(function () {
				$(".education.clone").clone().insertBefore($(this).prev()).removeClass("clone");
			});

			//DELETE education
			$(document).on('click', '.education-delete', function () {
				$(this).parent().remove();
			});

			//FORM JS
			$.extend($.expr[':'], {
				'containsi': function (elem, i, match, array) {
					return (elem.textContent || elem.innerText || '').toLowerCase()
						.indexOf((match[3] || "").toLowerCase()) >= 0;
				}
			});

			$("*[data-add-education]").hide();
			$("*[data-add-experience]").hide();

			$("input[type='radio'][name='radio-edu']").change(function () {
				if ($(this).val() == "yes") {
					$(".education.education:not('.clone'), *[data-add-education]").hide();
				} else {
					$(".education:not('.clone'), *[data-add-education]").show();

					if ($(".education:not('.clone')").length < 1) {
						$("*[data-add-education]").trigger("click");
					}
				}
			});

			$("input[type='radio'][name='radio-exp']").change(function () {
				if ($(this).val() == "yes") {
					$(".experience.experience:not('.clone'), *[data-add-experience]").hide();
				} else {
					$(".experience:not('.clone'), *[data-add-experience]").show();

					if ($(".experience:not('.clone')").length < 1) {
						$("*[data-add-experience]").trigger("click");
					}
				}
			});

			$('.tkpd-input .prepend').prev().css("padding-left", function () {
				// console.log($(this).next().width());
				return $(this).next().width() + 20 + "px";
			});

			$('.tkpd-input.select input[readonly="true"]').click(function () {
				$(this).next().css("display", "block");
				$(this).next().css("opacity", "1");
			});

			$('.tkpd-input.select input').on('input', function () {
				$(this).next().css("display", "block");
				$(this).next().css("opacity", "1");
				$(this).next().children('.option').css('display', 'none');
				$(this).next().children('.option:containsi(' + $(this).val() + ')').css('display', 'block');
			});

			$('.tkpd-input.select .icon').click(function () {
				$(this).next().next().next().css("display", "block");
				$(this).next().next().next().css("opacity", "1");
			});

			$('.option').click(function () {
				var value = $(this).text();
				// console.log(value);
				$(this).parent().prev().val(value.trim());
				$(this).parent().css("display", "none");
				$(this).parent().css("opacity", "0");
			});

			$(document).mouseup(function (e) {
				var container = $(".tkpd-input.select .select");

				// if the target of the click isn't the container nor a descendant of the container
				if (!container.is(e.target) && container.has(e.target).length === 0) {
					container.css("display", "none");
					container.css("opacity", "0");
				}
			});

			$('input[type="checkbox"]').each(function (i, obj) {
				$('<label for="' + $(this).attr('id') + '" class="tkpd-switch-wrap"><div class="tkpd-switch-track"><div class="tkpd-switch-thumb"></div></div></label>').insertAfter($(this));
			});

			$('input[type="radio"]').each(function (i, obj) {
				$('<label for="' + $(this).attr('id') + '" class="tkpd-switch-wrap-radio"><div class="tkpd-switch-track-radio"><div class="tkpd-switch-thumb-radio"></div></div></label>').insertAfter($(this));
			});


			var postArr;


			//KEMALS PROGRAMMING
			var base64Photo, base64Resume, resumeName, photoName;
			var maxSizePhoto = 5242880, maxSizeResume = 5242880;
			var educationIndex = -1, experienceIndex = -1;
			File.prototype.convertToBase64 = function (callback) {
				var reader = new FileReader();
				reader.onload = function (e) {
					callback(e.target.result)
				};
				reader.onerror = function (e) {
					callback(null);
				};
				reader.readAsDataURL(this);
			};

			function humanFileBase64Size(bytes, si) {
				// bytes = 4*Math.ceil((bytes/3));
				bytes = Math.ceil(bytes * 0.75);
				var thresh = si ? 1000 : 1024;
				if (Math.abs(bytes) < thresh) {
					return bytes + ' B';
				}
				var units = si
					? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
					: ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
				var u = -1;
				do {
					bytes /= thresh;
					++u;
				} while (Math.abs(bytes) >= thresh && u < units.length - 1);
				return bytes.toFixed(1) + ' ' + units[u];
			}


			$("#resume").on('change', function () {
				try {

					resumeName = $("#resume").val().replace(/^.*[\\\/]/, '');
					var selectedFile = this.files[0];
					selectedFile.convertToBase64(function (base64) {
						base64Resume = base64.toString().split(',')[1];
						$("*[data-file='resume']").text($("#resume").val().replace(/^.*[\\\/]/, '')
							+ " (" + humanFileBase64Size(base64Resume.length, true) + ") ");
						if (base64Resume.length > 5242880) {
							$("#resume").attr({
								"data-tooltip": "Max size exceeded",
								"data-tooltip-position": "top"
							});
							$('#resume').trigger("click");
							$("input[type=submit]").attr("disabled", "true");
							$("#resume").parent().parent().prevAll(".stepper, .stepper-point").removeClass("active");
						} else {
							$(".tkpd-tooltip").remove();
						}
					});

				} catch (err) {
					// when user clicks cancel ...
					// console.warn(err);
				}

			});

			$("#avatar").on('change', function () {
				try {

					var selectedFile = this.files[0];
					selectedFile.convertToBase64(function (base64) {
						base64Photo = base64.toString().split(',')[1];
						$("#avatar").next().text($("#avatar").val().replace(/^.*[\\\/]/, '')
							+ " (" + humanFileBase64Size(base64Photo.length, true) + ") ");

						if (base64Photo.length > 5242880) {
							$("#avatar").attr({
								"data-tooltip": "Max size exceeded",
								"data-tooltip-position": "top"
							});
							$('#avatar').trigger("click");
							$("input[type=submit]").attr("disabled", "true");
							$("#avatar").parent().parent().prevAll(".stepper, .stepper-point").removeClass("active");
						} else {
							$(".tkpd-tooltip").remove();
						}
					})
				} catch (err) {
					// when user clicks cancel ...
					// console.warn(err);
				}
			});


			//TOOLTIP TKPD JS
			$(document).on('click', '*[data-tooltip]', function () {
				var pos = $(this).position();
				var width = $(this).outerWidth();
				var position = "top";
				if ($(this).attr('data-tooltip-position')) {
					position = $(this).attr('data-tooltip-position');
				}
				var new_tooltip = $('<div class="tkpd-tooltip" data-tooltip-position=' + position + '>' + $(this).attr('data-tooltip') + '</div>').insertAfter($(this));

				//if tooltip has more width then the object
				if (new_tooltip.innerWidth() > $(this).innerWidth()) {
					var left = -(new_tooltip.innerWidth() - $(this).innerWidth()) / 2;
					var top = -new_tooltip.innerHeight() - 10;
					if (position == "left") {
						left = -new_tooltip.innerWidth() - 10;
						if (new_tooltip.innerHeight() < $(this).innerHeight()) {
							top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
						}
						if (new_tooltip.innerHeight() > $(this).innerHeight()) {
							top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
						}
					}
					if (position == "bottom") {
						top = new_tooltip.innerHeight() + 20;
					}
					if (position == "right") {
						left = $(this).innerWidth() + 10;
						if (new_tooltip.innerHeight() < $(this).innerHeight()) {
							top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
						}
						if (new_tooltip.innerHeight() > $(this).innerHeight()) {
							top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
						}
					}
				}
				if (new_tooltip.innerWidth() < $(this).innerWidth()) {
					var left = ($(this).innerWidth() - new_tooltip.innerWidth()) / 2;
				}
				// console.log(pos);
				new_tooltip.css({
					top: 0 + "px",
					left: pos.left + left + 1 + "px"
				});
			});


			//POST HANDLING
			$("input[type='submit']").click(function (event) {
				var domSubmit = $(this);
				$('<svg class="spinner" width="35px" height="35px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg>').insertAfter(domSubmit);
				domSubmit.hide();
				event.preventDefault();

				if ($("textarea[name='c-summary']").not(".experience-row textarea").length) {
					var summary = $("textarea[name='c-summary']").not(".experience-row textarea").val();
				}

				if ($(".education:not('.clone'):visible").length) {
					var education_entries = [];
					for (var i = 0; i < $(".education:not('.clone')").length; i++) {
						education_entries.push({
							school: $(".education:not('.clone')").eq(i).find("*[name='c-school']").val(),
							field_of_study: $(".education:not('.clone')").eq(i).find("*[name='c-study']").val(),
							degree: $(".education:not('.clone')").eq(i).find("*[name='c-degree']").val(),
							start_date: $(".education:not('.clone')").eq(i).find("*[name='c-from']").val(),
							end_date: $(".education:not('.clone')").eq(i).find("*[name='c-to']").val(),
						});
					}
				}

				if ($(".experience:not('.clone'):visible").length) {
					var experience_entries = [];
					for (var i = 0; i < $(".experience:not('.clone')").length; i++) {
						var curr = false;
						if ($(".experience:not('.clone')").eq(i).find("*[name='c-cwh']").is(":checked")) {
							curr = true;
						}
						experience_entries.push({
							title: $(".experience:not('.clone')").eq(i).find("*[name='c-position']").val(),
							summary: $(".experience:not('.clone')").eq(i).find("*[name='c-summary']").val(),
							start_date: $(".experience:not('.clone')").eq(i).find("*[name='c-from']").val(),
							end_date: $(".experience:not('.clone')").eq(i).find("*[name='c-to']").val(),
							current: curr,
							company: $(".experience:not('.clone')").eq(i).find("*[name='c-company']").val(),
							industry: $(".experience:not('.clone')").eq(i).find("*[name='c-industry']").val(),
						});
					}
				}

				if($("textarea[name='answer']").length){
				  var answers = [];
				  $("textarea[name='answer']").each(function(){
					answers.push({
					  question_key: $(this).attr("data-key"),
					  body: $(this).val()
					});
				  });
				}

				if($("input[name^='ans-radio']").length){
					var len = $('input[name^="ans-radio"]:checked').length;
					var answers = [];
					for( var i = 0; i < len; i ++ ) {
						answers.push({
						question_key: $('input[name^="ans-radio"]:checked').eq(i).attr("data-key"),
						checked: $('input[name^="ans-radio"]:checked').eq(i).val(),
						});
					}
					console.log(answers);
				}

				if ($("#resume").length) {
					var resume = {
						name: resumeName,
						data: base64Resume
					};
				}

				if ($("#avatar").length) {
					var avatar = {
						name: photoName,
						data: base64Photo
					};
				}


				postArr = {
					candidate: {
						name: $("input[name='c-firstname']").val() + " " + $("input[name='c-lastname']").val(),
						firstname: $("input[name='c-firstname']").val(),
						lastname: $("input[name='c-lastname']").val(),
						email: $("input[name='c-email']").val(),
						phone: $("input[name='c-phone']").val(),
						summary,
						education_entries,
						experience_entries,
						resume,
						answers,
						avatar
					}
				};
				console.log(answers);
				console.log(postArr);

				$.ajax({
					type: "POST",
					url: "<?php echo get_bloginfo( 'url' ) . '/submitform'; ?>",
					data: {
						dataform: postArr,
						sc: "<?php echo $job['shortcode']; ?>",
						nonce: "<?php echo wp_create_nonce( 'ko6W==S7iIp3-Nja.aTkPx@0t90' ); ?>"
					},
					success: function (response) {
						console.log(response);

						var response = JSON.parse(response);
						if (response.status == "fail") {
							$("html, body").animate({scrollTop: $("*[name='c-" + response.form + "']").offset().top - 120}, "slow");
							$('*[style*="border: 1px solid rgb(239, 67, 12)"],*[style*="border:1px solid rgb(239, 67, 12)"]').attr("style", "");
							$('*[data-tooltip]').removeAttr("data-tooltip");
							$(".tkpd-tooltip").remove();
							domSubmit.show();
							$("svg.spinner").remove();
							$("*[name='c-" + response.form + "']").css("border", "1px solid rgb(239, 67, 12)");
							$("*[name='c-" + response.form + "']").attr({
								"data-tooltip": response.message,
								"data-tooltip-position": "top"
							});
							$('*[data-tooltip]').trigger("click");
						} else {
							var msg = response.message;
							var objMsg = JSON.parse(msg);
							if (objMsg.status) {
								window.location.href = "<?php echo get_bloginfo( 'url' ) . '/success'; ?>";
							}
							else {
								window.location.href = "<?php echo get_bloginfo( 'url' ) . '/error?job_ref=' . base64_encode( $slug ) . '&err='; ?>" + window.btoa(objMsg.error) + "<?php echo '&app_url=' . base64_encode( $job['application_url'] ); ?>";
							}

						}
					}


				});

			});


		});
	})(jQuery);

</script>
