<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

/*
 * Validate Page if not exist
 * @action: Redirect to homepage
 * @author : @riobahtiar
 * */

?>

<!doctype html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebSite" prefix="og: http://ogp.me/ns#">

<?php

$slug   = sanitize_title( get_query_var( 'slug' ) );
$career = new Careers\workable;
$jobl   = $career->get_jobs();
$job    = $career->get_job_slug( $slug );

do_action( 'get_header' );

if ( ! isset( $job['department'] ) ) {
	wp_head();

	$wp_query->set_404();
	status_header( '404' );
	get_template_part( '404-job-not-found' );
} else {
	$faJobs = $career->get_job_application_form( $slug );
	$json   = $faJobs;
	$arr    = json_decode( $json );

	// Passing data to Header
	$the_title       = 'Info Lowongan Pekerjaan ' . $job['title'] . ' | Tokopedia Karir ';
	$the_description = limit_text( wp_strip_all_tags( $job['description'] ), 160 );
	$the_slug        = 'join/' . $slug . '/';
		include_once 'templates/head.php';
	?>
	<body <?php body_class(); ?>>
		<?php include_once 'templates/header.php'; ?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TZNNXTG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!--[if IE]>
	<div class="alert alert-warning">
		<?php
		_e(
			'You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
	    browser</a> to improve your experience.', 'sage'
		);
		?>
	</div>
	<![endif]-->
	<?php
	require Wrapper\template_path();

}

do_action( 'get_footer' );
get_template_part( 'templates/footer' );
wp_footer();
?>
</body>
</html>
