<?php

if ( isset( $_SERVER['HTTP_REFERER'] ) ) {
	if ( ! strpos( $_SERVER['HTTP_REFERER'], 'tokopedia.com/careers' ) !== false ) {
		?>
<script>
window.location.replace("<?php echo home_url(); ?>");
</script>
		<?php
	}
} else {
	?>
<script>
window.location.replace("<?php echo home_url(); ?>");
</script>
	<?php
}
?>
<style>
	.ct-success {
		display: table;
		width: 100%;
		height: 100%;
		vertical-align: middle;
		text-align: center;
	}

	.ct-cell {
		display: table-cell;
		vertical-align: middle;
	}

	.success__thanks {
		padding: 20px 0;
		font-size: 20px;
		font-weight: 600;
		color: rgba(0, 0, 0, 0.7);

	}

	.success__notify {
		font-size: 14px;
		color: rgba(0, 0, 0, 0.54);
	}

	.success__notify--bold {
		font-weight: 700;
	}

	.success-copy {
		margin-bottom: 40px;
	}


</style>

<div class="container ct-success">
	<div class="ct-cell">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-success.png" alt="">
		<h2 class="success__thanks">Thank you for applying at Tokopedia!</h2>

		<div class="success-copy">
			<h3 class="success__notify">
				We will notify you within
				<strong class="success__notify--bold">two weeks</strong>
				if you
				<strong class="success__notify--bold">pass</strong> the screening process.
			</h3>
			<h3 class="success__notify">Good luck!</h3>
		</div>
		<div class="text-center">
			<a href="../jobs" class="btn-tkpd btn--medium btn--green" style="width: 200px;">Browse other jobs</a>
		</div>
	</div>
</div>
