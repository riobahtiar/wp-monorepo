# [Tokopedia Careers ](https://tokopedia.com/careers)

Tokopedia Careers is a WordPress starter theme based on HTML5 Boilerplate, gulp, Bower, and Bootstrap Sass, that will help you make better themes developement. This theme forked from [Sage Theme Starter v8.5.0](https://github.com/roots/sage/tree/8.5.0). For Better bugs tracking you can open new issue  [https://github.com/microsite-careers/issues](https://github.com/tokopedia/microsite-careers/issues)

# Push live & Deployment
* Theme build only run twice a day 11.00 AM and 04.00 PM to staging.tokopedia.com/careers
* Push Live or Theme Build
⋅⋅* Staging: Everyday.
⋅⋅* Production: Everyday except Friday and must accepted by QA.

---
## Team
If file not exist please create, and linked to parent file.

#### Rio
* BE - Workable & Routing scoops

#### Radit
* Homepage - PHP Template: `front-page.php`
* Homepage - SCSS Template: `assets/styles/layouts/_front-page.scss`
* Homepage - JS Object Variable -> home : `assets/scripts/main.js`

#### Afif
* Careers Page - PHP Template: `template-careers.php`
* Careers Item - PHP Template: `template-careers-item.php`
* Careers Form - PHP Template: `template-careers-form.php`
* Careers Page - SCSS Template: `assets/styles/_careers.scss`
* Careers - JS Object Variable -> careers_page : `assets/scripts/main.js`

#### Steven
* Nakama Page - PHP Template: `template-nakama.php`
* Nakama Page - PHP Template: `template-about.php`
* Nakama Page - SCSS Template: `assets/styles/_team.scss`
* Nakama Page - SCSS Template: `assets/styles/_about.scss`
* Nakama Page - JS Object Variable -> nakama_page : `assets/scripts/main.js`

---
## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| PHP >= 5.4.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php) |
| Node.js >= 4.5  | `node -v`    | [nodejs.org](http://nodejs.org/) |
| gulp >= 3.8.10  | `gulp -v`    | `npm install -g gulp` |
| Bower >= 1.3.12 | `bower -v`   | `npm install -g bower` |

For more installation notes, refer to the [Install gulp and Bower](#install-gulp-and-bower) section in this document.
---
## JavaScript
### DOM-based Routing
* Based on http://goo.gl/EUTi53 by Paul Irish
* Only fires on body classes that match. If a body class contains a dash,
* replace the dash with an underscore when adding it to the object below.

### `.noConflict()`
* The routing is enclosed within an anonymous function so that you can
* always reference jQuery with $, even when in .noConflict() mode.
---
## PHP Code Style (Recomendation Style)
* PSR-2 [Read About PSR-2](http://www.php-fig.org/psr/psr-2/)

Recomended Editor `Atom` or `Visual Studio Code`, tested add-ons with support to PSR-2
* PHP-fmt https://atom.io/packages/php-fmt
* PHP Formatter https://marketplace.visualstudio.com/items?itemName=Sophisticode.php-formatter

---
## Theme installation

Install This theme using git.

```shell
# @ careers.dev/site/web/app/themes/
$ git clone git@github.com:tokopedia/microsite-careers.git theme-name
```
---
## Theme setup

Edit `lib/setup.php` to enable or disable theme features, setup navigation menus, post thumbnail sizes, post formats, and sidebars.

## Theme development

Sage uses [gulp](http://gulpjs.com/) as its build system and [Bower](http://bower.io/) to manage front-end packages.

### Install gulp and Bower

Building the theme requires [node.js](http://nodejs.org/download/). We recommend you update to the latest version of npm: `npm install -g npm@latest`.

From the command line:

1. Install [gulp](http://gulpjs.com) and [Bower](http://bower.io/) globally with `npm install -g gulp bower`
2. Navigate to the theme directory, then run `npm install`
3. Run `bower install`
4. Don't forget to remove `.git` folder. For new project

Yeyyy, ou now have all the necessary dependencies to run the build process.

### Available gulp commands

* `gulp` — Compile and optimize the files in your assets directory
* `gulp watch` — Compile assets when file changes are made
* `gulp --production` — Compile assets for production (no source maps).

### Using BrowserSync

To use BrowserSync during `gulp watch` you need to update `devUrl` at the bottom of `assets/manifest.json` to reflect your local development hostname.

For example, if your local development URL is `http://careers.dev` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://careers.dev"
  }
...
```
If your local development URL looks like `http://localhost:8888/project-name/` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://localhost:8888/project-name/"
  }
...
```


## Change Log

### 1.0: 21 Ags 2017
* First time release
