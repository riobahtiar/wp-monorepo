<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

 /*
  * Page Setter
  * */

 /*
  * Validate Page if not exist
  * @action: Redirect to homepage
  * @author : Rhoma Cahyanti
  * */

?>
  <!doctype html>
  <html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebSite" prefix="og: http://ogp.me/ns#">

	<?php
	$slug = sanitize_title( get_query_var( 'slug' ) );
	$wws  = new WorkdayAPI\workday;
	// $jobl = $wws->get_jobs();
	$job  = $wws->get_job_slug( $slug );

	do_action( 'get_header' );
	?>

	<?php if ( $job == null ) { ?>

<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.tokopedia.com\/careers\/","name":"Tokopedia Careers","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.tokopedia.com\/blog\/jobs\/?query={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"Organization","url":"https:\/\/www.tokopedia.com\/careers\/","sameAs":["https:\/\/www.facebook.com\/tokopedia\/","https:\/\/www.instagram.com\/tokopedia\/","https:\/\/www.linkedin.com\/company\/pt--tokopedia","https:\/\/www.youtube.com\/user\/tokopedia","https:\/\/twitter.com\/tokopedia"],"@id":"#organization","name":"PT. Tokopedia","logo":"https:\/\/ecs7.tokopedia.net\/blog-tokopedia-com\/uploads\/2015\/08\/tokopedia.png"}</script>

		<?php
		wp_head();

		global $wp_query;

		$wp_query->set_404();
		status_header( '404' );
		get_template_part( '404-job-not-found' );

} else {

	// $job_item = $wws->get_job_item( $job['shortcode'] );
	
	// $exp = $job_item->experience;

	// if ( $exp == "" ) {
	//    $exp = "-";
	// }

	// Passing data to Header
	$the_title       = $job['title'] . ' Job | ' . get_bloginfo( 'name' );
	$the_description = limit_text( wp_strip_all_tags( $job['description'] ), 160 );
	$the_slug        = 'job/' . $slug . '/';
	$the_shortcode   = $job['shortcode'];
	include_once 'templates/head.php';
	?>
<body <?php body_class(); ?>>
	<?php include_once 'templates/header.php'; ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TN8CNF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--[if IE]>
<div class="alert alert-warning">
	<?php _e( 'You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage' ); ?>
</div>
<![endif]-->
	<?php

	require Wrapper\template_path();

	?>

<?php } ?>
<?php
do_action( 'get_footer' );
get_template_part( 'templates/footer' );
wp_footer();
?>
</body>
</html>
