<?php

	// Fetch Jobs Data
	$workable      = new Careers\workable;
	$abcde         = $workable->get_jobs();
	$raw           = $workable->get_grouped_jobs( true );
	$the_shortcode = '';

	// $wws  = new WorkdayAPI\workday;
	// $raw  = $wws->get_grouped_jobs( true );
	// $the_shortcode = '';

	// Send Jobs Data to Javascript
	wp_localize_script( 'sage/js', 'jobsData', json_encode( $raw ) );
?>

<!-- <img src="https://ecs7.tokopedia.net/assets/images/careers/acc-topleft-fp.svg" alt="" class="acc-tl"> -->

<!-- <section class="section-banner">
	<div class="banner">
		<div class="container">
			<div class="banner-image">
				<div class="banner-image-group">
					<img class="banner-image-group__tree" src="https://ecs7.tokopedia.net/assets/images/careers/ic-tree.png" alt="">
					<img class="banner-image-group__owl" src="https://ecs7.tokopedia.net/assets/images/careers/ic-banner--new.png" alt="">
				</div>
			</div>
			<div class="banner-copy">
				<div class="banner-copy-header">
					<h2 class="banner-copy-header__title txt--black txt--primary">
						About Tokopedia
					</h2>
					<div class="banner-copy-header__line"></div>
				</div>
				<p class="banner-copy__desc txt--black txt--disabled">
					Tokopedia menyediakan beragam pembayaran produk digital seperti,  
					beli pulsa, beli paket data,bayar PLN, 
					hingga bayar BPJS. Jadi, tunggu apalagi? 
					Segera lakukan pembayaran digital hanya lewat Tokopedia!
				</p>
			</div>
		</div>
		<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/careers-mainbg-new.png" alt="" class="banner-mainbg">
	</div>
</section> -->

<section class="section-nbanner">
    <!-- <img class="section-nbanner__accr" src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/ca-ll-new.png" alt="">
    <img class="section-nbanner__accl" src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/ca-lr1.png" alt=""> -->
    <div class="nbanner">
        <div class="container">
            <div class="nbanner-header">
                <h1 class="nbanner-header__title">
                    Find Your Dream Job in Tokopedia!
                </h1>
                <h3 class="nbanner-header__copy">
                    Join Our Merry Band of Nakama Sailing the Seven Seas! 
                </h3>
            </div>

            <!-- SEARCH -->
            <section id="msrc" class="section_mobile-search">
                <div class="m-searchbar clearfix">
                    <div class="m-searchbar-arrow">
                        <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-back-mobile.png" alt="" class="m-searchbar-arrow__arrow">
                    </div>
                    <div class="m-searchbar-lens">
                        <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-search-mobile.png" alt="" class="m-searchbar-lens__lens">
                    </div>
                    <!-- <form id="search-job" action="#" method="GET"> -->
                    <input name="query" type="search" class="search-bar__thebar search-bar__thebar--m" placeholder="Type your dream jobs...">
                    <!-- </form> -->
                </div>
                <ul class="search-bar-thebar-ul search-bar-thebar-ul--m">
                    <p class="m-searchcopy__copy">Search Suggestions</p>
                    <!-- populated via JS -->
                </ul>
            </section>

            <section class="section_mobile-location clearfix">
                <div class="m-location clearfix">
                    <div class="m-location-copy">
                        <h2 class="m-location-copy__copy">
                            Choose your prefered place
                        </h2>
                    </div>
                    <ul class="search-bar-theloc-ul--m">
                        <li class="search-bar-theloc__li">
                            <div class="search-bar-theloc__liwrapper">
                                <span class="search-bar-theloc__liloc">All Locations</span>
                                <div class="pull-right">
                                </div>
                            </div>
                        </li>
                        <!-- populated via JS -->
                    </ul>
                </div>
            </section>

            <form action="<?php echo home_url(); ?>/jobs" method="get">
                <section class="section_search">
                    <div class="search-wrap">
                        <div class="container">

                            <!-- Search Bar -->
                            <div class="search-bar clearfix">

                                <div class="col-search-60">
                                    <div class="search-bar-wrapbar clearfix">
                                        <div class="search-bar-thebar clearfix">
                                            <!-- <div class="search-bar-thebar-image">
                                                <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-lup.svg" alt="Search Jobs" class="search-bar__icon">
                                                <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-jobsnew-scroll.svg" alt="Search Jobs" class="search-bar__icon--scroll">
                                            </div> -->
                                            <!-- <div class="search-bar-thebar-group">
                                                <label for="query" class="search-bar__label txt--black txt--primary fs--12 fw--smb">I want to join as</label>
                                            </div> -->
                                            <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-lup.svg" alt="Search Jobs" class="search-bar__icon">
                                            <input name="query" type="text" class="search-bar__thebar" placeholder="Type your dream jobs" autocomplete="off"></input>
                                            <ul class="search-bar-thebar-ul">
                                                <!-- populated via JS -->
                                            </ul>
                                        </div>

                                        <div class="search-bar-theloc">
                                            <!-- <div class="search-bar-thebar-image">
                                                <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-location-on.svg" alt="Search Jobs Location" class="search-bar__icon">
                                                <img src="https://ecs7.tokopedia.net/assets/images/careers/icon-lokasi-scroll.svg" alt="Search Jobs Location" class="search-bar__icon--scroll">
                                            </div> -->
                                            <!-- <div class="search-bar-thebar-group">
                                                <label for="country" class="search-bar__label txt--black txt--primary fs--12 fw--smb">Location</label>
                                                <input type="text" class="search-bar__theloc" placeholder="All Locations" readonly="true" value=""></input>
                                                <input name="country" type="hidden" class="search-bar__theloc--true" placeholder="All Locations" readonly="true" value=""></input>
                                            </div> -->

                                            <img src="https://ecs7.tokopedia.net/assets/images/careers/sen-percent.svg" alt="Search Jobs Location" class="search-bar__icon">
                                            <input type="text" class="search-bar__theloc" placeholder="All Locations" readonly="true" value=""></input>
                                            <input name="country" type="hidden" class="search-bar__theloc--true" placeholder="All Locations" readonly="true" value=""></input>

                                            <!-- <i class="fa fa-angle-down search-bar__thelocarrow" aria-hidden="true"></i> -->
                                            <img class="search-bar__thelocarrow" src="https://ecs7.tokopedia.net/assets/images/careers/arrowdown-careers.svg" alt="">
                                            <ul class="search-bar-theloc-ul">
                                                <li class="search-bar-theloc__li">
                                                    <div class="search-bar-theloc__liwrapper">
                                                        <span class="search-bar-theloc__liloc">All Locations</span>
                                                        <div class="pull-right">

                                                        </div>
                                                    </div>
                                                </li>
                    
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-search-40">
                                    <div class="search-or text-center fw--600">OR</div>
                                    <div class="search-or-line"></div>
                                    <div class="buttons-wrap clearfix">
                                        <button type="submit" class="btn-search unf-btn unf-btn--transaction unf-btn--small ripple-effect ripple-main">
                                            Search<div class="text">Search</div>
                                        </button>
                                        <a href="../jobs">
                                            <button class="btn-view unf-btn unf-btn--small ripple-effect ripple-main">
                                                View All Jobs<div class="text">View All Jobs</div>
                                            </button>
                                        </a>
                                        <!-- <div class="col-search-btn">
                                        </div> -->
                                        <!-- <div class="col-view-btn">
                                        </div> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </form>
            <!-- /SEARCH -->

            <div class="nbanner-copy">
                <div class="nbanner-copy-header">
                    <h2 class="nbanner-copy-header__title txt--black txt--primary">
                        About Tokopedia
                    </h2>
                    <div class="nbanner-copy-header__line"></div>
                </div>
                <p class="nbanner-copy__desc txt--black txt--disabled">
                    Tokopedia is an Indonesian technology company with a mission 
                    to democratize commerce through technology. We are the leading marketplace 
                    in Indonesia; we encourage millions of merchants and consumers to participate
                     in the future of commerce. Our vision is to build an ecosystem where everyone 
                     can start and discover anything with ease.
                </p>
            </div>

            <div class="nbanner-image">
                <img class="nbanner-image__img" src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/nbanner-hero-new.png" alt="">
                <!-- <div class="nbanner-image-group">
                    <img class="banner-image-group__tree" src="https://ecs7.tokopedia.net/assets/images/careers/ic-tree.png" alt="">
                    <img class="banner-image-group__owl" src="https://ecs7.tokopedia.net/assets/images/careers/ic-banner--new.png" alt="">
                </div> -->
            </div>
        </div>
        <img class="nbanner__mainbg" src="https://ecs7.tokopedia.net/assets/images/careers/ca-fp-mt.svg" alt="">
    </div>
</section>

<section class="section_thestory">
    <img src="https://ecs7.tokopedia.net/assets/images/careers/acc-middleleft-fp.svg" alt="" class="acc-ml">
    <img src="https://ecs7.tokopedia.net/assets/images/careers/acc-mr.svg" alt="" class="acc-mr">

    <!-- Story Header -->
    <div class="container">
        <div class="thestory-header wow fadeIn">
            <h2 class="thestory-header__bgtitle txt--black txt--primary fw--smb">
                What’s it like to work at Tokopedia?
            </h3>
            <div class="thestory-header__line"></div>
        </div>
    </div>

    <!-- Story Body Mobile -->
    <div class="container thestory-body-container">
        <div class="thestory-body">
            <!-- Ship -->
            <div class="row row--thestory">
                <div class="col-sm-5 col-xs-12">
                    <div class="thestory-body-imagebox thestory-body-imagebox--ship wow fadeInLeft">
                        <img src="https://ecs7.tokopedia.net/assets/images/careers/Kapal--new1.svg" alt=""
                            class="img-responsive thestory-body-imagebox__image"
                        >
                    </div>
                </div>
                <div class="col-sm-7 col-xs-12">
                    <div class="thestory-body-copybox wow fadeInRight">
                        <h2 class="thestory-body-copybox__title txt--black txt--primary fw--smb">
                            Tackle Every Hurdle
                        </h2>
                        <h3 class="thestory-body-copybox__content txt--black txt--disabled">
                            Much like Noah's Ark, the entirety of Tokopedia can be described as several different ships combined into one.
                            In the ocean of opportunities, it's not uncommon for ships to run into obstacles like storms and icebergs,
                            just ask Titanic. At Tokopedia, we believe that inspiration means different things to different people,
                            but the crew's hard work will always keep the ship moving.
                        </h3>
                    </div>
                </div>
            </div>

            <!-- Compass -->
            <div class="row row--thestory text-right">
                <div class="col-sm-5 col-xs-12 col-pr">
                    <div class="thestory-body-imagebox thestory-body-imagebox--compass wow fadeInRight">
                        <img src="https://ecs7.tokopedia.net/assets/images/careers/Jam--new.svg" alt=""
                            class="img-responsive thestory-body-imagebox__image"
                        >
                    </div>
                </div>
                <div class="col-sm-7 col-xs-12">
                    <div class="thestory-body-copybox wow fadeInLeft">
                        <h2 class="thestory-body-copybox__title txt--black txt--primary fw--smb">
                            Three Pointers, Same Direction
                        </h2>
                        <h3 class="thestory-body-copybox__content txt--black txt--disabled">
                            <span class="fw--smb">Focus on consumer</span>, <span class="fw--smb">growth mindset</span>, and the idea of
                            <span class="fw--smb">make-it-happen-and-make-it-better</span>
                            serve as the three directions that our rhetorical compass points towards.
                            We set our sights and keep our eyes on the prize.
                        </h3>
                    </div>
                </div>
            </div>

            <!-- Maps -->
            <div class="row row--thestory">
                <div class="col-sm-5 col-xs-12">
                    <div class="thestory-body-imagebox thestory-body-imagebox--map wow fadeInLeft">
                        <img src="https://ecs7.tokopedia.net/assets/images/careers/Map--new.svg" alt=""
                            class="img-responsive thestory-body-imagebox__image"
                        >
                    </div>
                </div>
                <div class="col-sm-7 col-xs-12">
                    <div class="thestory-body-copybox wow fadeInRight">
                        <h2 class="thestory-body-copybox__title txt--black txt--primary fw--smb">
                            Where We're Heading
                        </h2>
                        <h3 class="thestory-body-copybox__content txt--black txt--disabled">
                            Having the courage to sail through uncharted waters is no good without a map.
                            We use our mission to democratize commerce through technology to navigate our way around until we reach that goal.
                        </h3>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>

<div class="last-two-wrapper">
    <!-- <div class="last-two-wrapper-bg"></div> -->
    <section class="section_about-drive">
        <img src="https://ecs7.tokopedia.net/assets/images/careers/acc-bottomleft-fp.svg" alt="" class="acc-bl">
        <!-- Drive Header -->
        <div class="container">
            <div class="thestory-header thestory-header--drive wow fadeIn">
                <h2 class="thestory-header__bgtitle txt--black txt--primary fw--smb">
                    The Three Embodiments
                </h3>
                <div class="thestory-header__line"></div>
                <h2 class="about-drive__copy txt--black txt--disabled">
                    We are holding to these three principles that drive us forward.
                    We call them our DNA, the traits that<br/> embody who we are as Tokopedia Nakama.
                </h2>
            </div>
        </div>

        <div class="container hidden-xs">
            <div class="wwd-innerbox-wrap--drive clearfix">

                <div class="wwd-box wow fadeInUp" data-wow-delay="0.25s">
                    <div class="wwd-box-image text-center">
                        <img src="https://ecs7.tokopedia.net/assets/images/careers/focus-on-consumer.svg" class="wwd-box-image__image">
                    </div>
                    <h3 class="wwd-box__caption--drive text-center txt--black txt--primary">Focus On Consumer</h3>
                </div>

                <div class="wwd-box wow fadeInUp" data-wow-delay="0.5s">
                    <div class="wwd-box-image text-center">
                        <img src="https://ecs7.tokopedia.net/assets/images/careers/growth-mindset.svg" class="wwd-box-image__image">
                    </div>
                    <h3 class="wwd-box__caption--drive text-center txt--black txt--primary">Growth Mindset</h3>
                </div>

                <div class="wwd-box wow fadeInUp" data-wow-delay="0.75s">
                    <div class="wwd-box-image text-center">
                        <img src="https://ecs7.tokopedia.net/assets/images/careers/make-it-happen.svg" class="wwd-box-image__image">
                    </div>
                    <h3 class="wwd-box__caption--drive text-center txt--black txt--primary">Make it Happen <p>Make it Better</p></h3>
                </div>

            </div>
        </div>

        <!-- Mobile Slider -->
        <div class="wwd-innerbox-wrap--drive wwd-innerbox-wrap-mobile visible-xs">

            <div class="wwd-box">
                <div class="wwd-box-image text-center">
                    <img src="https://ecs7.tokopedia.net/assets/images/careers/focus-on-consumer.svg" class="wwd-box-image__image">
                </div>
                <h3 class="wwd-box__caption--drive text-center txt--black txt--primary">Focus On Consumer</h3>
            </div>

            <div class="wwd-box">
                <div class="wwd-box-image text-center">
                    <img src="https://ecs7.tokopedia.net/assets/images/careers/growth-mindset.svg" class="wwd-box-image__image">
                </div>
                <h3 class="wwd-box__caption--drive text-center txt--black txt--primary">Growth Mindset</h3>
            </div>

            <div class="wwd-box">
                <div class="wwd-box-image text-center">
                    <img src="https://ecs7.tokopedia.net/assets/images/careers/make-it-happen.svg" class="wwd-box-image__image">
                </div>
                <h3 class="wwd-box__caption--drive text-center txt--black txt--primary">Make it Happen <p>Make it Better</p></h3>
            </div>

        </div>

    </section>

    <section class="section-recruit">
        <div class="container">
            <div class="recruit-video">
                <div class="recruit-video-img" data-video="https://ecs7.tokopedia.net/assets/media/careers/recruitment-process.mp4">
                    <img class="recruit-video-img__img" src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/recruitment-video15.png" alt="">
                    <img class="recruit-video-img__play" src="https://ecs7.tokopedia.net/assets/images/careers/ic-play--green.svg" alt="" ">
                </div>
            </div>
            <div class="recruit-copy">
                <div class="recruit-copy-header">
                    <h2 class="recruit-copy-header__title txt--black txt--primary">
                        Recruitment Process
                    </h2>
                    <div class="recruit-copy-header__line"></div>
                </div>
                <p class="recruit-copy__desc txt--black txt--disabled">
                    Focus on consumer, growth mindset, and the idea of make-it-happen-and-make-it-better 
                    serve as the three directions that our rhetorical compass points towards. 
                    We set our sights and keep our eyes on the prize.
                </p>
            </div>
        </div>
        <!-- <img class="section-recruit__accr" src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/ca-ll-new.png" alt="">
        <img class="section-recruit__accl" src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/ca-lr1.png" alt=""> -->
    </section>
</div>

<div class="modal--comm-overlay modal--video-ovl"></div>
<div class="modal--video">
	<video src="https://ecs7.tokopedia.net/assets/media/careers/recruitment-process.mp4"></video>
	<img class="modal--video__closebtn" src="https://ecs7.tokopedia.net/assets/images/careers/careers-oc-modal-closebtn.svg" alt="">
</div>