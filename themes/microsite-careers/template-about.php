<?php
	/*////
	// About Page
	// Template Name: Careers About Page
	////*/
?>

<div class="main-overlay"></div>
<div class="video-iframe">
	<div class="iframe-wrapper">
		<iframe width="560" height="315" src="" 
			frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen>
		</iframe>
		<div class="video-close">
			<i class="fa fa-times" aria-hidden="true"></i>
		</div>
	</div>
</div>

<!-- Section Banner Start -->
<section class="section_about-banner">
	<div class="container">
		<div class="banner-wrapper">
			<h1 class="banner__title txt--white fw--smb">About Tokopedia</h1>
			<h2 class="banner-copy fs--20">
				Since 2009, we’ve been helping millions of people and businesses to 
				open and manage their own online store. 
				We’ve grown to the point of being more than an e-commerce, 
				but an ecosystem where anyone can start and discover anything.
				<p>Now we aim to democratize commerce through technology.</p>         
			</h2>
			<div>
				<a class="btn-tkpd btn--banner btn--orange-streak btn--big-streak">Find Out More</a>
			</div>
		</div>
	</div>
	<div id="scene" class="banner-filler">
		<div class="banner-filler__bg" data-speed="20"></div>
		<div class="banner-filler__bgadd1 parallax" data-speed="20"></div>
		<div class="banner-filler__bgadd2 parallax" data-speed="35"></div>
	</div>
</section>
<!-- /Section Banner End -->

<!-- Section What We Do Start -->
<section class="section_about-wwd">
	<div class="drive-circleorange--solid parallax" data-speed="10">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-circle-orange-small.png" alt="">
	</div>
	<div class="container">
		<div class="wwd--mobile-padding">
			<div class="wwd-header">
				<h2 class="wwd-header__title txt--black txt--disabled">WHAT WE DO</h2>
				<h2 class="wwd-header__prime fw--smb fs--24 txt--black txt--primary">
					What You Can Find in Tokopedia
				</h2>
				<h3 class="wwd-header__content fw--18 txt--black txt--secondary">
					We believe that success can only be achieved by helping others to be more successful.
					<span style="display: block;">And these are how we do it.</span>
				</h3>
			</div>
		</div>
		<div class="wwd-box-wrap">
			<!-- Slider -->
			<div class="wwd-innerbox-wrap">
				<div class="wwd-box active" data-wwd="wwd-marketplace">
					<div class="wwd-box-image text-center">
						<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-marketplace.png" class="wwd-box-image__image">
					</div>
					<h3 class="wwd-box__caption text-center">Marketplace</h3>
				</div>
				<div class="wwd-box" data-wwd="wwd-os">
					<div class="wwd-box-image text-center">
						<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-os.png" class="wwd-box-image__image">
					</div>
					<h3 class="wwd-box__caption text-center">Official Store</h3>
				</div>
				<div class="wwd-box" data-wwd="wwd-digital">
					<div class="wwd-box-image text-center">
						<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-digital.png" class="wwd-box-image__image">
					</div>
					<h3 class="wwd-box__caption text-center">Digital</h3>
				</div>
				<div class="wwd-box" data-wwd="wwd-finance">
					<div class="wwd-box-image text-center">
						<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-financial.png" class="wwd-box-image__image">
					</div>
					<h3 class="wwd-box__caption text-center">Financial Product</h3>
				</div>
			</div>
			<!-- What We Do Contents -->
			<div id="wwd-marketplace" class="wwd--mobile-padding wwd-content">
				<h3 class="wwd-box__copy txt--black txt--secondary">
					An open marketplace where everyone can build and grow their own online business easily and for free, 
					while also providing a better online shopping experience.
				</h3>
				<div>
					<a href="https://www.tokopedia.com/jualan-online" class="btn-tkpd btn--wwd btn--green-streak btn--big-streak btn--mr15">Why Sell at Tokopedia?</a>
					<a href="https://www.tokopedia.com/bebas-penipuan" class="btn-tkpd btn--wwd btn--green-streak btn--big-streak">Why Shop at Tokopedia?</a>
				</div>
			</div>
			<div id="wwd-os" class="wwd--mobile-padding wwd-content" style="display: none;">
				<h3 class="wwd-box__copy txt--black txt--secondary">
					A dedicated section that features official brand stores, authorized sellers, 
					and webstores to showcase their brands/products online with exclusive promo offered.
				</h3>
				<div>
					<a href="https://www.tokopedia.com/official-store/" class="btn-tkpd btn--wwd btn--green-streak btn--big-streak btn--mr15">Browse Official Store</a>
					<a href="https://www.tokopedia.com/bantuan/pengenalan-official-store/" class="btn-tkpd btn--wwd btn--green-streak btn--big-streak">What is Official Store</a>
				</div>
			</div>
			<div id="wwd-digital" class="wwd--mobile-padding wwd-content" style="display: none;">
				<h3 class="wwd-box__copy txt--black txt--secondary">
					A 24/7 access for everyday digital needs, from mobile recharge, game voucher, 
					train ticket, and many more. Now, it takes just one click away to access almost 
					everything that people need in everyday life.
				</h3>
				<div>
					<a href="https://www.tokopedia.com/pulsa/" class="btn-tkpd btn--wwd btn--green-streak btn--big-streak btn--mr15">Explore Digital Goods</a>
				</div>
			</div>
			<div id="wwd-finance" class="wwd--mobile-padding wwd-content" style="display: none;">
				<h3 class="wwd-box__copy txt--black txt--secondary">
					An easier, quicker, and more affordable way for users to access financial services such as loans, 
					insurance, savings, and investment.
				</h3>
				<div>
					<a href="https://www.tokopedia.com/keuangan/" class="btn-tkpd btn--wwd btn--green-streak btn--big-streak btn--mr15">Financial Products</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /Section What We Do End -->

<!-- Section What Drive Us START-->
<section class="section_about-drive">
	<div class="drive-circle--solid parallax" data-speed="5">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-circle-green-big.png" alt="">
	</div>
	<div class="drive-circle--dashed parallax" data-speed="10">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-circle-dash-green.png" alt="">
	</div>
	<div class="container">
		<div class="wwd--mobile-padding">
			<div class="wwd-drive">
				<h2 class="wwd-header__title txt--black txt--disabled">WHAT DRIVE US</h2>
				<h2 class="wwd-header__prime fw--smb fs--24 txt--black txt--primary">The Three Embodiments</h2>
				<h3 class="wwd-header__content wwd-header__content--drive fw--18 txt--black txt--secondary">
					We are holding to these three principles that drive us forward. 
					<p>We call them our DNA, the traits that embody who we are as Tokopedia Nakama.</p>
				</h3>
			</div>
		</div>
		<div class="wwd-innerbox-wrap--drive">
			<div class="wwd-box">
				<div class="wwd-box-image text-center">
					<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-focus.png" class="wwd-box-image__image">
				</div>
				<h3 class="wwd-box__caption--drive text-center">Focus On Consumer</h3>
			</div>
			<div class="wwd-box">
				<div class="wwd-box-image text-center">
					<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-growth.png" class="wwd-box-image__image">
				</div>
				<h3 class="wwd-box__caption--drive text-center">Growth Mindset</h3>
			</div>
			<div class="wwd-box">
				<div class="wwd-box-image text-center">
					<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-makeit.png" class="wwd-box-image__image">
				</div>
				<h3 class="wwd-box__caption--drive text-center">Make it Happen <p>Make it Better</p></h3>
			</div>
		</div>
	</div>
</section>
<!-- /Section What Drive Us END -->

<!-- Section About Video START -->
<section class="section_about-video">
	<img class="about-video__mainbg" src="https://ecs7.tokopedia.net/assets/images/careers/ic_green1.svg" alt="">
	<div class="container about-video--container">
		<div class="about-video-wrap">
			<div class="about-video">
				<div class="about-video-image">
					<div class="about-video-image__overlay"></div>
					<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-play--white.svg" alt="" class="about-video-image__playbtn">
					<img src="https://ecs7.tokopedia.net/assets/images/careers/bg-milestone--cropped.png" alt="" class="img-responsive about-video__img">            
				</div>
			</div><!--
		--><div class="about-video">
				<div class="about-video-content">
					<h2 class="about-video-content__header">
						How are you gonna fulfill your purpose?
					</h2>
					<h3 class="about-video-content__text">
						At Tokopedia, we're certain everybody brings something to the table. 
						Head on over to our Career Page and join the team today!
					</h3>
				</div>
				<div class="btn-about-video">
					<a href="https://www.tokopedia.com/careers/" class="btn-tkpd btn--about-video btn--orange-streak btn--big-streak">Browse Opportunities</a>
				</div>

				<div class="about-team-link">
					or
					<a href="https://www.tokopedia.com/team" class="about-team-link__link"> Meet the Team</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /Section About Video END -->

<!-- Section About Homes START -->
<section class="section_about-homes">
	<div class="homes-divider"></div>
	<div class="container">
		<h2 class="homes__header">Our Headquarter</h2>
		<div class="homes-map">
			<div class="homes-map-wrap">
				<img src="https://ecs7.tokopedia.net/assets/images/careers/PixelMap.svg" alt="" class="homes-map-wrap__map">
				<!-- Tooltip Content (mobile) -->
				<div class="homes-tooltip indonesia visible-xs">
					<div class="homes-tooltip-content">
						<h2 class="homes-tooltip__header">Tokopedia Tower</h2>
						<h3 class="homes-tooltip__address">
							Jl. Prof. DR. Satrio No.Kav 11, Karet Kuningan, 
							Kecamatan Setiabudi, Kota Jakarta Selatan, 
							Daerah Khusus Ibukota Jakarta, Indonesia
						</h3>
						<div class="homes-tooltip__open text-center">
							<a target="_blank" href="https://www.google.co.id/maps/place/Tokopedia+Tower/@-6.2219436,106.8174235,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69f3fe6d0bc9a1:0x88011349eeaac6a3!8m2!3d-6.2219489!4d106.8196122?hl=en">
								<i class="fa fa-map-marker"></i>
								<span>Open Maps</span>
							</a>
						</div>
					</div>
				</div>
				<div class="homes-map-marker">
					<div class="homes-map-wrap__marker" data-marker="indonesia">
						<div class="homes-tooltip-content__triangle"></div>
					</div>
					<!-- Tooltip Content (desktop) -->
					<div class="homes-tooltip indonesia hidden-xs">
						<div class="homes-tooltip-content">
							<h2 class="homes-tooltip__header">Tokopedia Tower</h2>
							<h3 class="homes-tooltip__address">
								Jl. Prof. DR. Satrio No.Kav 11, Karet Kuningan, 
								Kecamatan Setiabudi, Kota Jakarta Selatan, 
								Daerah Khusus Ibukota Jakarta, Indonesia
							</h3>
							<div class="homes-tooltip__open text-center">
								<a target="_blank" href="https://www.google.co.id/maps/place/Tokopedia+Tower/@-6.2219436,106.8174235,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69f3fe6d0bc9a1:0x88011349eeaac6a3!8m2!3d-6.2219489!4d106.8196122?hl=en">
									<i class="fa fa-map-marker"></i>
									<span>Open Maps</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /Section About Homes END -->





