<?php
	/*////
	// Explore Page
	// Template Name: Career Explore Page
	////*/
?>

<div class="explore">
	<div class="contain">
		<div style="height:300px;background:brown;"><br><br><br><br>
		<a href="javascript:void(0);" onclick="Download('')" >Save "Bad Romance" to your computer</a>
		<iframe id="my_iframe" style="display:none;"></iframe>
		<script>
		function Download(url) {
			document.getElementById('my_iframe').src = url;
		};
		</script>
		</div>
		<div style="position:fixed;top:90px;background:white;" class="navigation">
			<span class="active">section 1</span><span>section 2</span><span>section 3</span><span>section 4</span><span>section 5</span><span>section 6</span>
		</div>
		<div class="section section1" style="height:700px;background:black">
			 <h1><span class="text-wave-animate">header 1 wkwkwk adawakdka</span><br><span class="text-wave-animate"> aakd adkawkd akd akadk akdka</span></h1>
			 <h2 class="text-wave-animate">header 2</h2>
			<style>
				ol {
					padding-left: 24px;
				}
				ol li {
					padding-left: 8px;
					list-style-type: decimal;
				}
				ol ol li {
					list-style-type: lower-latin;
				}
				ul li {
					list-style-type: none;
					padding-left: 16px;
					position: relative;
						
				}
				ul li::before {
					content: "-";
					position: absolute;
					top: 0px;
					left: 0px;
				}
				th {
					font-weight: bold;
				}
				td:nth-child(2) {
					text-align: center;
				}
				table {
					margin: 0 auto;
				}
				i {
					font-style: italic;
				}
				b {
					font-weight: bold;
				}
			</style>
			<!--h3>Syarat dan Ketentuan Seller Competition</h3>
			<ol>
				<li>Kompetisi ini hanya dapat diikuti oleh merchant yang tergabung dalam Top Community Tokopedia sebelum tanggal 29 Juni 2018.</li>
				<li>Point dihitung dari setiap pesanan dengan pembayaran berhasil menggunakan kode MARIBELANJA dengan periode 16 - 31 Juli 2018 dan konfirmasi terima barang pada periode hingga 5 Agustus 2018.</li>
				<li>Perhitungan poin dihitung dari jumlah transaksi yang berhasil menggunakan kode promo MARIBELANJA, disesuaikan dengan  nilai perbandingan di daerah domisili merchant.
					<table>
						<tr><th>Region</th><th>Nilai Perbandingan</th></tr>
						<tr><td>Bali, Nusa Tenggara, Papua</td><td>1</td></tr>
						<tr><td>Jakarta</td><td>5</td></tr>
						<tr><td>Jawa</td><td>3</td></tr>
						<tr><td>Sumatera</td><td>1</td></tr>
						<tr><td>Kalimantan &amp; Sulawesi</td><td>1</td></tr>
					</table>
					<i>Simulasi:<br>
					Untuk merchant di Jakarta setiap 5 transaksi yang berhasil menggunakan MARIBELANJA sama dengan 1 point, sedangkan untuk merchant dari Sumatera setiap 1 transaksi sama dengan 1 point.
					</i>
					<br><br>
					<ol>
						<li>Perhitungan poin individu dihitung dari jumlah transaksi yang berhasil menggunakan kode promo MARIBELANJA pada merchant tersebut.</li>
						<li>Perhitungan poin region dihitung dari akumulasi jumlah transaksi yang berhasil oleh merchant yang tergabung di daerah tersebut menggunakan kode promo MARIBELANJA.</li>
					</ol>
				</li>
				<li>Merchant atau region yang mendapatkan poin terbanyak akan berkesempatan untuk memenangkan hadiah sebagai berikut
					<ol>
						<li>
							<p>Pemenang Individu</p>
							<p><b>Pemenang 1 : </b></p>
							<p>Total hadiah Rp 10 Juta </p>
							<p>dengan rincian TopAds senilai Rp 3 Juta, Gold Merchant senilai Rp 1 Juta, Seller Kit, senilai Rp 3 Juta, Giftcards senilai Rp 3 Juta.</p>
							<p><b>Pemenang 2 : </b></p>
							<p>Total hadiah Rp 7,5 Juta </p>
							<p>dengan rincian TopAds senilai Rp 2 Juta, Gold Merchant senilai Rp 1 Juta, Seller Kit  senilai Rp 2,5 Juta, Giftcards senilai Rp 2 Juta.</p>
							<p><b>Pemenang 3 : </b></p>
							<p>Total hadiah Rp 5 Juta </p>
							<p>dengan rincian TopAds senilai Rp 1 Juta, Gold Merchant senilai Rp 1 Juta,  Seller Kit  senilai Rp 2 Juta, Giftcards senilai Rp 1 Juta.</p>
						</li>
						<li>
							<p>Pemenang Region</p>
							<p>Berikut adalah hadiah yang dapat dimenangkan pada kategori grup:</p>
							<p><b>Pemenang 1</b></p>
							<p>Total hadiah Rp 5 Juta + Eksposur di  halaman utama selama 3 hari</p>
							<p>50 Merchant dengan poin tertinggi pada TopCommunity area tersebut akan mendapat TopAds masing-masing senilai Rp100.000 dan eksposur produk pada halaman utama Tokopedia selama 3 hari.</p>
							<p><b>Pemenang 2</b></p>
							<p>Total hadiah Rp 3 Juta dengan rincian + Eksposur di  halaman utama selama 2 hari</p>
							<p>30 Merchant dengan poin tertinggi pada TopCommunity area tersebut akan mendapat TopAds masing-masing senilai Rp100.000 dan eksposur produk pada halaman utama Tokopedia selama 2 hari.</p>
							<p><b>Pemenang 3</b></p>
							<p>Total hadiah Rp 2 Juta dengan rincian + Eksposur di  halaman utama selama 1 hari</p>
							<p>20 Merchant dengan poin tertinggi pada TopCommunity area tersebut akan mendapat TopAds masing-masing senilai Rp100.000 dan eksposur produk pada halaman utama Tokopedia selama 1 hari.</p>
						</li>
					</ol>
				</li>
				<li>Perhitungan poin akan ditutup pada 6 Agustus 2018.</li>
				<li>Tokopedia berhak, tanpa pemberitahuan sebelumnya, menarik kembali hadiah atau menangguhkan akun apabila dirasa akun Anda melakukan sesuatu yang melanggar aturan Program kompetisi ini maupun aturan Tokopedia secara keseluruhan.</li>
				<li>Tokopedia berhak, tanpa pemberitahuan sebelumnya, membatalkan partisipasi Anda dalam program ini serta berhak membatalkan, memodifikasi, atau menghentikan program kapan pun.</li>
				<li>Tokopedia berhak, tanpa pemberitahuan sebelumnya, mengubah Syarat dan Ketentuan ini.</li>
			</ol>
			<h3>Syarat &amp; Ketentuan untuk Pembeli </h3>
			<ol>
				<li>Hanya berlaku untuk pengguna baru yang belum pernah berbelanja di Marketplace dan Official Store.</li>
				<li>Promo berlaku selama periode 16 - 31 Juli 2018.</li>
				<li>Untuk mendapatkan <b><i>cashback</i> 100% hingga Rp30.000</b> ke TokoCash &amp; <b>Gratis Ongkir Rp10.000,</b> pengguna harus memasukkan kode promo <b>MARIBELANJA</b> di halaman pembayaran.</li>
				<li>Satu pengguna Tokopedia hanya bisa mengikuti promo ini sebanyak 1 (satu) kali transaksi selama periode promo.</li>
				<li>Cashback dihitung dari harga produk, tidak termasuk ongkir.</li>
				<li>Berlaku tanpa minimum pembelian.</li>
				<li>Cashback akan diterima oleh pembeli maksimal 1 x 24 jam saat transaksi sudah selesai (pembeli sudah melakukan Konfirmasi Terima Barang).</li>
				<li>Untuk mendapatkan cashback, pengguna harus melakukan aktivasi TokoCash dalam 7 hari, atau cashback akan hangus.</li>
				<li>Gratis ongkos kirim yang diberikan tidak termasuk biaya-biaya tambahan dari pihak logistik ataupun penjual seperti packing kayu, asuransi, dan sebagainya. </li>
				<li>Jika ongkos kirim melebihi nilai yang disubsidi oleh Tokopedia, maka sisa ongkos kirim ditanggung oleh pembeli.</li>
				<li>Pastikan nomor handphone yang sudah terverifikasi aktif pada saat melakukan transaksi. Jika terjadi perubahan nomor handphone pada akun.</li> 
				<li>Tokopedia pengguna dan nomor tersebut sudah tidak aktif, maka pengguna tidak akan mendapatkan cashback.</li>
				<li>
					<ul>
					Promo tidak berlaku untuk:
						<li>Metode pembayaran BCA KlikPay dan pembayaran di gerai ritel.</li>
						<li>Penggabungan dengan promo lain di Tokopedia.</li>
						<li>Pembeli yang bertransaksi langsung sebagai Dropshipper.</li>
					</ul>
				</li>
				<li>Tokopedia berhak, tanpa pemberitahuan sebelumnya, melakukan tindakan-tindakan yang diperlukan apabila diduga terjadi tindakan kecurangan dari pengguna yang merugikan pihak Tokopedia, termasuk pembuatan lebih dari 1 (satu) akun oleh 1(satu) pengguna yang sama dan/atau nomor handphone yang sama dan/atau alamat yang sama dan/atau ID pelanggan yang sama dan/atau identitas pembayaran yang sama dan/atau riwayat transaksi yang sama.</li>
				<li>Syarat dan Ketentuan promo ini merupakan bagian yang tidak terpisahkan dari Syarat dan Ketentuan Tokopedia, dan sewaktu-waktu dapat berubah tanpa pemberitahuan sebelumnya.</li>
			</ol -->
		</div>
		<div class="section section2" style="height:700px;background:blue"><h1 class="text-wave-animate">header 2</h1></div>
		<div class="section section3" style="height:700px;background:black"><h1 class="text-wave-animate">header 3</h1></div>
		<div class="section section4" style="height:700px;background:blue">header 4</div>
		<div class="section section5" style="height:700px;background:black">header 5</div>
		<div class="section section6" style="height:700px;background:blue">header 6</div>
	</div>
</div>
<style>
.text-wave-animate {opacity:0;}
.text-wave-animate.in-progress{opacity:1;}
.text-wave-animate span{
	animation: text-wave-animation 0.8s cubic-bezier(0.175, 1.885, 0.32, 1.275);
}
.navigation span.active{color:red;}
.section {color:white;}
.section span {position:relative;}

@keyframes text-wave-animation {
	0% {
		opacity: 0;
		top: 40px;
	}
	100% {
		opacity: 1;
		top: 0px;
	}
}
</style>
