/* jshint ignore:start */
/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {
    var jobsDataRaw;
    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function() {
                // JavaScript to be fired on all pages
                var overlay = $('.site-overlay');
                var $hamverlay = $('.site-overlay--ham');
                //  Drawer   //
                // Hamburger //
                ///////////////
                
                $('.top-navigation-menu__ham').click(function() {
                    $(this).toggleClass('open');
                    $('.top-navigation-menu,.mobile-animation-wrapper').toggleClass('active');
                    $hamverlay.toggleClass('active');
                    $('body').toggleClass('fix-body');
                });
                $('#burger-menu').click(function() {
                    $(this).toggleClass('close');
                    $('.mobile-animation-wrapper').removeClass('toLeft');
                    $('.top-navigation-menu,.mobile-animation-wrapper').toggleClass('active');
                    $hamverlay.toggleClass('active');
                    $('body').toggleClass('fix-body');
                    $('.top-nav-search').removeClass('active');
                    $('.search-icon').removeClass("open");
                    $('.section_search').removeClass('show');
                    var count = 0;
                    $('.top-navigation-menu ul>li').each(function(){
                        var _ = $(this);
                        _.css('transition-delay', count*50+'ms');
                        count++;
                    });
                });
                //drop down menu clicked MOBILE
                $('.top-navigation-menu__drop-down').click(function(){
                    $(this).toggleClass("active");
                });


                // Pivot //
                ///////////
                var pivotDelay = 50,
                    pivotEnterTimeout, pivotLeaveTimeout;

                $('.bridge-widget,.top-navigation-menu__drop-down').hover(function() {
                    var pivot = $(this).parent();
                    clearTimeout(pivotLeaveTimeout);

                    pivotEnterTimeout = setTimeout(function() {
                        $('.sub-menu').hide();
                        // overlay.addClass('active');
                    }, pivotDelay);
                }, function() {
                    var pivot = $(this).parent();
                    clearTimeout(pivotEnterTimeout);
                    pivotLeaveTimeout = setTimeout(function() {
                        // overlay.removeClass('active');
                    }, pivotDelay);
                });
                
                //Text menu Hover
                ////////////
                $('.top-navigation-menu__down').hover(function() {
                    clearTimeout(pivotLeaveTimeout);
                    pivotEnterTimeout = setTimeout(function() {
                        // overlay.addClass('active');
                    }, pivotDelay);
                }, function() {
                    var pivot = $(this).parent();
                    clearTimeout(pivotEnterTimeout);
                    pivotLeaveTimeout = setTimeout(function() {
                        // overlay.removeClass('active');
                    }, pivotDelay);
                });
                
                //add active page
                //

                if(window.location.href.indexOf("/vacancy") > -1)
                {
                    $(".top-navigation-menu a:contains('Vacancy')").addClass("active");
                }
                // Modal
                ////////


                $('*[data-modal-target]').click(function() {
                    var iframeW;
                    if ($(window).width() <= 350) {
                        iframeW = 300;
                    } else if ($(window).width() <= 480) {
                        iframeW = 320;
                    } else if ($(window).width() <= 992) {
                        iframeW = 560;
                    } else if ($(window).width() > 992) {
                        iframeW = 560;
                    }
                    popModal($(this).attr('data-modal-target'));
                    if ($('.tkpd-modal').length) {
                        // console.log('founded');
                        $('.tkpd-modal').append('<iframe width="' + iframeW + '" height="' + (iframeW * 9) / 16 + '" src="https://www.youtube.com/embed/LowfXMQzw70" frameborder="0" allowfullscreen></iframe>');
                    }
                });

                $('.tkpd-modal .tkpd-close').click(function() {
                    closeModal($(this));
                    if ($('.tkpd-modal').length) {
                        $('.tkpd-modal iframe').remove();
                    }
                });

                $('.tkpd-overlay').click(function() {
                    $('body').css('overflow', 'auto');
                    $('.tkpd-close').trigger('click');
                });

                function popModal(a) {
                    $('#' + a).addClass('active');
                    $('.tkpd-overlay').addClass('active');
                    $('body').css('overflow', 'hidden');
                }

                function closeModal(a) {
                    a.parent().removeClass('active');
                    $('.tkpd-overlay').removeClass('active');
                    $('body').css('overflow', 'auto');
                }

                /* hack for preventing html fire close event */
                $('.bw-container').on('click', function(e) {
                    e.stopPropagation();
                });
                // If wpadmin bar is visible, add header margin
                if ($('#wpadminbar').is(':visible') && (window.innerWidth>767)) {
                  $('#header').css('margin-top', '32px');
                }
                
                /* ---------------------------
                    Ripple Effect Native JS
                --------------------------- */

                Element.prototype.rippleEffect = function(e) {
                    var self;
                    var size;
                    var spanEl;
                    var rippleX;
                    var rippleY;
                    var offsetX;
                    var offsetY;
                    var eWidth;
                    var eHeight;
                
                    var btn = Object.prototype.hasOwnProperty.call(e, 'disabled') || e.classList.contains('disabled') ? false : e;
                
                    btn.addEventListener('mousedown', function(ev) {
                    self = e;
                    // Disable right click
                    if (e.button === 2) {
                        return false;
                    }
                
                    var rippleFlag = 0;
                    for (var i = 0; i < self.childNodes.length; i += 1) {
                        if (self.childNodes[i].nodeType === Node.ELEMENT_NODE) {
                        if (self.childNodes[i].matches('.ripple')) rippleFlag += 1;
                        }
                    }
                
                    if (rippleFlag === 0) {
                        var elChild = document.createElement('span');
                        elChild.classList.add('ripple');
                        self.insertBefore(elChild, self.firstChild);
                    }
                    spanEl = self.querySelectorAll('.ripple')[0];
                    spanEl.classList.remove('animated');
                
                    eWidth = self.getBoundingClientRect().width;
                    eHeight = self.getBoundingClientRect().height;
                    size = Math.max(eWidth, eHeight);
                
                    spanEl.style.width = size+'px';
                    spanEl.style.height = size+'px';
                
                    offsetX = self.ownerDocument.defaultView.pageXOffset;
                    offsetY = self.ownerDocument.defaultView.pageYOffset;
                
                    rippleX = parseInt(ev.pageX - (self.getBoundingClientRect().left + offsetX), 10) - (size / 2);
                    rippleY = parseInt(ev.pageY - (self.getBoundingClientRect().top + offsetY), 10) - (size / 2);
                
                    spanEl.style.top = rippleY+'px';
                    spanEl.style.left = rippleX+'px';
                    spanEl.classList.add('animated');
                
                    setTimeout(function() {
                        spanEl.remove();
                    }, 800);
                
                    return ev;
                    });
                };
                
                var rippleEl = document.querySelectorAll('.ripple-effect');
                for (var i = 0; i < rippleEl.length; i += 1) {
                    rippleEl[i].rippleEffect(rippleEl[i]);
                }

                // Workplace Menu
                var wpEnterTimeout, wpLeaveTimeout;

                // $('.nav-workplace, .nav-workplace a, .tn-workplace').hover(function() {
                //     clearTimeout(wpLeaveTimeout);
                //     wpEnterTimeout = setTimeout(function() {
                //         $('.site-overlay').addClass('active');
                //     }, 50);
                // },
                //     function() {
                //         clearTimeout(wpEnterTimeout);
                //         wpLeaveTimeout = setTimeout(function() {
                //             $('.site-overlay').removeClass('active');
                //         }, 50);
                //     }
                // );

                if( $(window).width() < 1024 ) {
                    $('.tn-arrow').click(function() {
                        // console.log('hey');
                        if( $('.tn-workplace').hasClass('mShow') ) {
                            $('.tn-workplace').removeClass('mShow');
                            $('.tn-workplace').slideUp();
                        }
                        else {
                            $('.tn-workplace').addClass('mShow');
                            $('.tn-workplace').slideDown();
                        }
                    });
                }

                //////////////////////////////
                // Top Navigation Hide/Show //
                /////////////////////////////

                var lastScrollTop = 0;
                var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
                w = $(window).width();
                var searchBar = document.querySelectorAll('.section_search')[0];
                var discovery = document.querySelectorAll('#discovery')[0];
                window.onscroll = function() {
                    var st = window.pageYOffset || document.documentElement.scrollTop; 
                    var header = document.getElementById("header");
                    if( searchBar || discovery ) {
                        if (st > lastScrollTop){
                            if(header && w>992) {
                                document.getElementById("header").classList.add('hid');
                            }
                            // hasClass
                            var classes = searchBar ? searchBar.classList + ' ' : discovery.classList + ' ';
                            if(classes.indexOf('fixed') >= 0) {
                                if(searchBar) searchBar.classList.add('hid');
                                if(discovery) discovery.classList.add('hid');

                            }
                        } 
                        else {
                            if(header && w>992){
                                document.getElementById("header").classList.remove('hid');
                                if( searchBar ) searchBar.classList.remove('hid');
                                if( discovery ) discovery.classList.remove('hid');
                            }
                        }
                    }
                    lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
                };

                /////////////////////////
                // New Modal Community //
                ////////////////////////

                function disableModal() {
                    $('.modal--comm-overlay').removeClass('active');
                    $('.modal--comm').removeClass('active');
                    $('body').css('overflow', 'auto');
                }

                $('[data-modal]').on('click', function(){
                    var $overlay = $('.modal--comm-overlay');
                    var $modal = $('.modal--comm');

                    var img = $(this).data('img');
                    var what = $(this).data('what');
                    var title = $(this).data('title');
                    var link = $(this).data('link');
                    var date = $(this).data('date');

                    if( date == 'PASSED') {
                        $('.modal--comm-rc__date').addClass('passed');
                        $('.modal--comm-rc__link button').attr('disabled', 'disabled');
                    }
                    else {
                        $('.modal--comm-rc__date').removeClass('passed');
                        $('.modal--comm-rc__link button').removeAttr('disabled');
                    }

                    $('.modal--comm-lc p').html(what);
                    $('.modal--comm-rc__title').html(title);
                    $('.modal--comm-right-img__img').attr('src', img);
                    $('.modal--comm-rc__link').attr('href', link);
                    $('.modal--comm-rc__date').html(date);

                    $overlay.addClass('active');
                    $modal.addClass('active');

                    $('body').css('overflow', 'hidden');
                });

                $('.modal--comm-overlay').on('click', disableModal);
                $('.modal--comm__closebtn').on('click', disableModal);

                ////////////////////////////////
                // Recruitment Process Video //
                //////////////////////////////
                $('[data-video]').on('click', function() {
                    var videoSrc = $(this).data('video');

                    var $video = $('.modal--video');
                    var $videoEl = $('.modal--video video');
                    var $modal = $('.modal--comm-overlay');

                    $modal.addClass('active');
                    $video.addClass('active');

                    if( videoSrc.indexOf('youtube') !== -1 ) {
                        var iframeW = $(window).width() >= 1000 ? 1000 : $(window).width() * 0.8;
                        var iframeDOM = '<iframe width="'+iframeW+'" height="'+aspectRatioCalc(iframeW)+'" src="'+ videoSrc+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
                        $video.addClass('yt');
                        $video.append(iframeDOM);
                        $videoEl.hide();
                    }
                    else {
                        $videoEl.attr('autoplay', 'autoplay');
                        $videoEl.attr('controls', 'controls');
                        $videoEl.attr('src', videoSrc);
                    }
                });
                $('.modal--video__closebtn, .modal--video-ovl').on('click', function() {
                    var $video = $('.modal--video');
                    var $videoEl = $('.modal--video video');
                    var $modal = $('.modal--comm-overlay');
                    var $iframe =  $('.modal--video iframe');

                    $modal.removeClass('active');
                    $video.removeClass('active');
                    $video.removeClass('yt');
                    $videoEl.attr('src', '');
                    $iframe.remove();
                    $videoEl.show();
                });

                // 16:9 //
                //Returns new Height
                function aspectRatioCalc(newWidth) {
                    return (315/560) * newWidth;
                }

                var selectedCountry = 'All Locations';

                ////////////////////////////////////////////////////
                // Search Bar Location & Search Select Controller //
                ////////////////////////////////////////////////////

                var $theloc = $('.search-bar-theloc');
                $theloc.on('click', function(e) {
                    // if ($(window).innerWidth() > 991) {
                    //     if ($(this).find('.search-bar-theloc-ul').hasClass('active')) {
                    //         $(this).find('.search-bar-theloc-ul').removeClass('active');
                    //         e.stopPropagation();
                    //     } else {
                    //         $(this).find('.search-bar-theloc-ul').addClass('active');
                    //         e.stopPropagation();
                    //     }
                    // }
                    if ($(this).find('.search-bar-theloc-ul').hasClass('active')) {
                        $(this).find('.search-bar-theloc-ul').removeClass('active');
                        e.stopPropagation();
                    } else {
                        $(this).find('.search-bar-theloc-ul').addClass('active');
                        e.stopPropagation();
                    }
                });

                $('.search-bar-theloc-ul').on('click', '.search-bar-theloc__li', function(e) {
                    var newVal = $(this).text().trim();
                    // selectedCountry = newVal;
                    var trueVal = $(this).data('cc');
                    // $(this).parent().siblings('.search-bar-thebar-group').children('.search-bar__theloc').attr('value', newVal);
                    $(this).parents().siblings('.search-bar__theloc').attr('value', newVal);
                    $('input[type="hidden"][name="country"]').attr('value', trueVal);
                });

                $('.search-bar-thebar-ul').on('click', '.search-bar-thebar__li', function(e) {
                    var newJob = $(this).data('jobs');
                    var newRegion = $(this).data('region');

                    $(this).parent().siblings('.search-bar-thebar').children('.search-bar__thebar').attr('value', newJob);
                    $(this).parent().siblings('.search-bar-theloc').find('.search-bar__theloc').attr('value', newRegion);
                });

                $('body').on('click', function() {
                    if ($('.search-bar-theloc-ul').hasClass('active')) {
                        $('.search-bar-theloc-ul').removeClass('active');
                    }
                });

                $('.search-bar-thebar-ul').on('click', '.search-bar-thebar__li', function() {
                    var newVal = $(this).data('jobs');
                    $('.search-bar__thebar').val(newVal);

                    if ($(window).innerWidth() < 992) {
                        $('.section_mobile-search').removeClass('active');
                        $('html').removeClass('noscroll');
                        $('.search-bar-thebar-ul--m').removeClass('active');
                    }
                });

                // Arcane Programming for Searching Job List
                $.extend($.expr[':'], {
                    'joblist': function(elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase()
                            .indexOf((match[3] || "").toLowerCase()) >= 0 && ($(elem).data('region') == selectedCountry || selectedCountry == 'All Locations');
                    }
                });

                // Dekstop
                $('.search-bar__thebar').on('input', function() {
                    if( $(this).val() !== '') {
                        $(this).siblings('.search-bar-thebar-ul').addClass('active');
                        $(this).siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li').css('display', 'none');
                        $(this).siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li:joblist(' + $(this).val() + ')').css('display', 'block');
                        if ($(this).siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li:not(:hidden)').length == 0) {
                            $(this).siblings('.search-bar-thebar-ul').removeClass('active');
                        }
                        $(this).siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li:visible').children('.search-bar-thebar__liwrapper').css('border-bottom', '1px solid #E0E0E0');
                        $(this).siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li:visible').last().children('.search-bar-thebar__liwrapper').css('border-bottom', 'none');
                    }
                    else {
                        $(this).siblings('.search-bar-thebar-ul').removeClass('active'); 
                    }
                });

                // Mobile
                $('.search-bar__thebar--m').on('input', function() {
                    $(this).parent().siblings('.search-bar-thebar-ul').addClass('active');
                    $(this).parent().siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li').css('display', 'none');
                    $(this).parent().siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li:joblist(' + $(this).val() + ')').css('display', 'block');
                    if ($(this).parent().siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li:not(:hidden)').length == 0) {
                        $(this).parent().siblings('.search-bar-thebar-ul').removeClass('active');
                    }
                    $(this).parent().siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li:visible').children('.search-bar-thebar__liwrapper').css('border-bottom', '1px solid #E0E0E0');
                    $(this).parent().siblings('.search-bar-thebar-ul').find('.search-bar-thebar__li:visible').last().children('.search-bar-thebar__liwrapper').css('border-bottom', 'none');
                });



                // Extra 
                // input on focus 

                $('.search-bar__theloc').blur(function() {
                    $(this).parent().css('border', '2px solid #EBEBEB');
                    $(this).parent().css('box-shadow', 'none');
                });

                $('.search-bar__theloc').focus(function() {
                    $(this).parent().css('transition', 'all 0.5s ease');
                    $(this).parent().css('border', '2px solid #38BC25');
                    $(this).parent().css('box-shadow', '0 7px 25px 0 rgba(0, 0, 0, 0.08)');
                });

                $('.search-bar__thebar').blur(function() {
                    $(this).siblings('.search-bar-thebar-ul').removeClass('active');
                    $(this).parent().css('border', '2px solid #EBEBEB');
                    $(this).parent().css('box-shadow', 'none');
                });

                $('.search-bar__thebar').focus(function() {
                    $(this).parent().css('transition', 'all 0.5s ease');
                    $(this).parent().css('border', '2px solid #38BC25');
                    $(this).parent().css('box-shadow', '0 7px 25px 0 rgba(0, 0, 0, 0.08)');
                });

                ////////////////////////////////////////////////////////
                // END Search Bar Location & Search Select Controller //
                ////////////////////////////////////////////////////////

                //=====================
                //Populate Search Bar
                //=====================

                var allCountries = [];
                var groupTitle = {};
                var groupDepartment = {};

                //Append Jobs
                // Workable Data From PHP
                /* -------------------------
                      Data Fetching
                ----------------------------*/
                jobsDataRaw = jobsData;
                jobsData = JSON.parse(jobsData);
                jobsData = jobsData.jobs;

                for (var key in jobsData) {
                    // console.log(jobsData);
                    // console.log(jobsData[key]);
                    var department = jobsData[key].department;
                    var title = jobsData[key].title;
                    var slug = jobsData[key].slug;
                    var country = jobsData[key].location.country.toLowerCase();
                    var CC = jobsData[key].location.country_code;
                    var checkPathname = location.pathname === '/careers/' ? '/' : '/careers/';
                    var currUrl = location.origin + '/careers/';
                    var jobsHtml = '<a href="' +
                        currUrl + 'job/' + slug + '"><li class="search-bar-thebar__li" data-jobs="' +
                        title + '" data-region="' +
                        country + '"><div class="search-bar-thebar__liwrapper"><span class="search-bar-thebar__lijob">' +
                        title + '</span><div class="pull-right"><span class="flag-' +
                        country + '"></span></div></div></li></a>';

                    $('.search-bar-thebar-ul').append(jobsHtml);
                    allCountries.push([country, CC]);

                    // Group Data by Title
                    if (!groupTitle[title]) {
                        groupTitle[title] = [];
                    }
                    groupTitle[title].push(jobsData[key]);

                    // Group Data by Department
                    if (!groupDepartment[department]) {
                        groupDepartment[department] = [];
                    }
                    groupDepartment[department].push(jobsData[key]);

                }
                // console.log(jobsData);

                //Get object Length
                objLen = Object.keys(groupTitle).length;

                // var testDouble = {
                //   "id":"5bebb",
                //   "title":"Product Owner",
                //   "full_title":"Product Owner - 02Prod",
                //   "shortcode":"88CD864A73",
                //   "code":"02Prod",
                //   "state":"published",
                //   "department":"Product",
                //   "url":"https://tokopedia.workable.com/jobs/375273",
                //   "application_url":"https://tokopedia.workable.com/jobs/375273/candidates/new",
                //   "shortlink":"https://tokopedia.workable.com/j/88CD864A73",
                //   "location":{"country":"India","country_code":"IN","region":"Uthar Pradesh","region_code":"UP","city":null,"zip_code":null,"telecommuting":false}
                // };
                // groupTitle['Product Owner'].push(testDouble);

                // Get total job openings on each department
                // And Append it
                // console.log(groupDepartment);
                for (var gp in groupDepartment) {
                    // console.log(i);
                    // console.log(groupDepartment[i].length);
                    var dName = gp.toLowerCase();
                    var dLen = groupDepartment[gp].length;

                    $('#' + dName + '-len').removeClass();
                    $('#' + dName + '-len').addClass('jobs-item-link__link');
                    $('#' + dName + '-len').html('See ' + dLen + ' Roles <i class="fa fa-angle-right" aria-hidden="true"></i>');
                }

                //Old Static
                // var lenBusiness = groupDepartment.Business.length ? groupDepartment.Business.length : 0;
                // var lenFinance = groupDepartment.Finance.length ? groupDepartment.Finance.length : 0;
                // var lenOperational = groupDepartment.Operational.length ? groupDepartment.Operational.length : 0;
                // var lenPeople = groupDepartment.People.length ? groupDepartment.People.length : 0;
                // var lenProduct = groupDepartment.Product.length ? groupDepartment.Product.length : 0;
                // var lenTechnology = groupDepartment.Technology.length ? groupDepartment.Technology.length : 0;
                // var lenInternship = groupDepartment.Internship.length ? groupDepartment.Internship.length : 0;

                // $('#business-len').html(lenBusiness);
                // $('#finance-len').html(lenFinance);
                // $('#operational-len').html(lenOperational);
                // $('#people-len').html(lenPeople);
                // $('#product-len').html(lenProduct);
                // $('#technology-len').html(lenTechnology);
                // $('#internship-len').html(lenInternship);

                /* ---------------------------------------
                  Filter Unique Contry in ever function
                ------------------------------------------*/
                var jobsFlag = {};
                for (var k in groupDepartment) {
                    var ctr = [];
                    if (!jobsFlag[k]) {
                        jobsFlag[k] = [];
                    }
                    for (var field in groupDepartment[k]) {
                        ctr.push(groupDepartment[k][field].location.country);
                    }
                    jobsFlag[k].push(ctr.filter(function(x, i, a) {
                        return a.indexOf(x) == i;
                    }));
                }

                // Append
                for (k in jobsFlag) {
                    for (var flag in jobsFlag[k]) {
                        var len = jobsFlag[k][flag].length;
                        for (var i = 0; i < len; i++) {
                            var c = jobsFlag[k][flag][i].toLowerCase();
                            var kk = k.toLowerCase();
                            $('.flag-' + kk).append('<div class="flag-' + c + '--big"></div>');
                        }
                    }
                }

                /* --------------------------------
                    Country Dropdown in Searchbar
                ----------------------------------*/

                // Search Unique Countries
                // var uCountries = allCountries.filter(function (x, i, a) {
                //   return a.indexOf(x) == i;
                // });

                var uCountries = [];
                for (var it = 0; it < allCountries.length; it++) {
                    if (!uCountries[allCountries[it][0]]) {
                        uCountries[allCountries[it][0]] = [];
                    }
                    if (uCountries[allCountries[it][0]].length == 0) {
                        uCountries[allCountries[it][0]].push(allCountries[it]);
                    }
                }

                // console.log(uCountries);

                // Append Countries
                for (key in uCountries) {
                    // console.log(uCountries[key][0][0]);
                    var countriesHtml = '<li class="search-bar-theloc__li" data-cc="' +
                        uCountries[key][0][1] + '"><div class="search-bar-theloc__liwrapper"><span class="search-bar-theloc__liloc">' +
                        uCountries[key][0][0] + '</span><div class="pull-right"><span class="flag-' +
                        uCountries[key][0][0] + '"></span></div></div></li>';

                    var mCountriesHtml = '<li class="search-bar-theloc__li" data-cc="' +
                        uCountries[key][0][1] + '"><div class="search-bar-theloc__liwrapper"><span class="search-bar-theloc__liloc">' +
                        uCountries[key][0][0] + '</span><div class="pull-left"><span class="flag-' +
                        uCountries[key][0][0] + '--big"></span></div></div></li>';

                    $('.search-bar-theloc-ul').append(countriesHtml);
                    $('.search-bar-theloc-ul--m').append(mCountriesHtml);
                }

                // Search Same Title

                /* ------------------
                /       SLICK
                -------------------*/

                /*--------------------->
                  ====   Mobile   ====
                -----------------------*/
                var cenPad = ($(document).width() >= 768) ? '25%' : '20%';

                // New WhatDrivesUs
                $('.wwd-innerbox-wrap-mobile').slick({
                    arrows: false,
                    dots: false,
                    infinite: false,
                    slidesToShow: 1,
                    centerMode: true,
                });

                // Search Bar
                $('.m-searchbar-arrow').on('click', function() {
                    // if ($(window).innerWidth() < 991) {
                    //     window.location.hash = '';
                    //     $('.section_mobile-search').removeClass('active');
                    //     $('html').removeClass('noscroll');
                    // }
                });

                $('.search-bar-thebar').on('click', function() {
                    // window.location.hash = '';
                    // if ($(window).innerWidth() < 991) {
                    //     window.location.hash = 'msrc';
                    //     window.addEventListener("hashchange", function() {
                    //         if (location.hash !== '#msrc') {
                    //             $('.section_mobile-search').removeClass('active');
                    //             $('html').removeClass('noscroll');
                    //             $('.search-bar-thebar-ul--m').removeClass('active');
                    //         } else {
                    //             $('.section_mobile-search').addClass('active');
                    //             $('html').addClass('noscroll');
                    //             // $('.search-bar-thebar-ul--m').addClass('active');
                    //             $('.search-bar__thebar--m').focus();
                    //         }
                    //     }, false);
                    // }
                });

                if ($(window).innerWidth() < 992) {
                    // $('.search-bar__thebar--m').keydown(function(e) {
                    //     var val = $(this).val();
                    //     if (e.which == 13) {
                    //         e.preventDefault();
                    //         $('.search-bar__thebar').val(val);
                    //         $('.section_mobile-search').removeClass('active');
                    //         $('html').removeClass('noscroll');
                    //         $('.search-bar-thebar-ul--m').removeClass('active');
                    //         $(this).blur();
                    //     }
                    // });
                }

                // Location
                $('.search-bar-theloc').on('click', function() {
                    // window.location.hash = '';
                    // if ($(window).innerWidth() < 992) {
                    //     window.location.hash = 'mloc';
                    //     window.addEventListener("hashchange", function() {
                    //         if (location.hash !== '#mloc') {
                    //             $('.site-overlay').removeClass('active');
                    //             $('html').removeClass('noscroll');
                    //             $('.section_mobile-location').removeClass('active');
                    //         } else {
                    //             $('.site-overlay').addClass('active');
                    //             $('html').addClass('noscroll');
                    //             $('.section_mobile-location').addClass('active');
                    //             $('.search-bar__theloc').blur();
                    //         }
                    //     });
                    // }
                });

                ///////////////////////////////////////////
                // Search Mobile New 2018 on Icon Click //
                /////////////////////////////////////////

                $('.top-nav-search').on('click', function() {
                    var $hamverlay = $('.site-overlay--ham');
                    if( !$('.section_search').hasClass('show') ) {
                        $(this).addClass('active');
                        $('.section_search').addClass('show');
                    }
                    else {
                        $(this).removeClass('active');
                        $('.section_search').removeClass('show');
                        // $hamverlay.removeClass('active');
                    }
                    $hamverlay.removeClass('active');
                    $('.search-icon').toggleClass("open");
                    $('#burger-menu').removeClass('close');
                    $('.top-navigation-menu').removeClass('active');
                    $('.mobile-animation-wrapper').removeClass('active');
                    $('.mobile-animation-wrapper').toggleClass('toLeft');
                });

                //////////////////////////////////////////////

                $('.site-overlay').on('click', function() {
                    if (location.hash == '#mloc') {
                        window.location.hash = '';
                        $(this).removeClass('active');
                        $('html').removeClass('noscroll');
                        $('.section_mobile-location').removeClass('active');
                    }
                });

                $('.search-bar-theloc-ul--m').on('click', '.search-bar-theloc__li', function() {
                    var v = $(this).text().trim();
                    var tv = $(this).data('cc');
                    $('.site-overlay').removeClass('active');
                    $('html').removeClass('noscroll');
                    $('.section_mobile-location').removeClass('active');
                    // selectedCountry = v;
                    $('.search-bar__theloc').val(v);
                    $('input[type="hidden"][name="country"]').attr('value', tv);
                });
                /* <-------------------- */


                if ($(window).width() < 992) {
                    $('.floater').on('click', function() {
                        window.location = $(this).find('a').attr('href');
                    });
                }

                // ==============================
                // Safari Hack for Jobs Floater
                // ===============================
                $('.floater').hover(function() {
                    $(this).css('z-index', '10');
                }, function() {
                    $(this).css('z-index', '');
                });
            },
            finalize: function() {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },

        // Home page
        'home': {
            init: function() {
                // JavaScript to be fired on the home page
                $(window).load(function() {
                    // if ($(window).width() >= 768) {
                    //     $('video').attr('src', 'https://ecs7.tokopedia.net/assets/media/careers/video-banner-small.mp4');
                    // }
                });

                // WOW
                $(document).ready(function() {
                    if ($(window).width() > 922) {
                        new WOW().init();
                    }

                    // $('.wwd-innerbox-wrap-mobile').slick({
                    //     arrows: false,
                    //     dots: false,
                    //     infinite: false,
                    //     slidesToShow: 1,
                    //     centerMode: true,
                    // });
                    
                });

                // WOW
                $(document).ready(function() {
                    if ($(window).width() > 922) {
                        new WOW().init();
                    }
                });

                

                $(window).load(function() {
                    if ($(window).width() >= 767) {
                        var colHeight = $('.inh').parent().height();
                        $('.inh').height(colHeight);
                    } else {
                        $('.inh').height('');
                    }
                });

                $(document).ready(function() {
                    $(window).resize(function() {
                        if ($(window).width() >= 767) {
                            var colHeight = $('.inh').parent().height();
                            $('.inh').height(colHeight);
                        } else {
                            $('.inh').height('');
                        }
                    });
                });

                // Fixate Search Bar On Desktop While Scrolled
                // =============================================
                var searchOffset = $(".section_search").offset().top + 78;

                function searchFixed() {
                    if ($('.section_search').length > 0) {
                        if ($(window).width() > 992) {
                            var top = $(window).scrollTop();

                            // console.log(top);
                            // console.log(searchOffset);

                            if ($(this).scrollTop() > searchOffset) {
                                if ($('.section_search').hasClass('fixed')) return;
                                // $('.section_thestory').css('padding-top', $('.section_search').height() + 'px');
                                $('.section_search').addClass('fixed');
                            } else {
                                if (!$('.section_search').hasClass('fixed')) return;
                                // $('.section_thestory').css('padding-top', '0');
                                $('.section_search').removeClass('fixed');
                            }
                        } else {
                            if (!$('.section_search').hasClass('fixed')) return;
                            $('.section_search').removeClass('fixed');
                        }
                    }
                }
                function exploreNav() {
                    if ($('.explore').length > 0) {
                        if ($(window).width() > 992) {
                            // console.log($('section'.length));
                        }
                    }
                }

                $(window).scroll(function() {
                    searchFixed();
                });
                $(window).resize(function() {
                    var searchOffset = $(".section_search").offset().top;
                    searchFixed();
                });
                searchFixed();

            },
            finalize: function() {
                // JavaScript to be fired on the home page, after the init JS
            }
        },

        // Vacancy Page
        'vacancy': {
            init: function() {
                // JavaScript to be fired on the home page

                $(window).load(function() {
                    if ($(window).width() >= 768) {
                        $('video').attr('src', 'https://ecs7.tokopedia.net/assets/media/careers/video-banner-small.mp4');
                    }
                });

                // WOW
                $(document).ready(function() {
                    if ($(window).width() > 922) {
                        new WOW().init();
                    }
                });

                $(window).load(function() {
                    if ($(window).width() >= 767) {
                        var colHeight = $('.inh').parent().height();
                        $('.inh').height(colHeight);
                    } else {
                        $('.inh').height('');
                    }
                });

                $(document).ready(function() {
                    $(window).resize(function() {
                        if ($(window).width() >= 767) {
                            var colHeight = $('.inh').parent().height();
                            $('.inh').height(colHeight);
                        } else {
                            $('.inh').height('');
                        }
                    });
                });
            },
            finalize: function() {
                // JavaScript to be fired on the home page, after the init JS
            }
        },

        // About us page, note the change from about-us to about_us.
        'about': {
            init: function() {
                // JavaScript to be fired on the about us page

                $(document).ready(function() {
                    /* ======================== 
                    === Parallax Movement ====
                    =========================== */

                    $(window).scroll(function() {
                        var windowScroll = $(window).scrollTop();

                        $('.parallax').each(function() {
                            var $layer = $(this);
                            var yPos = (windowScroll * $layer.data('speed') / 100);
                            $layer.css({
                                "transform": "translate3d(0px, " + yPos + "px, 0px)"
                            });

                        });

                    });

                    /* ======================== 
                    ====    Tooltip Marker ====
                    =========================== */


                    var range = 200;
                    // var tooltip = $(window).width() < 768 ? $('.homes-tooltip.visible-xs') : $('.homes-tooltip.hidden-xs');
                    // var offset = tooltip.offset().top;
                    // var height = tooltip.outerHeight();
                    // var offsetP = offset + height;
                    // tooltip.hide();
                    $(window).on('scroll', function() {
                        var tooltip = $(window).width() < 768 ? $('.homes-tooltip.visible-xs') : $('.homes-tooltip.hidden-xs');
                        var offset = tooltip.offset().top;
                        var height = tooltip.outerHeight();
                        var offsetP = offset + height;
                        var scrollTop = $(this).scrollTop();
                        var scrollTopP = scrollTop + $(window).height();
                        // offsetP = offset + height;
                        // console.log(scrollTopP);
                        // console.log(offsetP);

                        if (scrollTopP > offsetP) {
                            // calc = (scrollTop - offsetP) - range / height * 100;
                            // console.log(calc);
                            // tooltip.css({ 'opacity': calc / 100 });
                            setTimeout(function() {
                                tooltip.css({ 'opacity': '1' });
                                tooltip.addClass('activated');
                            }, 500);
                        }
                    });

                    /* ======================== 
                    === Video on click     ====
                    =========================== */

                    $('.about-video-image').on('click', function() {
                        $('body').css('overflow', 'hidden');
                        $('.video-iframe iframe').attr('src', 'https://www.youtube.com/embed/VnkZYzpzRYE?rel=0&autoplay=1');
                        $('.main-overlay').css({
                            opacity: 1,
                            zIndex: 99999
                        });
                        $('.video-iframe iframe').css({
                            zIndex: 99999,
                            transform: "translate(-50%, -50%) scale(1)"
                        });
                        $('.video-iframe').css({
                            zIndex: 99999,
                        });
                        $('.video-close').css({
                            display: 'block',
                        });
                    });

                    $('.main-overlay, .video-iframe, .video-close').on('click', function() {
                        $('body').css('overflow', 'scroll');
                        $('.main-overlay').css({
                            opacity: 0,
                            zIndex: -1
                        });
                        $('.video-iframe iframe').attr('src', '');
                        $('.video-iframe iframe').css({
                            zIndex: -1,
                            transform: "translate(-50%, -50%) scale(0)"
                        });
                        $('.video-iframe').css({
                            zIndex: -1,
                        });
                        $('.video-close').css({
                            display: 'none',
                        });
                    });

                    /* ======================== 
                    === Find out More button ==
                    =========================== */

                    $('.btn--banner').click(function() {
                        var $elTop = $('.section_about-wwd').offset().top - 50;
                        $('body, html').animate({
                            scrollTop: $elTop
                        }, 500);
                    });

                    /* ======================== 
                    = What We Do On Click  ====
                    =========================== */

                    $('.wwd-box').on('click', function() {
                        var contentId = $(this).data('wwd');
                        // console.log(contentId);
                        $('.wwd-box').removeClass('active');
                        $(this).addClass('active');
                        $('.wwd-content').hide();
                        $('#' + contentId).show('slow');
                    });

                    /* ======================== 
                    = Slick Initialization ====
                    =========================== */

                    $('.wwd-innerbox-wrap').slick({
                        arrows: false,
                        infinite: false,
                        slidesToShow: 4,
                        rows: 0,
                        responsive: [{
                            breakpoint: 500,
                            settings: {
                                slidesToShow: 1,
                                centerMode: true,
                                centerPadding: "60px",
                                focusOnSelect: true,
                            }
                        }]
                    });

                    $('.wwd-innerbox-wrap').on('afterChange', function(e, slick, currentSlide, nextSlide) {
                        var contentId = $('*[data-slick-index=' + currentSlide + ']').data('wwd');
                        $('.wwd-box').removeClass('active');
                        $('*[data-slick-index=' + currentSlide + ']').addClass('active');
                        $('.wwd-content').hide();
                        $('#' + contentId).show('slow');
                    });

                    if ($(window).width() < 500) {
                        $('.wwd-innerbox-wrap--drive').slick({
                            arrows: false,
                            infinite: false,
                            slidesToShow: 4,
                            rows: 0,
                            responsive: [{
                                breakpoint: 500,
                                settings: {
                                    slidesToShow: 1,
                                    centerMode: true,
                                    centerPadding: "60px",
                                    focusOnSelect: true,
                                }
                            }]
                        });
                    }
                });
            }
        },
        // Careers Page, Index of Jobs
        'jobs': {
            init: function() {
                // JavaScript to be fired on the about us page
                $(document).ready(function() {
                    var jobsData = jobsDataRaw;
                    /* ================
                     * Toaster
                     * ================ */

                    // $(document).on("DOMNodeInserted", "*[data-toaster]", function(){
                    //   var thisDom = $(this);
                    //   setTimeout(function() {
                    //     thisDom.fadeOut();
                    //     setTimeout(function() {
                    //       thisDom.remove();
                    //     }, 6000);
                    //   }, 2000);$(document).on("DOMNodeInserted", "*[data-toaster]", function(){
                    //   var thisDom = $(this);
                    //   setTimeout(function() {
                    //     thisDom.fadeOut();
                    //     setTimeout(function() {
                    //       thisDom.remove();
                    //     }, 6000);
                    //   }, 2000);
                    // });

                    function pushToaster(text) {
                        var toasterDOM = '' +
                            '<div class="tp-toaster" data-toaster>' +
                            '<span>' + text + '</span>' +
                            '<div class="tp-toaster__close"></div>' +
                            '</div>';

                        if (!$('#toaster-box').length) {
                            $('main').prepend('<div id="toaster-box"></div>');
                        }

                        $('#toaster-box').append(toasterDOM);

                        if ($('.tp-toaster').length > 1) {
                            $('.tp-toaster').eq(0).remove();
                        }

                    }

                    $('body').on('click', '.tp-toaster__close', function() {
                        $(this).parent().remove();
                    });

                    /* ================
                     * Tooltip
                     * ================ */
                    $(document).on('mouseenter', '*[data-tooltip]', function() {
                        var pos = $(this).position();
                        var width = $(this).outerWidth();
                        var position = "top";
                        var color = "black";
                        if ($(this).attr('data-tooltip-position')) {
                            position = $(this).attr('data-tooltip-position');
                        }
                        if ($(this).attr('data-tooltip-color')) {
                            color = $(this).attr('data-tooltip-color');
                        }
                        var new_tooltip = $('<div class="tkpd-tooltip" data-tooltip-position=' + position + ' data-tooltip-color=' + color + '>' + $(this).attr('data-tooltip') + '</div>').insertAfter($(this));

                        //if tooltip has more width then the object
                        var left = -(new_tooltip.innerWidth() - $(this).innerWidth()) / 2;
                        var top = -new_tooltip.innerHeight() - 10;
                        if (new_tooltip.innerWidth() > $(this).innerWidth()) {
                            if (position == "left") {
                                left = -new_tooltip.innerWidth() - 10;
                                if (new_tooltip.innerHeight() < $(this).innerHeight()) {
                                    top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
                                }
                                if (new_tooltip.innerHeight() > $(this).innerHeight()) {
                                    top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
                                }
                            }
                            if (position == "bottom") {
                                top = new_tooltip.innerHeight() + 20;
                            }
                            if (position == "right") {
                                left = $(this).innerWidth() + 10;
                                if (new_tooltip.innerHeight() < $(this).innerHeight()) {
                                    top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
                                }
                                if (new_tooltip.innerHeight() > $(this).innerHeight()) {
                                    top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
                                }
                            }
                        }
                        if (new_tooltip.innerWidth() < $(this).innerWidth()) {
                            left = ($(this).innerWidth() - new_tooltip.innerWidth()) / 2;
                            top = -new_tooltip.innerHeight() - 10;
                        }
                        new_tooltip.css({
                            top: pos.top + top + "px",
                            left: pos.left + left + 1 + "px"
                        });
                    });

                    $(document).on('mouseleave', '*[data-tooltip]', function() {
                        $(this).next().remove();
                    });

                    /* ==================
                     * Global functions
                     * ================== */
                    var createJobCard = function(shortcode, slug, title, plain_description, department, country) {
                        return (
                            '<div class="col-sm-4 col-xs-12">' +
                            '<div class="list-div" data-shortcode="' + shortcode + '">' +
                            '<h4 class="list-div__h4">' + (title.length < 35 ? title : (title.slice(0, 35) + '...')) + '</h4 >' +
                            '<p class="list-div__p">' + plain_description.slice(0, 120) + '...' + '</p>' +
                            '<div class="list-div-highlight--mobile clearfix">' +
                            '<div class="list-div-helper list-div-helper--function">' + department + '</div>' +
                            '<div class="list-div-helper list-div-helper--location">' + country + '</div>' +
                            '</div>' +
                            '<hr/>'+
                            '<div class="list-div-desc clearfix">' +
                            '<div class="list-div-highlight clearfix">' +
                            '<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top">' + department + '</div>' +
                            '<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top">' + country + '</div>' +
                            '</div>' +
                            '<div class="list-div-action">' +
                                '<div class="list-div-action-btn">' +                               
                                    '<a href="../join/' + slug + '" class="" rel="nofollow" title="Apply Now">' +
                                    '<button class="unf-btn unf-btn--no-shadow unf-btn--small full-width">Register<div class="text">Register</div></button>' +
                                    '</a>' +                      
                                '</div>' +
                                '<div class="list-div-action-btn">' +
                                    '<a href="../job/' + slug + '" class="">' +
                                        '<button class="unf-btn unf-btn--small unf-btn--secondary full-width">See Details<div class="text">See Details</div></button>' +
                                    '</a>' +
                                '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                        );
                    };

                    var updateJobCard = function() {
                        var totalJobs = 0;
                        var searchTerm = $('.list-search__input').val();
                        var location = $('.list-select-input[data-filter=location]').attr('data-value');
                        var sorting = $('.list-select-input[data-filter=sorting]').attr('data-value');
                        var selectedCategory = $('.list-category .list-category__item.active').length > 0 ?
                            $('.list-category .list-category__item.active') :
                            $('.list-category .list-category__item');

                        // clear current displayed jobs
                        $('.list-function.active').removeClass('active');

                        if ($('.list-category .list-category__item.active').length == 1) {
                            var func = $('.list-category .list-category__item.active').attr('data-function');
                            $('.list-jumbotron').attr('class', 'list-jumbotron');
                            // $('.list-jumbotron').addClass('list-jumbotron--' + func);
                        } else {
                            $('.list-jumbotron').attr('class', 'list-jumbotron');
                        }

                        // get categories
                        selectedCategory.each(function(index) {
                            var func = $(this).attr('data-function');
                            var funcID = '#' + func;
                            var searchResult = jobs.searchJob(func, searchTerm, location, sorting);
                            // Limit to Six
                            searchResult = searchResult.slice(0, 6);

                            if (searchResult.length > 0) {
                                // remove current element
                                $(funcID).find('.list-function-divs').children().remove();
                                $(funcID).addClass('active');

                                var jobContainer = $(funcID).find('.list-function-divs');
                                var incrementTime = 0;

                                searchResult.forEach(function(job) {
                                    setTimeout(function() {
                                        jobContainer.append(
                                            createJobCard(
                                                job.shortcode,
                                                job.slug,
                                                job.title,
                                                job.plain_description,
                                                job.department,
                                                job.location.country
                                            )
                                        );
                                    }, incrementTime += 100);
                                });

                                // add total jobs
                                totalJobs += searchResult.length;
                            }
                        });


                        if (totalJobs < 1) {
                            // console.log('none');
                            $('.section_default').show();
                        } else {
                            $('.section_default').hide();
                        }

                        // display total result jobs
                        $('.list-helper-label__count').text(totalJobs);
                    };

                    /* ==================
                     * Jobs class (ES5)
                     * ================== */
                    var Jobs = function(jobsData) {
                        // private property
                        var jobList = JSON.parse(jobsData);

                        // methods
                        this.getJobs = function() { return jobList; };
                        this.getFunctionJobs = function(func) { return jobList[func]; };
                        this.searchJob = function(func, term, location, sorting) {
                            if (location === 'all') location = '';

                            // ignore if job in certain function is 0
                            if (!jobList[func]) return [];

                            var searchResult = jobList[func].filter(function(job) {
                                return (job.title.toLowerCase().indexOf(term.toLowerCase()) !== -1) &&
                                    (job.location.country_code.indexOf(location) !== -1);
                            });

                            return sorting === 'name' ?
                                searchResult.sort(function(jobA, jobB) {
                                    return jobA.title.toLowerCase() < jobB.title.toLowerCase() ? -1 : 1;
                                }) :
                                searchResult;
                        };
                    };

                    /* ==================
                     * Global variables
                     * ================== */
                    var body = $('body');
                    var jobs = new Jobs(jobsData);

                    // console.log(jobs.getJobs());

                    /* ====================
                     * Select input handler
                     * ==================== */
                    // show select options
                    $('.list-select-input').on('click', function(e) {
                        $(this).parent().toggleClass('active');
                        e.stopPropagation();
                    });

                    // change value on option click
                    $('.list-select-options').on('click', 'li', function(e) {
                        $(this).parent().parent().parent('.list-select-input').attr('data-value', $(this).attr('data-value'));
                        $(this).closest('.list-select').find('.list-select-input__text').text($(this).text());

                        if ($(this).closest('.list-select').hasClass('active')) $(this).closest('.list-select').removeClass('active');
                        e.stopPropagation();
                        // console.log('abc');
                    });

                    // hide select options on body click
                    $('body').on('click', function() {
                        $('.list-select.active').removeClass('active');
                    });

                    /* ====================
                     * Initialize slick
                     * ==================== */
                    $('.list-category-slick').slick({
                        infinite: false,
                        arrows: false,
                        slidesToShow: 3.6,
                        responsive: [{
                            breakpoint: 480,
                            settings: {
                                infinite: false,
                                arrows: false,
                                slidesToShow: 2.8
                            }
                        }]
                    });

                    /* ====================
                     * Toggle filter
                     * ==================== */
                    $('.list-filter-more').on('click', function() {
                        $(this).toggleClass('expanded');
                        $('body').find('.list-filter').slideToggle();
                        if ($(this).hasClass('expanded')) {
                            $(this).children('.list-filter-more__text').text('Less filter');
                        } else {
                            $(this).children('.list-filter-more__text').text('More filter');
                        }
                    });

                    /* ====================
                     * Toggle category
                     * ==================== */
                    $('.list-category__item').on('click', function() {
                        var dataFunction = $(this).attr('data-function');
                        var categoryElem = '.list-category__item[data-function=' + dataFunction + ']';

                        // if ($(categoryElem).hasClass('active')) {
                        //     pushToaster('Department ' + dataFunction + ' removed from result');
                        // } else {
                        //     pushToaster('Department ' + dataFunction + ' added to result');
                        // }

                        $(categoryElem).toggleClass('active');

                        updateJobCard();
                    });

                    /* ====================
                     * Sticky discovery
                     * ==================== */
                    // avoid error if not in joblist page
                    if ($('#job-list').length > 0) {
                        $(window).scroll(function() {
                            if ($(this).scrollTop() > $('#job-list').offset().top) {
                                if ($('#discovery').hasClass('fixed')) return;
                                $('#job-list').css('padding-top', $('#discovery').height() + 'px');
                                $('#discovery').addClass('fixed');
                                // $(".list-search__input, .list-select-input").css("height", "40px");
                                // $(".list-search__icon").css("top", "10px");
                                // $(".list-select-input__text").css("margin-top", "4px");
                                if ($(window).width() < 768) {
                                    $(".list-search").css("margin-bottom", "0px");
                                } else {
                                    $(".list-search").css("margin-bottom", "18px");
                                }
                                $(".list-select .btn--big").css("padding", "9px");
                                // $(".list-search__input").css("font-size", "14px");
                                // $(".list-select-input__text").css("font-size", "14px");
                                // $(".list-select-input__icon--location").css({
                                //     "background-size": "21px 20px",
                                //     "height": "22px"
                                // });
                                // $(".list-search__icon").css("background-size", "25px 18px");
                            } else {
                                if (!$('#discovery').hasClass('fixed')) return;
                                $('#job-list').css('padding-top', '0');
                                $('#discovery').removeClass('fixed');
                                // $(".list-search__input, .list-select-input").css("height", "52px");
                                // $(".list-search__icon").css("top", "15px");
                                // $(".list-select-input__text").css("margin-top", "9px");
                                if ($(window).width() < 768) {
                                    $(".list-search").css("margin-bottom", "0px");
                                } else {
                                    $(".list-search").css("margin-bottom", "18px");
                                }
                                $(".list-select .btn--big").css("padding", "15px");
                                // $(".list-search__input").css("font-size", "16px");
                                // $(".list-select-input__text").css("font-size", "16px");
                                // $(".list-select-input__icon--location").css({
                                //     "background-size": "21px 25px",
                                //     "height": "25px"
                                // });
                                // $(".list-search__icon").css("background-size", "25px 25px");
                            }
                        });
                    }

                    /* ====================
                     * Expand more jobs
                     * ==================== */
                    // $('.list-function__btn-more').on('click', function(e) {
                    //   e.preventDefault();

                    //   var incrementTime = 0;
                    //   var targetFunction = $(this).attr('data-function');
                    //   var jobContainer = $(this).closest('.list-function-divs');
                    //   var remainJobs = jobs.getFunctionJobs(targetFunction).slice(6);

                    //   // remove button
                    //   $(this).parent().remove();

                    //   remainJobs.forEach(function(job) {
                    //     setTimeout(function() {
                    //       jobContainer.append(
                    //         createJobCard(
                    //           job.shortcode,
                    //           job.slug,
                    //           job.title,
                    //           job.plain_description,
                    //           job.department,
                    //           job.location.country
                    //         )
                    //       );
                    //     }, incrementTime += 150);
                    //   });
                    // });

                    /* ======================
                     * Search handler
                     * ====================== */
                    // $('#search-form').submit(function(e) {
                    //     e.preventDefault();
                    //     e.stopPropagation();

                    //     updateJobCard();
                    // });
                    $('.list-search__btn, .jobs-search-btn').on('click', function(e){
                        e.preventDefault();
                        e.stopPropagation();

                        updateJobCard();
                    });

                });
            }
        },
        // Nakama Page, Index of Nakma
        'nakama_page': {
            init: function() {
                // JavaScript to be fired on the about us page
            }
        },

        // Function Page
        'function': {
            init: function() {
                $(document).ready(function() {

                    /* ================
                     * Tooltip
                     * ================ */
                    $(document).on('mouseenter', '*[data-tooltip]', function() {
                        var pos = $(this).position();
                        var width = $(this).outerWidth();
                        var position = "top";
                        var color = "black";
                        if ($(this).attr('data-tooltip-position')) {
                            position = $(this).attr('data-tooltip-position');
                        }
                        if ($(this).attr('data-tooltip-color')) {
                            color = $(this).attr('data-tooltip-color');
                        }
                        var new_tooltip = $('<div class="tkpd-tooltip" data-tooltip-position=' + position + ' data-tooltip-color=' + color + '>' + $(this).attr('data-tooltip') + '</div>').insertAfter($(this));

                        //if tooltip has more width then the object
                        var left = -(new_tooltip.innerWidth() - $(this).innerWidth()) / 2;
                        var top = -new_tooltip.innerHeight() - 10;
                        if (new_tooltip.innerWidth() > $(this).innerWidth()) {
                            if (position == "left") {
                                left = -new_tooltip.innerWidth() - 10;
                                if (new_tooltip.innerHeight() < $(this).innerHeight()) {
                                    top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
                                }
                                if (new_tooltip.innerHeight() > $(this).innerHeight()) {
                                    top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
                                }
                            }
                            if (position == "bottom") {
                                top = new_tooltip.innerHeight() + 20;
                            }
                            if (position == "right") {
                                left = $(this).innerWidth() + 10;
                                if (new_tooltip.innerHeight() < $(this).innerHeight()) {
                                    top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
                                }
                                if (new_tooltip.innerHeight() > $(this).innerHeight()) {
                                    top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
                                }
                            }
                        }
                        if (new_tooltip.innerWidth() < $(this).innerWidth()) {
                            left = ($(this).innerWidth() - new_tooltip.innerWidth()) / 2;
                            top = -new_tooltip.innerHeight() - 10;
                        }
                        new_tooltip.css({
                            top: pos.top + top + "px",
                            left: pos.left + left + 1 + "px"
                        });
                    });

                    $(document).on('mouseleave', '*[data-tooltip]', function() {
                        $(this).next().remove();
                    });

                    // end of tooltip


                    // Fixate Search Bar On Desktop While Scrolled
                    // =============================================
                    var searchOffset = $('.section_search').length > 0 ? $(".section_search").offset().top : 0;

                    function searchFixed() {
                        if ($('.section_search').length > 0) {
                            if ($(window).width() > 992) {
                                var top = $(window).scrollTop();

                                // console.log(top);
                                // console.log(searchOffset);

                                if ($(this).scrollTop() > searchOffset) {
                                    if ($('.section_search').hasClass('fixed')) return;
                                    // $('.section_thestory').css('padding-top', $('.section_search').height() + 'px');
                                    $('.section_search').addClass('fixed');
                                } else {
                                    if (!$('.section_search').hasClass('fixed')) return;
                                    // $('.section_thestory').css('padding-top', '0');
                                    $('.section_search').removeClass('fixed');
                                }
                            } else {
                                if (!$('.section_search').hasClass('fixed')) return;
                                $('.section_search').removeClass('fixed');
                            }
                        }
                    }

                    $(window).scroll(function() {
                        // searchFixed();
                    });
                    $(window).resize(function() {
                        // var searchOffset = $(".section_search").offset().top;
                        // searchFixed();
                    });
                    // searchFixed();
                });
            }
        },

        'explore': {
            init: function() {
                $(document).ready(function() {
                    var sections = document.querySelectorAll('.section'), i,j;
                    var values = [];
                    var isInViewport = function (elem) {
                        var bounding = elem.getBoundingClientRect();
                        return (
                            bounding.top >= 0 &&
                            bounding.left >= 0 &&
                            bounding.bottom <= ((window.innerHeight) || document.documentElement.clientHeight) &&
                            bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
                        );
                    };
                    
                    $('.navigation span').on("click",function(){
                        var index=$(this).index()+1;
                        $('html, body').stop( true, false ).animate({
                            scrollTop: $(".section"+index).offset().top-100
                        }, 1000);
                    });
                    if ($(window).width() > 922) {
                        new WOW().init();
                    }
                    function exploreNav() {
                        if ($(window).width() > 992) {
                            $('section').removeClass('active');
                            $('.navigation span').removeClass('active');
                            for (i = 0; i < sections.length; ++i) {
                                if(isInViewport(sections[i]))
                                {
                                    $('section'+(i+1)).addClass("active");
                                    $('.navigation span').eq(i).addClass("active");
                                }
                            }
                        }
                    }
                    function splitLetter(elmt) 
                    {
                        $(elmt).addClass("in-progress");
                        var newTexts = $(elmt).text().split(""); 
                        var tempWord = $(elmt).text();
                        $(elmt).text("");
                        for(j = 0; j < newTexts.length; j++ ) {
                            (function(j) {
                                setTimeout(function(){
                                    $(elmt).append($("<span class='text-wave-animation'>"+newTexts[j]+"</span>"));
                                    if(j == newTexts.length-1)
                                    {
                                        j++;
                                        (function(j) {
                                            setTimeout(function(){
                                                $(elmt).text(""+tempWord);
                                                $(elmt).removeClass("text-wave-animate in-progress");
                                            },((20*j)+800));
                                        })(j);
                                    }
                                },20*j);
                             })(j);
                        }
                    }
                    function WaveAnimate() {
                        var waves = document.querySelectorAll('.text-wave-animate');
                        if (waves.length) {
                            for (i = 0; i < waves.length; i++) {
                                if(isInViewport(waves[i]) && !$(waves[i]).hasClass("in-progress"))
                                {
                                    splitLetter(waves[i]);
                                }
                            }
                        }
                    }
                    $(window).scroll(function() {
                       exploreNav();
                       WaveAnimate();
                    });
                    $(window).resize(function() {
                        var searchOffset = $(".section_search").offset().top;
                    });
                });
            }
        },

        // Our Workplace
        'our_workplace': {
            init: function() {
                // Card Height Adjustor
                function cardHeightAdjust() {
                    var $imgContainerH = $('.ievent-box-img').outerHeight(true);
                    var $titleH = $('.ievent-box-ct__title').outerHeight(true);
                    var $dateH = $('.ievent-box-ct__date').outerHeight(true);

                    var totalH = $imgContainerH + $titleH + $dateH;

                    $('.ievent-box').height(totalH);
                    
                }

                // function on hover
                function onEventHoverIn() {
                    $(this).css('transform', 'translate3d(0, -41%, 0)');
                    $(this).parent().addClass('ievent-box--hovered');
                }
                function onEventHoverOut() {
                    $(this).css('transform', 'translate3d(0, 0, 0)');
                    $(this).parent().removeClass('ievent-box--hovered');
                }
                function extendEvent() {
                    $('.ievent-ext').toggle(500, function() {
                        // console.log('dank');
                    });
                }
        
                ///////////////////
                // Nakama Random //
                ///////////////////
                var nakamaray = {
                    "data": [
                        {
                            "name":"Leontinus Alpha Edison",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/leon.png"
                        },
                        {
                            "name":"Steven Anthony Wijaya",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/steve.png"
                        },
                        {
                            "name":"Fitri Ilma Naviati",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/fivi.png"
                        },
                        {
                            "name":"Khaidir Afif",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/afif.png"
                        },
                        {
                            "name":"Dicki Dahrurozak",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/dicki.png"
                        },
                        {
                            "name":"Alviani Ardisa",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/vivi.png"
                        },
                        {  
                            "name":"Yusin",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/yusin.png"
                        },
                        {  
                            "name":"Karina  Susilo",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/karina.png"
                        },
                        {  
                            "name":"Mellisa",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/cimel.png"
                        },
                        {  
                            "name":"Soni",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/soni.png"
                        },
                        {  
                            "name":"Kella",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/kella.png"
                        },
                        {  
                            "name":"Wiryo",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/wiryo.png"
                        },
                        {  
                            "name":"William",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/william.png"
                        },
                        {  
                            "name":"Rio",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rio.png"
                        },
                        {  
                            "name":"Rhoma",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rhoma.png"
                        },
                        {  
                            "name":"Radit",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/radit.png"
                        },
                        {  
                            "name":"Michelle",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/michele.png"
                        },
                        {  
                            "name":"Alvin",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/alvin.png"
                        },
                        {  
                            "name":"Aldo",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/aldo.png"
                        },
                        {  
                            "name":"Aji",
                            "picture":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/aji.png"
                        },

                    ]
                };

                function getRandomImage(array) {
                    var len = array.length;
                    var getRand = Math.floor(Math.random() * len) + 1;
                    return array.data[getRand].picture;
                }
                
                function nakamaClassSlick() {
                    if( $(window).width() < 768 ) {
                        $('.class-images').slick({
                            infinite: false,
                            dots: false,
                            arrows: false,
                            slidesToShow: 1,
                            centerMode: true,
                            centerPadding: '10%'
                        });
                    }
                }
                function nakamaSportSlick() {
                    if( $(window).width() < 768 ) {
                        $('.sport-images').slick({
                            infinite: false,
                            dots: false,
                            arrows: false,
                            slidesToShow: 1,
                            centerMode: true,
                            centerPadding: '10%'
                        });
                    }
                }

                // OnLoad
                $(document).ready(function(){
                    // Ievent
                    cardHeightAdjust();

                    $('.ievent-wrapper').hover( onEventHoverIn, onEventHoverOut );
                    $('.ievent-more').click(extendEvent);
                    
                    var len = nakamaray.data.length;
                    var randed = [];

                    // nakamaray.data.splice(0, 1);
                    // console.log(nakamaray.data);


                    if($('.nakama-img').length > 0) {
                        for( var i = 0; i < 6; i++ ) {
                            var newLen = nakamaray.data.length;
                            var getRand = Math.floor(Math.random() * newLen);

                            var nakamaDOM = '<div class="nakama-img-ct nakamanimate" style="animation-delay:'+200*i+'ms;">' +
                                    '<img src="'+nakamaray.data[getRand].picture+'" alt="" class="nakama-img-ct__img">' +
                                '</div>';
                            $('.nakama-img').append(nakamaDOM);

                            nakamaray.data.splice(getRand, 1);

                        }
                    }

                    $('[data-gallery]').on('click', function() {
                        var pswpElement = document.querySelectorAll('.pswp')[0];

                        var items = [];
                        var urls = $(this).data('gallery');
                        // console.log(items);
                        // console.log(JSON.parse(items));
                        urls.data.forEach(function(el){
                            items.push(el);
                        });


                        // define options (if needed)
                        var options = {
                            index: 0 // start at first slide
                        };

                        // Initializes and opens PhotoSwipe
                        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                        gallery.listen('gettingData', function(index, item) {
                                if (item.w < 1 || item.h < 1) { // unknown size
                                var img = new Image(); 
                                img.onload = function() { // will get size after load
                                    item.w = this.width; // set image width
                                    item.h = this.height; // set image height
                                    gallery.invalidateCurrItems(); // reinit Items
                                    gallery.updateSize(true); // reinit Items
                                };
                            img.src = item.src; // let's download image
                            }
                        });
                        gallery.init();
                    });

                    nakamaClassSlick();
                    nakamaSportSlick();

                });
            }
        },

        // Our Community (User underscore)
        'our_community': {
            init: function() {
                // Card Height Adjustor
                function cardHeightAdjust() {
                    var $imgContainerH = $('.ievent-box-img').outerHeight(true);
                    var $titleH = $('.ievent-box-ct__title').outerHeight(true);
                    var $dateH = $('.ievent-box-ct__date').outerHeight(true);

                    var totalH = $imgContainerH + $titleH + $dateH;

                    $('.ievent-box').height(totalH);
                    
                }

                // function on hover
                function onEventHoverIn() {
                    $(this).css('transform', 'translate3d(0, -41%, 0)');
                    $(this).parent().addClass('ievent-box--hovered');
                }
                function onEventHoverOut() {
                    $(this).css('transform', 'translate3d(0, 0, 0)');
                    $(this).parent().removeClass('ievent-box--hovered');
                }
                function extendEvent() {
                    $('.ievent-ext').toggle(500, function() {
                        // console.log('dank');
                    });
                }

                // OnLoad
                $(document).ready(function(){
                    cardHeightAdjust();

                    $('.ievent-wrapper').hover( onEventHoverIn, onEventHoverOut );
					$('.ievent-more').click(extendEvent);
					
					

                    if( $(window).width() < 768 ) {
                        $('.ievent .row').slick({
                            infinite: false,
                            dots: false,
							arrows: false,
                            slidesToShow: 1.1
                        });
					}
					// else {
					// 	$('.ievent .row-institute').slick({
                    //         infinite: false,
                    //         dots: false,
					// 		arrows: true,
					// 		prevArrow: '<div class="slick-prev"></div>',
					// 		nextArrow: '<div class="slick-next"></div>',
                    //         slidesToShow: 5
                    //     });
					// }
                });
            }
        },

        'blogs': {
            init: function() {
                $(document).ready(function() {
                    
                    if( $(window).width() < 768 ) {
                        $('.section-blog .row').slick({
                            infinite: false,
                            dots: false,
                            arrows: false,
                            slidesToShow: 1.1
                        });
                    }

                });
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
