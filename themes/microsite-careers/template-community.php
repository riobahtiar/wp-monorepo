<?php
/**
* Template Name:  Career Our Community
*/
?>
<?php
	$workable      = new Careers\workable;
	$abcde         = $workable->get_jobs( 99 );
	$raw           = $workable->get_grouped_jobs( true );
	$the_shortcode = '';
	// Send Jobs Data to Javascript
	wp_localize_script( 'sage/js', 'jobsData', json_encode( $raw ) );
?>

<div class="breadcrumb-wrapper margin">
	<div class="container">
		<div class="careers-breadcrumb careers-breadcrumb--padding">
			<a href="../">
				<div>Career</div>
			</a>
			<a href="./">
				<div>Our Community</div>
			</a>
		</div>
	</div>
</div>

<!-- <section class="section-ibanner">
    <div class="ibanner">
        <div class="container">
            <div class="ibanner-content">
                <h3 class="ibanner-content__title">
                    Our Community
                </h3>
                <div class="ibanner-content__line"></div>
                <p class="ibanner-content__desc">
                    Our Nakama are passionate people. 
                    We enjoy learning as much as we enjoy sharing.
                     What better way to learn and share than to organize an activity 
                     that will engage you with other Nakama across departments?
                </p>
            </div>
        </div>
    </div>
    <div class="section-ibanner__l"></div>
    <div class="section-ibanner__r"></div>
</section> -->

<section class="section-institute">
    <div class="container">
        <div class="institute">
            <div class="institute-img">
                <img src="https://ecs7.tokopedia.net/assets/images/careers/institute-hero.svg" alt="" class="institute-img__img">
            </div>
            <div class="institute-content">
                <h3 class="institute-content__title">
                    Tokopedia Institute
                </h3>
                <div class="institute-content__line"></div>
                <p class="institute-content__desc">
                    We organize events for university students to give them 
                    insights and share inspirations. We are building a company 
                    with a strong mission, thus we want to encourage the 
                    new aspiring generation to be participative and inclusive.
                </p>
            </div>
        </div>
    </div>
    <img class="section-institute__bg1" src="https://ecs7.tokopedia.net/assets/images/careers/rectangle-26-copy-4.svg" alt="">
</section>

<section class="section-ievent">
    <div class="container container-ievent">
        <div class="ievent">
            <div class="row row-institute">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/devcamp%402x.png" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Tokopedia DevCamp
                                </h3>
                                <h3 class="ievent-box-ct__date">
                                    UPCOMING
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    Training and Hackathon program for university students with programming background during their final year
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <a href="http://tkp.me/dev-camp-2018">
                                    <button class="ievent-box-btn__btn1 unf-btn unf-btn--small unf-btn--no-shadow ripple-effect ripple-main">
                                        Register<span class="text">Register</span>
                                    </button>
                                </a>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary unf-btn--no-shadow ripple-effect ripple-main"
                                        data-img="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/devcamp%402x.png" 
                                        data-modal="true" 
                                        data-what="The intensive training and hackathon program is designed for the final year or postgraduate students from an IT background to compete and to create products and innovations. The selected students will be coached and trained by mentors from Tokopedia and the winner will get a special invitation to become full-time engineers in Tokopedia."
                                        data-title="Tokopedia DevCamp" 
                                        data-date="UPCOMING"
                                        data-link="http://tkp.me/dev-camp-2018">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/scholarship.png" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Tokopedia Scholarship
                                </h3>
                                <h3 class="ievent-box-ct__date">
                                    UPCOMING
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    We grant scholarship to the best student from Universities across Indonesia 
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <a href="http://tkp.me/tokopedia-scholarship">
                                    <button class="ievent-box-btn__btn1 unf-btn unf-btn--small unf-btn--no-shadow ripple-effect ripple-main">
                                        Register<span class="text">Register</span>
                                    </button>
                                </a>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--no-shadow unf-btn--secondary ripple-effect ripple-main" 
                                        data-img="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/scholarship.png" 
                                        data-modal="true" 
                                        data-title="Tokopedia Scholarship" 
                                        data-what="We want to encourage people to dream big and given the equal chance the dream, starting from the very early stage of their career life. Tokopedia grants scholarship to high-performing students to support them in fostering their aspiration and encourage their passion."
                                        data-date="UPCOMING" 
                                        data-link="http://tkp.me/tokopedia-scholarship" 
                                        >
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/gitu2.png" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Tokopedia Goes to Campus
                                </h3>
                                <h3 class="ievent-box-ct__date passed">
                                    PASSED
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    Roadshow to universities in Indonesia in the mission to educate students on various topics
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <button class="ievent-box-btn__btn1 unf-btn unf-btn--small unf-btn--no-shadow ripple-effect ripple-main" disabled>
                                    Register<span class="text">Register</span>
                                </button>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary unf-btn--no-shadow ripple-effect ripple-main" 
                                        data-modal="true" 
                                        data-img="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/gitu2.png" 
                                        data-what="We did roadshows across universities in Indonesia to educate students on various topics. We invited alumni to share their experiences and insights for the university students, in the hope to open their mind, provide perspectives, and encourage them to pursue their passion." 
                                        data-link="#" 
                                        data-date="PASSED" 
                                        data-title="Tokopedia Goes to Campus">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets/images/careers/intern-day.svg" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Intern Day
                                </h3>
                                <h3 class="ievent-box-ct__date passed">
                                    PASSED
                                </h3>
                                <p class="ievent-box-ct__desc">
									To us, internship should provide opportunities to confound what has been learnt and by joining our internship program, we are eager to provide a more practical knowledge implementation that could help develop our interns personal growth. We value our interns as much as our Nakama and we are keen to strengthen our bond that would inspire a more meaningful relationship between our interns and Tokopedia. Do join our Intern Day, share your experience and expand your networking!
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <button class="ievent-box-btn__btn1 unf-btn unf-btn--small unf-btn--no-shadow ripple-effect ripple-main" disabled>
                                    Register<span class="text">Register</span>
                                </button>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary unf-btn--no-shadow ripple-effect ripple-main" 
                                        data-modal="true" 
                                        data-img="https://ecs7.tokopedia.net/assets/images/careers/intern-day.svg" 
                                        data-what="To us, internship should provide opportunities to confound what has been learnt and by joining our internship program, we are eager to provide a more practical knowledge implementation that could help develop our interns personal growth. We value our interns as much as our Nakama and we are keen to strengthen our bond that would inspire a more meaningful relationship between our interns and Tokopedia. Do join our Intern Day, share your experience and expand your networking!" 
                                        data-link="#" 
                                        data-date="PASSED" 
                                        data-title="Intern Day">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets/images/careers/data-exclusive-workshop.svg" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
									Data Exclusive Workshop
                                </h3>
                                <h3 class="ievent-box-ct__date passed">
                                    PASSED
                                </h3>
                                <p class="ievent-box-ct__desc">
									Tokopedia is a data-driven organization that believes in data being a critical asset in every decision making process. We want to help university students get better access to learn more about how to analyze and visualize data by giving them 1 day intensive workshop for 30 selected students.
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <button class="ievent-box-btn__btn1 unf-btn unf-btn--small unf-btn--no-shadow ripple-effect ripple-main" disabled>
                                    Register<span class="text">Register</span>
                                </button>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary unf-btn--no-shadow ripple-effect ripple-main" 
                                        data-modal="true" 
                                        data-img="https://ecs7.tokopedia.net/assets/images/careers/data-exclusive-workshop.svg" 
                                        data-what="Tokopedia is a data-driven organization that believes in data being a critical asset in every decision making process. We want to help university students get better access to learn more about how to analyze and visualize data by giving them 1 day intensive workshop for 30 selected students." 
                                        data-link="#" 
                                        data-date="PASSED" 
                                        data-title="Data Exclusive Workshop">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="ievent-more">
            <button class="ievent-more__btn unf-btn unf-btn--small unf-btn--secondary ripple-effect ripple-main">
                See More<span class="text">See More</span>
            </button>
        </div> -->
    </div>
    <!-- <img class="section-ievent__cl" src="https://ecs7.tokopedia.net/assets/images/careers/careers-oc-circleft.svg" alt=""> -->
</section>

<section class="section-circle">
    <div class="container">
        <div class="circle-com">
            <div class="circle-com-img">
                <img src="https://ecs7.tokopedia.net/assets/images/careers/circle-hero.svg" alt="" class="circle-com-img__img">
            </div>
            <div class="circle-com-content">
                <h3 class="circle-com-content__title">
                    Tokopedia Circle
                </h3>
                <div class="circle-com-content__line"></div>
                <p class="circle-com-content__desc">
                    We believe learning is an endless journey. 
                    We are always open to sharing the journey and experience 
                    with like-minded people from across the industry. 
                    We organize events and invite experienced professionals to meet and connect.
                </p>
            </div>
        </div>
    </div>
    <img class="section-circle__bg1" src="https://ecs7.tokopedia.net/assets/images/careers/rectangle-26-copy-2.svg" alt="">
</section>

<section class="section-ievent">
    <div class="container">
        <div class="ievent">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/techabreak.png" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Tech a Break
                                </h3>
                                <h3 class="ievent-box-ct__date">
                                    UPCOMING
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    We held an external sharing session talking about technology. 
                                    We invite tech people from various places to share insights 
                                    and ideas together with Tokopedia.
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <a href="https://www.eventbrite.com/e/tech-a-break-24-be-ahead-of-the-game-using-machine-learning-tickets-48437283218#">
                                    <button class="ievent-box-btn__btn1 unf-btn unf-btn--small unf-btn--no-shadow ripple-effect ripple-main">
                                        Register<span class="text">Register</span>
                                    </button>
                                </a>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary unf-btn--no-shadow ripple-effect ripple-main" 
                                        data-modal="true" 
                                        data-what="The evolution of technology has revolutionized the life of many. It changes the way we do things and help us break through barriers. It is aligned with our mission to democratize commerce through technology. As a technology company, Tokopedia strives to create an environment where people can do anything that they dream of. Make it happen, make it better. Technology has been advancing at rapid speed and we have to have growth mindset to keep up with it. In Tech a Break, we collaborate with tech enthusiasts and discuss in a panel on various topics related to technology. The speakers shared their knowledge, insights, tips and tricks, difficulties, and solutions to overcome them." 
                                        data-title="Tech a Break" 
                                        data-date="UPCOMING" 
                                        data-img="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/techabreak.png" 
                                        data-link="https://www.eventbrite.com/e/tech-a-break-24-be-ahead-of-the-game-using-machine-learning-tickets-48437283218#">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/bgp.png" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Behind Great Product
                                </h3>
                                <h3 class="ievent-box-ct__date">
                                    UPCOMING
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    We talked about product design with other UI UX people in the industry. 
                                    We discuss on recent development, new ideas, and many more.
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <a href="https://www.eventbrite.com/e/tokopedia-behind-great-product-5-tickets-48586727209#">
                                    <button class="ievent-box-btn__btn1 unf-btn unf-btn--small unf-btn--no-shadow ripple-effect ripple-main">
                                        Register<span class="text">Register</span>
                                    </button>
                                </a>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary unf-btn--no-shadow ripple-effect ripple-main" 
                                        data-modal="true" 
                                        data-what="Innovation is the key to progress. Every day, we are working towards bringing a new way to solve problems that most people would rather not have to deal with. We are aiming to create and evolve our product offerings that will truly make people’s lives better. This is what makes our work so meaningful. We are in the Golden Age of innovation. Designing products are not just about creating a new category, it is about resetting how we do things. We want to create products that push boundaries in so many ways. We like to hear and share insights and ideas with people from similar industry. We invited people who do UI and UX related work who are always hungry to learn and to innovate." 
                                        data-title="Behind Great Product" 
                                        data-date="UPCOMING" 
                                        data-img="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/bgp.png" 
                                        data-link="https://www.eventbrite.com/e/tokopedia-behind-great-product-5-tickets-48586727209#">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/collab.png" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Collaboration Events
                                </h3>
                                <h3 class="ievent-box-ct__date passed">
                                    PASSED
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    Tokopedia collaborates with a lot of communities. We are opening our door and facilitating a place for people to organize a meet up and to hold a sharing session in Tokopedia Tower.
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <button class="ievent-box-btn__btn1 unf-btn unf-btn--small unf-btn--no-shadow ripple-effect ripple-main" disabled>
                                    Register<span class="text">Register</span>
                                </button>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary unf-btn--no-shadow ripple-effect ripple-main" 
                                        data-modal="true" 
                                        data-what="Tokopedia believes learning is an endless journey and everything is a work in progress. We cultivate our people to have the sincerity to share like a teacher and the humility and curiosity to learn like a student. We always encourage collaboration in the workplace to keep us moving. We like to learn from like-minded people and facilitating the discussion in our space. At our new Tokopedia tower, we have plenty of rooms for everyone! We like to encourage people to speak more about their dream and passion and talk about how to make it come true." 
                                        data-title="Collaboration Events" 
                                        data-date="PASSED" 
                                        data-img="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/collab.png" 
                                        data-link="#">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="http://placekitten.com/315/280" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Tokopedia Goes to Campus
                                </h3>
                                <h3 class="ievent-box-ct__date">
                                    8 May 2018
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    Much like Noah's Ark, the entirety of 
                                    Tokopedia can be described as several different ships combined into one. 
                                    In the ocean of opportunities, 
                                    it's not uncommon for ships to run
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <div class="ievent-box-btn__btn1 btn-tkpd btn-tkpd--green btn-tkpd--medium ripple-effect ripple-main">
                                    <span class="btn-tkpd__text">Register</span>
                                </div>
                                <div class="ievent-box-btn__btn2 btn-tkpd btn-tkpd--lgreen btn-tkpd--medium ripple-effect ripple-main">
                                    <span class="btn-tkpd__text">See Details</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <!-- <img class="section-ievent__cr" src="https://ecs7.tokopedia.net/assets/images/careers/careers-oc-circright.svg" alt=""> -->
</section>

<!-- Community Modal -->

<div class="modal--comm-overlay"></div>
<div class="modal--comm">
	<div class="modal--comm-left">
		<div class="modal--comm-lc">
			<h2>What is</h2>
			<p>
				Much like Noah's Ark, the entirety of Tokopedia can be described as several different ships combined into one. In the ocean of opportunities, it's not uncommon for ships to run into obstacles like storms and icebergs, just ask Titanic. At Tokopedia, we believe that inspiration means different things to different people, but the crew's hard work will always keep the ship moving.
			</p>
		</div>
	</div>
	<div class="modal--comm-right">
		<div class="modal--comm-right-img">
			<img src="http://placekitten.com/300/500" alt="" class="modal--comm-right-img__img">
		</div>
		<div class="modal--comm-rc">
			<h2 class="modal--comm-rc__title">
				Behind Great Product
			</h2>
			<h3 class="modal--comm-rc__date">
				8 May 2018
			</h3>
			<a href="" class="modal--comm-rc__link">
				<button class="ievent-box-btn__btn1 unf-btn unf-btn--small ripple-effect ripple-main">
					Register<span class="text">Register</span>
				</button>
			</a>
			<!-- <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary ripple-effect ripple-main" data-modal="true">
				See Details<span class="text">See Details</span>
			</button> -->
		</div>
	</div>
	<img class="modal--comm__closebtn" src="https://ecs7.tokopedia.net/assets/images/careers/careers-oc-modal-closebtn.svg" alt="">
</div>

