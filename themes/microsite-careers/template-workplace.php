<?php
/**
* Template Name:  Career Our Workplace
*/
?>

<?php
	$workable      = new Careers\workable;
	$abcde         = $workable->get_jobs( 99 );
	$raw           = $workable->get_grouped_jobs( true );
	$the_shortcode = '';
	// Send Jobs Data to Javascript
	wp_localize_script( 'sage/js', 'jobsData', json_encode( $raw ) );
?>

<div class="breadcrumb-wrapper margin">
	<div class="container">
		<div class="careers-breadcrumb careers-breadcrumb--padding">
			<a href="../">
				<div>Career</div>
			</a>
			<a href="./">
				<div>Our Workplace</div>
			</a>
		</div>
	</div>
</div>

<!-- <section class="section-workplace-banner">
    <div class="wb">
        <div class="container">
            <div class="wb-header">
                <h2 class="wb-header__title">Our Workplace</h2>
                <div class="wb-header__line"></div>
            </div>
            <p class="wb__copy">
                Every day, we create innovations while building a healthy workplace. 
                We believe that happiness is the key to productivity. 
                A happy and passionate Nakama is the best Nakama, and 
                that’s when we came up with the best products and solutions for our users.
            </p>
        </div>
    </div>
    <div class="section-workplace-banner__l"></div>
    <div class="section-workplace-banner__r"></div>
</section> -->

<!-- Life at Tokopedia -->
<section class="section-life">
	<div class="life">
		<div class="container">
			<div class="life-content">
				<h3 class="life-content__title">
					See How We are Still the Number One Preferred
					E-Commerce in Indonesia
				</h3>
				<!-- <p class="life-content__desc">
					We are holding to these three principles that drive us forward. 
					We call them our DNA, the traits that embody who we are as Tokopedia Nakama.
				</p> -->
				<div class="life-content-btn">
					<a href="https://www.youtube.com/tokopedia">
						<button class="life-content-btn__btn unf-btn unf-btn--small">
							View All Videos<div class="text">View All Videos</div>
						</button>
					</a>
				</div>
			</div>
			<div class="life-video" data-video="https://www.youtube.com/embed/LowfXMQzw70">
				<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/ca-wp-nakama.png" alt="" class="life-video__thumbnail">
				<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-play--green.svg" alt="" class="life-video__play">
			</div>
			<div class="life-content-btn--mobile">
				<a href="https://www.youtube.com/tokopedia">
					<button class="life-content-btn__btn unf-btn unf-btn--small">
						View All Videos<div class="text">View All Videos</div>
					</button>
				</a>
			</div>
		</div>
	</div>
	<img src="https://ecs7.tokopedia.net/assets/images/careers/careers-life-acc1.svg" alt="" class="section-life__acc1">
	<img src="https://ecs7.tokopedia.net/assets/images/careers/careers-life-acc2.svg" alt="" class="section-life__acc2">
</section>

<section class="section_jobs">
	<div class="container">
		<div class="teams-header">
			<h2 class="teams-header__h2">Our Teams</h2>
			<div class="teams-header__line"></div>
		</div>
		<p class="teams__content">
		In Tokopedia, you can do things that you love and grow yourself while creating 
		a useful platform that will change the future of commerce. 
		We do work with purpose. We believe that everyone has the
		 chance to find their stands against all odds and win. 
		 We are building an ecosystem where anyone can find everything and start anything.
		</p>
	</div>
	<!-- Jobs Header -->
	<div class="container">

		<div class="floater-wrapper clearfix">
			<div class="clearfix">
				<!-- Business -->
				<div class="floater">
					<div class="jobs-item ji1">
						<div class="jobs-item-flag flag-business"></div>
						<div class="jobs-item-image">
							<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/Business.png" alt="Business" class="jobs-item-image__image">
						</div>
						<div>
							<h2 class="jobs-item-content__title text-center">
								Business
							</h2>
						</div>
						<div class="jobs-item-content text-center">
							<h3 class="jobs-item-content__body">
								Foresee business opportunities and strategize business plan to create a valuable marketing actions
							</h3>
						</div>

						<div class="jobs-item-link fs--14 fw--smb text-center">
							<a href="../function/business">
								<span id="business-len" class="jobs-item-link__link--disabled">No Roles Available</span>
							</a>
						</div>
					</div>
				</div>
				<!-- Finance -->
				<div class="floater">
					<div class="jobs-item ji2">
						<div class="jobs-item-flag flag-finance"></div>
						<div class="jobs-item-image">
							<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/Finance.png" alt="Finance" class="jobs-item-image__image">
						</div>
						<div>
							<h2 class="jobs-item-content__title text-center">
								Finance
							</h2>
						</div>
						<div class="jobs-item-content text-center">
							<h3 class="jobs-item-content__body">
								Plan, control, and monitor the financial purpose
							</h3>
						</div>

						<div class="jobs-item-link fs--14 fw--smb text-center">
							<a href="../function/finance">
								<span id="finance-len" class="jobs-item-link__link--disabled">No Roles Available</span>
							</a>
						</div>
					</div>
				</div>
				<!-- Operational -->
				<div class="floater">
					<div class="jobs-item ji3">
						<div class="jobs-item-flag flag-business"></div>
						<div class="jobs-item-image">
							<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/Operational.png" alt="Operational" class="jobs-item-image__image">
						</div>
						<div>
							<h2 class="jobs-item-content__title text-center">
								Operational
							</h2>
						</div>
						<div class="jobs-item-content text-center">
							<h3 class="jobs-item-content__body">
								Monitor, assist, and solve customer’s problem professionally through providing valuable guidance
							</h3>
						</div>

						<div class="jobs-item-link fs--14 fw--smb text-center">
							<a href="../function/operational">
								<span id="operational-len" class="jobs-item-link__link--disabled">No Roles Available</span>
							</a>
						</div>
					</div>
				</div>
				<!-- People -->
				<div class="floater">
					<div class="jobs-item ji4">
						<div class="jobs-item-flag flag-people"></div>
						<div class="jobs-item-image">
							<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/People.png" alt="People" class="jobs-item-image__image">
						</div>
						<div>
							<h2 class="jobs-item-content__title text-center">
								People
							</h2>
						</div>
						<div class="jobs-item-content text-center">
							<h3 class="jobs-item-content__body">
								Manage, coordinate, and collaborate with the Nakamas to create a healthy workplace
							</h3>
						</div>

						<div class="jobs-item-link fs--14 fw--smb text-center">
							<a href="../function/people">
								<span id="people-len" class="jobs-item-link__link--disabled">No Roles Available</span>
							</a>
						</div>
					</div>
				</div>
				<!-- Product -->
				<div class="floater">
					<div class="jobs-item ji5">
						<div class="jobs-item-flag flag-product"></div>
						<div class="jobs-item-image">
							<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/Product.png" alt="Product" class="jobs-item-image__image">
						</div>
						<div>
							<h2 class="jobs-item-content__title text-center">
								Product
							</h2>
						</div>
						<div class="jobs-item-content text-center">
							<h3 class="jobs-item-content__body">
								Design, build, and beautify the visual of the product
							</h3>
						</div>

						<div class="jobs-item-link fs--14 fw--smb text-center">
							<a href="../function/product">
								<span id="product-len" class="jobs-item-link__link--disabled">No Roles Available</span>
							</a>
						</div>
					</div>
				</div>
				<!-- Technology -->
				<div class="floater">
					<div class="jobs-item ji6">
						<div class="jobs-item-flag flag-technology"></div>
						<div class="jobs-item-image">
							<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/Technology.png" alt="Technology" class="jobs-item-image__image">
						</div>
						<div>
							<h2 class="jobs-item-content__title text-center">
								Technology
							</h2>
						</div>
						<div class="jobs-item-content text-center">
							<h3 class="jobs-item-content__body">
								Build and maintain the architecture of the product
							</h3>
						</div>

						<div class="jobs-item-link fs--14 fw--smb text-center">
							<a href="../function/technology">
								<span id="technology-len" class="jobs-item-link__link--disabled">No Roles Available</span>
							</a>
						</div>
					</div>
				</div>
				<!-- Internship -->
				<div class="floater">
					<div class="jobs-item ji7">
						<div class="jobs-item-flag flag-internship"></div>
						<div class="jobs-item-image">
							<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/Internship.png" alt="Internship" class="jobs-item-image__image">
						</div>
						<div>
							<h2 class="jobs-item-content__title text-center">
								Internship
							</h2>
						</div>
						<div class="jobs-item-content text-center">
							<h3 class="jobs-item-content__body">
								Learn and experience through project contribution
							</h3>
						</div>

						<div class="jobs-item-link fs--14 fw--smb text-center">
							<a href="../function/internship">
								<span id="internship-len" class="jobs-item-link__link--disabled">No Roles Available</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Internship -->
			<!-- <div>
				<div class="floater floater7">
					<div class="jobs-item ji7">
						<div class="jobs-item-flag flag-internship"></div>
						<div class="jobs-item-image">
							<img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/Internship.png" alt="Internship" class="jobs-item-image__image">
						</div>
						<div>
							<h2 class="jobs-item-content__title txt--black txt--secondary fw--smb text-center">
								Internship
							</h2>
						</div>
						<div class="jobs-item-content text-center">
							<h3 class="jobs-item-content__body txt--black txt--secondary">
								Learn and experience through project contribution
							</h3>
						</div>

						<div class="jobs-item-link fs--14 fw--smb text-center">
							<a href="../function/internship">
								<span id="internship-len" class="jobs-item-link__link--disabled">No Roles Available</span>
							</a>
						</div>
					</div>
				</div>
			</div> -->

		</div>

		<!-- <div class="text-center">
			<a href="../jobs" class="btn-tkpd btn-tkpd--large btn-tkpd--sgreen ripple-effect ripple-main">
				<span class="btn-tkpd__text">View All Jobs</span>
			</a>
		</div> -->
	</div>
</section>

<div class="modal--comm-overlay modal--video-ovl"></div>
<div class="modal--video">
	<video src="https://ecs7.tokopedia.net/assets/media/careers/recruitment-process.mp4"></video>
	<img class="modal--video__closebtn" src="https://ecs7.tokopedia.net/assets/images/careers/careers-oc-modal-closebtn.svg" alt="">
</div>
<div class="modal--comm">
	<div class="modal--comm-left">
		<div class="modal--comm-lc">
			<h2>What is</h2>
			<p>
				Much like Noah's Ark, the entirety of Tokopedia can be described as several different ships combined into one. In the ocean of opportunities, it's not uncommon for ships to run into obstacles like storms and icebergs, just ask Titanic. At Tokopedia, we believe that inspiration means different things to different people, but the crew's hard work will always keep the ship moving.
			</p>
		</div>
	</div>
	<div class="modal--comm-right">
		<div class="modal--comm-right-img">
			<img src="http://placekitten.com/300/500" alt="" class="modal--comm-right-img__img">
		</div>
		<div class="modal--comm-rc">
			<h2 class="modal--comm-rc__title">
				Behind Great Product
			</h2>
			<h3 class="modal--comm-rc__date">
				8 May 2018
			</h3>
			<a href="" class="modal--comm-rc__link">
				<button class="ievent-box-btn__btn1 unf-btn unf-btn--small ripple-effect ripple-main">
					Register<span class="text">Register</span>
				</button>
			</a>
			<!-- <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary ripple-effect ripple-main" data-modal="true">
				See Details<span class="text">See Details</span>
			</button> -->
		</div>
	</div>
	<img class="modal--comm__closebtn" src="https://ecs7.tokopedia.net/assets/images/careers/careers-oc-modal-closebtn.svg" alt="">
</div>

<!-- Nakama -->
<section class="section-nakama">
	<div class="nakama">
		<div class="container">
			<div class="nakama-desc">
				<div class="nakama-desc-header">
					<h2 class="nakama-desc-header__title">
						Nakama
					</h2>
					<div class="nakama-desc-header__line"></div>
				</div>
				<p class="nakama-desc__desc">
					Our Nakama are the best talents that share the 
					same strong belief that anyone, regardless who they 
					are and where they are from, can dream of anything and 
					follow their passion. Together, we are dedicated to 
					democratizing commerce through technology.
				</p>
			</div>
			<div class="nakama-img">
				<img class="nakama-img__acc1 nakamanimate" src="https://ecs7.tokopedia.net/assets/images/careers/ca-wp-letbulet.svg" alt="">
				<img class="nakama-img__acc2 nakamanimate" src="https://ecs7.tokopedia.net/assets/images/careers/ca-wp-letbulet.svg" alt="">
				<img class="nakama-img__acc3 nakamanimate" src="https://tokopedia-upload.s3.amazonaws.com/assets/images/careers/ca-wp-takkotak.svg" alt="">
				<img class="nakama-img__acc4 nakamanimate" src="https://tokopedia-upload.s3.amazonaws.com/assets/images/careers/ca-wp-takkotak.svg" alt="">
			</div>
		</div>
	</div>
	<img src="https://ecs7.tokopedia.net/assets/images/careers/careers-nakama-acc2.svg" alt="" class="section-nakama__acc2">
</section>

<!-- Nakama Class -->
<section class="section-class">
    <div class="class">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="class-desc">
                            <h3 class="class-desc__title">
                                Nakama Class
                            </h3>
                            <p class="class-desc__desc">
                                Nakama class is dedicated for Nakama and held by Nakama who 
                                has specialty or skill in a specific field. 
                                These fun, engaging classes encourage collaboration 
                                and it is one of the touchpoints where we get to 
                                know other Nakama from other functions who share the same interests.
                            </p>
                        </div>
                        <div class="class-images">
                            <div class="col-sm-6">
                                <div class="class-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/YOGA-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/YOGA-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/YOGA-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/YOGA-4.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/YOGA-5.jpg","w":0,"h":0}]}'>
                                    <div class="class-images-overlay">
                                        <div class="class-images-overlay-ct">
                                            <h3 class="class-images-overlay-ct__h3">
                                                Yoga Class
                                            </h3>
                                            <div class="class-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Yoga.png" alt="" class="class-images__img">
                                </div>
                                <h3 class="class-images__caption">Yoga Class</h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="class-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/ZUMBA-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/ZUMBA-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/ZUMBA-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/ZUMBA-4.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/ZUMBA-5.jpg","w":0,"h":0}]}'>
                                    <div class="class-images-overlay">
                                        <div class="class-images-overlay-ct">
                                            <h3 class="class-images-overlay-ct__h3">
                                                Zumba Class
                                            </h3>
                                            <div class="class-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Zumba.png" alt="" class="class-images__img">
                                </div>
                                <h3 class="class-images__caption">Zumba Class</h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="class-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/JAPANESE-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/JAPANESE-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/JAPANESE-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/JAPANESE-4.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/JAPANESE-5.jpg","w":0,"h":0}]}'>
                                    <div class="class-images-overlay">
                                        <div class="class-images-overlay-ct">
                                            <h3 class="class-images-overlay-ct__h3">
                                                Japanese Class
                                            </h3>
                                            <div class="class-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Japanese.png" alt="" class="class-images__img">
                                </div>
                                <h3 class="class-images__caption">Japanese Class</h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="class-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/DANCE-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/DANCE-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/DANCE-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/DANCE-4.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/DANCE-5.jpg","w":0,"h":0}]}'>
                                    <div class="class-images-overlay">
                                        <div class="class-images-overlay-ct">
                                            <h3 class="class-images-overlay-ct__h3">
                                                Dance Class
                                            </h3>
                                            <div class="class-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Dance.png" alt="" class="class-images__img">
                                </div>
                                <h3 class="class-images__caption">Dance Class</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img class="section-class__acc1" src="https://ecs7.tokopedia.net/assets/images/careers/careers-nakamaclass-bg.svg" alt="">
    <!-- <img class="section-class__acc2" src="https://ecs7.tokopedia.net/assets/images/careers/careers-nakamaclass-acc2.svg" alt=""> -->
</section>

<!-- Nakama Sport -->
<section class="section-sport">
    <div class="sport">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="sport-desc">
                            <h3 class="sport-desc__title">
                                Nakama Sport Club
                            </h3>
                            <p class="sport-desc__desc">
                                Doing sports is always fun, especially when you’re with a team. 
                                Joining Naka Sports Club will get you to know more about other 
                                Nakama from other teams that you might never engage with before. 
                                Every month, Tokopedia encourages you to organize your team’s sports activities.
                            </p>
                        </div>
                        <div class="sport-images">
                            <div class="col-sm-6 col-xs-12">
                                <div class="sport-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/FUTSAL-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/FUTSAL-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/FUTSAL-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/FUTSAL-4.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/FUTSAL-5.jpg","w":0,"h":0}]}'>
                                    <div class="sport-images-overlay">
                                        <div class="sport-images-overlay-ct">
                                            <h3 class="sport-images-overlay-ct__h3">
                                                Futsal
                                            </h3>
                                            <div class="sport-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Futsal.png" alt="" class="sport-images__img">
                                </div>
                                <h3 class="sport-images__caption">Futsal</h3>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="sport-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BADMINTON-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BADMINTON-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BADMINTON-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BADMINTON-4.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BADMINTON-5.jpg","w":0,"h":0}]}'>
                                    <div class="sport-images-overlay">
                                        <div class="sport-images-overlay-ct">
                                            <h3 class="sport-images-overlay-ct__h3">
                                                Badminton
                                            </h3>
                                            <div class="sport-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Badminton.png" alt="" class="sport-images__img">
                                </div>
                                <h3 class="sport-images__caption">Badminton</h3>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="sport-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BASKETBALL-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BASKETBALL-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BASKETBALL-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BASKETBALL-4.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/BASKETBALL-5.jpg","w":0,"h":0}]}'>
                                    <div class="sport-images-overlay">
                                        <div class="sport-images-overlay-ct">
                                            <h3 class="sport-images-overlay-ct__h3">
                                                Basket
                                            </h3>
                                            <div class="sport-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Basketball.png" alt="" class="sport-images__img">
                                </div>
                                <h3 class="sport-images__caption">Basket</h3>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="sport-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/SWIMMING-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/SWIMMING-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/SWIMMING-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/SWIMMING-4.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/SWIMMING-5.jpg","w":0,"h":0}]}'>
                                    <div class="sport-images-overlay">
                                        <div class="sport-images-overlay-ct">
                                            <h3 class="sport-images-overlay-ct__h3">
                                                Swimming
                                            </h3>
                                            <div class="sport-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Swimming.png" alt="" class="sport-images__img">
                                </div>
                                <h3 class="sport-images__caption">Swimming</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img class="section-sport__acc1" src="https://ecs7.tokopedia.net/assets/images/careers/careers-sport-acc1.svg" alt="">
</section>

<!-- Internal Events -->
<section class="section-internal">
    <!-- <img class="section-internal__accr" src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/ca-ll-new.png" alt="">
    <img class="section-internal__accl" src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/ca-ll-new.png" alt=""> -->
    <div class="class">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="class-desc">
                            <h3 class="class-desc__title">
                                Internal Events
                            </h3>
                            <p class="class-desc__desc">
                                A healthy workplace needs to be fun. 
                                We regularly held special events only for 
                                Nakama to celebrate many causes 
                                to take care of their well-being 
                                and promoting a productive, happy 
                                and collaborative work environment.
                            </p>
                        </div>
                        <div class="class-images">
                            <div class="col-sm-6">
                                <div class="class-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Hackathon-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Hackathon-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Hackathon-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/Hackathon-4.jpg","w":0,"h":0}]}'>
                                    <div class="class-images-overlay">
                                        <div class="class-images-overlay-ct">
                                            <h3 class="class-images-overlay-ct__h3">
                                                Hackathon
                                            </h3>
                                            <div class="class-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rsz_hackathon-9.jpg" alt="" class="class-images__img">
                                </div>
                                <h3 class="class-images__caption">Hackathon</h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="class-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/nakamaday-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/nakamaday-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/nakamaday-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/nakamaday-4.jpg","w":0,"h":0}]}'>
                                    <div class="class-images-overlay">
                                        <div class="class-images-overlay-ct">
                                            <h3 class="class-images-overlay-ct__h3">
                                                Nakama Day
                                            </h3>
                                            <div class="class-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rsz_img_9668.jpg" alt="" class="class-images__img">
                                </div>
                                <h3 class="class-images__caption">Nakama Day</h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="class-images-wrapper" data-gallery='{"data":[{"src":" https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/birthday-1.jpg","w":0,"h":0},{"src":" https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/birthday-1.jpg","w":0,"h":0},{"src":" https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/birthday-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/birthday-4.jpg","w":0,"h":0}]}'>
                                    <div class="class-images-overlay">
                                        <div class="class-images-overlay-ct">
                                            <h3 class="class-images-overlay-ct__h3">
                                                Tokopedia Birthday
                                            </h3>
                                            <div class="class-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rsz_dsc09847.jpg" alt="" class="class-images__img">
                                </div>
                                <h3 class="class-images__caption">Tokopedia Birthday</h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="class-images-wrapper" data-gallery='{"data":[{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/ngt-1.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/ngt-2.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/ngt-3.jpg","w":0,"h":0},{"src":"https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/10/ngt-4.jpg","w":0,"h":0}]}'>
                                    <div class="class-images-overlay">
                                        <div class="class-images-overlay-ct">
                                            <h3 class="class-images-overlay-ct__h3">
                                                Nakama Got Talent
                                            </h3>
                                            <div class="class-images-overlay-ct__div">
                                                View
                                            </div>
                                        </div>
                                    </div>
                                    <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rsz_dsc06119.jpg" alt="" class="class-images__img">
                                </div>
                                <h3 class="class-images__caption">Nakama Got Talent</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="internal">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="internal-header">
                        <h2 class="internal-header__title">
                            Internal Events
                        </h2>
                        <p class="internal-header__desc">
                            A healthy workplace needs to be fun. 
                            We regularly held special events only for 
                            Nakama to celebrate many causes 
                            to take care of their well-being 
                            and promoting a productive, happy 
                            and collaborative work environment.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rsz_hackathon-9.jpg" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Hackathon
                                </h3>
                                <h3 class="ievent-box-ct__date">
                                    8 May 2018
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    Much like Noah's Ark, the entirety of 
                                    Tokopedia can be described as several different ships combined into one. 
                                    In the ocean of opportunities, 
                                    it's not uncommon for ships to run
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <button class="ievent-box-btn__btn1 unf-btn unf-btn--small ripple-effect ripple-main">
                                    Register<span class="text">Register</span>
                                </button>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary ripple-effect ripple-main" data-img="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rsz_dsc09404.jpg" 
                                        data-modal="true" 
                                        data-what="The intensive training and hackathon program is designed for the final year or postgraduate students from an IT background to compete and to create products and innovations. The selected students will be coached and trained by mentors from Tokopedia and the winner will get a special invitation to become full-time engineers in Tokopedia."
                                        data-title="Hackathon" 
                                        data-date="12 May 2018"
                                        data-link="https://www.eventbrite.com/">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rsz_img_9668.jpg" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Nakama Day
                                </h3>
                                <h3 class="ievent-box-ct__date">
                                    8 May 2018
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    Much like Noah's Ark, the entirety of 
                                    Tokopedia can be described as several different ships combined into one. 
                                    In the ocean of opportunities, 
                                    it's not uncommon for ships to run
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <button class="ievent-box-btn__btn1 unf-btn unf-btn--small ripple-effect ripple-main">
                                    Register<span class="text">Register</span>
                                </button>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary ripple-effect ripple-main" 
                                        data-modal="true" 
                                        data-what="The intensive training and hackathon program is designed for the final year or postgraduate students from an IT background to compete and to create products and innovations. The selected students will be coached and trained by mentors from Tokopedia and the winner will get a special invitation to become full-time engineers in Tokopedia."
                                        data-title="Nakama Day" 
                                        data-date="12 May 2018"
                                        data-link="https://www.eventbrite.com/">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rsz_dsc09847.jpg" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Tokopedia Birthday
                                </h3>
                                <h3 class="ievent-box-ct__date">
                                    8 May 2018
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    Much like Noah's Ark, the entirety of 
                                    Tokopedia can be described as several different ships combined into one. 
                                    In the ocean of opportunities, 
                                    it's not uncommon for ships to run
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <button class="ievent-box-btn__btn1 unf-btn unf-btn--small ripple-effect ripple-main">
                                    Register<span class="text">Register</span>
                                </button>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary ripple-effect ripple-main" 
                                        data-modal="true" 
                                        data-what="The intensive training and hackathon program is designed for the final year or postgraduate students from an IT background to compete and to create products and innovations. The selected students will be coached and trained by mentors from Tokopedia and the winner will get a special invitation to become full-time engineers in Tokopedia."
                                        data-title="Tokopedia Birthday" 
                                        data-date="12 May 2018"
                                        data-link="https://www.eventbrite.com/">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="ievent-box">
                        <div class="ievent-wrapper">
                            <div class="ievent-box-img">
                                <img src="https://ecs7.tokopedia.net/assets-tokopoints/prod/images/2018/09/rsz_dsc06119.jpg" alt="" class="ievent-box-img__img">
                            </div>
                            <div class="ievent-box-ct">
                                <h3 class="ievent-box-ct__title">
                                    Nakama Got Talent
                                </h3>
                                <h3 class="ievent-box-ct__date">
                                    8 May 2018
                                </h3>
                                <p class="ievent-box-ct__desc">
                                    Much like Noah's Ark, the entirety of 
                                    Tokopedia can be described as several different ships combined into one. 
                                    In the ocean of opportunities, 
                                    it's not uncommon for ships to run
                                </p>
                            </div>
                            <div class="ievent-box-btn text-center">
                                <button class="ievent-box-btn__btn1 unf-btn unf-btn--small ripple-effect ripple-main">
                                    Register<span class="text">Register</span>
                                </button>
                                <button class="ievent-box-btn__btn2 unf-btn unf-btn--small unf-btn--secondary ripple-effect ripple-main" 
                                        data-modal="true" 
                                        data-what="The intensive training and hackathon program is designed for the final year or postgraduate students from an IT background to compete and to create products and innovations. The selected students will be coached and trained by mentors from Tokopedia and the winner will get a special invitation to become full-time engineers in Tokopedia."
                                        data-title="Nakama Got Talent" 
                                        data-date="12 May 2018"
                                        data-link="https://www.eventbrite.com/">
                                    See Details<span class="text">See Details</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</section>


<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

	<!-- Background of PhotoSwipe. 
		 It's a separate element as animating opacity is faster than rgba(). -->
	<div class="pswp__bg"></div>

	<!-- Slides wrapper with overflow:hidden. -->
	<div class="pswp__scroll-wrap">

		<!-- Container that holds slides. 
			PhotoSwipe keeps only 3 of them in the DOM to save memory.
			Don't modify these 3 pswp__item elements, data is added later on. -->
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>

		<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
		<div class="pswp__ui pswp__ui--hidden">

			<div class="pswp__top-bar">

				<!--  Controls are self-explanatory. Order can be changed. -->

				<div class="pswp__counter"></div>

				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

				<button class="pswp__button pswp__button--share" title="Share"></button>

				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

				<!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
				<!-- element will get class pswp__preloader--active when preloader is running -->
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
					  <div class="pswp__preloader__cut">
						<div class="pswp__preloader__donut"></div>
					  </div>
					</div>
				</div>
			</div>

			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div> 
			</div>

			<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
			</button>

			<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
			</button>

			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>

		</div>

	</div>

</div>

<script src="https://ecs7.tokopedia.net/assets/careers/js/photoswipe.min.js"></script>
<script src="https://ecs7.tokopedia.net/assets/careers/js/photoswipe-ui-default.min.js"></script>
