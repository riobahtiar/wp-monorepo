<?php
/**
 * Page Job
 *
 */


if ( $job['department'] == 'Business' ) {
	$desktopBanner = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174726/bg_business.png';
	$mobileBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150918/bg_business_mobile.jpg';
}

if ( $job['department'] == 'Finance' ) {
	$desktopBanner = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174728/bg_finance.png';
	$mobileBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150919/bg_finance_mobile.jpg';
}

if ( $job['department'] == 'Operational' ) {
	$desktopBanner = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174733/bg_operational.png';
	$mobileBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150922/bg_operational_mobile.jpg';
}

if ( $job['department'] == 'People' ) {
	$desktopBanner = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174729/bg_HR_people.png';
	$mobileBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150920/bg_HR_People_mobile.jpg';
}

if ( $job['department'] == 'Product' ) {
	$desktopBanner = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174734/bg_product1.png';
	$mobileBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150922/bg_product_mobile.jpg';
}

if ( $job['department'] == 'Technology' ) {
	$desktopBanner = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174735/bg_tech.png';
	$mobileBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150923/bg_tech_mobile.jpg';
}

if ( $job['department'] == 'Internship' ) {
	$desktopBanner = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174730/bg_intern.png';
	$mobileBanner  = 'https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/27150921/bg_intern_mobile.jpg';
}

?>
<script type="application/ld+json">
{
 "@context": "http://schema.org",
 "@type": "BreadcrumbList",
 "itemListElement":
 [
  {
   "@type": "ListItem",
   "position": 1,
   "item":
   {
	"@id": "<?php echo get_bloginfo( 'url' ); ?>",
	"name": "Careers"
	}
  },
  {
   "@type": "ListItem",
  "position": 2,
  "item":
   {
	 "@id": "<?php echo get_bloginfo( 'url' ) . '/jobs?function=' . strtolower( $job['department'] ); ?>",
	 "name": "<?php echo $job['department']; ?>"
   }
 },
 {
  "@type": "ListItem",
 "position": 3,
 "item":
  {
	"@id": "<?php echo $job['title']; ?>",
	"name": "<?php echo $job['title']; ?>"
  }
 }
 ]
}


</script>
<div class="detail-job">
	<div class="detail-job-banner" style='background-image:URL("<?php echo $desktopBanner; ?>");'
		 data-background-mobile="<?php echo $mobileBanner; ?>">
	</div>
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="white-container">

					<div class="breadcrumb-container">
						<div class="breadcrumb-wrapper">
							<span class="job-breadcrumb"><a
										href="<?php echo get_bloginfo( 'url' ); ?>">Careers</a></span>
							<span class="job-breadcrumb"><a
										href="<?php echo get_bloginfo( 'url' ) . '/function/' . strtolower( $job['department'] ); ?>"><?php echo $job['department']; ?></a></span>
							<span class="job-breadcrumb"><?php echo $job['title']; ?></span>
						</div>
						<div style="clear:both"></div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<h1><?php echo $job['title']; ?></h1>
							<div class="detail-job-sub-container">
								<div class="detail-job-sub" data-tooltip="Function" data-tooltip-position="top"><img
											src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174125/ic-experience-white.png"> <?php echo $job['department']; ?>
								</div>
								<div class="detail-job-sub" data-tooltip="Location" data-tooltip-position="top"><img
											src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/11/22174131/ic-location-white.png"> <?php echo $job['location']['country']; ?>
								</div>
							</div>
							<h3>Description</h3>
							<p>
								<?php
								echo $job_item->description;
								?>
							</p>
							<h3>Requirements</h3>
							<p>
								<?php
								if ( strpos( $job_item->requirements, '<ul>' ) !== 0 ) {
									?>
							<ul>
									<?php
								}
								?>
								<?php
								echo $job_item->requirements;
								?>
								<?php
								if ( strpos( $job_item->requirements, '<ul>' ) !== 0 ) {
									?>
								<ul>
									<?php
								}
								?>
							</p>
								<?php if ( $job_item->benefits != '' ) { ?>
														<h3>Benefits</h3>
														<p>
									<?php
}
								echo $job_item->benefits;
?>
														</p>
						</div>
						<div class="col-md-4 side-share-width">
							<div class="side-share-container">
								<div style="clear:both"></div>
								<a href="<?php echo get_bloginfo( 'url' ) . '/join/' . $slug; ?>">
									<div class="side-apply-btn">Apply Now</div>
								</a>
								<div class="text-center apply-text">Not what you had in mind? <a
											href="<?php echo get_bloginfo( 'url' ) . '/function/' . strtolower( $job['department'] ); ?>"><br>Look for another
										job</a></div>
								<div class="apply-divider">
									<span>OR</span>
								</div>
								<div class="apply-share-text">Know someone who would be perfect for this role? let them
									know:
								</div>
								<div style="text-align:center">
									<div class="addthis_inline_share_toolbox"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- mobile white card -->


	</div>

	<div class="job-apply-mobile">
		<div class="container">
			<div class="apply-bottom-mobile">
				<div style="clear:both"></div>
				<div class="text-center apply-text">Not what you had in mind? <a
							href="<?php echo get_bloginfo( 'url' ) . '/function/' . strtolower( $job['department'] ); ?>"><br>Look for another job</a></div>
				<div class="apply-divider">
					<span>OR</span>
				</div>
				<div class="apply-share-text">Know someone who would be perfect for this role? let them know:</div>
				<div style="text-align:center">
					<div class="addthis_inline_share_toolbox_0460"></div>
				</div>
			</div>
		</div>
		<a href="<?php echo get_bloginfo( 'url' ) . '/join/' . $slug; ?>" rel="nofollow" title="Apply Now">
			<div class="text-center job-apply-btn fixed btn--big btn-tkpd btn--green">Apply Now</div>
		</a>

	</div>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a3ee5468ebdecce"></script>
<script>
	(function ($) {
		$(document).ready(function () {
			var sideTop = $(".side-share-container").offset().top;
			$(document).on('scroll', function () {
				if ($(document).scrollTop() > sideTop) {
					$(".side-share-container").css({
						position: "fixed",
						marginTop: "5px",
						top: "0",
						width: $(".side-share-width").width()
					});
				}
				if ($(document).scrollTop() < sideTop) {
					$(".side-share-container").css({
						position: "static",
						width: "auto",
						marginTop: "145px"
					});
				}
			});
			if ($(window).width() < 993) {
				$(document).on('scroll', function () {
					console.log($(document).scrollTop(), $(".footer").offset().top - $(window).height());
					if ($(document).scrollTop() > $(".footer").offset().top - $(window).height()) {
						$(".footer").css("margin-top", "0");
						$(".job-apply-btn").removeClass("fixed");
					}
					else {
						$(".footer").css("margin-top", "71px");
						$(".job-apply-btn").addClass("fixed");
					}
				});
			}
			if ($(window).width() < 481) {
				$(".detail-job-banner").each(function () {
					$(this).css("background-image", "URL('" + $(this).attr("data-background-mobile") + "')");
				});
			}


			$('*[data-tooltip]').on('mouseenter', function () {
				var pos = $(this).position();
				var width = $(this).outerWidth();
				var position = "top";
				if ($(this).attr('data-tooltip-position')) {
					position = $(this).attr('data-tooltip-position');
				}
				if ($(this).attr('data-tooltip-color')) {
					var color = $(this).attr('data-tooltip-color');
				}
				var new_tooltip = $('<div class="tkpd-tooltip" data-tooltip-position=' + position + ' data-tooltip-color=' + color + '>' + $(this).attr('data-tooltip') + '</div>').insertAfter($(this));

				//if tooltip has more width then the object
				if (new_tooltip.innerWidth() > $(this).innerWidth()) {
					var left = -(new_tooltip.innerWidth() - $(this).innerWidth()) / 2;
					var top = -new_tooltip.innerHeight() - 10;
					if (position == "left") {
						left = -new_tooltip.innerWidth() - 10;
						if (new_tooltip.innerHeight() < $(this).innerHeight()) {
							top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
						}
						if (new_tooltip.innerHeight() > $(this).innerHeight()) {
							top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
						}
					}
					if (position == "bottom") {
						top = new_tooltip.innerHeight() + 20;
					}
					if (position == "right") {
						left = $(this).innerWidth() + 10;
						if (new_tooltip.innerHeight() < $(this).innerHeight()) {
							top = ($(this).innerHeight() - new_tooltip.innerHeight()) / 2;
						}
						if (new_tooltip.innerHeight() > $(this).innerHeight()) {
							top = -(new_tooltip.innerHeight() - $(this).innerHeight()) / 2;
						}
					}
				}
				if (new_tooltip.innerWidth() < $(this).innerWidth()) {
					var left = ($(this).innerWidth() - new_tooltip.innerWidth()) / 2;
					var top = -new_tooltip.innerHeight() - 10;
				}
				new_tooltip.css({
					top: pos.top + top + "px",
					left: pos.left + left + 1 + "px"
				});
			});

			$('*[data-tooltip]').on('mouseleave', function () {
				$(this).next().remove();
			});

		});
	})(jQuery);
</script>


<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "JobPosting",
  "datePosted": "<?php echo $job['created_at']; ?>",
  "title": "<?php echo $job['title']; ?>",
  "description": "<?php echo limit_text( wp_strip_all_tags( $job_item->description ), 160 ); ?>",
  "educationRequirements": "Bachelor's Degree or related fields of study.",
  "employmentType": "Full-time",
  "industry": "<?php echo $job['department']; ?>",
  "jobLocation" : {
	"@type" : "Place",
	"address" : {
	  "@type" : "PostalAddress",
	  "streetAddress" : "Tokopedia Tower 46th Floor,
Jalan Prof Dr. Satrio Kav 11, Karet Semanggi, Setiabudi",
	  "addressLocality" : "Jakarta Selatan",
	  "addressRegion" : "Jakarta",
	  "postalCode" : "12930",
	  "addressCountry": "ID"
	}
  },
  "hiringOrganization" : {
	"@type" : "Organization",
	"name" : "PT. Tokopedia",
	"sameAs" : "https://www.tokopedia.com",
	"logo" : "https://ecs7.tokopedia.net/promo/wp-content/uploads/2016/12/14183107/logo.png"
  },
  "salaryCurrency": "IDR",
  "workHours": "40 hours per week"
}


</script>
