<?php
/**
* Template Name:  Career Vacancy
*/
?>

	<?php

	// Fetch Jobs Data
	$workable      = new Careers\workable;
	$abcde         = $workable->get_jobs( 99 );
	$raw           = $workable->get_grouped_jobs( true );
	$the_shortcode = '';
	// Send Jobs Data to Javascript
	wp_localize_script( 'sage/js', 'jobsData', json_encode( $raw ) );

	//===============>
	// Options //
	/////////////

	/* Banner */
	// $banner_title = get_field('banner_title', 'option');
	// $banner_desc = get_field('banner_description', 'option');

	// $about_sliders = get_field('slider_images', 'option');
	// $team_sliders = get_field('slider_images_team', 'option');

	// echo '<pre>';
	// print_r($about_sliders);
	// echo '</pre>';

	// End Of Options //
	// <==================
	?>
		<section id="msrc" class="section_mobile-search">
			<div class="m-searchbar clearfix">
				<div class="m-searchbar-arrow">
					<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-back-mobile.png" alt="" class="m-searchbar-arrow__arrow">
				</div>
				<div class="m-searchbar-lens">
					<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-search-mobile.png" alt="" class="m-searchbar-lens__lens">
				</div>
				<!-- <form id="search-job" action="#" method="GET"> -->
				<input name="query" type="search" class="search-bar__thebar search-bar__thebar--m" placeholder="Type your dream jobs...">
				<!-- </form> -->
			</div>
			<ul class="search-bar-thebar-ul search-bar-thebar-ul--m">
				<p class="m-searchcopy__copy">Search Suggestions</p>
				<!-- populated via JS -->
			</ul>
		</section>

		<section class="section_mobile-location clearfix">
			<div class="m-location clearfix">
				<div class="m-location-copy">
					<h2 class="m-location-copy__copy">
						Choose your prefered place
					</h2>
				</div>
				<ul class="search-bar-theloc-ul--m">
					<li class="search-bar-theloc__li">
						<div class="search-bar-theloc__liwrapper">
							<span class="search-bar-theloc__liloc">All Locations</span>
							<div class="pull-right">
							</div>
						</div>
					</li>
					<!-- populated via JS -->
				</ul>
			</div>
		</section>

		<section class="section_banner">
			<div class="banner-video-wrapper">
				<video loop muted autoplay poster="" class="banner-video__video"></video>
			</div>
			<div class="container container-center">
				<!-- Title -->
				<div class="row">
					<div class="col-xs-12">
						<div class="search-title text-left txt--white">
							<h1 class="search-title__hero fw--smb ls--2">
								Find Your Purpose
							</h1>
							<h2 class="search-title__small fs--14">
								You were born to make an impact
							</h2>
						</div>
					</div>
				</div>
			</div>
		</section>

		<form action="../jobs" method="get">
			<section class="section_search">
				<div class="search-wrap">
					<div class="container">

						<!-- Search Bar -->
						<div class="search-bar clearfix">

							<div class="col-search-60">
								<div class="search-bar-wrapbar clearfix">
									<div class="search-bar-thebar clearfix">
										<div class="search-bar-thebar-image">
											<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-jobsnew.svg" alt="Search Jobs" class="search-bar__icon">
											<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-jobsnew-scroll.svg" alt="Search Jobs" class="search-bar__icon--scroll">
										</div>
										<div class="search-bar-thebar-group">
											<label for="query" class="search-bar__label txt--black txt--primary fs--12 fw--smb">I want to join as</label>
											<input name="query" type="text" class="search-bar__thebar" placeholder="Type your dream jobs" autocomplete="off"></input>
										</div>
										<ul class="search-bar-thebar-ul">
											<!-- populated via JS -->
										</ul>
									</div>

									<div class="search-bar-theloc">
										<div class="search-bar-thebar-image">
											<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-location-on.svg" alt="Search Jobs Location" class="search-bar__icon">
											<img src="https://ecs7.tokopedia.net/assets/images/careers/icon-lokasi-scroll.svg" alt="Search Jobs Location" class="search-bar__icon--scroll">
										</div>
										<div class="search-bar-thebar-group">
											<label for="country" class="search-bar__label txt--black txt--primary fs--12 fw--smb">Location</label>
											<input type="text" class="search-bar__theloc" placeholder="All Locations" readonly="true" value=""></input>
											<input name="country" type="hidden" class="search-bar__theloc--true" placeholder="All Locations" readonly="true" value=""></input>
										</div>
										<!-- <i class="fa fa-map-marker search-bar__thelocmarker" aria-hidden="true"></i> -->
										<i class="fa fa-angle-down search-bar__thelocarrow" aria-hidden="true"></i>
										<ul class="search-bar-theloc-ul">
											<li class="search-bar-theloc__li">
												<div class="search-bar-theloc__liwrapper">
													<span class="search-bar-theloc__liloc">All Locations</span>
													<div class="pull-right">

													</div>
												</div>
											</li>
											<!-- populated via JS -->
										</ul>
									</div>
								</div>
							</div>

							<div class="col-search-40">
								<div class="buttons-wrap clearfix">
									<div class="col-search-btn">
										<button type="submit" class="btn-tkpd btn-search btn-tkpd--orange ripple-effect ripple-main">
											<span class="btn-tkpd__text">Search</span>
										</button>
									</div>
									<div class="search-or text-center fw--600">OR</div>
									<div class="search-or-line"></div>
									<div class="col-view-btn">
										<a href="../jobs" class="btn-tkpd btn-view btn-tkpd--sgreen ripple-effect ripple-main">
											<span class="btn-tkpd__text">View All Jobs</span>
										</a>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>
		</form>

		<section class="section_jobs">
			<!-- Jobs Header -->
			<div class="container">
				<div class="thestory-header thestory-header--drive">
					<h3 class="thestory-header__smtitle txt--black txt--disabled fs--body1 fw--smb">
						Career Opportunities
					</h3>
					<h2 class="thestory-header__bgtitle txt--black txt--primary fw--smb">
						Take the Action: Pick Your Team!
						</h3>
						<div class="thestory-header__overlap thestory-header__overlap--jobs">
							Opportunities
						</div>
						<div class="thestory-header__underline thestory-header__underline--jobs"></div>
				</div>
			</div>
			<div class="container">

				<div class="floater-wrapper clearfix">
					<div class="clearfix">
						<!-- Business -->
						<div class="floater">
							<div class="jobs-item ji1">
								<div class="jobs-item-flag flag-business"></div>
								<div class="jobs-item-image">
									<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-business-big.svg" alt="Business" class="jobs-item-image__image">
								</div>
								<div>
									<h2 class="jobs-item-content__title txt--black txt--secondary fw--smb text-center">
										Business
									</h2>
								</div>
								<div class="jobs-item-content text-center">
									<h3 class="jobs-item-content__body txt--black txt--secondary">
										Foresee business opportunities and strategize business plan to create a valuable marketing actions
									</h3>
								</div>

								<div class="jobs-item-link fs--14 fw--smb text-center">
									<a href="../function/business">
										<span id="business-len" class="jobs-item-link__link--disabled">No Roles Available</span>
									</a>
								</div>
							</div>
						</div>
						<!-- Finance -->
						<div class="floater">
							<div class="jobs-item ji2">
								<div class="jobs-item-flag flag-finance"></div>
								<div class="jobs-item-image">
									<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-finance-big.svg" alt="Finance" class="jobs-item-image__image">
								</div>
								<div>
									<h2 class="jobs-item-content__title txt--black txt--secondary fw--smb text-center">
										Finance
									</h2>
								</div>
								<div class="jobs-item-content text-center">
									<h3 class="jobs-item-content__body txt--black txt--secondary">
										Plan, control, and monitor the financial purpose
									</h3>
								</div>

								<div class="jobs-item-link fs--14 fw--smb text-center">
									<a href="../function/finance">
										<span id="finance-len" class="jobs-item-link__link--disabled">No Roles Available</span>
									</a>
								</div>
							</div>
						</div>
						<!-- Operational -->
						<div class="floater">
							<div class="jobs-item ji3">
								<div class="jobs-item-flag flag-business"></div>
								<div class="jobs-item-image">
									<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-operational-big.svg" alt="Operational" class="jobs-item-image__image">
								</div>
								<div>
									<h2 class="jobs-item-content__title txt--black txt--secondary fw--smb text-center">
										Operational
									</h2>
								</div>
								<div class="jobs-item-content text-center">
									<h3 class="jobs-item-content__body txt--black txt--secondary">
										Monitor, assist, and solve customer’s problem professionally through providing valuable guidance
									</h3>
								</div>

								<div class="jobs-item-link fs--14 fw--smb text-center">
									<a href="../function/operational">
										<span id="operational-len" class="jobs-item-link__link--disabled">No Roles Available</span>
									</a>
								</div>
							</div>
						</div>
						<!-- People -->
						<div class="floater">
							<div class="jobs-item ji4">
								<div class="jobs-item-flag flag-people"></div>
								<div class="jobs-item-image">
									<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-people-big.svg" alt="People" class="jobs-item-image__image">
								</div>
								<div>
									<h2 class="jobs-item-content__title txt--black txt--secondary fw--smb text-center">
										People
									</h2>
								</div>
								<div class="jobs-item-content text-center">
									<h3 class="jobs-item-content__body txt--black txt--secondary">
										Manage, coordinate, and collaborate with the Nakamas to create a healthy workplace
									</h3>
								</div>

								<div class="jobs-item-link fs--14 fw--smb text-center">
									<a href="../function/people">
										<span id="people-len" class="jobs-item-link__link--disabled">No Roles Available</span>
									</a>
								</div>
							</div>
						</div>
						<!-- Product -->
						<div class="floater">
							<div class="jobs-item ji5">
								<div class="jobs-item-flag flag-product"></div>
								<div class="jobs-item-image">
									<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-product-big.svg" alt="Product" class="jobs-item-image__image">
								</div>
								<div>
									<h2 class="jobs-item-content__title txt--black txt--secondary fw--smb text-center">
										Product
									</h2>
								</div>
								<div class="jobs-item-content text-center">
									<h3 class="jobs-item-content__body txt--black txt--secondary">
										Design, build, and beautify the visual of the product
									</h3>
								</div>

								<div class="jobs-item-link fs--14 fw--smb text-center">
									<a href="../function/product">
										<span id="product-len" class="jobs-item-link__link--disabled">No Roles Available</span>
									</a>
								</div>
							</div>
						</div>
						<!-- Technology -->
						<div class="floater">
							<div class="jobs-item ji6">
								<div class="jobs-item-flag flag-technology"></div>
								<div class="jobs-item-image">
									<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-tech-big.svg" alt="Technology" class="jobs-item-image__image">
								</div>
								<div>
									<h2 class="jobs-item-content__title txt--black txt--secondary fw--smb text-center">
										Technology
									</h2>
								</div>
								<div class="jobs-item-content text-center">
									<h3 class="jobs-item-content__body txt--black txt--secondary">
										Build and maintain the architecture of the product
									</h3>
								</div>

								<div class="jobs-item-link fs--14 fw--smb text-center">
									<a href="../function/technology">
										<span id="technology-len" class="jobs-item-link__link--disabled">No Roles Available</span>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Internship -->
					<div>
						<div class="floater floater7">
							<div class="jobs-item ji7">
								<div class="jobs-item-flag flag-internship"></div>
								<div class="jobs-item-image">
									<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-internship-big.svg" alt="Internship" class="jobs-item-image__image">
								</div>
								<div>
									<h2 class="jobs-item-content__title txt--black txt--secondary fw--smb text-center">
										Internship
									</h2>
								</div>
								<div class="jobs-item-content text-center">
									<h3 class="jobs-item-content__body txt--black txt--secondary">
										Learn and experience through project contribution
									</h3>
								</div>

								<div class="jobs-item-link fs--14 fw--smb text-center">
									<a href="../function/internship">
										<span id="internship-len" class="jobs-item-link__link--disabled">No Roles Available</span>
									</a>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="text-center">
					<a href="../jobs" class="btn-tkpd btn-tkpd--large btn-tkpd--sgreen ripple-effect ripple-main">
						<span class="btn-tkpd__text">View All Jobs</span>
					</a>
				</div>
			</div>
		</section>
