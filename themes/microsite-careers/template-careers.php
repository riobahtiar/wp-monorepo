<?php
/**
 * Template Name: Careers Listing
 */

/* =================
 * Variable Init
 * ================= */
$wk          = new Careers\workable;
$cat         = json_decode( $wk->get_jobs( 150 ), true );
$jobs        = $wk->get_grouped_jobs( false );
$all_jobs    = $wk->get_grouped_jobs( true );
$isBusiness  = $isFinance = $isProduct = $isOperational = $isPeople = $isTechnology = $isInternship = false;
$countryCode = array(
	'ID' => 'Indonesia',
	'IN' => 'India',
);

/* ================
 * Localize script
 * ================ */
wp_localize_script( 'sage/js', 'jobsData', json_encode( $jobs ) );
wp_localize_script( 'sage/js', 'baseUrl', get_bloginfo( 'url' ) );

/* ========================
 * Fetch Param if available
 * ======================== */
$searchTerm = isset( $_GET['query'] ) ? sanitize_text_field( $_GET['query'] ) : '';
$country    = isset( $_GET['country'] ) ? sanitize_text_field( $_GET['country'] ) : '';
$function   = isset( $_GET['function'] ) ? sanitize_text_field( $_GET['function'] ) : 'all';

switch ( $function ) {
	case 'business':
		$isBusiness = true;
		break;
	case 'finance':
		$isFinance = true;
		break;
	case 'product':
		$isProduct = true;
		break;
	case 'operational':
		$isOperational = true;
		break;
	case 'people':
		$isPeople = true;
		break;
	case 'technology':
		$isTechnology = true;
		break;
	case 'internship':
		$isInternship = true;
		break;
	default:
		$isBusiness = $isFinance = $isProduct = $isOperational = $isPeople = $isTechnology = $isInternship = true;
		break;
}

// echo "<pre>";
// print_r($all_jobs);
// echo "</pre>";
/* ===================
 * Total jobs
 * =================== */
$totalJobs = $function === 'all' ? sizeof( $all_jobs['jobs'] ) : sizeof( $jobs[ $function ] );

/* ======================
 * Server side searching
 * ====================== */
function searchJob( $job ) {
	if ( $GLOBALS['searchTerm'] ) :
		if ( $GLOBALS['country'] ) :
			return ( strpos( strtolower( $job['title'] ), strtolower( $GLOBALS['searchTerm'] ) ) !== false ) &&
			( strpos( strtolower( $job['location']['country_code'] ), strtolower( $GLOBALS['country'] ) ) !== false );
	  endif;
		return ( strpos( strtolower( $job['title'] ), strtolower( $GLOBALS['searchTerm'] ) ) !== false );
  endif;
	return ( strpos( strtolower( $job['location']['country_code'] ), strtolower( $GLOBALS['country'] ) ) !== false );
}

if ( $searchTerm || $country ) :
	// reset total jobs
	$totalJobs = 0;

	// reassign each job function
	foreach ( $jobs as $func => $value ) :
		$jobs[ $func ] = array_filter( $jobs[ $func ], 'searchJob' );
		$totalJobs    += sizeof( $jobs[ $func ] );
  endforeach;
endif;

// If Job not found
if ( $totalJobs == 0 ) {
	$displayDefault = 'style="display: block;"';
}

/* ======================
 * Banner Handler
 * ====================== */

switch ( $function ) {
	case 'business':
		 $banner = 'list-jumbotron--business';
		break;
	case 'finance':
		 $banner = 'list-jumbotron--finance';
		break;
	case 'product':
		 $banner = 'list-jumbotron--product';
		break;
	case 'operational':
		 $banner = 'list-jumbotron--operational';
		break;
	case 'people':
		 $banner = 'list-jumbotron--people';
		break;
	case 'technology':
		 $banner = 'list-jumbotron--technology';
		break;
	case 'internship':
		 $banner = 'list-jumbotron--internship';
		break;
	default:
		 $banner = '';
}
?>

<section class="list-jumbotron <?php echo $banner; ?>">
  <div class="container">
	<div class="list-jumbotron-text">
	  <h1 class="list-jumbotron-text__h1">Find Your Purpose</h1>
	  <h2 class="list-jumbotron-text__h2">Life is too short to just fulfill a job description</h2>
	</div>
  </div>
</section>

<section id="discovery">
  <div class="container">
	<div class="list-discovery">
	  <div class="list-search clearfix">
		<form id="search-form">
		  <div class="list-select">
			<div class="relative">
			  <i class="list-search__icon"></i>
			  <input type="text" class="list-search__input" placeholder="Search job.." value="<?php echo $searchTerm ? $searchTerm : ''; ?>"/>
			  <input type="submit" class="visible-xs list-search__btn btn-tkpd btn-tkpd--large btn-tkpd--orange ripple-effect ripple-main" value="Search"/>
			</div>
			<div class="list-select-input list-filter" data-filter="location" data-value="<?php echo $country ? $country : 'all'; ?>">
			  <i class="list-select-input__icon list-select-input__icon--location"></i>
			  <span class="list-select-input__text"><?php echo $country ? $countryCode[ $country ] : 'All location'; ?></span>
			  <i class="fa fa-angle-down list-select-input__arrow" aria-hidden="true"></i>
			  <div class="list-select-options">
				<ul>
				  <li data-value="all">All location</li>
				  <li data-value="ID">Indonesia<div class="pull-right"><span class="flag-indonesia"></span></div></li>
				  <li data-value="IN">India<div class="pull-right"><span class="flag-india"></span></div></li>
				</ul>
			  </div>
			</div>
			<div class="hidden-xs list-search__btn btn-tkpd btn-tkpd--large btn-tkpd--orange ripple-effect ripple-main">
			  <span class="btn-tkpd__text">Search</span>
			</div>
		  </div>
		</form>
	  </div>
	  <div class="list-category">
		<span class="list-category__label">Job category:</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'business' ) {
			echo 'active';}
		?>
		" data-function="business">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Business
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'finance' ) {
			echo 'active';}
		?>
		" data-function="finance">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Finance
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'operational' ) {
			echo 'active';}
		?>
		" data-function="operational">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Operational
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'people' ) {
			echo 'active';}
		?>
		" data-function="people">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> People
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'product' ) {
			echo 'active';}
		?>
		" data-function="product">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Product
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'technology' ) {
			echo 'active';}
		?>
		" data-function="technology">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Technology
		</span>
		<span class="list-category__item 
		<?php
		if ( $function == 'internship' ) {
			echo 'active';}
		?>
		" data-function="internship">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Internship
		</span>
	  </div>
	  <div class="list-category-slick">
		<div class="list-category__item 
		<?php
		if ( $function == 'business' ) {
			echo 'active';}
		?>
		" data-function="business">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Business
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'finance' ) {
			echo 'active';}
		?>
		" data-function="finance">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Finance
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'operational' ) {
			echo 'active';}
		?>
		" data-function="operational">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Operational
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'people' ) {
			echo 'active';}
		?>
		" data-function="people">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> People
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'product' ) {
			echo 'active';}
		?>
		" data-function="product">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Product
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'technology' ) {
			echo 'active';}
		?>
		" data-function="technology">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Technology
		</div>
		<div class="list-category__item 
		<?php
		if ( $function == 'internship' ) {
			echo 'active';}
		?>
		" data-function="internship">
		  <i class="fa fa-check list-category__icon" aria-hidden="true"></i> Internship
		</div>
	  </div>
	  <div class="list-filter-more">
		<span class="list-filter-more__text">More filter</span>
		<i class="fa fa-angle-down list-filter-more__icon"></i>
	  </div>
	</div>
	<div class="list-helper">
	  <div class="list-helper-label pull-left">
		Showing <span class="list-helper-label__count"><?php echo $totalJobs; ?></span> matching jobs <span class="list-helper-label__speed"></span>
	  </div>
	</div>
  </div>
</section>

<section id="job-list">
  <div class="container">
	<!-- Business Function -->
	<div class="list-function 
	<?php
	if ( $isBusiness && sizeof( $jobs['business'] ) ) {
		echo 'active';}
	?>
	" id="business">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-business-big.svg" alt="business" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/business"><h3 class="list-function-header__h3">Business</h3></a>
		  <p class="list-function-header__p">Foresee business opportunities and strategize business plan to create a valuable marketing actions</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isBusiness && sizeof( $jobs['business'] ) ) :
			$divs = $jobs['business'];
			?>


			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
			<div class="col-sm-6 col-md-4 col-xs-12">
			  <div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
				<h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
				<p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
				<div class="list-div-highlight--mobile clearfix">
				  <div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				  <div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
				</div>
				<div class="list-div-highlight clearfix">
				  <div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				  <div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
				</div>
				<div class="list-div-desc clearfix">
				  <div class="list-div-action">
					<div class="list-div-action-btn">
					  <a href="<?php echo '../job/' . $job['slug']; ?>" class="btn-tkpd btn-tkpd--large btn-tkpd--green ripple-effect ripple-main list-div-action-btn__action">
						<span class="btn-tkpd__text">See Details</span>
					  </a>
					</div>
					<div class="hidden-xs list-div-action-btn">
					  <a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--lgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
						<span class="btn-tkpd__text">Apply Now</span>
					  </a>
					</div>
					<div class="visible-xs list-div-action-btn">
					  <a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--sgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
						<span class="btn-tkpd__text">Apply Now</span>
					  </a>
					</div>
				  </div>
				</div>
			  </div>
			</div>
			<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center">
			<a href="<?php echo bloginfo( 'url' ); ?>/function/business" class="btn-tkpd btn-tkpd--large btn-tkpd--lgreen list-function__btn-more ripple-effect ripple-main" data-function="business">
			  <span class="btn-tkpd__text">See <?php echo sizeof( $divs ) - 6; ?> More Jobs in Business</span>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div> <!-- ./container -->
	<!-- ./Business Function -->

	<!-- Finance Function -->
	<div class="list-function 
	<?php
	if ( $isFinance && sizeof( $jobs['finance'] ) ) {
		echo 'active';}
	?>
	" id="finance">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-finance-big.svg" alt="finance" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/finance"><h3 class="list-function-header__h3">Finance</h3></a>
		  <p class="list-function-header__p">Plan, control, and monitor the financial purpose</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isFinance && sizeof( $jobs['finance'] ) ) :
			$divs = $jobs['finance'];
			?>


			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="btn-tkpd btn-tkpd--large btn-tkpd--green ripple-effect ripple-main list-div-action-btn__action list-div-action-btn__action">
					  <span class="btn-tkpd__text">See Details</span>
					</a>
				  </div>
				  <div class="hidden-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--lgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				  <div class="visible-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--sgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center">
			<a href="<?php echo bloginfo( 'url' ); ?>/function/finance" class="btn-tkpd btn-tkpd--large btn-tkpd--lgreen list-function__btn-more ripple-effect ripple-main" data-function="finance">
			  <span class="btn-tkpd__text">See <?php echo sizeof( $divs ) - 6; ?> More Jobs in Finance</span>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<!-- ./Finance Function -->

	<!-- Operational Function -->
	<div class="list-function 
	<?php
	if ( $isOperational && sizeof( $jobs['operational'] ) ) {
		echo 'active';}
	?>
	" id="operational">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-operational-big.svg" alt="operational" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/operational"><h3 class="list-function-header__h3">Operational</h3></a>
		  <p class="list-function-header__p">Monitor, assist, and solve customer’s problem professionally through providing valuable guidance</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isOperational && sizeof( $jobs['operational'] ) ) :
			$divs = $jobs['operational'];
			?>

			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="btn-tkpd btn-tkpd--large btn-tkpd--green ripple-effect ripple-main list-div-action-btn__action">
					  <span class="btn-tkpd__text">See Details</span>
					</a>
				  </div>
				  <div class="hidden-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--lgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				  <div class="visible-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--sgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>
		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center">
			<a href="<?php echo bloginfo( 'url' ); ?>/function/operational" class="btn-tkpd btn-tkpd--large btn-tkpd--lgreen list-function__btn-more ripple-effect ripple-main" data-function="operational">
			  <span class="btn-tkpd__text">See <?php echo sizeof( $divs ) - 6; ?> More Jobs in Operational</span>
			</a>
		  </div>
		<?php endif; ?>
	  </div> <!-- ./ row -->
	</div>
	<!-- ./Operational Function -->

	<!-- Product Function -->
	<div class="list-function 
	<?php
	if ( $isProduct && sizeof( $jobs['product'] ) ) {
		echo 'active';}
	?>
	" id="product">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-product-big.svg" alt="product" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/product"><h3 class="list-function-header__h3">Product</h3></a>
		  <p class="list-function-header__p">Design, build, and beautify the visual of the product</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isProduct && sizeof( $jobs['product'] ) ) :
			$divs = $jobs['product'];
			?>


			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
			<div class="col-sm-6 col-md-4 col-xs-12">
			  <div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
				<h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
				<p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
				<div class="list-div-highlight--mobile clearfix">
				  <div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				  <div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
				</div>
				<div class="list-div-highlight clearfix">
				  <div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				  <div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
				</div>
				<div class="list-div-desc clearfix">
				  <div class="list-div-action">
					<div class="list-div-action-btn">
					  <a href="<?php echo '../job/' . $job['slug']; ?>" class="btn-tkpd btn-tkpd--large btn-tkpd--green ripple-effect ripple-main list-div-action-btn__action">
						<span class="btn-tkpd__text">See Details</span>
					  </a>
					</div>
					<div class="hidden-xs list-div-action-btn">
					  <a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--lgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
						<span class="btn-tkpd__text">Apply Now</span>
					  </a>
					</div>
					<div class="visible-xs list-div-action-btn">
					  <a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--sgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
						<span class="btn-tkpd__text">Apply Now</span>
					  </a>
					</div>
				  </div>
				</div>
			  </div>
			</div>
			<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center">
			<a href="<?php echo bloginfo( 'url' ); ?>/function/product" class="btn-tkpd btn-tkpd--large btn-tkpd--lgreen list-function__btn-more ripple-effect ripple-main" data-function="product">
			  <span class="btn-tkpd__text">See <?php echo sizeof( $divs ) - 6; ?> More Jobs in Product</span>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<!-- ./Product Function -->

	<!-- People Function -->
	<div class="list-function 
	<?php
	if ( $isPeople && sizeof( $jobs['people'] ) ) {
		echo 'active';}
	?>
	" id="people">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-people-big.svg" alt="people" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/people"><h3 class="list-function-header__h3">People</h3></a>
		  <p class="list-function-header__p">Manage, coordinate, and collaborate with the Nakamas to create a healthy workplace</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isPeople && sizeof( $jobs['people'] ) ) :
			$divs = $jobs['people'];
			?>


			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="btn-tkpd btn-tkpd--large btn-tkpd--green ripple-effect ripple-main list-div-action-btn__action">
					  <span class="btn-tkpd__text">See Details</span>
					</a>
				  </div>
				  <div class="hidden-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--lgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				  <div class="visible-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--sgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center">
			<a href="<?php echo bloginfo( 'url' ); ?>/function/people" class="btn-tkpd btn-tkpd--large btn-tkpd--lgreen list-function__btn-more ripple-effect ripple-main" data-function="people">
			  <span class="btn-tkpd__text">See <?php echo sizeof( $divs ) - 6; ?> More Jobs in People</span>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<!-- ./People Function -->

	<!-- Tech Function -->
	<div class="list-function 
	<?php
	if ( $isTechnology && sizeof( $jobs['technology'] ) ) {
		echo 'active';}
	?>
	" id="technology">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-tech-big.svg" alt="tech" class="list-function-header__img"/>
		<div class="list-function-header-text">
		<a href="../function/technology"><h3 class="list-function-header__h3">Technology</h3></a>
		  <p class="list-function-header__p">Build and maintain the architecture of the product</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isTechnology && sizeof( $jobs['technology'] ) ) :
			$divs = $jobs['technology'];
			?>

			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function" data-tooltip="Function" data-tooltip-position="top"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location" data-tooltip="Location" data-tooltip-position="top"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="btn-tkpd btn-tkpd--large btn-tkpd--green ripple-effect ripple-main list-div-action-btn__action">
					  <span class="btn-tkpd__text">See Details</span>
					</a>
				  </div>
				  <div class="hidden-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--lgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				  <div class="visible-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--sgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center">
			<a href="<?php echo bloginfo( 'url' ); ?>/function/technology" class="btn-tkpd btn-tkpd--large btn-tkpd--lgreen list-function__btn-more ripple-effect ripple-main" data-function="technology">
			  <span class="btn-tkpd__text">See <?php echo sizeof( $divs ) - 6; ?> More Jobs in Technology</span>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<!-- ./Tech Function -->

	<!-- Intern -->
	<div class="list-function 
	<?php
	if ( $isInternship && sizeof( $jobs['internship'] ) ) {
		echo 'active';}
	?>
	" id="internship">
	  <div class="list-function-header clearfix">
		<img src="https://ecs7.tokopedia.net/assets/images/careers/ic-internship-big.svg" alt="intern" class="list-function-header__img"/>
		<div class="list-function-header-text">
		  <a href="../function/internship"><h3 class="list-function-header__h3">Internship</h3></a>
		  <p class="list-function-header__p">Learn and experience through project contribution</p>
		</div>
	  </div>
	  <div class="row">
	  <div class="list-function-divs clearfix">

		<?php
		if ( $isInternship && sizeof( $jobs['internship'] ) ) :
			$divs = $jobs['internship'];
			?>

			<?php foreach ( array_slice( $divs, 0, 6 ) as $job ) : ?>
		  <div class="col-sm-6 col-md-4 col-xs-12">
			<div class="list-div" data-shortcode="<?php echo $job['shortcode']; ?>">
			  <h4 class="list-div__h4"><?php echo strlen( $job['title'] ) < 45 ? $job['title'] : substr( $job['title'], 0, 45 ) . '...'; ?></h4>
			  <p class="list-div__p"><?php echo substr( $job['plain_description'], 0, 120 ) . '...'; ?></p>
			  <div class="list-div-highlight--mobile clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-highlight clearfix">
				<div class="list-div-helper list-div-helper--function"><?php echo $job['department']; ?></div>
				<div class="list-div-helper list-div-helper--location"><?php echo $job['location']['country']; ?></div>
			  </div>
			  <div class="list-div-desc clearfix">
				<div class="list-div-action">
				  <div class="list-div-action-btn">
					<a href="<?php echo '../job/' . $job['slug']; ?>" class="btn-tkpd btn-tkpd--large btn-tkpd--green ripple-effect ripple-main list-div-action-btn__action">
					  <span class="btn-tkpd__text">See Details</span>
					</a>
				  </div>
				  <div class="hidden-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--lgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				  <div class="visible-xs list-div-action-btn">
					<a href="<?php echo '../join/' . $job['slug']; ?>" class="list-div-action-btn__action btn-tkpd btn-tkpd--sgreen btn-tkpd--large ripple-effect ripple-main" rel="nofollow" title="Apply Now">
					  <span class="btn-tkpd__text">Apply Now</span>
					</a>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		<?php endforeach; ?>
		<?php endif; ?>
	  </div>

		<?php if ( sizeof( $divs ) > 6 ) : ?>
		  <div class="text-center">
			<a href="<?php echo bloginfo( 'url' ); ?>/function/internship" class="btn-tkpd btn-tkpd--large btn-tkpd--lgreen list-function__btn-more ripple-effect ripple-main" data-function="internship">
			  <span class="btn-tkpd__text">See <?php echo sizeof( $divs ) - 6; ?> More Jobs in Internship</span>
			</a>
		  </div>
		<?php endif; ?>

	  </div> <!-- ./row -->
	</div>
	<!-- ./Internship -->
  </div>
</section>

<section class="section_default" <?php echo $displayDefault; ?>>
  <div class="container ct-error">
	  <div class="ct-cell">
		  <img src="https://ecs7.tokopedia.net/assets/images/careers/ic-oops.png" alt="">
		  <h2 class="error__thanks">Oops, We can't find the job</h2>

		  <div class="error-copy">
			  <h3 class="error__notify">
				  <a href="<?php echo get_bloginfo( 'url' ); ?>/jobs">Try searching other jobs</a>
			  </h3>
		  </div>
	  </div>
  </div>
</section>
