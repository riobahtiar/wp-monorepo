<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

// Passing data to Header
$the_title       = 'Daftar Lowongan Kerja di Tokopedia';
$the_description = 'At Tokopedia, we believe life is too short to spend everyday just to fulfill a job description. We are seeking people who wants to create real positive impacts and build something meaningful for the world.';
$the_slug        = 'jobs/';

?>

<!doctype html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebSite" prefix="og: http://ogp.me/ns#">
<?php include_once'templates/head.php'; ?>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TN8CNF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--[if IE]>
<div class="alert alert-warning">
	<?php
	_e(
		'You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
    browser</a> to improve your experience.', 'sage'
	);
	?>
</div>
<![endif]-->
<?php
do_action( 'get_header' );
get_template_part( 'templates/header' );
?>
<main class="main-careers">
	<?php include Wrapper\template_path(); ?>
</main><!-- /.main -->
<?php if ( Setup\display_sidebar() ) : ?>
	<aside class="sidebar">
		<?php include Wrapper\sidebar_path(); ?>
	</aside><!-- /.sidebar -->
<?php endif; ?>

<?php
do_action( 'get_footer' );
get_template_part( 'templates/footer' );
wp_footer();
?>
</body>
</html>
