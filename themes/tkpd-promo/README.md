# Promotion WP Theme
This is wordpress theme for [Tokopedia Promo](https://www.tokopedia.com/promo).

## Table of contents
1. <a href="#file-structure">File Structure</a>
1. <a href="#templates">Templates</a>
1. <a href="#functions">Functions</a>
1. <a href="#meta-fields">Meta Fields</a>
1. <a href="#stylesheets">Stylesheets</a>
1. <a href="#scripts">Scripts</a>
1. <a href="#flag-app">Flag App</a>
1. <a href="#sticky">Sticky Promo</a>
1. <a href="#personalized">Personalized Promo</a>
1. <a href="#gtm">GTM Tracker</a>

## <div id="file-structure">File structure</div>
This file structure is customized from WP template files built from scratch.
```
tkpd-promo
├── assets                        # Store all assets
│   ├── css
│   ├── images
│   └── js
├── category.php                  # [Template file] Category
├── footer.php
├── functions.php                 # Store all WP and global functions
├── header.php
├── index.php                     # [Template File] Index
├── libs                          # Libraries directory
│   ├── pagination.libs.php       # Library used for pagination
│   └── simple_html_dom.php       # Library used for scrap merchant data
├── screenshot.png                # Theme image
├── search.php                    # [Template File] Search
├── single.php                    # [Template File] Single Page
├── style.css                     # Main stylesheet
├── template-index.php            # Promo index page - SEO related. (endpoint /index)
├── template-login.php            # Login page (endpoint /nest-tkpd)
├── template-parts
│   └── content
│       ├── content-category.php  # Category section
│       ├── content-search.php    # Search section
│       └── content-slider.php    # Banner slider section
└── template-scraper.php          # Scraper page (endpoint /scraper)
```

## <div id="templates">Templates</div>
Promo has 4 main template files ([Index](#template-index), [Category](#template-category), [Search](#template-search), [Single Page](#template-single)) and 3 template pages.

### <div id="template-index">Index</div>
**Index** is the main template that used for show all promos. By default **index** is displaying 9 promos, contains sticky (if available) and ordinary promo. Category `promo-berakhir` is excluded from the query.
> Example: if there's 3 sticky promos, the remain ordinary promo is 6.

Index page has 2 different query for sort by popular or latest. Sorting is determined by parameter `sort`.   
   
Index page implements [SaleEvent schema](http://schema.org/SaleEvent) using `ld+json`. SaleEvent script placed after promotion box in query loop.
   
### <div id="template-category">Category</div>
Same as Index, **Category** is used to show promo by its category. Currently Promo has 6 categories (Jual Beli Online, Official Store, Pulsa, Tiket, Promo Berakhir, and Semua Promo). This template use separated sticky post query too. We have to use separate query because in category template, by default sticky post is not displayed in first place.   
   
Official store category has different template from other category. So in this template, we used different Loop for official store content.

### <div id="template-search">Search</div>
Displayed promo based on search term.

### <div id="template-single">Single Page / Promo Landing Page</div>
This template display term and conditions, promo info, and recommendation promo from same category. Promo info contains `promo code`, `promo period` (start & end date), and `minimum transaction` (if available) from meta fields. Promo code is blank in personalized promo landing page, because its value is fetched from parameter passed by URL. See [Personalized Promo](#personalized) section below.

   
## <div id="functions">Functions</div>
All global and WP functions is located inside `functions.php`. There's 3 cluster function: _Global Function_, _Action Functions_, _Filter Functions_. On the top of file, `WP_PHOTON` variable is declared. `WP_PHOTON` is used to determined, which service will be used to serve thumbnail image.
```
WP_PHOTON == true     # thumbnail images will be served from WP_PHOTON
WP_PHOTON == false    # thumbnail images will be served from Tokopedia's Cloudinary
```
***CAUTION!*** Please look current function name before create the new one.

## <div id="meta-fields">Meta Fields</div>
Currently promo has 9 meta fields:
+ `thumbnail_image` (available on API)
+ `promo_link` (available on API)
+ `mobile_web_link`
+ `app_link` (available on API)
+ `promo_code` (available on API)
+ `min_transaction` (available on API)
+ `start_date` (available on API)
+ `end_date` (available on API)
+ `bypass_link`

Meta fields is placed separately from this template. If you want to add/change metabox or custom post type, you can change it in [tkpd-plugin-promo](https://github.com/riobahtiar/tkpd-plugin-promo) repo (private).

## <div id="stylesheets">Stylesheets</div>
Main stylesheet is located under `main.css`, and media queries is located separately under `assets/css/mediaquery.css`.

## <div id="scripts">Scripts</div>
All event/controller script is centralized in `assets/js/controller.js`. 

## <div id="flag-app">Flag App</div>
Flag app is used to indicate promo page is opened from apps (Android or iOS). Flag app is fetched from parameter `flag_app`. Once `flag_app` is set, header won't display. 

Also if `flag_app` is already set, all links inside promo domain must include `flag_app` parameter. We can handle it from server side (PHP), example:
```
...
$app = isset($_GET['flag_app']) ? $_GET['flag_app'] : NULL; // flag from app

$homeLink = $app ?
      get_bloginfo('url').'?flag_app=1&newpage=1' :
      get_bloginfo('url');
...
```

## <div id="sticky">Sticky Promo</div>
Sticky promo is used for post that will be pinned in the first place. Sticky post query is running first before the ordinary post.

In sticky post, meta `bypass_link` have a role to bypass landing page. For promo that has `bypass_link` value is **true**, button `Lihat Detail` will anchor to `promo_link` directly.

## <div id="personalized">Personalized Promo</div>
Personalized promo is addressed for individual user. Each user has unique code that can be viewed based on the url it receives. Unique code and its validity periods is represented in base64 value from `pid` parameter. Then, `pid` will be split into `cid` (promo code), `did` (expire date of code in epoch format), and `mid` (hash value of combination `cid` and `did`). Hash is produced using HMAC with `SHA256` algorithm and secret key.

Step by step:
```
1. Explode `pid` using delimiter ',,' (double commas). The result is array, first item is `cid`, second item is `did`, and the last one should be `mid`.
2. Decode `cid` and `did` using base64. Decoded value of `did` would be in epoch format. So, we have to convert it to `Y-m-d` format. 

Let $plain_code = `cid` decoded value,
    $plain_date = `did` decoded value in `Y-m-d` format
    
3. Create md5 value of concatenation from both $plain_code and $plain_date (in PHP: $plain_code.$plain_date)
4. And then we can produce hash value using HMAC.
$hash = hmac('sha256', [md5 value], [secret key])

5. Finally, we compare our generated hashed value with `mid` parameter. If both match, we can display the promo code and the expire date in landing page.
```

## <div id="gtm">GTM Tracker</div>
All GTM tracker script can be found in `assets/js/controller.js`. Promo page also used [enhanced ecommerce](https://developers.google.com/tag-manager/enhanced-ecommerce#promo) to measure promotions. For enhance ecommerce, there's 2 action needed: promo impressions and promo click. Promo impressions will be sent if the promo banner is visible to the user. Promo click will be sent if user click the promo banner.
