<?php
/* Require pagination library */
include( 'libs/pagination.libs.php' );

/* Define global variables */
define( 'WP_PHOTON', false );


/* ===========================================================
 * GLOBAL FUNCTIONS
 *
 * This is global function, you can define custom function here
 * ============================================================ */

/* Get Logo */
function logo() {
	echo get_template_directory_uri() . '/assets/images/promo_logo.png';
}

/* Get custom Image in assets folder */
function images( $images ) {
	echo get_template_directory_uri() . '/assets/images/' . $images;
}

/* Print out meta of the post */
function print_meta( $metaname ) {
	global $post;
	echo get_post_meta( $post->ID, $metaname, true );
}

/* Get meta from the post */
function get_meta( $metaname ) {
	global $post;
	return get_post_meta( $post->ID, $metaname, true );
}

/* Get Promo thumbnail image */
function get_thumbnail() {
	$thumb_url = wp_get_attachment_url( get_meta( 'thumbnail_image' ) ) ?: get_meta( 'thumbnail_image' ) ;

	if ( WP_PHOTON ) {
		/* Get thumbnail from WP PHOTON */
		if ( ! empty( $thumb_url ) ) {
			preg_match( '/(?:https:\/\/)(.*)/i', $thumb_url, $preg_url );
			return 'https://i0.wp.com/' . $preg_url[1] . '?h=390';
		}

		/* Fallback thumbnail image */
		return 'https://i0.wp.com/ecs7.tokopedia.net/promo/assets/images/tkpd-promo-thumbnail.png?h=390';
	} else {
		/* Get thumbnail from tkpd cloudinary */
		if ( ! empty( $thumb_url ) ) {
			return $thumb_url;
		}

		/* Fallback thumbnail image */
		return 'https://ecs7.tokopedia.net/img/blog/promo/2018/06/tokopedia404-promo-image.jpg';
	}
}

/* Get feature image */
function feature_img_url( $postid, $imagesize ) {
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $postid ), $imagesize );

	if ( $thumb ) {
		return $thumb['0'];
	}

	/* Fallback image */
	return 'https://ecs7.tokopedia.net/img/blog/promo/2018/06/tokopedia404-promo-image.jpg';
}

/* Get post views */
function banana_get_post_views( $postID ) {
	$count_key = 'banana_post_views_count';
	$count     = get_post_meta( $postID, $count_key, true );
	if ( $count == '' ) {
		$count = 1;
		delete_post_meta( $postID, $count_key );
		add_post_meta( $postID, $count_key, $count );
	}
	return $count . ' Views';
}

/* Get platform from user agent */
function get_platform() {

	/* User agent */
	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	$os_platform = 'Web';
	$os_array    = array(
		'/iphone/i'  => 'iPhone',
		'/ipod/i'    => 'iPod',
		'/ipad/i'    => 'iPad',
		'/android/i' => 'Android',
	);

	foreach ( $os_array as $regex => $value ) {
		if ( preg_match( $regex, $user_agent ) ) {
			$os_platform = $value;
		}
	}
	return $os_platform;
}



/* ===========================================================
 * FILTER FUNCTIONS
 *
 * Any functions used for filter WP function
 * ============================================================ */

/* Change excerpt more with ellipsis */
function wpdocs_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

/* Limit excerpt length */
function custom_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/* Display excerpt metabox as default */
function show_hidden_meta_boxes( $hidden, $screen ) {
	if ( 'post' == $screen->base ) {
		foreach ( $hidden as $key => $value ) {
			if ( 'postexcerpt' == $value ) {
				unset( $hidden[ $key ] );
				break;
			}
		}
	}
	return $hidden;
}
add_filter( 'default_hidden_meta_boxes', 'show_hidden_meta_boxes', 10, 2 );



/* ===========================================================
 * ACTION FUNCTIONS
 *
 * You can add/remove any action function here
 * ============================================================ */

/* After setup function */
function promotion_setup() {
	add_theme_support( 'automatic-feed-links' );  // Feed links
	add_theme_support( 'title-tag' );             // Title tag
	add_theme_support( 'post-thumbnails' );       // Post Thumbnail
	add_theme_support(
		'infinite-scroll', array(
			'container' => 'content',
		)
	);                                         // Infinite scroll
}
add_action( 'after_setup_theme', 'promotion_setup' );

/* Enqueue scripts */
function promotion_scripts() {
	//wp_enqueue_style('bootstrapcss','https://ecs7.tokopedia.net/assets/css/bootstrap.min.css');   // Bootstrap
	wp_enqueue_style( 'slickcss', 'https://ecs7.tokopedia.net/assets/css/slick.css' );                // Slick CSS
	wp_enqueue_style( 'stylesheet', get_stylesheet_uri() );                                            // Theme style CSS
	wp_enqueue_style( 'mediaquery', get_template_directory_uri() . '/assets/css/mediaquery.css' );       // Media queries CSS

	wp_enqueue_style( 'opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' ); // Font Open Sans

	wp_enqueue_script( 'bootstrap', 'https://ecs7.tokopedia.net/assets/js/bootstrap.min.js', array( 'jquery' ), '2016', true ); // Bootstrap JS
	wp_enqueue_script( 'slickjs', 'https://ecs7.tokopedia.net/assets/js/slick.min.js', true );                             // Slick JS
	wp_enqueue_script( 'freshmarketerjs', '//cdn.freshmarketer.com/222317/774992.js', true );                             // Slick JS

	if ( is_single() ) {
		wp_enqueue_script( 'addthisjs', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58b542138a43512f' );  // Addthis JS
	}

	wp_enqueue_script( 'controller', get_template_directory_uri() . '/assets/js/controller.js', array( 'jquery' ), '2016', true );// Controller

}
add_action( 'wp_enqueue_scripts', 'promotion_scripts' );

/* Betaout Scripts */
function betaout_script_footer(){ ?>
<!-- Betaout #betaout-27032018 -->
<script type="text/javascript">
var _bout = _bout || [];
var _boutAKEY = "la8jnjksma8jn8rhq14us9h4jlv8yhk47l54fjslv4", _boutPID = "30045";
var d = document, f = d.getElementsByTagName("script")[0], _sc = d.createElement("script");
_sc.type = "text/javascript";
_sc.async = true;
_sc.src = "//js.betaout.com/jal-v2.min.js";
f.parentNode.insertBefore(_sc, f);
_bout.push(["identify", {} ]);
</script>
	<?php
}

add_action( 'wp_footer', 'betaout_script_footer' );


/* Set meta count post view */
function banana_set_post_views( $postID ) {
	$count_key = 'banana_post_views_count';
	$count     = get_post_meta( $postID, $count_key, true );
	if ( $count == '' ) {
		$count = 1;
		delete_post_meta( $postID, $count_key );
		add_post_meta( $postID, $count_key, $count );
	} else {
		$count++;
		update_post_meta( $postID, $count_key, $count );
	}
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

/* Set tracker in header */
function banana_track_post_views( $postID ) {
	if ( ! is_single() ) {
		return;
	}
	if ( empty( $postID ) ) {
		global $post;
		$postID = $post->ID;
	}
	banana_set_post_views( $postID );
}
add_action( 'wp_head', 'banana_track_post_views' );

/* Remove Wp Embed */
function disable_embeds_code_init() {
	add_filter( 'embed_oembed_discover', '__return_false' );
	remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
}
add_action( 'init', 'disable_embeds_code_init', 9999 );

/* Remove Wp Emoji Release */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


/*
* < AMP Configuration >
*/

// Setting custom stylesheet
add_filter( 'amp_post_template_file', 'tkpdpromo_amp_set_custom_style_path', 10, 3 );

function tkpdpromo_amp_set_custom_style_path( $file, $type, $post ) {
	if ( 'style' === $type ) {
		$file = dirname( __FILE__ ) . '/amp/style.php';
	}
	return $file;
}

add_filter( 'amp_content_max_width', 'tkpdpromo_amp_change_content_width' );

function tkpdpromo_amp_change_content_width( $content_max_width ) {
	return 600;
}

// Set tokopedia promo logo
add_action( 'amp_post_template_css', 'tkpdpromo_amp_additional_css_styles' );

function tkpdpromo_amp_additional_css_styles( $amp_template ) {
	// only CSS here please...
	?>
	header.amp-wp-header a {
		background-image: url( 'https://ecs7.tokopedia.net/promo/assets/images/promo_logo.png' );
		background-repeat: no-repeat;
		background-size: contain;
		display: block;
		height: 25px;
		alt: "tokopedia promo";
		margin: 0 auto;
		top: 0;
		  right: 0;
		  bottom: 0;
		  left: 0;
		text-indent: -9999px;
	}
	<?php
}

// Setting custom template for amp page
add_filter( 'amp_post_template_file', 'tkpdpromo_amp_set_custom_template', 10, 3 );

function tkpdpromo_amp_set_custom_template( $file, $type, $post ) {
	if ( 'single' === $type ) {
		$file = dirname( __FILE__ ) . '/amp/amp-single.php';
	}
	return $file;
}

// Setting post excerpt
function tkpdpromo_prefix_excerpt_to_content( $content ) {
	$excerpt = get_post()->post_excerpt;
	if ( $excerpt ) {
		$content = $excerpt . $content;
	}
	return $excerpt;
}

// Setting footer
add_action( 'amp_post_template_footer', 'tkpdpromo_amp_my_custom_footer' );
function tkpdpromo_amp_my_custom_footer( $amp_template ) {
	?>
  <footer class="footer">
	 <div class="footer-top">
		<div class="pull-left footer-copyright">
		   <p>Dapatkan Aplikasi Mobile Tokopedia</p>
		</div>
		<div class="pull-right footer-connect">
		   <ul>
			  <li><a href="https://itunes.apple.com/us/app/tokopedia/id1001394201?ls=1&mt=8" target="_blank"><amp-img src="https://ecs7.tokopedia.net/promo/assets/images/appstore.png" class="footer_mobile-app-img" height="40px" width="130px" alt="AppStore" layout="responsive"></amp-img></a></li>
			  <li><a href="https://play.google.com/store/apps/details?id=com.tokopedia.tkpd" target="_blank"><amp-img src="https://ecs7.tokopedia.net/promo/assets/images/googleplay.png" class="footer_mobile-app-img" height="40px" width="130px" alt="PlayStore" layout="responsive"></amp-img></a></li>
		   </ul>
		</div>
	 </div>
	 <div class="footer-bottom">
		<div class="footer__lower u-clearfix u-center">
		  <p class="u-line-height-4"><a class="u-ml1" href="https://m.tokopedia.com/help">Hubungi Kami</a>&nbsp; | &nbsp;<a href="https://www.tokopedia.com/promo/">Situs Desktop</a></p>
		  <p class="u-line-height-4"><a href="https://m.tokopedia.com/terms.pl">Syarat &amp; Ketentuan</a>&nbsp; | &nbsp;<a href="https://m.tokopedia.com/privacy.pl">Kebijakan Privasi</a></p>
		  <div class="u-line-height-4 footer__lower-text--smaller"><h3>Copyright 2009-<!-- -->2018<!-- -->, PT Tokopedia</h3></div></div>
	 </div>
  </footer>
	<?php
}

/*
* </ AMP Configuration >
*/
