<?php
/*
 * Template Name: Cashback Post
 * Template Post Type: post
 * Author: Khaidir Afif
 */

if ( isset( $_GET['mobile'] ) ) {
	$url = 'https://www.tokopedia.com/promo/cashback-pasti/?mobile=' . $_GET['mobile'];
	if ( isset( $_GET['utm_source'] ) ) {
		$url .= '&utm_source=' . $_GET['utm_source'];
	}
} else {
	$url = 'https://www.tokopedia.com/promo/cashback-pasti/';
	if ( isset( $_GET['utm_source'] ) ) {
		$url .= '?utm_source=' . $_GET['utm_source'];
	}
}
if ( isset( $_GET['utm_medium'] ) ) {
	$url .= '&utm_medium=' . $_GET['utm_medium'];
}
if ( isset( $_GET['utm_campaign'] ) ) {
	$url .= '&utm_campaign=' . $_GET['utm_campaign'];
}
if ( isset( $_GET['utm_content'] ) ) {
	$url .= '&utm_content=' . $_GET['utm_content'];
}

wp_redirect( $url );
exit;



