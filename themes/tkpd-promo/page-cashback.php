<?php

/*
Template Name: PROMO 10 CASHBACK
*/


	$flagApp = isset( $_GET['flag_app'] ) ? $_GET['flag_app'] : '';


if ( isset( $_GET['mobile'] ) ) {
	$flagApp = true;
}


	$homeLink = ( $flagApp ) ?
		'tokopedia://home' :
		'https://www.tokopedia.com';



	$promoLink = ( $flagApp ) ? 'tokopedia://promo' : 'https://www.tokopedia.com/promo';

	$contactUsLink = ( $flagApp ) ? 'tokopedia://contact-us' : 'https://www.tokopedia.com/contact-us';

	$oppoLink = ( $flagApp ) ? 'tokopedia://shop/1562471' : 'https://www.tokopedia.com/oppo';

	$whiteLink = ( $flagApp ) ? 'tokopedia://hot/whitelight-pemutih-gigi' : 'https://www.tokopedia.com/hot/whitelight-pemutih-gigi';

	$backpackLink = ( $flagApp ) ? 'tokopedia://hot/backpack-rolltop-kekinian' : 'https://www.tokopedia.com/hot/backpack-rolltop-kekinian';

	$kulkasLink = ( $flagApp ) ? 'tokopedia://hot/kulkas-mini' : 'https://www.tokopedia.com/hot/kulkas-mini';

	$mugLink = ( $flagApp ) ? 'tokopedia://hot/magic-mug' : 'https://www.tokopedia.com/hot/magic-mug';

	$applinkOverride = '';

if ( isset( $_GET['mobile'] ) && $_GET['mobile'] == 'android' ) {
		$flagApp = false;
}

	$hotLink = ( $flagApp ) ? 'tokopedia://hot' : 'https://wwww.tokopedia.com/hot?override_url=1';

if ( isset( $_GET['mobile'] ) && $_GET['mobile'] == 'android' ) {
		$hotLink = 'tokopedia://home';
}



	$officialStoreLink = ( $flagApp ) ? 'tokopedia://official-store/mobile' : 'https://www.tokopedia.com/official-store';

		$metaTitle = 'Promo Cashback Tokopedia - Belanja Dapat Uang Kembali | Tokopedia';
		$metaDesc  = 'Cashback di setiap transaksi dan dapatkan uang kembali untuk belanja atau bayar apapun kebutuhan mu hanya di Tokopedia.';

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <style>
  html.noscroll{
	  position: fixed;
	  width: 100%;
	  top:0;
	  left: 0;
	  height: 100%;
	  overflow-y: scroll !important;
	  z-index: 10;
   }
  </style>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://ecs7.tokopedia.net/assets/css/promo/scrollrevealjs.js"></script>
	<link rel="stylesheet" href="https://ecs7.tokopedia.net/assets/css/promo/promo10style-1506859898651.css">
	<link rel="stylesheet" href="https://ecs7.tokopedia.net/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://ecs7.tokopedia.net/assets/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<link href="https://ecs7.tokopedia.net/assets/fonts/fontawesome-webfont.ttf" rel="stylesheet">
	<link href="https://ecs7.tokopedia.net/assets/fonts/fontawesome-webfont.woff" rel="stylesheet">
	<link href="https://ecs7.tokopedia.net/assets/fonts/fontawesome-webfont.woff2" rel="stylesheet">

		<title>Promo Cashback Tokopedia - Belanja Dapat Uang Kembali | Tokopedia</title>

	<meta itemprop="name" content="<?php echo $metaTitle; ?>">

	<meta itemprop="image" content="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/28132259/Tokopedia_Buyer_Cashback-Oktober_ig-socmed.jpg">
	<link rel="dns-prefetch" href="//ecs7.tokopedia.net" />
	<link rel="dns-prefetch" href="//fonts.googleapis.com" />

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TZNNXTG');</script>
<!-- End Google Tag Manager -->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TZNNXTG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<!-- Header -->
	<?php if ( ! isset( $_GET['flag_app'] ) ) { ?>
	<div style="overflow-x:hidden" class="mf">

	<div class="overlay"></div>
  <a href="<?php echo $homeLink; ?>"><div class="get-cashback-container-m">Dapatkan Cashback Sekarang</div></a>

	<header class="header" style="z-index:201">
		<div class="container">
			<div class="logo">
				<a href="<?php echo $homeLink; ?>"> <img src="https://ecs7.tokopedia.net/assets/images/tokopedia-logo.png" height="30" alt="tokopedia promo" scale="0"> </a>
				  </div>
			<div class="wrap_bridge-menu pull-right hidden-xs">
				<div class="bridge-widget inline-block">
				<button class="bw-button">Expander</button>
				<div class="bw-container">
					<div class="bw-item">
						<a href="https://www.tokopedia.com/" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-toped"></div>
							</div>
							<p class="bw-icon__text">Jual Beli Online</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://www.tokopedia.com/official-store" target="_blank" id="bw-official-store">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-official"></div>
							</div>
							<p class="bw-icon__text">Official Store</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://www.tokopedia.com/pulsa" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-pulsa"></div>
							</div>
							<p class="bw-icon__text">Produk Digital</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://tiket.tokopedia.com/" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-tiket"></div>
							</div>
							<p class="bw-icon__text">Tiket Kereta</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://www.tokopedia.com/berbagi" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-donasi"></div>
							</div>
							<p class="bw-icon__text">Donasi</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://www.tokopedia.com/bantuan/" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-help"></div>
							</div>
							<p class="bw-icon__text">Bantuan</p>
							<span class="clear-b"></span>
						</a>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</header>
	<?php } ?>
	<!-- Header -->

<div class="p10-banner">
  <div class="container">
	<div class="banner-content">
	<div class="banner-text">
	  <div class="banner-text-left">
		<span>
		  <!-- <img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/22102254/asset_7_4x-100-2.png" class="header-spark1">
		  <img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/22102254/asset_7_4x-100-2.png" class="header-spark2"> -->
		  Tanpa Minimum Pembelian</span>
		  <br> Dapatkan 20% Cashback hingga
		<div>100 RIBU PER TRANSAKSI!</div>
		<small style="
	letter-spacing: 0px;
	display: block; letter-spacing:-1px">BISA SAMPAI 5X TRANSAKSI</small>
	  </div>
	  <div class="banner-text-right">
		<div>Setiap hari dari 2-6 Oktober 2017</div>
		<input type="text" value="CASHBACKPASTI" id="kodecashback" style="position:absolute;left:-90000000px" readonly><div id="kode">CASHBACKPASTI</div><div class="salin-kode">SALIN</div>
		<div style="clear:both"></div>
		<div><span id="see-tnc">Lihat S&K </span>&nbsp;•&nbsp; <span id="see-faq">Lihat FAQ</span></div>
	  </div>
	  <div style="clear:both"></div>
	</div>
	<img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/27084221/assets-new.png" class="img-responsive">
	</div>
  </div>
</div>
<div class="container">
  <div class="Dapatkan-Cashback-da text-center">
	Dapatkan Cashback dalam 4 Langkah Mudah
  </div>
  <div class="p10-step-container">
	<div class="p10-step-wrapper">
	  <div class="p10-step">
		<img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/22032949/step1.png" class="img-responsive">
		<div class="p10-step-text">
		  <h2 class="text-center">Pilih Produk & Bayar</h2>
		  <p class="text-center">Pilih produk favoritmu di <a href="<?php echo $homeLink; ?>">Marketplace</a> & <a href="<?php echo $officialStoreLink; ?>">Official Store</a></p>
		</div>
	  </div>
	  <div class="p10-step">
		<img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/22032950/step2.png" class="img-responsive">
		<div class="p10-step-text">
		  <h2 class="text-center">Masukkan kode <span style="color:#FF5722">CASHBACKPASTI</span></h2>
		  <p class="text-center">Gunakan kode promo <strong>CASHBACKPASTI</strong> di halaman pembayaran</p>
		</div>
		<img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/25042854/single_sparkle.png">
	  </div>
	  <div class="p10-step">
		<img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/22032950/step3.png" class="img-responsive">
		<div class="p10-step-text">
		  <h2 class="text-center">Konfirmasi Terima Barang</h2>
		  <p class="text-center">Silahkan Lakukan konfirmasi saat barang sudah diterima</p>
		</div>
	  </div>
	  <div class="p10-step">
		<img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/22032951/step4.png" class="img-responsive">
		<div class="p10-step-text">
		  <h2 class="text-center">Nikmati Cashback-mu</h2>
		  <p class="text-center">Cashback akan masuk dalam 1 x 24 jam ke TokoCash & dapat digunakan untuk transaksi berikutnya</p>
		</div>
		<img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/25042854/single_sparkle.png">
	  </div>
	</div>
  </div>
  <div class="text-center">
	<div class="get-cashback-container">
	  <img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/25042854/single_sparkle.png">
	  <a href="<?php echo $homeLink; ?>"><button class="btn-orange">Dapatkan Cashback Sekarang</button></a>
	  <img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/25042920/double_sparkles_360.png">
	</div>
  </div>
  <div class="text-center">
   <div class="addthis_inline_share_toolbox"></div>
  </div>
</div>
<div class="p10-tnc-container">
  <div class="container">
	<div class="p10-tnc-wrapper">
	  <div class="p10-tnc-reveal">
		<div class="p10-tnc">
		  <h2><img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/25043057/tnc_360.png">Syarat dan Ketentuan</h2>
		  <ul>
			<li>Promo berlaku pada tanggal <strong>2 - 6 Oktober 2017.</strong></li>
			<li>Promo berlaku di <a href="<?php echo $homeLink; ?>">Marketplace</a> dan <a href="<?php echo $officialStoreLink; ?>">Official Store</a>.</li>
			<li>Cashback akan diterima oleh pembeli maksimal 1 x 24 jam saat transaksi sudah selesai (pembeli sudah melakukan Konfirmasi Terima Barang).</li>
		  </ul>
		  <div class="text-center">
			<span data-modal-target="tnc">Selengkapnya <i class="fa fa-angle-right" aria-hidden="true"></i></span>
		  </div>
		</div>
	  </div>
	  <div class="p10-faq-wrapper-reveal">
		<div class="p10-faq-wrapper">
		  <h2><img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/25043103/faq.png">FAQ</h2>
		  <div class="p10-faq no-margin-top" data-modal-target="faq1">Kapan Cashback Tokopedia  akan diterima?</div>
		  <div class="p10-faq" data-modal-target="faq2">Apakah promo Cashback Tokopedia dapat digabungkan dengan promo Cashback Merchant?</div>
		  <div class="p10-faq no-border-btm" data-modal-target="faq3">Mengapa cashback tidak diterima padahal transaksi sudah selesai?</div>
		  <div class="text-center">
			<span><a href="https://www.tokopedia.com/bantuan/faq-promo-cashback-tokopedia/">Selengkapnya <i class="fa fa-angle-right" aria-hidden="true"></i></a></span>
		  </div>
		</div>
	  </div>
	</div>
  </div>
  <div class="p10-tnc-wrapper-m">

  </div>
	  <!-- Tokocash bisa digunakan dimana saja -->
	  <div class="container">
		<div class="text-center p10-tokocash-header">
		  <h2>Rekomendasi Produk</h2>
		  <p>Temukan deretan produk favorit yang bisa jadi inspirasi belanjamu, kini lebih untung dengan Promo Cashback hingga 100 ribu per transaksi! Yuk Belanja sekarang!</p>
		</div>
		<div class="p10-tokocash-container">

		  <div class="p10-tokocash-wrapper rekomendasi-produk">
			<a href="<?php echo $whiteLink; ?>">
			  <img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/28083652/Homepage-max-150KB_-1.jpg">
			  <div class="p10-tokocash-mulai-dari">Mulai dari</div>
			  <div class="p10-tokocash-rp"><span style="color:#ff5722">Rp 25rb</span></div>
			</a>
		  </div>
		  <div class="p10-tokocash-wrapper rekomendasi-produk">
			<a href="<?php echo $backpackLink; ?>">
			  <img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/28083652/Homepage-max-150KB_-2.jpg">
			  <div class="p10-tokocash-mulai-dari">Mulai dari</div>
			  <div class="p10-tokocash-rp"><span style="color:#ff5722">Rp 80rb</span></div>
			</a>
		  </div>
		  <div class="p10-tokocash-wrapper rekomendasi-produk">
			<a href="<?php echo $kulkasLink; ?>">
			  <img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/09/28083651/Homepage-max-150KB_-3.jpg">
			  <div class="p10-tokocash-mulai-dari">Mulai dari</div>
			  <div class="p10-tokocash-rp"><span style="color:#ff5722">Rp 150rb</span></div>
			</a>
		  </div>
		  <div class="p10-tokocash-wrapper rekomendasi-produk">
			<a href="https://www.tokopedia.com/toppicks/official-store-top-brands/">
			  <img src="https://ecs7.tokopedia.net/toppicks/wp-content/uploads/2017/10/02044611/600x600_REV.jpg">
			  <div class="p10-tokocash-mulai-dari">Mulai dari</div>
			  <div class="p10-tokocash-rp"><span style="color:#ff5722">Rp 18rb</span></div>
			</a>
		  </div>
		  <div style="clear:both"></div>
		</div>

		<div class="text-center">
		  <a href="<?php echo $hotLink; ?>"><button class="btn-orange btm ghost">Rekomendasi Lainnya</button></a>
		</div>

		<!-- <div class="text-center p10-tokocash-header">
		  <h2>Rekomendasi Produk</h2>
		  <p>Vim in audiam latine. Eu idque volumnus scriptorem quo. Semper interpretaris mediocritatem cu usu, quas pertinax concetam vel in.</p>
		</div>
		<div class="p10-tokocash-container">
		  <div class="p10-tokocash-wrapper rekomendasi-produk">
			<img src="http://via.placeholder.com/150x150">
		  </div>
		  <div class="p10-tokocash-wrapper rekomendasi-produk">
			<img src="http://via.placeholder.com/150x150">
		  </div>
		  <div class="p10-tokocash-wrapper rekomendasi-produk">
			<img src="http://via.placeholder.com/150x150">
		  </div>
		  <div class="p10-tokocash-wrapper rekomendasi-produk">
			<img src="http://via.placeholder.com/150x150">
		  </div>
		</div>
		<div class="text-center">
		  <button class="btn-orange">Beli Sekarang</button>
		</div> -->
	  </div>
  </div>
</div>

<div class="tkpd-modal" id="faq1">
  <div class="tkpd-close"><i class="fa fa-angle-times"></i></div>
  <div>
	<h2 class="text-center">Kapan Cashback Tokopedia  akan diterima?</h2>
	<p>
	  <div>Cashback ke TokoCash akan diterima maksimal 1×24 jam setelah:</div>
	  <ul>
		<li>Anda telah melakukan konfirmasi terima barang untuk transaksi di <a href="<?php echo $homeLink; ?>">www.tokopedia.com</a></li>
		<li>Transaksi telah berhasil untuk pembelian Produk Digital & Tiket Kereta</li>
	  </ul>
	</p>
  </div>
</div>

<div class="tkpd-modal" id="faq2">
  <div class="tkpd-close"><i class="fa fa-angle-times"></i></div>
  <div>
	<h2 class="text-center">Apakah promo Cashback Tokopedia dapat digabungkan dengan promo Cashback Merchant?</h2>
	<p>
	  <div>Saat melakukan transaksi di Marketplace dan Official Store, Anda dapat menggunakan promo cashback Tokopedia digabung dengan cashback dari penjual.</div>
	</p>
  </div>
</div>

<div class="tkpd-modal" id="faq3">
  <div class="tkpd-close"><i class="fa fa-angle-times"></i></div>
  <div>
	<h2 class="text-center"> Mengapa cashback tidak diterima padahal transaksi sudah selesai?</h2>
	<p>
	  <div>Pastikan transaksi yang Anda lakukan sesuai dengan syarat dan ketentuan <a href="<?php echo $promoLink; ?>">Promo Cashback Tokopedia</a>, serta <a href="https://www.tokopedia.com/terms.pl">syarat dan ketentuan Tokopedia</a>. Untuk info lebih lanjut, silakan hubungi <a href="<?php echo $contactUsLink; ?>">Customer Care Tokopedia</a>.</div>
	</p>
  </div>
</div>

<div class="tkpd-modal" id="tnc">
  <div class="tkpd-close"><i class="fa fa-angle-times"></i></div>
  <div>
	<h2 class="text-center">Syarat dan Ketentuan</h2>
	<div class="limit-height">
	  <ol style="padding-left:25px;">
		<li>Promo berlaku pada tanggal <strong>2 - 6 Oktober 2017.</strong></li>
		<li>Promo berlaku di <a href="<?php echo $homeLink; ?>">Marketplace</a> dan <a href="<?php echo $officialStoreLink; ?>">Official Store</a>.</li>
		<li>Promo ini tidak ada minimum transaksi.</li>
		<li>Promo berlaku untuk semua metode pembayaran yang tersedia di Tokopedia.</li>
		<li>Untuk mendapatkan cashback ke TokoCash 20% dengan maksimum cashback sebesar Rp100.000 dari harga produk per transaksi, pengguna harus memasukkan kode voucher <strong>CASHBACKPASTI</strong> di halaman pembayaran <a href="<?php echo $homeLink; ?>">Tokopedia</a> dengan ketentuan sebagai berikut:
1 (satu) akun Tokopedia, 1 (satu) nomor handphone, atau 1 (satu) identitas pembayaran hanya bisa mengikuti promo ini sebanyak <strong>5 kali transaksi selama periode promo</strong></li>
		<li>Untuk mendapatkan cashback, pengguna harus melakukan aktivasi TokoCash dalam 7 hari, atau cashback akan hangus.</li>
		<li>Cashback akan diterima oleh pembeli maksimal 1x24 jam saat transaksi sudah selesai (pembeli sudah melakukan Konfirmasi Terima Barang).</li>
		<li>Pastikan nomor handphone yang sudah terverifikasi aktif pada saat melakukan transaksi. Jika terjadi perubahan nomor handphone pada akun Tokopedia pengguna dan nomor tersebut sudah tidak aktif, maka pengguna tidak akan mendapatkan cashback. </li>
		<li>Promo tidak berlaku untuk:
		  <ul>
			<li>Pengguna Virtual Account yang baru mendaftar akun Tokopedia setelah tanggal 2 Oktober 2017.</li>
			<li>Penggabungan dengan promo lain di Tokopedia</li>
			<li>Dropshipper</li>
			<li>Pembelian produk <a href="<?php echo $oppoLink; ?>">Official Store OPPO</a>.</li>
		  </ul>
		</li>
		<li>Tokopedia berhak dengan tanpa pemberitahuan sebelumnya, melakukan tindakan-tindakan yang diperlukan apabila ditemukan tindakan manipulasi/kecurangan.</li>
		<li>Syarat dan Ketentuan promo ini merupakan bagian yang tidak terpisahkan dari <a href="https://www.tokopedia.com/terms.pl">Syarat dan Ketentuan Tokopedia</a>, dan sewaktu-waktu dapat berubah tanpa pemberitahuan sebelumnya.</li>
	  </ol>
	</div>
  </div>
</div>


	<scrip src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

	  <script>
	  (function($){



		$(document).ready(function(){
		  

		  $(window).load(function(){
			if($(window).width() > 650){
			  if($('.p10-tnc').height() < $('.p10-faq-wrapper').height()){
				$('.p10-tnc').height($('.p10-faq-wrapper').height());
			  }
			  else{
				$('.p10-faq-wrapper').height($('.p10-tnc').height());
			  }
			}
		  });


		  //gtmtracker

		  $('.header .logo a').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Header - Tokopedia'
			  });
		  });

		  var flagHelmon = false;
		  $('button.bw-button').on("mouseenter" , function() {
			  if(!flagHelmon){
				window.dataLayer = window.dataLayer || [];
				dataLayer.push({
				'event' : 'clickPromoMicrosite',
				'eventCategory' : 'Promo',
				'eventAction' : 'Click',
				'eventLabel' : 'Cashback Pasti - Header - Keypad'
				});
			  }
			  flagHelmon = true;
		  });

		  $('.salin-kode').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Top - Tersalin'
			  });
		  });

		  $('#see-tnc').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Top - Lihat S&K'
			  });
		  });

		  $('#see-faq').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Top - Lihat FAQ'
			  });
		  });

		  $('.get-cashback-container a, .get-cashback-container-m').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Mid - Dapatkan Cashback Sekarang'
			  });
		  });

		  $('.p10-tnc span[data-modal-target]').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Mid - Selengkapnya (Syarat & Ketentuan)'
			  });
		  });

		  $('.p10-faq-wrapper a[href*=promo-cashback-tokopedia]').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Mid - Selengkapnya (FAQ)'
			  });
		  });

		  $('.p10-faq-wrapper .p10-faq[data-modal-target]').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Mid - FAQ '+$(this).index()
			  });
		  });

		  $('.p10-tokocash-wrapper').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Bottom - Rekomendasi Produk '+($(this).index() + 1)
			  });
		  });

		  $('.p10-step-text a[href="https://www.tokopedia.com"]').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Top - Tokopedia.com'
			  });
		  });

		  $('.p10-step-text a[href="https://www.tokopedia.com/official-store"]').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Top - Official Store'
			  });
		  });

		  $('.btn-orange.btm').click(function() {
			  window.dataLayer = window.dataLayer || [];
			  dataLayer.push({
			  'event' : 'clickPromoMicrosite',
			  'eventCategory' : 'Promo',
			  'eventAction' : 'Click',
			  'eventLabel' : 'Cashback Pasti - Bottom - Lihat Rekomendasi Lainnya'
			  });
		  });



		  if($(window).width() < 650){
			 $(document).on('scroll',function(){
			   if($(window).scrollTop() > $('.get-cashback-container').offset().top - $(window).height()+62){
				 $(".get-cashback-container-m").addClass("godown");
			   }else{
				 $(".get-cashback-container-m").removeClass("godown");
			   }
			 });
		  }else{


		  }



		  $("#see-tnc").click(function() {
			  $('html, body').animate({
				  scrollTop: $(".p10-tnc").offset().top - 20
			  }, 1000);
		  });

		  $("#see-faq").click(function() {
			  $('html, body').animate({
				  scrollTop: $(".p10-faq-wrapper").offset().top - 20
			  }, 1000);
		  });

		  $(".p10-drop-header").click(function(){
			if( $(this).next(".p10-drop-content").is(":hidden"))
			  {
				$(this).find(".chevron").css("transform","rotate(180deg)");
			  }else{
				$(this).find(".chevron").css("transform","rotate(0deg)");
			  }
			$(this).siblings(".p10-drop-content").slideToggle();

		  });

		  //MODAL
		  $('*[data-modal-target]').click(function(){
			popModal($(this).attr('data-modal-target'));
		  });

		  $('.tkpd-modal .tkpd-close').click(function(){
			closeModal();
		  });

		  $(".overlay").click(function(){
			closeModal();
		  });

		  var curScrollTop;

		  function popModal(a){
			$('#'+a).addClass('active');
			$('.overlay').addClass('active');
			// $("body").css("overflow-y","hidden");
			curScrollTop = $(window).scrollTop();
			$('html').toggleClass('noscroll').css('top', '-' + curScrollTop + 'px');
		  }

		  function closeModal(){
			$(".tkpd-modal.active").removeClass('active');
			$('.overlay').removeClass('active');
			$('html').toggleClass('noscroll');
			$(document).scrollTop(curScrollTop);
		  }
		  $('.salin-kode').click(function(){
			copycashbackpasti();
		  });
		  function copycashbackpasti() {
			  document.getElementById("kodecashback").select();

			try {
				var successful = document.execCommand('copy');
				var msg = successful ? 'successful' : 'unsuccessful';
				$(".salin-kode").html("TERSALIN");
				$(".salin-kode").css("background","#ef430c");
			  } catch (err) {
			  }
		  }
		});
		
	  })(jQuery);


	  </script>

	<!-- Footer -->
	<footer class="footer" id="slide4">
		<div class="container">
			<div class="pull-left footer-copyright"> © 2009-2017, PT Tokopedia</div>
			<div class="pull-right footer-connect">
				<ul>
					<li>
						<a href="https://itunes.apple.com/us/app/tokopedia/id1001394201?ls=1&amp;mt=8" target="_blank"><img class="footer-connect__apps" src="https://ecs7.tokopedia.net/promo/assets/images/appstore.png" alt="appstore" scale="0"></a>
					</li>
					<li>
						<a href="https://play.google.com/store/apps/details?id=com.tokopedia.tkpd" target="_blank"><img class="footer-connect__apps" src="https://ecs7.tokopedia.net/promo/assets/images/googleplay.png" alt="playstore" scale="0"></a>
					</li>
				</ul>
				<ul>
					<li>
						<a href="https://www.facebook.com/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/facebook.png" alt="facebook" scale="0"></a>
					</li>
					<li>
						<a href="http://instagram.com/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/instagram.png" alt="instagram" scale="0"></a>
					</li>
					<li>
						<a href="https://twitter.com/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/twitter.png" alt="twitter" scale="0"></a>
					</li>
					<li>
						<a href="https://www.youtube.com/user/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/youtube.png" alt="youtube" scale="0"></a>
					</li>
					<li>
						<a href="https://plus.google.com/+tokopedia/posts" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/googleplus.png" alt="google_plus" scale="0"></a>
					</li>
					<li>
						<a href="https://www.tokopedia.com/blog" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/blog.png" alt="blog_tokopedia" scale="0"></a>
					</li>
				</ul>
			</div>
		</div>
	</footer>
	<!-- Footer -->






	<?php wp_footer(); ?>
 </div>
<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59d1b74c65e04d5e"></script> 
</body>

</html>
