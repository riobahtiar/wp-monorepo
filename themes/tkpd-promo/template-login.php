<?php
/*
Template Name: Custom Login Page
Author: Rio Bahtiar <rio.bahtiar@tokopedia.com>

*/

  global $current_user;
  wp_get_current_user();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="noindex,nofollow">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Tokopedia Auth Login</title>
	<!-- Bootstrap -->
	<link href="https://ecs7.tokopedia.net/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
	  body{
		font-family: 'Open Sans', sans-serif;
	  }
	  a{
		color: #41b548;
	  }
	  a:hover{
		color: #fe5722;
	  }
	  .login-toped blockquote{
		margin-top: 50px;
	  }
	  .navbar-default{
		background-color: #41b548;
		border: none;
		border-radius: 0;
		color: white;
	  }
	  .navbar-default > a,
	  .navbar-default .navbar-nav>li>a{
		color: white;
	  }
	  .navbar-default .navbar-brand{
		color: white;
	  }
	  ul.nav.navbar-nav.navbar-right.naka-stats {
		padding-right: 10px;
	  }
	  .navbar-default a.navbar-brand,
	  .btn-danger{
		background-color: #fe5722;
	  }
	  .navbar-default a.navbar-brand:hover{
		background-color: hsl(13, 100%, 40%);
		color: white;
	  }
	  .navbar-default .navbar-nav>li>a:hover{
		color: white;
	  }
	  .bg-success {
		  background-color: #41b549;
		  padding: 5px;
		  font-size: 15px;
		  font-weight: 600;
		  color: white;
		  border-radius: 15px;
		  letter-spacing: 1px;
		  text-transform: uppercase;
	  }
	  .btn-success {
		  color: #fff;
		  background-color: #5cb85c;
		  border-color: #4cae4c;
		  border-radius: 17px;
		  min-width: 100px;
	  }
	  blockquote {
		  padding: 10px 20px;
		  margin: 0 0 20px;
		  font-size: 17.5px;
		  border-left: 10px solid #5cb85c;
		  background: rgba(92, 184, 92, 0.18);
		  color: #5cb85c;
		  text-transform: uppercase;
		  letter-spacing: 1px;
	  }
	</style>
  </head>
  <body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
	<div class="navbar-header hidden-xs">
	  <a class="navbar-brand" href="<?php echo home_url(); ?>">
		<i class="glyphicon glyphicon-chevron-left"></i> homepage
	  </a>
	</div>
<?php
if ( is_user_logged_in() ) {
	?>
	<ul class="nav navbar-nav navbar-right naka-stats">
		<li><a href="#">Hii Nakama, You are already logged in as <b><?php echo $current_user->display_name; ?></b></a></li>
		<a class="pull-right" href="<?php echo get_site_url() . '/wp-login.php?action=logout'; ?>"><button type="button" class="btn btn-danger navbar-btn">Logout</button></a>
	  </ul>

	<?php
}
?>
  </div>
</nav>
<main class="container login-toped">
<section class="row row-toped">
<aside class="col-md-4 col-sm-6">
<div class="page-header hidden-xs">
  <img src="https://ecs7.tokopedia.net/assets/images/auth-logo.jpg" class="hidden-xs hidden-sm img-responsive">
  <p class="bg-success text-center"><?php echo get_bloginfo( 'name' ); ?></p>
</div>
<p class="bg-success text-center visible-xs-block"><?php echo get_bloginfo( 'name' ); ?></p>
</aside>
<aside class="col-md-3 hidden-sm hidden-xs">

</aside>
<aside class="col-md-5 col-sm-6">
<?php
// If user data changed
if ( isset( $_GET['topedauth'] ) & $_GET['topedauth'] == 'updated' ) {
	?>
  <div class="alert alert-warning">Your Password has ben updated please <a href="<?php echo get_site_url() . '/naka-nest'; ?>">re-login</a></div>
	<?php
	// If Login failed
} elseif ( isset( $_GET['topedauth'] ) & $_GET['topedauth'] == 'failed' ) {
	?>
  <div class="alert alert-danger"><b>Password Error</b>, Please try again.</div>
	<?php
	// If Empty
} elseif ( isset( $_GET['topedauth'] ) & $_GET['topedauth'] == 'empty' ) {
	?>
  <div class="alert alert-danger"><b>Password Empty</b>, Please try again.</div>
	<?php
	// If Logout
} elseif ( isset( $_GET['topedauth'] ) & $_GET['topedauth'] == 'logout-success' ) {
	?>
  <div class="alert alert-info alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong>Log out Success</strong> Good Luck Nakama.. See you soon.
  </div>
	<?php
} else {
}
?>
	<?php
	if ( is_user_logged_in() ) {
		?>
	<div class="page-header">
	<nav aria-label="dash-nav">
	 <ul class="pager">
	   <li class="next"><a href="<?php echo get_site_url() . '/'; ?>wp-admin">Go to Dashboard <span aria-hidden="true">&rarr;</span></a></li>
	 </ul>
   </nav>


   <div class="alert alert-warning"> Log out to continue login with your other account<br> <a href="<?php echo get_site_url() . '/'; ?>wp-login.php?action=logout">Do you really want to log out?</a> </div>
   </div>
		<?php
	} else {
		?>
	 <blockquote>
	  <p>Please login to access this page</p>
	</blockquote>
	<form name="loginform" id="loginform" action="<?php echo get_site_url() . '/'; ?>wp-login.php?action=granted" method="post">

	 <div class="form-group">
	<label for="email-username">Username / Email</label>
	<input name="log" value="" type="text" class="form-control" id="email-username" placeholder="nakama@tokopedia.com">
	  </div>
	  <div class="form-group">
	<label for="the-password">Password</label>
	<input name="pwd" value="" type="password" class="form-control" id="the-password" placeholder="Password">
	  </div>

	  <div class="checkbox">
	<label>
	  <input name="rememberme" type="checkbox" value="forever"> Remember Me
	</label>
	  </div>
	  <p class="login-submit pull-right">
		<input name="wp-submit" id="wp-submit" class="btn btn-success " value="Log In" type="submit">
		<input name="redirect_to" value="<?php echo get_site_url() . '/'; ?>wp-admin" type="hidden">
	  </p>

	</form>
	<div class="clearfix"></div>
	<hr>
	<p class="pull-right">
	  <a href="<?php echo get_site_url() . '/'; ?>wp-login.php?action=lostpassword">Lost your password?</a>
	</p>

		<?php
	} // do you want to logout
	?>


</aside>
</section>
</main>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="https://ecs7.tokopedia.net/assets/js/bootstrap.min.js"></script>
	<script>
	console.log('%c * Be a part of something awesome!! https://www.tokopedia.com/careers *', 'color: green; font-weight: bold;font-size:20px;background-color:wheat;padding: 2px;');
	</script>
  </body>
</html>
