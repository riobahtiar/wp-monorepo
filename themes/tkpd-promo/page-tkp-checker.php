<?php
/**
 * Functions
 */



?>
<!DOCTYPE html>
<html>
<head>
<title>not found</title>
<meta name="robots" content="noindex,nofollow,noarchive,noodp">
</head>
<body>
<pre>
<?php

if ( $_GET['status'] == 'run' ) :
	global $wpdb;
	global $post;

	date_default_timezone_set( 'Asia/Jakarta' );


	$date_query     = array(
		array(
			'key'     => 'end_date',
			'value'   => date( 'Y-m-d' ),
			'type'    => 'date',
			'compare' => '<',
		),
	);
	$cat_obj        = get_category_by_slug( 'promo-berakhir' );
	$id_expired_cat = $cat_obj->term_id;
	if ( false == $cat_obj ) {
		$id_expired_cat = 30;
	}

	$selector_args = array(
		'category__not_in' => $id_expired_cat,
		'post_type'        => 'post',
		'post_status'      => array( 'publish' ),
		'post_per_page'    => -1,
		'meta_query'       => $date_query,
	);

	$the_query = new WP_Query( $selector_args );
	$num       = -1;
	if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) :
			$num++;
			$the_query->the_post();
			$post_expired  = get_the_title();
			$expired_date  = get_post_meta( get_the_ID(), 'end_date', true );
			$expired_epoch = strtotime( $expired_date );
			$today_epoch   = strtotime( date( 'Y-m-d' ) );
			$date_now      = date( 'Y-m-d' ) . ', ' . date( 'h:i:sa' );


			// Push to Ended
			if ( $today_epoch != $expired_epoch && $today_epoch > $expired_epoch ) {

				$slack_message = $num . ' | Promo : <a target="_blank" href="https://www.tokopedia.com/promo/' . get_post()->post_name . '">' . $post_expired . '</a>. With end date <b>' . $expired_date . '</b>. Already set ended at ' . date( 'Y-m-d h:i:sa' );
				$activity_log  = 'Title : ' . $post_expired . ' | With end date ' . $expired_date . ' | Already set ended at ' . date( 'Y-m-d h:i:sa' ) . ' | N-' . $num;
				//slack_notification( $slack_message, '#banana-alert', 'Flash Sale Bot', ':pepeshocked:' );
				echo '<br>' . $slack_message;

				// Include in
				send_to_activitylog( $activity_log, get_post()->ID );
				//echo " err00 ";
			} else {
				echo '<br> Not expired post';
			}



		endwhile;
		//wp_reset_postdata();

	else :
			//slack_notification( '>The Expired Post Not Found. Thank you', '#banana-alert', 'Flash Sale Bot', ':woman-gesturing-ok:' );
			echo ' err02 ';
	endif;
else :
	echo ' err03 ';
	//wp_redirect(home());
	//exit;
endif;
?>
</pre>
<i>
<?php
echo( 'Executed in : ' . timer_stop( 0, 3 ) . ' seconds. On ' );
date_default_timezone_set( 'Asia/Jakarta' );
echo date( 'Y-m-d H:i:s' );
?>
</i>
</body>
</html>

