<?php
/**
 * Main template file
 *
 * @author Khaidir Afif
 * @version 4.0
 */

/* Insert header */
get_header();

/* ===============================
 * Fetch parameter if available
 * =============================== */
$app  = isset( $_GET['flag_app'] ) ? sanitize_text_field( $_GET['flag_app'] ) : null; // flag from app
$s    = isset( $_GET['s'] ) ? sanitize_text_field( $_GET['s'] ) : null;               // search query
$sort = isset( $_GET['sort'] ) ? sanitize_text_field( $_GET['sort'] ) : null;         // sort option

/* ===============================
 * Concat flag_app in home url
 * =============================== */
$homeLink = $app ?
	  get_bloginfo( 'url' ) . '?flag_app=1&newpage=1' :
		get_bloginfo( 'url' );

/* ===============================
 * Get selected sort option
 * =============================== */
$popularOption = $sort == 'popular' ? 'selected' : null;
$newestOption  = $sort == 'newest' ? 'selected' : null;

/* =====================================
 * Initial promo slot (position for GTM)
 * ===================================== */
$promoSlot = 1;

/* =====================================
 * Get category belanja for index page
 * ===================================== */
$cat        = get_category_by_slug( 'belanja' );
$catID      = $cat->cat_ID;
$catSlug    = $cat->slug;
$catExpired = get_category_by_slug( 'promo-berakhir' ); ?>

	<!-- Slick Slider -->
	<?php get_template_part( 'template-parts/content/content', 'slider' ); ?>

	<!-- Category List -->
	<?php get_template_part( 'template-parts/content/content', 'category' ); ?>

	<div class="container">

		<!-- Search Input & Filter, hidden in mobile -->
		<?php get_template_part( 'template-parts/content/content', 'search' ); ?>

		<!-- SEO Content -->
		<section class="site-seo" id="seo-section">
			<h1>Promo Tokopedia</h1>
			<h2>Promo Terbaru Hari Ini - Cek Harga Promo di Tokopedia</h2>
		</section>

		<!-- Promotion Contents -->
		<section id="content" data-category="<?php echo $catSlug; ?>">
			<div class="row promo-row">
			
			<?php
			/* =====================================
			 * Sticky post loop
			 * ===================================== */
			/* Get all Sticky Posts */
			$sticky = get_option( 'sticky_posts' );

			if ( sizeof( $sticky ) > 0 ) :

				/* Sort Sticky Posts, newest at the top */
				rsort( $sticky );

				/* Query Sticky Posts */
				$stickyPost = new WP_Query(
					array(
						'post__in'            => $sticky,
						'ignore_sticky_posts' => 1,
						'post_type'           => 'post',
						'cat'                 => $catID,
						'category__not_in'    => array( $catExpired->cat_ID ),
					)
				);

				if ( $stickyPost->have_posts() ) :
					while ( $stickyPost->have_posts() ) :
						$stickyPost->the_post();

						/* Promo card */
						get_template_part( 'template-parts/post/promo', 'card' );

					endwhile;
				endif;
			endif;

			/* =====================================
			 * Remain post loop
			 * ===================================== */
			if ( $popularOption ) :
				$catArgs = array(
					'post_type'        => 'post',
					'cat'              => $catID,
					'category__not_in' => array( $catExpired->cat_ID ),
					'post__not_in'     => get_option( 'sticky_posts' ),
					'meta_key'         => 'banana_post_views_count',
					'orderby'          => 'meta_value_num',
					'order'            => 'DESC',
				);
			else :
				$catArgs = array(
					'post_type'        => 'post',
					'cat'              => $catID,
					'category__not_in' => array( $catExpired->cat_ID ),
					'post__not_in'     => get_option( 'sticky_posts' ),
					'orderby'          => 'date',
					'order'            => 'DESC',
				);
			endif;

			$catPosts = new WP_Query( $catArgs );

			if ( $catPosts->have_posts() ) :
				while ( $catPosts->have_posts() ) :
					$catPosts->the_post();

					/* Promo card */
					get_template_part( 'template-parts/post/promo', 'card' );

				endwhile;
			endif;
			?>

			</div>
			<!-- ./row -->
		</section>
		<!-- ./section -->

		<!-- SEO Section for Homepage -->
		<section class="section seo">
			<?php
				$home_page_description = get_field( 'home_page_description', 'option' );
			?>
			<div class="seo-container"><?php echo wpautop( $home_page_description ); ?></div>
			<button class="seo__btn">Lihat Selengkapnya</button>
		</section>
	</div>
	<!-- ./container -->
	
	<?php get_footer(); ?>
