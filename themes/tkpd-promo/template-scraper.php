<?php
/*
 * Template Name: Merchant Scraper
 * Author: Khaidir Afif
 */

require_once( 'libs/simple_html_dom.php' );

/* =======================
 * Option: 1 = shop-id
 *         2 = product-id
 *         3 = etalase-id
 * ======================= */

if ( $_POST['url'] !== '' ) {

	/* fetch post data */
	$url    = $_POST['url'];
	$option = $_POST['option'];

	$html = file_get_html( $url );

	if ( $html !== false ) {
		if ( $option == 1 ) {
			$shopID = $html->find( '#shop-id' );
			if ( $shopID ) {
				$response['success'] = true;
				$response['shop_id'] = $shopID[0]->value;
			} else {
				$response['success'] = false;
				$response['message'] = 'Not found';
			}
		}

		if ( $option == 2 ) {
			$productID = $html->find( '#product-id' );
			if ( $productID ) {
				$response['success']    = true;
				$response['product_id'] = $productID[0]->value;
			} else {
				$response['success'] = false;
				$response['message'] = 'Not found';
			}
		}

		if ( $option == 3 ) {
			$etalaseID = $html->find( '#active_etalase_id' );
			$shopID    = $html->find( '#shop-id' );
			if ( $etalaseID && $shopID ) {
				$response['success']    = true;
				$response['etalase_id'] = $etalaseID[0]->value;
				$response['shop_id']    = $shopID[0]->value;
			} else {
				$response['success'] = false;
				$response['message'] = 'Not found';
			}
		}
	} else {
		$response['success'] = false;
		$response['message'] = 'Promo link is not valid';
	}
} else {
	$response['success'] = false;
	$response['message'] = 'Please fill out promo link';
}

echo json_encode( $response );

