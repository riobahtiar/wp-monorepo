<?php
/**
 * Search template file
 *
 * @author Khaidir Afif
 * @version 4.0
 */

/* Insert header */
get_header();

/* ===============================
 * Fetch parameter if available
 * =============================== */
$app  = isset( $_GET['flag_app'] ) ? sanitize_text_field( $_GET['flag_app'] ) : null; // flag from app
$s    = isset( $_GET['s'] ) ? sanitize_text_field( $_GET['s'] ) : null;               // search query
$sort = isset( $_GET['sort'] ) ? sanitize_text_field( $_GET['sort'] ) : null;           // sort option

/* ===============================
 * Concat flag_app in home url
 * =============================== */
$homeLink = $app ?
	  get_bloginfo( 'url' ) . '?flag_app=1&newpage=1' :
	  get_bloginfo( 'url' );

/* ===============================
 * Get selected sort option
 * =============================== */
$popularOption = $sort == 'popular' ? 'selected' : null;
$newestOption  = $sort == 'newest' ? 'selected' : null;

/* =====================================
 * Initial promo slot (position for GTM)
 * ===================================== */
$promoSlot = 1;

/* =====================================
 * Get expired category
 * ===================================== */
$catExpired = get_category_by_slug( 'promo-berakhir' ); ?>

	<!-- Slick Slider -->
	<?php get_template_part( 'template-parts/content/content', 'slider' ); ?>

	<!-- Category List -->
	<?php get_template_part( 'template-parts/content/content', 'category' ); ?>

	<div class="container">

		<!-- Search Input & Filter, hidden in mobile -->
		<?php get_template_part( 'template-parts/content/content', 'search' ); ?>

		<!-- SEO Content -->
		<section class="site-seo" id="seo-section">
			<h1>Pencarian Promo</h1>
			<h2>Hasil Pencarian
				<?php echo $s; ?>
			</h2>
		</section>

		<!-- Promotion Contents -->
		<section id="content">
			<div class="row promo-row">

			<?php
			/* =====================================
				* Search post loop
				* ===================================== */
			if ( $popularOption ) :
				$searchArgs = array(
					'post_type'        => 'post',
					's'                => esc_html( $s ),
					'category__not_in' => array( $catExpired->cat_ID ),
					'meta_key'         => 'banana_post_views_count',
					'orderby'          => 'meta_value_num',
					'order'            => 'DESC',
				);
			else :
				$searchArgs = array(
					'post_type'        => 'post',
					's'                => esc_html( $s ),
					'category__not_in' => array( $catExpired->cat_ID ),
					'orderby'          => 'date',
					'order'            => 'DESC',
				);
			endif;

			$searchPosts = new WP_Query( $searchArgs );

			if ( $searchPosts->have_posts() ) :
				while ( $searchPosts->have_posts() ) :
					$searchPosts->the_post();

					/* Promo card */
					get_template_part( 'template-parts/post/promo', 'card' );
				endwhile;
			else :
				/* Promo empty */
				get_template_part( 'template-parts/post/promo', 'empty' );
			endif;
			?>

			</div>
			<!-- ./row -->
		</section>
		<!-- ./section -->
	</div>
	<!-- ./container -->
<?php get_footer(); ?>
