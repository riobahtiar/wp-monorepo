<?php
/*
 * Template Name: Index Page
 * Author: Khaidir Afif
 */
?>

<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					?>
			<div class="col-sm-12">
				<div class="main-content">
					<div class="post-body">
						<div class="post-content">
							<h1 class="post-content__title"><?php echo get_the_title(); ?></h1>
						</div>
						<div class="post-content">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
					<?php
			endwhile;
endif;
			?>
		</div>
	</div>
</div>
<!-- End of Promo Container -->
<?php get_footer(); ?>
