<?php
/**
 * Template part for displaying recomendation promo
 *
 * @author Rhoma Cahyanti
 * @version 4.0
 */

/* =====================================
 * Global variables
 * ===================================== */
global $app, $cat, $catID, $catSlug, $promoSlot;

/* =====================================
* Recommendation post loop
* ===================================== */

$postID      = get_the_ID();
$postLink    = ( $app == 1 ) ? get_the_permalink() . '?flag_app=1&newpage=1' : get_the_permalink();
$postExcerpt = get_the_excerpt();
$postTitle   = get_the_title();
$postMeta    = (object) array(
    'thumbnail'   => get_thumbnail(),
    'link'        => get_meta( 'promo_link' ),
    'code'        => get_meta( 'promo_code' ),
    'start_date'  => get_meta( 'start_date' ),
    'end_date'    => get_meta( 'end_date' ),
    'bypass_link' => get_meta( 'bypass_link' ),
    'promo_codes' => get_field_object( 'promo_codes' )['value'],
);
?>

<div class="col-md-4 col-sm-6 
<?php
if ( $catIteration == 2 ) {
    echo 'hidden-sm';
}
?>
">
<div class="promotion-box" 
    data-promo-id="<?php echo $postID; ?>" 
    data-promo-title="<?php echo $postTitle; ?>" 
    data-promo-code="<?php echo $postMeta->code; ?>"
    data-promo-category="<?php echo $catSlug; ?>" 
    data-promo-creative="<?php echo $postMeta->thumbnail; ?>" 
    data-promo-position="<?php echo $promoSlot++; ?>"
    data-promo-page="<?php echo '/promo/' . $singleSlug; ?>" 
    data-promo-group="promo recommendation banner">
    <div class="promotion-image">
        <a href="<?php echo $postLink; ?>">
            <img src="<?php echo $postMeta->thumbnail; ?>" class="img-responsive img-full" alt="<?php echo $postTitle; ?>" />
        </a>
    </div>
    <div class="promotion-description">
        <p>
            <?php echo $postTitle; ?>
        </p>
        <div class="promotion-date">
            <div class="promotion-date-detail">
                <div class="promotion-box-label text-secondary">Periode Promo</div>
                <div class="promotion-box__value">
                    <?php echo tkp_unify_date( $postMeta->start_date, $postMeta->end_date ); ?>
                </div>
            </div>
        </div>
        <div class="promotion-code">

            <?php if ( $postMeta->promo_codes ) : ?>
            <div class="promotion-code-detail">
                <div class="promotion-box-label text-secondary">
                    <?php echo tkp_get_total_code( $postMeta->promo_codes ); ?> Kode Promo
                </div>
            </div>
            <?php elseif ( ! empty( $postMeta->code ) ) : ?>
            <div class="promotion-code-detail">
                <div class="promotion-box-label text-secondary">
                    Kode Promo
                    <div class="promotion-box-label-tooltip">
                        <span class="promotion-box-label-tooltip__icon-info"></span>
                        <span class="promotion-box-label-tooltip__text">
                            Masukkan kode promo di halaman pembayaran
                        </span>
                    </div>
                </div>
                <input type="text" class="sticky-code-voucher__text" value="<?php echo $postMeta->code; ?>" readonly/>
            </div>
            <a class="btn btn-ghost btn-small promotion-code__btn" href="#" data-code-category="<?php echo $catSlug; ?>">Salin Kode</a>
            <?php else : ?>
            <div class="promotion-code-detail">
                <div class="promotion-box-label text-secondary">Tanpa Kode Promo</div>
            </div>
            <?php endif; ?>

        </div>
    </div>
    <div class="promotion-cta">

        <?php if ( ! strcmp( $catSlug, 'official-store' ) ) : ?>
        <div class="row">
            <div class="col-xs-6 pr-5">
                <a target="_blank" class="promotion__btn--ghost official-store-detail" href="<?php echo $postLink; ?>" data-campaign="<?php echo $postTitle; ?>">
                    Lihat Detail
                </a>
            </div>
            <div class="col-xs-6 pl-5">
                <a class="promotion__btn official-store-cta" href="<?php echo $postMeta->link; ?>" data-campaign="<?php echo $postTitle; ?>"
                    target="_blank">
                    Cek Sekarang
                </a>
            </div>
        </div>
        <?php else : ?>
        <a target="_blank" href="<?php echo $postLink; ?>" class="promotion__btn">Lihat Detail</a>
        <?php endif; ?>

    </div>
</div>
</div>      