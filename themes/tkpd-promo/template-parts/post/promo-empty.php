<?php
/**
 * Template part for empty promo
 *
 * @author Khaidir Afif
 * @version 4.0
 */
?>

<div class="col-md-12">
  <div class="search-error">
	<p class="search-error__p">
		<?php _e( 'Promo tidak ditemukan.' ); ?>
	</p>
  </div>
</div>
