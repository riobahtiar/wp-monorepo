<?php
/**
 * Template part for displaying promo card
 *
 * @author Khaidir Afif
 * @version 4.0
 */

/* =====================================
 * Global variables
 * ===================================== */
global $app, $cat, $catID, $catSlug, $promoSlot;


/* =====================================
 * CTA SRC = Override Default CTA
 * ===================================== */
if ( isset( $_GET['cta_src'] ) ) {
	$cta_src = (int) sanitize_title( $_GET['cta_src'] );
} else {
	$cta_src = 0;
}


/* =====================================
 * Post content
 * ===================================== */
$postID      = get_the_ID();
$postExcerpt = get_the_excerpt();
$postTitle   = get_the_title();
$postMeta    = (object) array(
	'thumbnail'   => get_thumbnail(),
	'link'        => get_meta( 'promo_link' ),
	'code'        => get_meta( 'promo_code' ),
	'start_date'  => get_meta( 'start_date' ),
	'end_date'    => get_meta( 'end_date' ),
	'bypass_link' => get_meta( 'bypass_link' ),
	'promo_codes' => get_field_object( 'promo_codes' )['value'],
);

/* Bypass detail page link for sticky promo */
if ( $postMeta->bypass_link === 'true' ) :
	$postLink = $app ?
	get_meta( 'promo_link' ) . '?cta_src=' . $cta_src . '&flag_app=1&newpage=1' :
	get_meta( 'promo_link' ) . '?cta_src=' . $cta_src;
else :
	$postLink = $app ?
	get_the_permalink() . '?cta_src=' . $cta_src . '&flag_app=1&newpage=1' :
	get_the_permalink() . '?cta_src=' . $cta_src;
endif;

/* ==============
 * GTM Data
 * ============== */
$ecommerce['id']           = $postID;
$ecommerce['name']         = '/promo - p' . $promoSlot . ' - ' . 'promo list banner';
$ecommerce['position']     = $promoSlot++;
$ecommerce['creative']     = $post->post_name;
$ecommerce['creative_url'] = $postMeta->thumbnail;

if ( $postMeta->code ) {
	$ecommerce['promo_code'] = $postMeta->code;
}
if ( $postMeta->promo_codes ) {
	$ecommerce['promo_code'] = $postMeta->promo_codes;
}
if ( $catSlug ) {
	$ecommerce['category'] = $catSlug;
}

/* impression data */
$gtm['impression']['event']                                = 'promoView';
$gtm['impression']['eventCategory']                        = 'promo microsite - promo list';
$gtm['impression']['eventAction']                          = 'impression on promo';
$gtm['impression']['eventLabel']                           = $postTitle;
$gtm['impression']['ecommerce']['promoView']['promotions'] = array( $ecommerce );

/* click data */
$gtm['click']['event']                                 = 'promoClick';
$gtm['click']['eventCategory']                         = 'promo microsite - promo list';
$gtm['click']['eventAction']                           = 'user click on promo';
$gtm['click']['eventLabel']                            = $postTitle;
$gtm['click']['ecommerce']['promoClick']['promotions'] = array( $ecommerce );
?>

<div class="col-md-4 col-sm-6">
  <div class="promotion-box">
	<div class="promotion-image">
	  <a target="_blank" href="<?php echo $postLink; ?>" data-click='<?php echo json_encode( $gtm['click'] ); ?>'>
		<img data-impression='<?php echo json_encode( $gtm['impression'] ); ?>' src="<?php echo $postMeta->thumbnail; ?>" class="img-responsive img-full" alt="<?php echo $postTitle; ?>" />
	  </a>
	</div>
	<div class="promotion-description">
	  <p>
		<?php echo $postTitle; ?>
	  </p>
	  <div class="promotion-date">
		<div class="promotion-date-detail">
		  <div class="promotion-box-label text-secondary">Periode Promo</div>
		  <div class="promotion-box__value">
			<?php echo tkp_unify_date( $postMeta->start_date, $postMeta->end_date ); ?>
		  </div>
		</div>
	  </div>
	  <div class="promotion-code">

		<?php if ( $postMeta->promo_codes ) : ?>
		<div class="promotion-code-detail">
		  <div class="promotion-box-label text-secondary">
			<?php echo tkp_get_total_code( $postMeta->promo_codes ); ?> Kode Promo
		  </div>
		</div>
		<?php elseif ( ! empty( $postMeta->code ) ) : ?>
		<div class="promotion-code-detail">
		  <div class="promotion-box-label text-secondary">
			Kode Promo
			<div class="promotion-box-label-tooltip">
			  <span class="promotion-box-label-tooltip__icon-info"></span>
			  <span class="promotion-box-label-tooltip__text">
				Masukkan kode promo di halaman pembayaran
			  </span>
			</div>
		  </div>
		  <input type="text" class="sticky-code-voucher__text" value="<?php echo $postMeta->code; ?>" readonly/>
		</div>
		<a class="btn btn-ghost btn-small promotion-code__btn" href="#" data-code-category="<?php echo $catSlug; ?>">Salin Kode</a>
		<?php else : ?>
		<div class="promotion-code-detail">
		  <div class="promotion-box-label text-secondary">Tanpa Kode Promo</div>
		</div>
		<?php endif; ?>

	  </div>
	</div>
	<div class="promotion-cta">

		<?php if ( ! strcmp( $catSlug, 'official-store' ) && $postMeta->bypass_link === 'false' ) : ?>
	  <div class="row">
			<div class="col-xs-6 pr-5">
				<a target="_blank" class="promotion__btn--ghost official-store-detail" href="<?php echo $postLink; ?>" data-campaign="<?php echo $postTitle; ?>" data-click='<?php echo json_encode( $gtm['click'] ); ?>'>
				Lihat Detail
				</a>
			</div>
			<div class="col-xs-6 pl-5">
				<a target="_blank" class="promotion__btn official-store-cta" href="<?php echo $postMeta->link; ?>" data-campaign="<?php echo $postTitle; ?>"
				target="_blank">
				Cek Sekarang
				</a>
			</div>
	  </div>
		<?php else : ?>
	  <a target="_blank" href="<?php echo $postLink; ?>" class="promotion__btn" data-click='<?php echo json_encode( $gtm['click'] ); ?>'>Lihat Detail</a>
		<?php endif; ?>

	</div>
  </div>
</div>
