<?php
/* ===============================
 * Fetch parameter if available
 * =============================== */
$app  = isset( $_GET['flag_app'] ) ? sanitize_text_field( $_GET['flag_app'] ) : null; // flag from app
$s    = isset( $_GET['s'] ) ? sanitize_text_field( $_GET['s'] ) : null;                 // search query
$sort = isset( $_GET['sort'] ) ? sanitize_text_field( $_GET['sort'] ) : null;        // sort option

/* ===============================
 * Concat flag_app in home url
 * =============================== */
$homeLink = $app ?
		get_bloginfo( 'url' ) . '?flag_app=1&newpage=1' :
		get_bloginfo( 'url' );

/* ===============================
 * Get selected sort option
 * =============================== */
$popularOption = $sort == 'popular' ? 'selected' : null;
$newestOption  = $sort == 'newest' ? 'selected' : null;

/* =====================================
 * Initial promo slot (position for GTM)
 * ===================================== */
$bannerPosition = 1; ?>

	<?php
	if ( $app != 1 ) :
		$sliders = new WP_Query( array( 'post_type' => 'promo_slider' ) );

		if ( $sliders->have_posts() ) :
			?>

		<div class="slider-container">
			<div class="slider-wrapper">
				<?php
				while ( $sliders->have_posts() ) :
					$sliders->the_post();

					$sliderID    = get_the_ID();
					$sliderLink  = get_meta( 'slider_link' );
					$sliderTitle = get_the_title();
					$sliderImg   = feature_img_url( $post->ID, 'full' );

					/* ==============
					 * GTM Data
					 * ============== */
					$ecommerce['id']           = $sliderID;
					$ecommerce['name']         = '/promo - p1 - slider banner';
					$ecommerce['position']     = $bannerPosition++;
					$ecommerce['creative']     = $post->post_name;
					$ecommerce['creative_url'] = $sliderImg;

					/* impression data */
					$gtm['impression']['event']                                = 'promoView';
					$gtm['impression']['eventCategory']                        = 'promo microsite - promo list';
					$gtm['impression']['eventAction']                          = 'impression on promo';
					$gtm['impression']['eventLabel']                           = $sliderTitle;
					$gtm['impression']['ecommerce']['promoView']['promotions'] = array( $ecommerce );

					/* click data */
					$gtm['click']['event']                                 = 'promoClick';
					$gtm['click']['eventCategory']                         = 'promo microsite - promo list';
					$gtm['click']['eventAction']                           = 'user click on promo';
					$gtm['click']['eventLabel']                            = $sliderTitle;
					$gtm['click']['ecommerce']['promoClick']['promotions'] = array( $ecommerce );
					?>

					<div class="slider-slide">
						<a href="<?php echo $sliderLink; ?>" data-click='<?php echo json_encode( $gtm['click'] ); ?>'>
							<img src="<?php echo $sliderImg; ?>" class="slider-image" alt="<?php echo $sliderTitle; ?>" data-slider-impression='<?php echo json_encode( $gtm['impression'] ); ?>'/>
						</a>
					</div>

				<?php endwhile; ?>

			</div>
		</div>
			<?php
		endif;
	endif;
	?>
