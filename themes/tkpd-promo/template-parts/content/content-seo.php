<?php
/**
 * SEO Content at bottom of category page
 *
 * @author Khaidir Afif
 * @version 4.0
 */

$term_description = get_queried_object()->description;
?>

<section class="section seo">
  <div class="seo-container"><?php echo wpautop( $term_description ); ?></div>
  <button class="seo__btn">Lihat Selengkapnya</button>
</section>
