<?php
/* ===============================
 * Fetch flag_app
 * =============================== */
$app = isset( $_GET['flag_app'] ) ? sanitize_text_field( $_GET['flag_app'] ) : null; // flag from app

$homeLink = $app ?
	  get_bloginfo( 'url' ) . '?flag_app=1&newpage=1' :
	  get_bloginfo( 'url' );

/* ======================================
 * Get all categories
 * (exclude semua-promo & promo-berakhir)
 * ====================================== */
$all_category     = get_category_by_slug( 'semua-promo' );
$expired_category = get_category_by_slug( 'promo-berakhir' );

$categories = get_categories(
	array(
		'parent'     => 0,                // Get top parent
		'hide_empty' => 0,            // Show all category, even if have no posts
		'exclude'    => [
			$all_category->cat_ID,      // exclude semua-promo
			$expired_category->cat_ID,   // exlude promo-berakhir
		],
	)
);

/* ========================================
 * Show active category and its subcategory
 * ======================================== */
if ( is_category() || is_home() ) :

	/* =================================
	* Get active category and its child
	* ================================= */
	$active_category = is_home() ?
					 get_category_by_slug( 'belanja' ) :
					 get_category( get_query_var( 'cat' ) );

	if ( $active_category->category_parent != 0 ) :
		$parent_link = $app ?
				   get_category_link( $active_category->category_parent ) . '?cta_src=' . $active_category->category_parent . '&flag_app=1&newpage=1' :
				   get_category_link( $active_category->category_parent ) . '?cta_src=' . $active_category->category_parent;
  endif;

	$subcategories = $active_category->category_parent == 0 ?
				get_categories(
					array(
						'parent'     => $active_category->cat_ID,
						'hide_empty' => 0,
					)
				) :
				get_categories(
					array(
						'parent'     => $active_category->category_parent,
						'hide_empty' => 0,
					)
				);
	?>
  
  <!-- Categories -->
  <section id="categories">
	<div class="container">
	  <div class="row">
		<div class="col-sm-12 cat-column">
		  <div class="cat-list clearfix">
			
			<?php
			/* Display all category */
			foreach ( $categories as $category ) :
				$cat_link   = $app ?
				   get_category_link( $category->cat_ID ) . '?cta_src=' . $category->cat_ID . '&flag_app=1&newpage=1' :
				   get_category_link( $category->cat_ID ) . '?cta_src=' . $category->cat_ID;
				$cat_icon   = 'cat-list__icon-' . $category->slug;
				$cat_name   = $category->cat_name;
				$cat_active = $active_category->category_parent != 0 ?
							( $active_category->category_parent == $category->cat_ID ) :
							( $active_category->cat_ID == $category->cat_ID );
				?>
			  
			  <a href="<?php echo ( $cat_active && ! is_home() ) ? '#' : $cat_link; ?>" class="cat-list__item 
									<?php
									if ( $cat_active ) {
										echo 'active';}
									?>
">
				<span class="cat-list__icon <?php echo $cat_icon; ?>"></span>
				<span class="cat-list__text"><?php echo $cat_name; ?></span>
			  </a>
			  
			<?php endforeach; ?>
			
		  </div>
		</div>
	  </div>
	</div>
  </section>
  <!-- ./Categories -->
  
  <!-- Subcategories -->
  <section id="subcategories">
	<div class="container">
	  <div class="subcat-list">
		<!-- all -->
		<a href="<?php echo $active_category->category_parent == 0 ? '#' : $parent_link; ?>" 
		   class="subcat-list__link 
			<?php
			if ( $active_category->category_parent == 0 ) {
				echo 'active';}
			?>
">
		  Semua
		</a>
		
		<?php
		/* Display all subcategories */
		foreach ( $subcategories as $subcategory ) :
			$subcat_link   = $app ?
				   get_category_link( $subcategory->cat_ID ) . '?cta_src=' . $subcategory->cat_ID . '&flag_app=1&newpage=1' :
				   get_category_link( $subcategory->cat_ID ) . '?cta_src=' . $subcategory->cat_ID;
			$subcat_name   = $subcategory->cat_name;
			$subcat_active = $active_category->cat_ID == $subcategory->cat_ID;
			?>
		
		  <a href="<?php echo $subcat_active ? '#' : $subcat_link; ?>" class="subcat-list__link 
								<?php
								if ( $subcat_active ) {
									echo 'active';}
								?>
">
			<?php echo $subcat_name; ?>
		  </a>
		
		<?php endforeach; ?>
		
	  </div>
	</div>
  </section>
  <!-- ./Subcategories -->
  
	<?php
	/* ====================
	* Show only categories
	* ==================== */
else :
	?>
  
  <!-- Categories -->
  <section id="categories">
	<div class="container">
	  <div class="row">
		<div class="col-sm-12 cat-column">
		  <div class="cat-list clearfix">
			
			<?php
			/* Display all category */
			foreach ( $categories as $category ) :
				$cat_link = $app ?
				   get_category_link( $category->cat_ID ) . '?cta_src=' . $category->cat_ID . '&flag_app=1&newpage=1' :
				   get_category_link( $category->cat_ID ) . '?cta_src=' . $category->cat_ID;
				$cat_icon = 'cat-list__icon-' . $category->slug;
				$cat_name = $category->cat_name;
				?>
			  
			  <a href="<?php echo $cat_link; ?>" class="cat-list__item">
				<span class="cat-list__icon <?php echo $cat_icon; ?>"></span>
				<span class="cat-list__text"><?php echo $cat_name; ?></span>
			  </a>
			  
			<?php endforeach; ?>
			
		  </div>
		</div>
	  </div>
	</div>
  </section>
  <!-- ./Categories -->
  
<?php endif; ?>
