<?php
/**
 * Template part for official store section
 *
 * @author Khaidir Afif
 * @version 4.0
 */
?>

<section class="section official-store clearfix" id="offical-store-section">
  <div class="row">
	<div class="col-sm-4">
	  <img src="https://ecs7.tokopedia.net/promo/assets/images/official-store-promo.png" alt="official-store" class="img-responsive official-store__img">
	</div>
	<div class="col-sm-8">
	  <h2 class="official-store__heading">Sudah Pernah Kunjungi Official Store?</h2>
	  <p class="official-store__p">Nikmati banyak keunggulan yang bisa Anda dapatkan dengan berbelanja di Official Store mulai dari:</p>
	  <div class="official-store-advantage">
		<div class="row">
		  <div class="col-xs-6">
			<p class="official-store__p">
			  <span class="glyphicon glyphicon-ok icon-green"></span> Produk dari Brand Resmi</p>
		  </div>
		  <div class="col-xs-6">
			<p class="official-store__p">
			  <span class="glyphicon glyphicon-ok icon-green"></span> Penawaran Promo Eksklusif</p>
		  </div>
		  <div class="col-xs-6">
			<p class="official-store__p">
			  <span class="glyphicon glyphicon-ok icon-green"></span> Pelayanan Berkualitas untuk Anda</p>
		  </div>
		  <div class="col-xs-6">
			<p class="official-store__p">
			  <span class="glyphicon glyphicon-ok icon-green"></span> Cicilan 0%, Gratis Biaya Admin</p>
		  </div>
		</div>
	  </div>
	  <div class="official-store-btn">
		<a href="https://www.tokopedia.com/official-store" class="btn btn-green btn-medium" id="official-store-site">Kunjungi Sekarang</a>
	  </div>
	</div>
  </div>
</section>
