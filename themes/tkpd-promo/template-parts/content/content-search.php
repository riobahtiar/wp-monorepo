<?php
	$app  = isset( $_GET['flag_app'] ) ? sanitize_text_field( $_GET['flag_app'] ) : null;
	$s    = isset( $_GET['s'] ) ? sanitize_text_field( $_GET['s'] ) : null;
	$sort = isset( $_GET['sort'] ) ? sanitize_text_field( $_GET['sort'] ) : null;

	$homeLink = $app ?
				get_bloginfo( 'url' ) . '?flag_app=1&newpage=1' :
				get_bloginfo( 'url' );

	$popularOption = $sort == 'popular' ? 'selected' : null;
	$newestOption  = $sort == 'newest' ? 'selected' : null;
?>


<section class="row promo-discovery hidden-xs" id="discovery">
	<div class="col-md-4 col-sm-6">
		<div class="promo-search" itemscope itemtype="http://schema.org/WebSite">
			<meta itemprop="url" content="https://www.tokopedia.com/promo/">
			<form action="<?php echo get_bloginfo( 'url' ); ?>" itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction" method="GET">
				<meta itemprop="target" content="https://www.tokopedia.com/promo/?s={s}"/>
				<div class="input-group">
					<input type="text" itemprop="query-input" name="s" class="form-control promo-search__input" placeholder="Cari Promo" value="<?php echo esc_attr( $s ); ?>" />
					<span class="input-group-btn">
						<button type="submit" class="btn btn-default promo-search__btn">
							<span class="glyphicon glyphicon-search"></span>
						</button>
					</span>
				</div>
			</form>
		</div>
	</div>
	<div class="col-md-4 col-md-offset-4 col-sm-6">
		<div class="promo-filter form-horizontal clearfix">
			<form method="GET" id="filter-form" action=".">
				<label for="filter" class="col-sm-6 control-label promo-filter__label hidden-xs">Urutkan :</label>
				<div class="col-sm-6 promo-filter-input">
					<?php if ( $s ) : ?>
					<input type="hidden" name="s" value="<?php echo esc_attr( $s ); ?>">
					<?php endif; ?>
					<select name="sort" id="sort" class="form-control promo-filter__select">
						<option value="newest" <?php echo $newestOption; ?>>Terbaru</option>
						<option value="popular" <?php echo $popularOption; ?>>Terpopuler</option>
					</select>
				</div>
			</form>
		</div>
	</div>
</section>
