<?php if ( is_user_logged_in() ) : ?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
  <meta name="robots" content="noindex,nofollow,noarchive,noodp">
  <title>404</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>

<body>
	<?php
	$step = $_GET['step'];

	switch ( $step ) {
		case 'act1':
			?>
  <!-- Step 1 -->
  <div class="jumbotron jumbotron-fluid">
	<div class="container">
	  <h4 class="display-5">Langkah ke 1</h4>
	  <p class="lead">Masukkan slug url. Contoh: https://www.tokopedia.com/<code>ini-slug-url</code>.</b>
	  </p>
	</div>
  </div>
  <div class="container-fluid">
  <form action="<?php echo site_url(); ?>/ajx-etalase/?step=act2" method="post">
	<label for="tokopedia-shop-uri">Slug Toko</label>
	<div class="input-group mb-3">
	  <div class="input-group-prepend">
		<span class="input-group-text" id="shop-uri">https://www.tokopedia.com/</span>
	  </div>
	  <input type="text" class="form-control" id="tokopedia-shop-uri" aria-describedby="shop-uri" placeholder="store-slug" name="shop_slug">
	</div>
	<hr>
<a class="btn btn-danger float-left" href="<?php echo site_url(); ?>/ajx-etalase/?step=act1">Reset</a>
<button class="btn btn-primary float-right" type="submit"> Next &raquo; </button>
  </form>
  </div>
			<?php
			break;
		case 'act2':
			$shop_slug       = $_POST['shop_slug'];
			$get_shop_detail = wp_remote_retrieve_body( wp_remote_get( "https://tome.tokopedia.com/v1/shop/get_summary?domain=$shop_slug" ) );
			$shop_detail     = json_decode( $get_shop_detail );
			$shopID          = $shop_detail->data[0]->shop_id;

			if ( $shopID ) {
				$sid = $shopID;
			} else {
				?>
		  <div class="container">
		  <br>
		  <div class="alert alert-danger" role="alert">
		  Shop Not Found or URL invalid. 
		  </div>
		  <br>
		  </div>
				<?php
			}
			$headers   = array(
				'Content-Type: application/json',
			);
			$shop_info = curl_init();
			curl_setopt( $shop_info, CURLOPT_URL, 'https://tome.tokopedia.com/v2/shop/' . $sid . '/showcase' );
			curl_setopt( $shop_info, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $shop_info, CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $shop_info, CURLOPT_HTTPHEADER, $headers );
			$val = (array) json_decode( curl_exec( $shop_info ) );
			curl_close( $shop_info );

			?>
 <!-- Step 2 -->
  <div class="jumbotron jumbotron-fluid">
	<div class="container">
	  <h4 class="display-5">Langkah ke 2</h4>
	  <p class="lead">Pilih etalase yang akan di ambil applink-nya.</p>
	</div>
  </div>
<form action="<?php echo site_url(); ?>/ajx-etalase/?step=act3" method="post">
  <div class="container-fluid">
	<div class="form-group">
	  <label for="etalase-chooser">Pilih Etalase</label>

	<select class="form-control js-etalase" id="etalase-chooser" name="etalase_applink">
			<?php
			//var_dump( $val["data"]->showcase);
			$datame = $val['data']->showcase;
			foreach ( $datame as $data ) {
					echo "<option value='tokopedia://shop/" . $sid . '/etalase/' . $data->id . "'>";
					echo $data->name;
					echo '</option>';
			}
			?>
</select>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
$(document).ready(function() {
	$('.js-etalase').select2();
});
</script>
	</div>

<a class="btn btn-danger float-left" href="<?php echo site_url(); ?>/ajx-etalase/?step=act1">Reset</a>
<button class="btn btn-primary float-right" type="submit"> Next &raquo; </button>
	</form>
  </div>

			<?php
			break;
		case 'act3':
			?>
  <!-- Step 3 -->
  <div class="jumbotron jumbotron-fluid">
	<div class="container">
	  <h4 class="display-5">Langkah ke 3</h4>
	  <p class="lead">Klik <b>COPY</b> untuk menyalin applink, lalu tutup pop-up ini, dan paste di kolom applink yang disediakan.</p>
	</div>
  </div>

  <div class="container-fluid">
			<?php if ( empty( $_POST['etalase_applink'] ) ) { ?>
		  <br>
		  <div class="alert alert-danger" role="alert">
		  Shop Not Found or URL invalid. 
		  </div>
		  <br>
	<?php } ?>
	<div class="input-group mb-3">
	  <input id="linkVal" type="text" class="form-control" aria-label="Etalase AppLink" aria-describedby="etalase-applink" value="<?php echo $_POST['etalase_applink']; ?>">
	  <div class="input-group-append">
		<button class="btn btn-outline-secondary" type="button" onclick="copyLink()">Copy</button>
	  </div>
	</div>
	<div class="copy-me"><div>
	
	<script>
	  function copyLink() {
		/* Get the text field */
		var copyText = document.getElementById("linkVal");

		/* Select the text field */
		copyText.select();

		/* Copy the text inside the text field */
		document.execCommand("copy");

		/* Alert the copied text */
		//alert("Copied the text: " + copyText.value);
		jQuery(".copy-me").append("<br><br><p class='alert alert-primary'>Applink berikut <code>" + copyText.value + "</code> telah di salin ke clipboard. Silahkan paste/tempelkan di field applink.</p>");
	  }
	</script>
	<a class="btn btn-warning float-right" href="<?php echo site_url(); ?>/ajx-etalase/?step=act1">Reset Form / Mulai dari awal.</a>
  </div>

			<?php
			break;
		default:
			wp_redirect( home_url() );
			exit;
	}

	?>


</body>

</html>

<?php endif; ?>
