<?php
/* Insert Header */
get_header();

/* Define global post */
global $post;

/* Get flag from app */
$app = isset( $_GET['flag_app'] ) ? sanitize_text_field( $_GET['flag_app'] ) : null; // flag from app


/*
* Override data with $ctasrc
**/
if ( isset( $_GET['cta_src'] ) ) {
	$cta_src = (int) sanitize_text_field( $_GET['cta_src'] );
} else {
	$cta_src = 0;
}
$dm_cta = get_field( 'dm_cta' );

// Check if CTA Overrider exist
if ( $dm_cta != null ) {
	$cdt = in_array( $cta_src, array_column( $dm_cta, 'dm_cta_cat' ) );
} else {
	$cdt = false;
}

$cta = cta_picker( $cta_src, get_the_ID() );

// CTA OVD: Get Value
if ( isset( $cta ) && $cdt ) {
	$ct_applink = $cta['applink'];
	$ct_link    = $cta['uri'];
} else {
	$ct_applink = get_field( 'app_link' );
	$ct_link    = get_field( 'promo_link' );
}


/* Get unique code if available */
$cid = isset( $_GET['cid'] ) ? $_GET['cid'] : null; // promo code base64
$mid = isset( $_GET['mid'] ) ? $_GET['mid'] : null; // hash value
$did = isset( $_GET['did'] ) ? $_GET['did'] : null; // epoch date base64
$pid = isset( $_GET['pid'] ) ? $_GET['pid'] : null; // imploded page id
$slg = isset( $_GET['slg'] ) ? $_GET['slg'] : null; // validation slug
$pdp = isset( $_GET['pdp'] ) ? $_GET['pdp'] : null; // pdp

if ( $pid ) {
	$params = explode( ',,', $pid );

	$cid = $params[0];
	$did = $params[1];
	$mid = $params[2];
	$slg = $params[3];
	$pdp = $params[4];

}

/* Set default timezone (Asia/Jakarta) */
date_default_timezone_set( 'Asia/Jakarta' );


/* =====================================
 * Fetch parameter for personalize promo
 * ===================================== */
$plainCode = '';
$plainDate = '';
$epochDate = '';
$hmacCode  = '';

if ( $cid ) {
	if ( $did ) {
		$epochDate = base64_decode( $did );
		$plainDate = date( 'Y-m-d', substr( $epochDate, 0, 10 ) );
	}
	$plainCode = base64_decode( $cid );
	$md5Code   = md5( $plainCode . $epochDate );
	$hmacCode  = hash_hmac( 'sha256', $md5Code, 'nicogantengproductownerpromotion123' );
}

if ( $pdp ) {
	wp_localize_script( 'controller', 'is_single', (array) is_single() );
	wp_localize_script( 'controller', 'paramPDP', (array) $pdp );
}

/* =====================================
 * Initial promo slot (position for GTM)
 * ===================================== */
$promoSlot = 1;

/* ==============================
 * Insert Flag App in mobile view
 * ============================== */
$homeLink = $app ?
	  get_bloginfo( 'url' ) . '?flag_app=1&newpage=1' :
	  get_bloginfo( 'url' );

/* ===================
 * Excluded category
 * =================== */
$showRecommendation = true;
$expiredCategory    = get_category_by_slug( 'promo-berakhir' );

/* ===================
 * Get post categories
 * =================== */
$categories = get_the_category( $post->ID );
$catName    = $categories[0]->cat_name;
$catSlug    = $categories[0]->slug;
$catLink    = $app ?
	  get_category_link( $categories[0]->cat_ID ) . '?flag_app=1&newpage=1' :
	  get_category_link( $categories[0]->cat_ID );
$catIDs     = [];
$catSlugs   = [];

foreach ( $categories as $category ) {
	if ( $category->cat_ID == $expiredCategory->cat_ID ) {
		$showRecommendation = true;
	}
	array_push( $catIDs, $category->cat_ID );
	array_push( $catSlugs, $category->slug );
}

/* =====================================
 * Generate CTA button copy
 * ===================================== */
//$ctaButton = 'Cek Sekarang';

// CTA Configurator
$cta = get_the_terms( get_the_ID(), 'category' );
if ( null != $cta ) {
	// Validation If single array, show cat metas
	if ( count( $cta ) > 1 && ! $cdt ) {
		if ( get_field( 'default_cta_button', 'option' ) != null ) {
			$ctaButton = get_field( 'default_cta_button', 'option' );
		} else {
			$ctaButton = esc_html__( 'Cek Sekarang' );
		}
	} else {
		if ( $cta_src != 0 && $cdt ) {
			$cta_picker = cta_picker( $cta_src, get_the_ID() );
			$ctaButton  = $cta_picker['text'];
		} else {
			$ctaButton = get_field( 'cta_button', 'category_' . $cta[0]->term_id );
		}
	}
} else {
	$ctaButton = esc_html__( 'Cek Sekarang' );
}




/* ==============
 * Post variables
 * ============== */
$singleSlug       = $post->post_name;
$singleImg        = feature_img_url( $post->ID, 'full' );
$singleTitle      = get_the_title();
$singleExcerpt    = get_the_excerpt();
$singleMeta       = (object) array(
	'link'            => $ct_link,
	'code'            => get_meta( 'promo_code' ),
	'min_transaction' => get_meta( 'min_transaction' ),
	'periode'         => get_meta( 'bataspromo' ), // will be deprecated
	'start_date'      => get_meta( 'start_date' ),
	'end_date'        => get_meta( 'end_date' ),
	'app_link'        => $ct_applink,
	'promo_codes'     => get_field_object( 'promo_codes' )['value'],
	'topads'          => get_meta( 'topads' ),
);
$singleLinkMobile = ( $app && $singleMeta->app_link ) ?
	$singleMeta->app_link :
	$singleMeta->link;
$singleLink       = $singleMeta->link;

/* ========================
 * Get merchant param (topads)
 * ======================== */
$promoSource = isset( $_GET['promo_source'] ) && ( in_array( 'merchant', $catSlugs ) || in_array( 'promo-berakhir', $catSlugs ) ) ?
	'promo_source=' . sanitize_text_field( $_GET['promo_source'] ) :
	null;

preg_match( '/(#[a-zA-Z0-9_-]+)/', $singleLink, $anchor );
preg_match( '/(\?\w+)/', $singleLink, $qs );

if ( $promoSource ) {
	if ( $qs ) {
		$singleLink       = $singleLink . '&' . $promoSource;
		$singleLinkMobile = $singleLinkMobile . '&' . $promoSource;
	} else {
		$singleLink       = $singleLink . '?' . $promoSource;
		$singleLinkMobile = $singleLinkMobile . '?' . $promoSource;
	}

	if ( $anchor ) {
		$singleLink       = str_replace( $anchor, '', $singleLink ) . $anchor[1];
		$singleLinkMobile = str_replace( $anchor, '', $singleLink ) . $anchor[1];
	}
}

/* ==============
 * GTM Data
 * ============== */
$ecommerce['id']           = $post->ID;
$ecommerce['name']         = '/promo/' . $singleSlug;
$ecommerce['position']     = 1;
$ecommerce['creative']     = $singleSlug;
$ecommerce['creative_url'] = $singleImg;

if ( $singleMeta->code ) {
	$ecommerce['promo_code'] = $singleMeta->code;
}
if ( $singleMeta->promo_codes ) {
	$ecommerce['promo_code'] = $singleMeta->promo_codes;
}

/* impression data */
$gtm['impression']['event']                                = 'promoView';
$gtm['impression']['eventCategory']                        = 'promo microsite - promo detail';
$gtm['impression']['eventAction']                          = 'impression on promo';
$gtm['impression']['eventLabel']                           = $singleTitle;
$gtm['impression']['ecommerce']['promoView']['promotions'] = array( $ecommerce );

/* click data */
$gtm['click']['event']                                 = 'promoClick';
$gtm['click']['eventCategory']                         = 'promo microsite - promo detail';
$gtm['click']['eventAction']                           = 'user click belanja sekarang';
$gtm['click']['eventLabel']                            = $singleTitle;
$gtm['click']['ecommerce']['promoClick']['promotions'] = array( $ecommerce );
?>

	<div class="content" id="<?php echo $singleSlug; ?>">
		<div class="container">
			<div class="row">
				<!-- Main Content -->
				<div class="col-sm-8 main-column">
					<main class="main-content">
						<!-- Post Image -->
						<div class="post-image">
							<img data-impression='<?php echo json_encode( $gtm['impression'] ); ?>' src="<?php echo $singleImg; ?>" class="img-full" alt="<?php echo $singleTitle; ?>" />
						</div>
						<!-- ./Post Image -->

						<!-- Post Header -->
						<?php if ( ! $pdp ) : ?>
						<div class="post-header clearfix">
							<!-- Breadcrumbs -->
							<div class="post-header-group post-breadcrumbs pull-left hidden-xs">
								<ol class="post-breadcrumbs-list pull-left" itemscope itemtype="http://schema.org/BreadcrumbList">
									<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" href="https://www.tokopedia.com">
											<span itemprop="name">Home</span>
										</a>
									</li>
									<span class="glyphicon glyphicon-menu-right text-secondary post-breadcrumbs-icon"></span>
									<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" href="<?php echo $homeLink; ?>">
											<span itemprop="name">Tokopedia Promo</span>
										</a>
									</li>
									<span class="glyphicon glyphicon-menu-right text-secondary post-breadcrumbs-icon"></span>
									<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
										<a itemprop="item" href="<?php echo $catLink; ?>">
											<span itemprop="name">
												<?php echo $catName; ?>
											</span>
										</a>
									</li>
								</ol>
							</div>
							<!-- Share label -->
							<div class="post-header-group post-share-label pull-left visible-xs">Bagikan :</div>

							<!-- Addthis Social Share Plugin-->
							<div class="post-header-group addthis_inline_share_toolbox pull-right"></div>
							<script>
								var addthis_share = {
									url_transforms: {
										add: {
											utm_source: 'desktop',
											utm_medium: 'share',
											utm_campaign: 'promo share',
											utm_content: '{{code}}'
										}
									}
								}
							</script>
						</div>
						<?php endif; ?>
						<!-- ./Post Header -->

						<!-- Post Body -->
						<div class="post-body" id="post-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="post-content">
										<h1 class="post-content__title">
											<?php echo $singleTitle; ?>
										</h1>
									</div>
									<div class="post-content">
										<h2 class="post-content__heading text-primary">Deskripsi</h2>
										<p>
											<?php echo $singleExcerpt; ?>
										</p>
									</div>
									<!-- Post Box Mobile -->
									<div class="postbox-mobile visible-xs">
										<div class="postbox clearfix">
											<div class="postbox-header hidden-xs">
												<h3 class="postbox-header__h3">Info Promo</h3>
											</div>
											<div class="postbox-content">
												<div class="postbox-content-detail postbox-content--period">
													<img src="https://ecs7.tokopedia.net/assets/images/ic_periode.png" class="img-responsive postbox-content__img" alt="periode"
													/>
													<p class="postbox-content-title text-secondary">
														<?php echo ( $hmacCode === $mid && $plainDate && $singleSlug == base64_decode( $slg ) ) ? 'Promo Berakhir' : 'Periode Promo'; ?>
													</p>
													<p class="postbox-content__p">
														<?php
														if ( $hmacCode === $mid && $plainDate && $singleSlug == base64_decode( $slg ) ) :
															echo tkp_unify_date( null, $plainDate );
															elseif ( ! empty( $singleMeta->start_date ) && ! empty( $singleMeta->end_date ) ) :
																echo tkp_unify_date( $singleMeta->start_date, $singleMeta->end_date );
															else :
																echo $singleMeta->periode;
															endif;
															?>
													</p>
												</div>
												<?php if ( ! empty( $singleMeta->min_transaction ) ) : ?>
												<div class="postbox-content-detail postbox-content--min-transaction">
													<img src="https://ecs7.tokopedia.net/assets/images/ic_minimum.png" class="img-responsive postbox-content__img" alt="minimum transaksi"/>
													<p class="postbox-content-title text-secondary">Minimum Transaksi</p>
													<p class="postbox-content__p">
														<?php echo $singleMeta->min_transaction; ?>
													</p>
												</div>
												<?php endif; ?>
											</div>

											<div class="postbox-content">

												<?php if ( $singleMeta->promo_codes ) : ?>
												<div class="postbox-content-detail postbox-content--group-code">
													<?php foreach ( $singleMeta->promo_codes as $groupCode ) : ?>
													<div class="postbox-group">
														<div class="postbox-group-title">
															<h4 class="postbox-group-title__h4">
																<?php echo $groupCode['group_code_title']; ?>
															</h4>
															<span class="glyphicon glyphicon-menu-down postbox-group-title__icon"></span>
														</div>
														<div class="postbox-group-body clearfix">
															<div class="postbox-content-title text-secondary">
																<p class="postbox-group-body__p">
																	<?php echo $groupCode['group_code_description']; ?>
																</p>
																<span class="promotion-code-detail">Kode Promo</span>
																<div class="promotion-box-label-tooltip">
																	<span class="promotion-box-label-tooltip__icon-info"></span>
																	<span class="promotion-box-label-tooltip__text">Masukkan kode promo di halaman pembayaran</span>
																</div>
															</div>

															<?php
															$singleCodeIter = 0;  // single code iteration

															foreach ( $groupCode['group_code'] as $singleCode ) :
																?>
																<div class="postbox-content-voucher">
																	<div class="postbox-group-voucher-number">
																		<?php echo ++$singleCodeIter; ?>
																	</div>
																	<input type="text" class="postbox-content-voucher__input" value="<?php echo $singleCode['single_code']; ?>" data-code-category="<?php echo $catSlug; ?>"
																	  readonly>
																	<div class="postbox-group-voucher-tooltip">
																		<img src="https://ecs7.tokopedia.net/assets/images/promo/ic-copy-mobile.png" alt="copy" class="postbox-group-voucher-tooltip___icon-copy">
																		<span class="promotion-box-label-tooltip__text postbox-group-voucher-tooltip__text hidden-xs">Salin kode promo</span>
																	</div>
																</div>
															<?php endforeach; ?>
														</div>
													</div>
													<?php endforeach; ?>
												</div>
												<?php elseif ( ! empty( $singleMeta->code ) || ( $hmacCode === $mid && $singleSlug == base64_decode( $slg ) ) ) : ?>
												<div class="postbox-content-detail">
													<div class="postbox-content-title text-secondary">
														<span class="promotion-code-detail">Kode Promo</span>
														<div class="promotion-box-label-tooltip">
															<span class="promotion-box-label-tooltip__icon-info"></span>
															<span class="promotion-box-label-tooltip__text">Masukkan kode promo di halaman pembayaran</span>
														</div>
													</div>
													<div class="postbox-content-voucher">
														<input type="text" class="postbox-content-voucher__input" data-code-category="<?php echo $catSlug; ?>" value="<?php echo $singleMeta->code ? $singleMeta->code : strtoupper( $plainCode ); ?>"
														  readonly>
														<button class="btn btn-ghost postbox-content-voucher__btn">Salin Kode</button>
													</div>
												</div>
												<?php elseif ( $catSlug !== 'promo-berakhir' ) : ?>
												<div class="postbox-content-detail">
													<div class="postbox-content-title text-secondary">
														<span class="promotion-code-detail">Tanpa kode promo</span>
													</div>
												</div>
												<?php endif; ?>

											</div>
										</div>
										<!-- Mobile Toast -->
										<div class="postbox-toast-mobile">
											<p class="postbox-toast-mobile__p">Kode Tersalin</p>
											<div class="postbox-toast-mobile-action">Tutup</div>
										</div>

										<!-- Mobile CTA button-->
										<?php if ( ! $pdp ) : ?>
										<a href="<?php echo $singleLinkMobile; ?>" 
											target="_blank" 
											class="btn btn-green btn-large btn-full postbox-mobile__btn active"
											data-click='<?php echo json_encode( $gtm['click'] ); ?>'>
											<?php echo $ctaButton; ?>
										</a>
										<?php endif; ?>
									</div>
									<!-- ./Post Box Mobile -->
									<div class="post-content post-content-main">
										<h2 class="post-content__heading post-content__heading--mobile-block text-primary">Syarat dan Ketentuan</h2>
										<div class="post-content__p">
											<?php
											if ( have_posts() ) {
												the_post();
												the_content();
											}
											?>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="post-more">
										<span class="post-more__text">Read More
											<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
										</span>
									</div>
								</div>
							</div>
						</div>
						<!-- ./Post Body -->
					</main>
				</div>
				<!-- ./Main Section -->

				<!-- Sidebar -->
				<div class="col-sm-4 hidden-xs">
					<div class="post-sidebar">
						<!-- Promo Info -->
						<div class="postbox clearfix">
							<div class="postbox-header">
								<h3 class="postbox-header__h3">Info Promo</h3>
							</div>
							<div class="postbox-content">
								<div class="postbox-content-detail postbox-content--period">
									<img src="https://ecs7.tokopedia.net/assets/images/ic_periode.png" class="img-responsive postbox-content__img" alt="periode"
									/>
									<p class="postbox-content-title text-secondary">
										<?php echo ( $hmacCode === $mid && $plainDate && $singleSlug == base64_decode( $slg ) ) ? 'Promo Berakhir' : 'Periode Promo'; ?>
									</p>
									<p class="postbox-content__p">
										<?php
										if ( $hmacCode === $mid && $plainDate && $singleSlug == base64_decode( $slg ) ) :
											echo tkp_unify_date( null, $plainDate );
											elseif ( ! empty( $singleMeta->start_date ) && ! empty( $singleMeta->end_date ) ) :
												echo tkp_unify_date( $singleMeta->start_date, $singleMeta->end_date );
											else :
												echo $singleMeta->periode;
											endif;
											?>
									</p>
								</div>
								<?php if ( ! empty( $singleMeta->min_transaction ) ) : ?>
								<div class="postbox-content-detail postbox-content--min-transaction">
									<img src="https://ecs7.tokopedia.net/assets/images/ic_minimum.png" class="img-responsive postbox-content__img" alt="minimum transaksi"/>
									<p class="postbox-content-title text-secondary">Minimum Transaksi</p>
									<p class="postbox-content__p">
										<?php echo $singleMeta->min_transaction; ?>
									</p>
								</div>
								<?php endif; ?>
							</div>

							<div class="postbox-content">

								<?php if ( $singleMeta->promo_codes ) : ?>
								<div class="postbox-content-detail postbox-content--group-code">
									<?php foreach ( $singleMeta->promo_codes as $groupCode ) : ?>
									<div class="postbox-group">
										<div class="postbox-group-title">
											<h4 class="postbox-group-title__h4">
												<?php echo $groupCode['group_code_title']; ?>
											</h4>
											<span class="glyphicon glyphicon-menu-down postbox-group-title__icon"></span>
										</div>
										<div class="postbox-group-body clearfix">
											<div class="postbox-content-title text-secondary">
												<p class="postbox-group-body__p">
													<?php echo $groupCode['group_code_description']; ?>
												</p>
												<span class="promotion-code-detail">Kode Promo</span>
												<div class="promotion-box-label-tooltip">
													<span class="promotion-box-label-tooltip__icon-info"></span>
													<span class="promotion-box-label-tooltip__text">Masukkan kode promo di halaman pembayaran</span>
												</div>
											</div>

											<?php
											$singleCodeIter = 0;  // single code iteration

											foreach ( $groupCode['group_code'] as $singleCode ) :
												?>
												<div class="postbox-content-voucher">
													<div class="postbox-group-voucher-number">
														<?php echo ++$singleCodeIter; ?>
													</div>
													<input type="text" class="postbox-content-voucher__input" value="<?php echo $singleCode['single_code']; ?>" data-code-category="<?php echo $catSlug; ?>"
													  readonly>
													<div class="postbox-group-voucher-tooltip">
														<img src="https://ecs7.tokopedia.net/assets/images/promo/ic-copy-mobile.png" alt="copy" class="postbox-group-voucher-tooltip___icon-copy">
														<span class="promotion-box-label-tooltip__text postbox-group-voucher-tooltip__text hidden-xs">Salin kode promo</span>
													</div>
												</div>
											<?php endforeach; ?>
										</div>
									</div>
									<?php endforeach; ?>
								</div>
								<?php elseif ( ! empty( $singleMeta->code ) || ( $hmacCode === $mid && $singleSlug == base64_decode( $slg ) ) ) : ?>
								<div class="postbox-content-detail">
									<div class="postbox-content-title text-secondary">
										<span class="promotion-code-detail">Kode Promo</span>
										<div class="promotion-box-label-tooltip">
											<span class="promotion-box-label-tooltip__icon-info"></span>
											<span class="promotion-box-label-tooltip__text">Masukkan kode promo di halaman pembayaran</span>
										</div>
									</div>
									<div class="postbox-content-voucher">
										<input type="text" class="postbox-content-voucher__input" data-code-category="<?php echo $catSlug; ?>" value="<?php echo $singleMeta->code ? $singleMeta->code : strtoupper( $plainCode ); ?>"
										  readonly>
										<button class="btn btn-ghost postbox-content-voucher__btn">Salin Kode</button>
									</div>
								</div>
								<?php elseif ( $catSlug !== 'promo-berakhir' ) : ?>
								<div class="postbox-content-detail">
									<div class="postbox-content-title text-secondary">
										<span class="promotion-code-detail">Tanpa kode promo</span>
									</div>
								</div>
								<?php endif; ?>

							</div>
						</div>
						<!-- Desktop Toast -->
						<div class="postbox-toast">
							<p class="postbox-toast__p">Kode Tersalin</p>
							<div class="postbox-toast-action">Tutup</div>
						</div>

						<!-- CTA Button -->
						<?php if ( ! $pdp ) : ?>
						<a href="<?php echo $singleLink; ?>" 
							target="_blank" 
							class="promo-btn btn btn-green btn-medium btn-full hidden-xs"
							data-click='<?php echo json_encode( $gtm['click'] ); ?>'>
							<?php echo $ctaButton; ?> 
						</a>
						<?php endif; ?>

						<!-- TopAds section -->
						<div class="ta-inventory" 
							id="promo-1" 
							src="promo_detail" 
							trigger="complete" 
							is_userinfo_rcmd="true" 
							ep="product" 
							item="4"
						  device="desktop" 
							template="promo_detail">
						</div>
					</div>
				</div>
			</div>
			<!-- ./Sidebar -->

			<!-- Other Promo Recommendation -->
			<?php if ( ! $pdp && $showRecommendation ) : ?>
			<section id="other-promo" data-category="<?php echo $catSlug; ?>">
				<!-- TopAds Mobile -->
				<div class="ta-inventory hidden-sm hidden-md hidden-lg" 
					id="promo-2" 
					src="promo_detail" 
					trigger="complete" 
					is_userinfo_rcmd="true"
					ep="product" 
					item="2" 
					device="desktop" 
					template="promo_detail">
				</div>

				<h3 class="other-promo__heading text-primary text-center">Promo Lainnya</h3>
				<div class="row promo-row">

					<?php
					$posts_per_page = 3;

					// If promo berakhir, generate query for fetching top 3 new post	
					if ( $catSlug == "promo-berakhir" ) {
						// At first, get sticky posts
						$sticky = get_option( 'sticky_posts' );

						/* Sort Sticky Posts, newest at the top */
						rsort( $sticky );
						
						if ( sizeof( $sticky ) == 0 ) {
							$recomendations = new WP_Query(
								array(
									'posts_per_page'	  => 3,
									'post_type'           => 'post',
									'category__not_in'    => $catIDs,
								)
							);

							$catIteration = 0;
							if ( $recomendations->have_posts() ) :
								while ( $recomendations->have_posts() ) :
									$recomendations->the_post();
			
									/* Promo recomendation */
									get_template_part( 'template-parts/post/promo', 'recomendation' );
									$catIteration++;
			
								endwhile;
							endif;
						}
						else if ( sizeof( $sticky ) != 0 ){
							if ( sizeof( $sticky ) > 0  && sizeof( $sticky ) < 3 ) {
								/* Query Sticky Posts */
								$stickyPost = new WP_Query(
									array(
										'post__in'            => $sticky,
										'post_type'           => 'post',
										'category__not_in'    => $catIDs,
									)
								);

								$iteration = 0;
								if ( $stickyPost->have_posts() ) :
									while ( $stickyPost->have_posts() ) :
										$stickyPost->the_post();

										/* Promo recomendation */
										get_template_part( 'template-parts/post/promo', 'recomendation' );
										$iteration++;

									endwhile;
								endif;

								/* Continue Query Posts */
								$otherPromoArgs = (object) array(
									'posts_per_page' 	=> $posts_per_page - sizeof($sticky),
									'category__not_in' 	=> $catIDs,
									'post__not_in'   	=> array( $post->ID ),
									'ignore_sticky_posts' => 1
								);

								$recomendations = new WP_Query( $otherPromoArgs );
								$iteration = 0;

								if ( $recomendations->have_posts() ) :
									while ( $recomendations->have_posts() ) :
										$recomendations->the_post();

										/* Promo recomendation */
										get_template_part( 'template-parts/post/promo', 'recomendation' );

										$iteration++;
									endwhile;
								endif;
							}
							else if ( sizeof( $sticky ) >= 3 ) {
								$stickyPost = new WP_Query(
									array(
										'post__in'            => $sticky,
										'posts_per_page'	  => 3,
										'ignore_sticky_posts' => 1,
										'post_type'           => 'post',
										'category__not_in'    => $catIDs,
									)
								);

								$catIteration = 0;
								if ( $stickyPost->have_posts() ) :
									while ( $stickyPost->have_posts() ) :
										$stickyPost->the_post();

										/* Promo recomendation */
										get_template_part( 'template-parts/post/promo', 'recomendation' );
										$catIteration++;

									endwhile;
								endif;
							}	
						}
					} 
					// If not, generate query for fetching another post from same category
					else {
						$otherPromoArgs = (object) array(
							'cat'            => $catIDs,
							'posts_per_page' => 3,
							'post__not_in'   => array( $post->ID ),
						);
						
						$recomendations     = new WP_Query( $otherPromoArgs );
						$iteration = 0;

						if ( $recomendations->have_posts() ) :
							while ( $recomendations->have_posts() ) :
								$recomendations->the_post();

								/* Promo recomendation */
								get_template_part( 'template-parts/post/promo', 'recomendation' );
								$iteration++;

							endwhile;
						endif;
					}
					?>
				</div>
				<div class="row">
					<div class="col-xs-12 text-center">
						<a href="<?php echo $homeLink; ?>" class="btn btn-ghost btn-medium">Lihat Semua Promo</a>
					</div>
				</div>
			</section>
			<?php endif; ?>
		</div>
	</div>

	<?php if ( $singleMeta->topads == 'true' ) : ?>
	<script src="https://ta.tokopedia.com/plugin/js/display"></script>
	<script>
		(function ($) {
			$.ajax({
				url: 'https://accounts.tokopedia.com/profile/json',
				global: true,
				type: "GET",
				dataType: "jsonp",
				success: function (result) {
					if (result.message_error) {
						userId = '0';
					} else {
						userId = result.data.user_id.toString();
					}

					// sidebar topads
					taHandler.pushStates([{
						id: "promo-1",
						user_id: userId
					}]);
					// mobile topads
					taHandler.pushStates([{
						id: "promo-2",
						user_id: userId
					}]);
				}
			});
		})(jQuery);
	</script>
	<?php endif; ?>

	<?php get_footer(); ?>
