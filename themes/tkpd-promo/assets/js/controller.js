/* eslint-disable prefer-arrow-callback, func-names, indent, no-tabs */
(function ($) {
	/**
	 * Returns unify date format from date range
	 * @param {Date} start
	 * @param {Date} end
	 */
	function unifyDate(start, end) {
		const startDate  = new Date( start );
		const startDay   = startDate.getDate();
		const startMonth = startDate.getMonth();
		const startYear  = startDate.getFullYear();
		const endDate    = new Date( end );
		const endDay     = endDate.getDate();
		const endMonth   = endDate.getMonth();
		const endYear    = endDate.getFullYear();

		const newMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];

		if (startYear === endYear) {
			if (startMonth === endMonth) {
				if (startDay === endDay) {
					return `${startDay} ${newMonth[startMonth]} ${startYear}`;
				}
				return `${startDay} - ${endDay} ${newMonth[startMonth]} ${startYear}`;
			}
			return `${startDay} ${newMonth[startMonth]} - ${endDay} ${newMonth[endMonth]} ${startYear}`;
		}
		return `${startDay} ${newMonth[startMonth]} ${startYear} - ${endDay} ${newMonth[endMonth]} ${endYear}`;
	}

	/**
	 * Check if certain element is on viewport
	 * @param {Element} element
	 */
	function isOnViewport(element) {
		const $elem   = $( element );
		const $window = $( window );

		const windowTop    = $window.scrollTop();
		const windowBottom = windowTop + $window.height();

		const elemTop    = $elem.offset().top;
		const elemBottom = elemTop + $elem.height();

		return (elemTop >= windowTop) && (elemBottom <= windowBottom);
	}

	/**
	 * Send gtm impression to dataLayer
	 * @param {Array} dataLayer
	 */
	function gtmBannerImpression(dataLayer) {
		$( '[data-impression]:not(.viewed)' ).each(
			function () {
				if (isOnViewport( $( this ) )) {
					dataLayer.push( JSON.parse( $( this ).attr( 'data-impression' ) ) );
					$( this ).addClass( 'viewed' );
				}
			}
		);

		return false;
	}

	/**
	 * Send gtm of slider impression
	 * @param {Array} dataLayer
	 */
	function gtmSliderImpression(dataLayer) {
		const bannerDisplayed = [];
		const $sliderWrapper  = $( '.slider-wrapper' );

		return function () {
			if ($sliderWrapper.hasClass( 'slick-initialized' )) {
				$( '[data-slider-impression]' ).each(
					function () {
						const activeSlide       = $sliderWrapper.slick( 'slickCurrentSlide' );
						const $sliderImg        = $( '.slider-slide' ).eq( activeSlide ).find( 'img' );
						const isBannerDisplayed = bannerDisplayed.some(
							function (val) {
								return val === activeSlide;
							}
						);

						if ( ! isBannerDisplayed && isOnViewport( $sliderImg )) {
							dataLayer.push( JSON.parse( $sliderImg.attr( 'data-slider-impression' ) ) );
							bannerDisplayed.push( activeSlide );
						}
					}
				);
			}
		};
	}

	/**
	 * Pivot initialization
	 */
	function initPivot() {
		const pivotDelay = 200;
		const $overlay   = $( '.overlay' );
		const $pivot     = $( '.bridge-widget' );
		var pivotEnterTimeout;
		var pivotLeaveTimeout;

		$( '.bw-button, .bw-container' ).hover(
			function () {
				clearTimeout( pivotLeaveTimeout );

				pivotEnterTimeout = setTimeout(
					function () {
						$pivot.addClass( 'active' );
						$overlay.show();
					}, pivotDelay
				);
			}, function () {
				clearTimeout( pivotEnterTimeout );

				pivotLeaveTimeout = setTimeout(
					function () {
						$pivot.removeClass( 'active' );
						$overlay.hide();
					}, pivotDelay
				);
			}
		);

		// hack for preventing html fire close event
		$( '.bw-container' ).on(
			'click', function (e) {
				e.stopPropagation();
			}
		);

		return false;
	}

	/**
	 * Sticky header
	 */
	function initStickyHeader() {
		const $categories = $( '#categories' );
		const $content    = $( '#content' );

		if ($categories.length > 0) {
			return function () {
				if ($( window ).scrollTop() > $content.offset().top) {
					$categories.next().css( 'padding-top', '108px' );
					$categories.addClass( 'sticky' );
				} else {
					$categories.next().css( 'padding-top', '0' );
					$categories.removeClass( 'sticky' );
				}
			};
		}
		return function () { return false; };
	}

	/**
	 * Initialize slick of subcategory
	 * @param {Object} settings
	 */
	function initSubcatSlick(settings) {
		const $subcatSlider = $( '.subcat-list' );

		if ($( window ).width() < 768) {
			$subcatSlider.slick( settings );
		}

		return function () {
			if ($( window ).width() > 768) {
				if ($subcatSlider.hasClass( 'slick-initialized' )) {
					$subcatSlider.slick( 'unslick' );
				}
				return;
			}
			if ( ! $subcatSlider.hasClass( 'slick-initialized' )) {
				$subcatSlider.slick( settings );
			}
		};
	}

	/**
	 * Run script when window is onload
	 *
	 * TODO: Fix linter problem
	 */
	$( document ).ready(
		function () {
			// global variables
			const dataLayer            = window.dataLayer || [];
			const catSliderSettings    = {
				arrows: true,
				fade: true,
				autoplay: true,
				autoplaySpeed: 5000,
				prevArrow: '<a class="slick-prev"><img src="https://ecs7.tokopedia.net/assets/images/arrow/banner-arrow-left.png" class="slick-arrow__img" alt="slider left"/></a>',
				nextArrow: '<a class="slick-next"><img src="https://ecs7.tokopedia.net/assets/images/arrow/banner-arrow-right.png" class="slick-arrow__img" alt="slider right"/></a>',
				infinite: true,
			};
			const subcatSliderSettings = {
				mobileFirst: true,
				infinite: false,
				arrows: false,
				focusOnSelect: true,
				variableWidth: true,
			};

			const pushStickyHeader        = initStickyHeader();
			const reslickSubcategory      = initSubcatSlick( subcatSliderSettings );
			const sendGtmSliderImpression = gtmSliderImpression( dataLayer );

			const $body          = $( 'body' );
			const $sliderWrapper = $( '.slider-wrapper' );
			const $sliderSlide   = $( '.slider-slide' );

			// slick initialization
			if ($sliderSlide.length > 1) {
				$sliderWrapper.slick( catSliderSettings );
			}

			initPivot();
			gtmBannerImpression( dataLayer );
			sendGtmSliderImpression();

			$( window ).on(
				'resize', function () {
					reslickSubcategory();
				}
			);

			$( window ).scroll(
				function () {
					gtmBannerImpression( dataLayer );
					sendGtmSliderImpression();
					pushStickyHeader();
				}
			);

			$sliderWrapper.on(
				'afterChange', function () {
					sendGtmSliderImpression();
				}
			);

			// gtm click listener
			$body.on(
				'click', '[data-click]', function (event) {
					const gtmProps  = JSON.parse( $( this ).attr( 'data-click' ) );
					dataLayer.push( gtmProps );
				}
			);

			// official store pivot
			$( '#bw-official-store' ).on(
				'click', function () {
					dataLayer.push(
						{
							event: 'clickHeader',
							eventCategory: 'Header',
							eventAction: 'Click',
							eventLabel: 'Promo - Official Store',
						}
					);
				}
			);

			// official store banner
			$( '#content[data-category="official-store"]' ).on(
				'click', '.promotion-image a', function () {
					const promoTitle = $( this ).closest( '.promotion-box' ).attr( 'data-promo-title' );
					const eventLabel = `Official Store Banner - ${promoTitle}`;

					dataLayer.push(
						{
							event: 'clickOSPromo',
							eventCategory: 'Official Store - Promo Microsite',
							eventAction: 'Click',
							eventLabel,
						}
					);
				}
			);

			// official store detail
			$( '.official-store-detail' ).on(
				'click', function () {
					const eventLabel = `Official Store Promo Detail - ${$( this ).attr( 'data-campaign' )}`;

					dataLayer.push(
						{
							event: 'clickOSPromo',
							eventCategory: 'Official Store - Promo Microsite',
							eventAction: 'Click',
							eventLabel,
						}
					);
				}
			);

			// official store CTA
			$( '.official-store-cta' ).on(
				'click', function () {
					const eventLabel = `Official Store Promo CTA - ${$( this ).attr( 'data-campaign' )}`;

					dataLayer.push(
						{
							event: 'clickOSPromo',
							eventCategory: 'Official Store - Promo Microsite',
							eventAction: 'Click',
							eventLabel,
						}
					);
				}
			);

			// official store site
			$( '#official-store-site' ).on(
				'click', function () {
					dataLayer.push(
						{
							event: 'clickOSPromo',
							eventCategory: 'Official Store - Promo Microsite',
							eventAction: 'Click',
							eventLabel: 'View All Official Store',
						}
					);
				}
			);

			/* =============================
			* Get active subcategory for
			* initialize subcategory slick
			* ============================= */
			var activeSubcat = 0;
			const $subcat    = $( '.subcat-list__link' );

			for (var i = 0; i < $subcat.length; i += 1) {
				if ($( $subcat[i] ).hasClass( 'active' )) {
					activeSubcat = i;
				}
			}

			/* =========================
			* Sort Function Controller
			* ========================= */
			const $filterForm   = $( '#filter-form' );
			const $filterSelect = $( '.promo-filter__select' );

			$filterSelect.change(
				function () {
					$filterForm.submit();
				},
			);

			/* ====================================================
			* Copy promo code
			* for index, category, and search page
			* ==================================================== */
			const copyCode = function (elem) {
				  elem.focus();
				  elem.setSelectionRange( 0, elem.value.length );

				try {
					return document.execCommand( 'copy' );
				} catch (e) {
					return false;
				}
			};

			const checkListIcon = '<span class="glyphicon glyphicon-ok icon-green"></span>';

			$( '.promo-row' ).on(
				'click', '.promotion-code__btn',
				function (e) {
					// Prevent default click
					e.preventDefault();

					// Remove current active code
					if ($( '.copied' ).length >= 1) {
						$( '.copied' ).children( '.glyphicon' ).remove();
						$( '.copied' ).text( 'Salin Kode' ).removeClass( 'copied' );
					}

					// Get selected voucher code
					const voucherCode = $( this ).closest( '.promotion-code' ).find( '.sticky-code-voucher__text' ).get( 0 );

					// Set selection range from voucher code
					const succeed = copyCode( voucherCode );

					// If succeed, change button style
					if (succeed) {
						$( this ).text( ' Tersalin' ).addClass( 'copied' ).prepend( checkListIcon );
						voucherCode.blur();

						dataLayer.push(
							{
								event: 'clickPromoMicrosite',
								eventCategory: 'promo microsite - promo list',
								eventAction: 'user click salin kode',
								eventLabel: voucherCode.value,
							}
						);
					}
				},
			);

			/* =============================
			* Copy promo code (single page)
			* ============================= */
			const postCopyCodeBtn  = $( '.postbox-content-voucher__btn' );
			const groupCopyCodeBtn = $( '.postbox-group-voucher-tooltip' );

			postCopyCodeBtn.click(
				function () {
					/* Get selected voucher code */
					const voucherCode = $( this ).siblings( '.postbox-content-voucher__input' ).get( 0 );

					/* Set selection range from voucher code */
					const succeed = copyCode( voucherCode );

					/* If succeed, change button style */
					if (succeed) {
						$( this ).text( ' Tersalin' ).addClass( 'copied' ).prepend( checkListIcon );
						voucherCode.blur();

						dataLayer.push(
							{
								event: 'clickPromoMicrosite',
								eventCategory: 'promo microsite - promo detail',
								eventAction: 'user click salin kode',
								eventLabel: voucherCode.value,
							}
						);
					}
				},
			);

			groupCopyCodeBtn.on(
				'click',
				function () {
					/* Get selected voucher code */
					const voucherCode = $( this ).siblings( '.postbox-content-voucher__input' ).get( 0 );

					/* Set selection range from voucher code */
					const succeed = copyCode( voucherCode );

					/* If succeed, change button style */
					if (succeed) {
						if ($( window ).width() < 768) {
							  $( '.postbox-mobile__btn' ).hasClass( 'active' ) ?
							$( '.postbox-toast-mobile' ).animate(
								{
									bottom: '52px',
								}
							) :
							$( '.postbox-toast-mobile' ).animate(
								{
									bottom: '0',
								}
							);

							var toastTimeout = setTimeout(
								function () {
									$( '.postbox-toast-mobile' ).animate(
										{
											bottom: '-52px',
										}
									);
								}, 3000,
							);
						} else {
							  $( '.postbox-toast' ).addClass( 'show' );

							var toastTimeout = setTimeout(
								function () {
									$( '.postbox-toast' ).removeClass( 'show' );
								}, 2000,
							);
						}

						voucherCode.blur();

						dataLayer.push(
							{
								event: 'clickPromoMicrosite',
								eventCategory: 'promo microsite - promo detail',
								eventAction: 'user click copy icon',
								eventLabel: voucherCode.value,
							}
						);
					}
				},
			);

			// close copy toaster
			$( '.postbox-toast-action' ).on(
				'click',
				function () {
					$( '.postbox-toast' ).removeClass( 'show' );
				},
			);

			// close copy toaster mobile
			$( '.postbox-toast-mobile-action' ).on(
				'click',
				function () {
					$( '.postbox-toast-mobile' ).animate(
						{
							bottom: '-52px',
						}
					);
				},
			);

			/* =========================
			* Toggle display promo code
			* ========================= */
			const postboxGroupTitle = $( '.postbox-group-title' );

			postboxGroupTitle.on(
				'click',
				function () {
					$( this ).closest( '.postbox-group' ).toggleClass( 'active' );
					$( this ).siblings( '.postbox-group-body' ).slideToggle();
				},
			);

			// open the first promo group
			$( '.postbox-mobile .postbox-group-title' ).eq( 0 ).trigger( 'click' );
			$( '.post-sidebar .postbox-group-title' ).eq( 0 ).trigger( 'click' );

			/* =====================================================
			* Remove Floating Button while Scrolling Recommendation
			* ===================================================== */
			const postboxMobileBtn = $( '.postbox-mobile__btn' );

			if ($( '#other-promo' ).length > 0) {
				$( window ).scroll(
					function () {
						if ($( this ).scrollTop() + $( this ).height() > $( '#other-promo' ).offset().top) {
							postboxMobileBtn.removeClass( 'active' );
						} else {
							postboxMobileBtn.addClass( 'active' );
						}
					},
				);
			}

			/* ================
			* Expand SEO
			* ================ */
			const $seoBtn = $( '.seo__btn' );

			$seoBtn.on(
				'click', function () {
					$( this ).parent().addClass( 'active' );
				}
			);

			/* ================
			* Expand More Text
			* ================ */
			const post     = $( '.post-content-main' );
			const postMore = $( '.post-more__text' );
			const content  = $( '.post-content__p' );

			// 248 is .post-content-main height
			// 28 is height of h2 (13) + margin-top of .post-content-main (15)
			if (content.height() <= (248 - 28)) {
				  $( '.post-more' ).hide();
			}

			$( '.post-more' ).click(
				function () {
					var html;
					var contentLen;

					if (post.hasClass( 'active' )) {
						html       = 'Read More <span class="glyphicon glyphicon-menu-down"></span>';
						contentLen = '248px';
					} else {
						html       = 'Show Less <span class="glyphicon glyphicon-menu-up"></span>';
						contentLen = `${content.height() + 28}px`;
					}

					post.toggleClass( 'active' )
					.animate(
						{
							height: contentLen,
						}
					);

					postMore.html( html );
				},
			);

			/* ==============
			* Sticky Sidebar
			* ============== */
			const sidebar = $( '.post-sidebar' );

			if (sidebar.length > 0) {
				  const sidebarOffset = sidebar.offset().top - 20;

				$( window ).scroll(
					function () {
						const stopOffset    = ($( '.main-content' ).height() + 70) - (sidebar.height() + 20);
						const stopOffsetVal = `${stopOffset - 50}px`;

						// Handle float sidebar
						if ($( this ).scrollTop() > sidebarOffset) {
							if ($( this ).scrollTop() < stopOffset) {
								sidebar.addClass( 'fixed' ).css( 'top', '0' );
							} else {
								sidebar.removeClass( 'fixed' ).css( 'top', stopOffsetVal );
							}
						} else {
							sidebar.removeClass( 'fixed' ).css( 'top', '0' );
						}
					},
				);
			}

			/* ====================================
			* Change Small Button in Small Devices
			* ==================================== */
			if ($( window ).width() < 480) {
				$( '.post-header__btn' ).removeClass( 'btn-medium' );
				$( '.post-header__btn' ).addClass( 'btn-small' );
			}

			/* ===================
			* Sticky Promo Header
			* =================== */
			// Centering Position
			var stickyWidth      = $( '.sticky-header' ).width();
			var stickyMarginLeft = ` - ${(stickyWidth / 2) + 15}px`;
			$( '.sticky-header' ).css( 'margin-left', stickyMarginLeft );

			// Toggle Display
			if ($( 'div#info' ).length > 0) {
				$( window ).scroll(
					function () {
						if ($( this ).width() <= 767) {
							if ($( this ).scrollTop() > $( '#post-body' ).offset().top) {
								$( '.sticky-header' ).fadeIn();
							} else {
								$( '.sticky-header' ).fadeOut();
							}
						} else if ($( this ).scrollTop() > $( '#info' ).offset().top - 70) {
								$( '.sticky-header' ).fadeIn();
						} else {
								$( '.sticky-header' ).fadeOut();
						}
					},
				);
			}

			/* ===================
			* Responsive Handler
			* =================== */
			$( window ).resize(
				function () {
					// Centering Position Sticky Promo
					stickyWidth      = $( '.sticky-header' ).width();
					stickyMarginLeft = ` - ${(stickyWidth / 2) + 15}px`;
					$( '.sticky-header' ).css( 'margin-left', stickyMarginLeft );

					// Change Post Header Button Size
					if ($( this ).width() < 480) {
						$( '.post-header__btn' ).removeClass( 'btn-medium' );
						$( '.post-header__btn' ).addClass( 'btn-small' );
					} else {
						$( '.post-header__btn' ).removeClass( 'btn-small' );
						$( '.post-header__btn' ).addClass( 'btn-medium' );
					}
				},
			);
		}
	);
}(jQuery));
/* eslint-disable prefer-arrow-callback, func-names */
