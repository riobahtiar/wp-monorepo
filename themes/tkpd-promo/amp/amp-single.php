<?php
/**
 * Single view template.
 *
 * @package AMP
 */

/**
 * Context.
 *
 * @var AMP_Post_Template $this
 */

$this->load_parts( array( 'html-start' ) );
?>

<?php $this->load_parts( array( 'header' ) ); ?>

<?php
	global $post;

	/* Get flag from app */
	$app = isset( $_GET['flag_app'] ) ? sanitize_text_field( $_GET['flag_app'] ) : null; // flag from app

	/* Get unique code if available */
	$cid = isset( $_GET['cid'] ) ? $_GET['cid'] : null; // promo code base64
	$mid = isset( $_GET['mid'] ) ? $_GET['mid'] : null; // hash value
	$did = isset( $_GET['did'] ) ? $_GET['did'] : null; // epoch date base64
	$pid = isset( $_GET['pid'] ) ? $_GET['pid'] : null; // imploded page id
	$slg = isset( $_GET['slg'] ) ? $_GET['slg'] : null; // validation slug
	$pdp = isset( $_GET['pdp'] ) ? $_GET['pdp'] : null; // pdp

if ( $pid ) {
	$params = explode( ',,', $pid );

	$cid = $params[0];
	$did = $params[1];
	$mid = $params[2];
	$slg = $params[3];
	$pdp = $params[4];

}

	/* Set default timezone (Asia/Jakarta) */
	date_default_timezone_set( 'Asia/Jakarta' );

	/* =====================================
	 * Fetch parameter for personalize promo
	 * ===================================== */
	$plain_code = '';
	$plain_date = '';
	$epoch_date = '';
	$hmac_code  = '';

if ( $cid ) {
	if ( $did ) {
		$epoch_date = base64_decode( $did );
		$plain_date = date( 'Y-m-d', substr( $epoch_date, 0, 10 ) );
	}
	$plain_code = base64_decode( $cid );
	$md5_code   = md5( $plain_code . $epoch_date );
	$hmac_code  = hash_hmac( 'sha256', $md5_code, 'nicogantengproductownerpromotion123' );
}

if ( $pdp ) {
	wp_localize_script( 'controller', 'is_single', (array) is_single() );
	wp_localize_script( 'controller', 'paramPDP', (array) $pdp );
}

	/* =====================================
	 * Initial promo slot (position for GTM)
	 * ===================================== */
	$promo_slot = 1;

	/* ==============================
	 * Insert Flag App in mobile view
	 * ============================== */
	$home_link = $app ?
		  get_bloginfo( 'url' ) . '?flag_app=1&newpage=1' :
		  get_bloginfo( 'url' );

	/* ===================
	 * Excluded category
	 * =================== */
	$show_recommendation = true;
	$expired_category    = get_category_by_slug( 'promo-berakhir' );

	/* ===================
	 * Get post categories
	 * =================== */
	$categories = get_the_category( $post->ID );
	$cat_name   = $categories[0]->cat_name;
	$cat_slug   = $categories[0]->slug;
	$cat_link   = $app ?
		  get_category_link( $categories[0]->cat_ID ) . '?flag_app=1&newpage=1' :
		  get_category_link( $categories[0]->cat_ID );
	$cat_IDs    = [];

foreach ( $categories as $category ) {
	if ( $category->cat_ID == $expired_category->cat_ID ) {
		$show_recommendation = false;
	}
	array_push( $cat_IDs, $category->cat_ID );
}

	/* =====================================
	 * Generate CTA button copy
	 * ===================================== */
	$cta_button = 'Cek Sekarang';

if ( sizeof( $cat_IDs ) == 1 ) {
	$terms      = get_the_terms( get_the_ID(), 'category' );
	$term       = array_pop( $terms );
	$cta_button = get_field( 'cta_button', $term );
}

	/* ==============
	 * Post variables
	 * ============== */
	 $post_slug    = $post->post_name;
	 $post_img     = feature_img_url( $post->ID, 'full' );
	 $post_title   = get_the_title();
	 $post_excerpt = get_the_excerpt();
	 $post_content = get_the_content();
	 $post_meta    = (object) array(
		 'link'            => get_meta( 'promo_link' ),
		 'code'            => get_meta( 'promo_code' ),
		 'min_transaction' => get_meta( 'min_transaction' ),
		 'periode'         => get_meta( 'bataspromo' ), // will be deprecated
		 'start_date'      => get_meta( 'start_date' ),
		 'end_date'        => get_meta( 'end_date' ),
		 'app_link'        => get_meta( 'app_link' ),
		 'promo_codes'     => get_field_object( 'promo_codes' )['value'],
	 );
		?>

<article class="amp-wp-article">
<div class="content" id="<?php echo $post_slug; ?>">
	<div class="container">
			<div class="main-column">
				<!-- Main Section -->
				<main class="main-content">
					<!-- Post Image -->
					<div class="post-image">
						<amp-img src="<?php echo $post_img; ?>" class="img-full" alt="<?php echo $post_title; ?>" layout="responsive" height="40" width="100"></amp-img>
					</div>
					<!-- ./Post Image -->

					<!-- Post Body -->
					<div class="post-body" id="post-body">
						<div class="post-container">
							<div class="post-content">
									<h1 class="post-content__title"><?php echo esc_html( $this->get( 'post_title' ) ); ?></h1>
							</div>
							<div class="post-content">
								<h2 class="post-content__heading text-primary">Deskripsi</h2>
								<p><?php echo tkpdpromo_prefix_excerpt_to_content( 'post_amp_content' ); ?></p>
							</div>
							<!-- Post Box Mobile -->
							<div class="postbox-mobile">
								<div class="postbox clearfix">
									<div class="postbox-content">
										<div class="postbox-content-detail postbox-content--period">
											<amp-img src="https://ecs7.tokopedia.net/assets/images/ic_periode.png" class="postbox-content__img" alt="periode" layout="responsive" height="50" width="10"></amp-img>
											<p class="postbox-content-title text-secondary">
												<?php echo ( $hmac_code === $mid && $plain_date && $post_slug == base64_decode( $slg ) ) ? 'Promo Berakhir' : 'Periode Promo'; ?>
											</p>
											<p class="postbox-content__p">
												<?php
												if ( $hmac_code === $mid && $plain_date && $post_slug == base64_decode( $slg ) ) :
													echo tkp_unify_date( null, $plain_date );
													elseif ( ! empty( $post_meta->start_date ) && ! empty( $post_meta->end_date ) ) :
														echo tkp_unify_date( $post_meta->start_date, $post_meta->end_date );
													else :
														echo $post_meta->periode;
													endif;
													?>
											</p>
										</div>
									<?php if ( ! empty( $post_meta->min_transaction ) ) : ?>
										<div class="postbox-content-detail postbox-content--min-transaction">
											<amp-img src="https://ecs7.tokopedia.net/assets/images/ic_minimum.png" class="postbox-content__img" alt="minimum transaksi" layout="responsive" height="50" width="10"></amp-img>
											<p class="postbox-content-title text-secondary">Minimum Transaksi</p>
											<p class="postbox-content__p"><?php echo $post_meta->min_transaction; ?></p>
										</div>
									<?php endif; ?>
									</div>

									<div class="postbox-content">
									<?php if ( $post_meta->promo_codes ) : ?>
										<div class="postbox-content-detail postbox-content--group-co">
										<?php foreach ( $post_meta->promo_codes as $group_code ) : ?>

											<div class="postbox-group">
												<div class="postbox-group-title">
													<h4 class="postbox-group-title__h4"><?php echo $group_code['group_code_title']; ?></h4>
												</div>
												<div class="postbox-group-body clearfix">
													<div class="postbox-content-title text-secondary">
														<p class="postbox-group-body__p"><?php echo $group_code['group_code_description']; ?></p>
														<span class="promotion-code-detail">Kode Promo</span>
													</div>
												<?php
												$single_code_iter = 0;  // single code iteration
												foreach ( $group_code['group_code'] as $single_code ) :
													?>
													<div class="postbox-content-voucher">
														<div class="postbox-group-voucher-number"></div>
														<input type="text" class="postbox-content-voucher__input" value="<?php echo $single_code['single_code']; ?>" data-code-category="<?php echo $cat_slug; ?>" readonly>
													</div>
												<?php endforeach; ?>
												</div>
											</div>
										<?php endforeach; ?>
										</div>
									<?php elseif ( ! empty( $post_meta->code ) || ( $hmac_code === $mid && $post_slug == base64_decode( $slg ) ) ) : ?>
										<div class="postbox-content-detail">
											<div class="postbox-content-title text-secondary">
												<span class="promotion-code-detail">Kode Promo</span>
											</div>
											<div class="postbox-content-voucher">
												<input type="text" class="postbox-content-voucher__input" data-code-category="<?php echo $cat_slug; ?>" value="<?php echo $post_meta->code ? $post_meta->code : strtoupper( $plain_code ); ?>" readonly>
											</div>
										</div>
									<?php elseif ( $cat_slug !== 'promo-berakhir' ) : ?>
										<div class="postbox-content-detail">
											<div class="postbox-content-title text-secondary">
												<span class="promotion-code-detail">Tanpa kode promo</span>
											</div>
										</div>
									<?php endif; ?>
									</div>
								</div>
							</div>
							<!-- ./Post Box Mobile -->
							<div class="post-content post-content-main">
								<h2 class="post-content-terms__heading">Syarat dan Ketentuan</h2>
								<div class="post-content__p"><?php echo $this->get( 'post_amp_content' ); // WPCS: XSS ok. Handled in AMP_Content::transform(). ?></div>
							</div>
						</div>
						<!-- CTA BTN -->
						<div class="cta-container">
							<a href="<?php echo ( $app && $post_meta->app_link ) ? $post_meta->app_link : $post_meta->link; ?>" class="cta-btn">Cek Sekarang</a>
						</div>
						<!-- ./CTA BTN -->
					</div>
					<!-- ./Post Body -->
				</main>
				<!-- ./Main Section -->
			</div>
	</div>
</div>
</article>

<?php
$this->load_parts( array( 'html-end' ) );
