?php
/**
 * Style template.
 *
 * @package AMP
 */

/**
 * Context.
 *
 * @var AMP_Post_Template $this
 */

?>
/* Generic WP styling */

.alignright {
	float: right;
}

.alignleft {
	float: left;
}

.aligncenter {
	display: block;
	margin-left: auto;
	margin-right: auto;
}

.amp-wp-enforced-sizes {
	/** Our sizes fallback is 100vw, and we have a padding on the container; the max-width here prevents the element from overflowing. **/
	max-width: 100%;
	margin: 0 auto;
}

.amp-wp-unknown-size img {
	/** Worst case scenario when we can't figure out dimensions for an image. **/
	/** Force the image into a box of fixed dimensions and use object-fit to scale. **/
	object-fit: contain;
}

/* Template Styles */

.amp-wp-content,
.amp-wp-title-bar div {
	<?php if ( 600 > 0 ) : ?>
	margin: 0 auto;
	max-width: <?php echo sprintf( '%dpx', 600 ); ?>;
	<?php endif; ?>
}

html {
	background: #F8F8F8;
}

body {
	background: #F8F8F8;
	font-family: 'Open Sans', sans-serif;
	color: rgba(0, 0, 0, 0.7);
	font-weight: 300;
	line-height: 1.75em;
	width: 100%;
	max-width: 600px;
	position: absolute;
	margin: auto;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
}

.img-full{
	width: 100%;
	max-width: none;
}

p,
ol,
ul,
figure {
	margin: 0 0 1em;
	padding: 0;
}

a,
a:visited {
	color: #42b549;
}

a:hover,
a:active,
a:focus {
	color: black;
}

/* Quotes */

blockquote {
	color: black;
	background: rgba(127,127,127,.125);
	border-left: 2px solid #42b549;
	margin: 8px 0 24px 0;
	padding: 16px;
}

blockquote p:last-child {
	margin-bottom: 0;
}

/* UI Fonts */

.amp-wp-meta,
.amp-wp-header div,
.amp-wp-title,
.wp-caption-text,
.amp-wp-tax-category,
.amp-wp-tax-tag,
.amp-wp-comments-link,
.amp-wp-footer p,
.back-to-top {
	font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen-Sans", "Ubuntu", "Cantarell", "Helvetica Neue", sans-serif;
}

/* Header */

.amp-wp-header {
	background-color: #42b549;
}

.amp-wp-header div {
	color: #42b549;
	font-size: 1em;
	font-weight: 400;
	margin: 0 auto;
	max-width: calc(840px - 32px);
	padding: .875em 16px;
	position: relative;
}

.amp-wp-header a {
	color: #42b549;
	text-decoration: none;
}

/* Site Icon */

.amp-wp-header .amp-wp-site-icon {
	/** site icon is 32px **/
	background-color: #42b549;
	border: 1px solid #42b549;
	border-radius: 50%;
	position: absolute;
	right: 18px;
	top: 10px;
}

/* Article */

.amp-wp-article {
	font-weight: 400;
	margin: 1.5em auto;
	margin-top: 0;
	width: 100%;
	overflow-wrap: break-word;
	word-wrap: break-word;
}

/* Article Header */

.amp-wp-article-header {
	align-items: center;
	align-content: stretch;
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
	margin: 1.5em 16px 0;
}

.amp-wp-title {
	color: black;
	display: block;
	flex: 1 0 100%;
	font-weight: 900;
	margin: 0 0 .625em;
	width: 100%;
}

/* Article Meta */

.amp-wp-meta {
	color: #bbbbbb;
	display: inline-block;
	flex: 2 1 50%;
	font-size: .875em;
	line-height: 1.5em;
	margin: 0 0 1.5em;
	padding: 0;
}

.amp-wp-article-header .amp-wp-meta:last-of-type {
	text-align: right;
}

.amp-wp-article-header .amp-wp-meta:first-of-type {
	text-align: left;
}

.amp-wp-byline amp-img,
.amp-wp-byline .amp-wp-author {
	display: inline-block;
	vertical-align: middle;
}

.amp-wp-byline amp-img {
	border: 1px solid #42b549;
	border-radius: 50%;
	position: relative;
	margin-right: 6px;
}

.amp-wp-posted-on {
	text-align: right;
}

/* Featured image */

.amp-wp-article-featured-image {
	margin: 0 0 1em;
}
.amp-wp-article-featured-image amp-img {
	margin: 0 auto;
}
.amp-wp-article-featured-image.wp-caption .wp-caption-text {
	margin: 0 18px;
}

/* Article Content */

.amp-wp-article-content {
	margin: 0 16px;
}

.amp-wp-article-content ul,
.amp-wp-article-content ol {
	margin-left: 1em;
}

.amp-wp-article-content amp-img {
	margin: 0 auto;
}

.amp-wp-article-content amp-img.alignright {
	margin: 0 0 1em 16px;
}

.amp-wp-article-content amp-img.alignleft {
	margin: 0 16px 1em 0;
}

/* Captions */

.wp-caption {
	padding: 0;
}

.wp-caption.alignleft {
	margin-right: 16px;
}

.wp-caption.alignright {
	margin-left: 16px;
}

.wp-caption .wp-caption-text {
	border-bottom: 1px solid #42b549;
	color: #bbbbbb;
	font-size: .875em;
	line-height: 1.5em;
	margin: 0;
	padding: .66em 10px .75em;
}

/* AMP Media */

amp-carousel {
	background: #42b549;
	margin: 0 -16px 1.5em;
}
amp-iframe,
amp-youtube,
amp-instagram,
amp-vine {
	background: #42b549;
	margin: 0 -16px 1.5em;
}

.amp-wp-article-content amp-carousel amp-img {
	border: none;
}

amp-carousel > amp-img > img {
	object-fit: contain;
}

.amp-wp-iframe-placeholder {
	background-size: 48px 48px;
	min-height: 48px;
}

/* Article Footer Meta */

.amp-wp-article-footer .amp-wp-meta {
	display: block;
}

.amp-wp-tax-category,
.amp-wp-tax-tag {
	color: #bbbbbb;
	font-size: .875em;
	line-height: 1.5em;
	margin: 1.5em 16px;
}

.amp-wp-comments-link {
	color: #bbbbbb;
	font-size: .875em;
	line-height: 1.5em;
	text-align: center;
	margin: 2.25em 0 1.5em;
}

.amp-wp-comments-link a {
	border-style: solid;
	border-color: #42b549;
	border-width: 1px 1px 2px;
	border-radius: 4px;
	background-color: transparent;
	color: #42b549;
	cursor: pointer;
	display: block;
	font-size: 14px;
	font-weight: 600;
	line-height: 18px;
	margin: 0 auto;
	max-width: 200px;
	padding: 11px 16px;
	text-decoration: none;
	width: 50%;
	-webkit-transition: background-color 0.2s ease;
			transition: background-color 0.2s ease;
}

/* AMP Footer */

.amp-wp-footer {
	border-top: 1px solid #42b549;
	margin: calc(1.5em - 1px) 0 0;
}

.amp-wp-footer div {
	margin: 0 auto;
	max-width: calc(840px - 32px);
	padding: 1.25em 16px 1.25em;
	position: relative;
}

.amp-wp-footer h2 {
	font-size: 1em;
	line-height: 1.375em;
	margin: 0 0 .5em;
}

.amp-wp-footer p {
	color: #bbbbbb;
	font-size: .8em;
	line-height: 1.5em;
	margin: 0 85px 0 0;
}

.amp-wp-footer a {
	text-decoration: none;
}

.back-to-top {
	bottom: 1.275em;
	font-size: .8em;
	font-weight: 600;
	line-height: 2em;
	position: absolute;
	right: 16px;
}

/*------------------ CUSTOM CSS ----------------*/

* -------------------
 * Promotion Content
 * ------------------- */
.promotion-box{
	background:#fff;
	border-radius: 4px;
	overflow:hidden;
	margin-bottom: 25px;
	-webkit-transition:0.5s;
	transition:0.5s;
}
.promotion-box:hover{
	-webkit-box-shadow: 0 2px 20px 2px rgba(0,0,0,0.1);
	-moz-box-shadow: 0 2px 20px 2px rgba(0,0,0,0.1);
	box-shadow: 0 2px 20px 2px rgba(0, 0, 0, 0.1);
}
.promotion-box p {
	height: 40px;
	overflow: hidden;
	font-weight: 600;
}
.promotion-box-label {
	font-size: 12px;
}
.promotion-box-label-tooltip {
	position: relative;
	display: inline-block;
}
.promotion-box-label-tooltip__icon-info {
	background-image: url(https://ecs7.tokopedia.net/assets/images/ic_info.png);
	background-repeat: no-repeat;
	background-size: 10px 10px;
	display: block;
	width: 10px;
	height: 10px;
}
.promotion-box-label-tooltip__text {
	display: none;
	position: absolute;
	top: 0;
	left: 50%;
	transform: translate(-50%,-100%);
	margin-top: -5px;
	background: rgba(0, 0, 0, 0.8);
	color: #FFF;
	border-radius: 4px;
	padding: 7px;
	font-size: 11px;
	text-align: center;
	width: 162px;
	z-index: 10;
}
.promotion-box-label-tooltip__text:after {
	content: "";
	position: absolute;
	bottom: 0;
	left: 50%;
	margin-bottom: -5px;
	transform: translateX(-50%);
	border-top: 5px solid rgba(0, 0, 0, .8);
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
}
.promotion-box-label-tooltip:hover .promotion-box-label-tooltip__text {
	display: block;
}
.promotion-box__value {
	font-size: 13px;
	line-height: 13px;
}
.promotion-image{
	height: 100%;
	overflow:hidden;
}
.promotion-description{
	border-color: #E0E0E0;
	border-style: solid;
	border-width: 0 1px;
	padding: 18px;
}
.promotion-description h3 {
	font-weight: 600;
	font-size: 18px;
	height: 45px;
	line-height: 1.3em;
	color: #42b549;
	margin-top: 0px;
	margin-bottom: 10px;
	overflow: hidden;
}
.promotion-description h3 a{
	color: #42b549;
}
.promotion-description h3 a:hover{
	color:#008000;
}
.promotion-date {
	margin-top: 10px;
	margin-bottom: 10px;
	height: auto;
}
.promotion-code {
	position: relative;
	height: 40px;
}
.promotion-code-detail,
.promotion-date-detail {
	position: relative;
	padding-left: 30px;
}
.promotion-code-detail {
	display: inline-block;
}
.promotion-date-detail:before,
.promotion-code-detail:before {
	position: absolute;
	content: " ";
	left: 0;
	top: 50%;
	background-repeat: no-repeat;
	background-size: 20px 20px;
	transform: translateY(-50%);
	width: 20px;
	height: 20px;
}
.promotion-date-detail:before {
	background-image: url(https://ecs7.tokopedia.net/assets/images/ic_periode.png);
}
.promotion-code-detail:before {
	background-image: url(https://ecs7.tokopedia.net/assets/images/ic_kode.png);
}


/* ----------
 * Content
 * ---------- */
.post-image {
	height: 100%;
	width: 100%;
	max-width: 600px;
	overflow:hidden;
}
.main-content{
	background: #FFF;
	border-radius: 4px;
	border-top: none;
}
.post-body{
	padding: 30px;
	font-size: 14px;
	line-height: 1.6em;
}
.post-body p{
	line-height: 1.7em;
}
.post-body li {
	margin: 10px 0;
	padding-left: 5px;
}
.post-container {
	width: 100%;
	max-width: 600px;
	margin: auto;
	padding: 0;
	left: 0;
	right: 0;
}
.post-content {
	margin-bottom: 30px;
		width: 100%;
		max-width: 600px;
}
.post-content__title {
	margin-top: 0;
	margin-bottom: 30px;
	font-size: 18px;
	font-weight: 600;
	letter-spacing: -0.8px;
	line-height: 22px;
}
.post-content__heading {
	margin: 15;
	font-size: 12px;
	font-weight: bold;
	text-transform: uppercase;
}
.post-content-terms__heading {
	  margin: 20px;
	  font-size: 12px;
	  font-weight: bold;
	  text-transform: uppercase;
		text-align: center;

}
.post-content__p,
.post-content p {
	margin-top: 15px;

}
.post-content__p ol,
.post-content__p ul {
	padding-left: 25px;
}
.post-content__p img {
	display: block;
	max-width: 100%;
	height: auto;
}
.post-content-main p {
	margin: 0 0 10px;
}
.postbox {
		width: 100%;
	max-width: 600px;
	margin-bottom: 20px;
	background-color: #FFFFFF;
	text-align: center;
}
.postbox-mobile {
	  width: 100%;
	max-width: 600px;
	padding-top: 0px;
		padding-bottom: 0px;
}
.postbox-header {
	padding-top: 18px;
	padding-bottom: 18px;
	border-radius: 3px 3px 0 0;
	border-style: solid;
	border-color: #E0E0E0;
	border-width: 1px 1px 0;
}
.postbox-header__h3 {
	margin: 0;
	font-size: 14px;
	font-weight: 600;
}
.postbox-content {
	position: relative;
	text-align: center;
	display: flex;
	flex-wrap: nowrap;
	justify-content: space-around;
}
.postbox-content-detail {
	border: 1px solid #E0E0E0;
	width: 100%;
	padding: 20px 0;
}
.postbox-content--min-transaction {
	border-left: 0;
	border-bottom: 0;
}
.postbox-content--period {
	border-bottom: 0;
}
.postbox-content--group-code {
	padding: 0 20px;
}
.postbox-content-title {
	font-size: 12px;
		padding-bottom: 5px;
}
.postbox-content__p {
	color: rgba(0, 0, 0, 0.7);
	font-weight: 600;
	font-size: 14px;
}
.postbox-content__img {
	display: block;
	margin: 10px auto;
	width: 24px;
	height: 24px;
}
.postbox-content-voucher {
	display: inline-table;
	vertical-align: middle;
	margin-top: 15px;
	text-align: center;
}
.postbox-content-voucher__input {
	float: none;
	height: 34px;
	color: #FF5722;
	margin-bottom: 10px;
	text-align: center;
	border-radius: 4px;
	background-color: #F8F8F8;
	border: solid 1px #E0E0E0;
}

/* Group code */
.postbox-group {
	padding: 10px;

}
.postbox-group-title__h4 {
	font-size: 14px;
	line-height: 18px;
	font-weight: 600;
	margin: 0;
	color: rgba(0, 0, 0, 0.54);
	padding-top: 5px;
}
.postbox-group-body__p {
	margin-bottom: 10px;
		text-align: center;
		padding-left: 30px;
		padding-right: 30px;
}

/* -------------------
 * Footer
 * ------------------- */

.footer{
	background: #fff;
	-webkit-box-shadow: 0 -1px 1px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: 0 -1px 1px rgba(0, 0, 0, 0.05);
	box-shadow: 0 -1px 1px rgba(0, 0, 0, 0.05);
	text-align: center;
}
.footer-top{
		background: #ffffff;
}
.footer-top p {
		font-size: 12px;
		color: rgba(0,0,0,.54);
		padding-top: 10px;
}
.footer-copyright{
	padding-bottom: 0px;
}
.footer-connect{
	padding-bottom: 10px;
}
.footer-connect > ul{
	margin: 0;
	padding: 0 10px;
	display: inline-block;
}
.footer-connect > ul > li{
	list-style: none;
	display: inline-block;
}
.footer-connect__apps{
	height: 30px;
}
.footer-connect__socmed{
	height: 30px;
}
.footer-bottom{
	background-color: #f8f8f8;
}
.footer__lower {
	padding: 10px;
	color: rgba(0,0,0,.54);
}
.footer__lower a {
		font-size: 14px;
	color: #42b549;
	text-decoration: underline;
	font-weight: 600;
}
.u-center {
	text-align: center;
}
.footer__lower-text--smaller {
	font-size: 11px;
}
.u-line-height-4 {
	line-height: 1.5;
}
.footer_mobile-app-img {
  height: 40px;
  width: 130px;
}

/* -------------------
 * CTA Button
 * ------------------- */

.cta-container{
	width: 100%;
	text-align: center;
	border-top: dashed 1px #bdbdbd;
  padding-top: 30px;
}

a.cta-btn {
	padding: 15px 40px;
	background: #42b549;
	border: 0;
	text-decoration: none;
	color: white;
	font-weight: bold;
}
