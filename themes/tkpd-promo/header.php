<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=0">
	
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<link rel="stylesheet" href="https://ecs7.tokopedia.net/assets/css/bootstrap.min.css">
	<?php wp_head(); ?>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TZNNXTG');</script>
	<!-- End Google Tag Manager -->
	
	<!-- Branch SDK -->
	<script type="text/javascript">
		(function(b,r,a,n,c,h,_,s,d,k){if(!b[n]||!b[n]._q){for(;s<_.length;)c(h,_[s++]);d=r.createElement(a);d.async=1;d.src="https://cdn.branch.io/branch-latest.min.js";k=r.getElementsByTagName(a)[0];k.parentNode.insertBefore(d,k);b[n]=h}})(window,document,"script","branch",function(b,r){b[r]=function(){b._q.push([r,arguments])}},{_q:[],_v:1},"addListener applyCode autoAppIndex banner closeBanner closeJourney creditHistory credits data deepview deepviewCta first getCode init link logout redeem referrals removeListener sendSMS setBranchViewData setIdentity track validateCode trackCommerceEvent logEvent".split(" "), 0);
		branch.init('key_live_abhHgIh1DQiuPxdBNg9EXepdDugwwkHr', function(err, data) {
			// callback to handle err or data
		});
	</script>
	<!-- ./Branch SDK -->
	
	<?php
		global $wp, $post;
		$currentUrl = home_url( add_query_arg( array(), esc_html( $wp->request ) ) );

	if ( is_single() ) {
		$postSlug      = $post->post_name;
		$branchContent = 'promo/' . $postSlug;
	} else {
		$branchContent = 'promo';
	}
	?>
	
	<meta name="branch:deeplink:$desktop_url" content="<?php echo $currentUrl; ?>" />
	<meta name="branch:deeplink:$ios_deeplink_path" content="<?php echo $branchContent; ?>" />
	<meta name="branch:deeplink:$android_deeplink_path" content="<?php echo $branchContent; ?>" />
  </head>
  <body>
  

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TZNNXTG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php

$app = isset( $_GET['flag_app'] ) ? sanitize_text_field( $_GET['flag_app'] ) : null; // flag from app

$pdp = isset( $_GET['pdp'] ) ? $_GET['pdp'] : null; // pdp
$pid = isset( $_GET['pid'] ) ? $_GET['pid'] : null; // imploded page id

if ( $pid ) {
	$params = explode( ',,', $pid );
	$pdp    = $params[4];
}

if ( $app != 1 && ! $pdp ) :
	?>

<!-- Pivot Overlay -->
<div class="overlay"></div>

<!-- Header -->
<header class="header">
	<div class="container">
		<div class="logo">
			<a href="<?php bloginfo( 'url' ); ?>">
				<img src="https://ecs7.tokopedia.net/promo/assets/images/promo_logo.png" height='25' alt="tokopedia promo"/>
			</a>
		</div>
		<div class="wrap_bridge-menu pull-right">
			<div class="bridge-widget inline-block">
				<button class="bw-button">Expander</button>
				<div class="bw-container">
					<div class="bw-item">
						<a href="https://www.tokopedia.com/" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-toped"></div>
							</div>
							<p class="bw-icon__text">Jual Beli Online</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://www.tokopedia.com/official-store" target="_blank" id="bw-official-store">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-official"></div>
							</div>
							<p class="bw-icon__text">Official Store</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://www.tokopedia.com/pulsa" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-pulsa"></div>
							</div>
							<p class="bw-icon__text">Produk Digital</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://tiket.tokopedia.com/" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-tiket"></div>
							</div>
							<p class="bw-icon__text">Tiket Kereta</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://www.tokopedia.com/berbagi" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-donasi"></div>
							</div>
							<p class="bw-icon__text">Donasi</p>
							<span class="clear-b"></span>
						</a>
					</div>
					<div class="bw-item">
						<a href="https://www.tokopedia.com/bantuan/" target="_blank">
							<div class="bw-icon__wrapper">
								<div class="bw-icon bw-icon-help"></div>
							</div>
							<p class="bw-icon__text">Bantuan</p>
							<span class="clear-b"></span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="top-navigation-menu hidden-xs">

		</div>
		<div class="top-navigation-menu">
			<ul>
				<li>
					<a href="https://www.tokopedia.com/bantuan/" class="top-navigation-menu__link menu-help" target="_blank">Bantuan</a>
				</li>
			</ul>
		</div>
   </div>
</header>
<!-- End Header -->

<?php endif; ?>

<!-- Promo Container -->
<main class="promo-container">
