<?php
/*
 * Template Name: Cookie Post
 * Template Post Type: post
 * Author: Khaidir Afif
 */
?>

<!DOCTYPE html>
<?php
$cookie_name  = 'ultah9';
$cookie_value = 'Selamat ulang tahun, Tokopedia!';
setcookie( $cookie_name, $cookie_value, time() + ( 86400 * 30 ), '/' ); // 86400 = 1 day
?>
<html>
<body>

<?php
if ( ! isset( $_COOKIE[ $cookie_name ] ) ) {
	echo "Cookie named '" . $cookie_name . "' is not set!";
} else {
	echo "Cookie '" . $cookie_name . "' is set!<br>";
	echo 'Value is: ' . $_COOKIE[ $cookie_name ];
}
?>

</body>
</html>
