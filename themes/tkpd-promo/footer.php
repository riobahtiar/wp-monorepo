	  
	  </main>
	  <!-- End of promo-container -->
	  <?php
		$pdp = isset( $_GET['pdp'] ) ? $_GET['pdp'] : null; // pdp
		$pid = isset( $_GET['pid'] ) ? $_GET['pid'] : null; // imploded page id

	  if ( $pid ) {
		  $params = explode( ',,', $pid );
		  $pdp    = $params[4];
		}

		if ( $pdp != 1 ) :
			?>
		<!-- Footer -->
		<footer class="footer">
		 <div class="container">
			<div class="pull-left footer-copyright">
			   &copy; 2009-<?php echo date( 'Y' ); ?>, PT Tokopedia
			</div>
			<div class="pull-right footer-connect">
			   <ul>
				  <li><a href="https://itunes.apple.com/us/app/tokopedia/id1001394201?ls=1&mt=8" target="_blank"><img class="footer-connect__apps" src="https://ecs7.tokopedia.net/promo/assets/images/appstore.png" alt="appstore"></a></li>
				  <li><a href="https://play.google.com/store/apps/details?id=com.tokopedia.tkpd" target="_blank"><img class="footer-connect__apps" src="https://ecs7.tokopedia.net/promo/assets/images/googleplay.png" alt="playstore"></a></li>
			   </ul>
			   <ul>
				  <li><a href="https://www.facebook.com/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/facebook.png" alt="facebook"></a></li>
				  <li><a href="http://instagram.com/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/instagram.png" alt="instagram"></a></li>
				  <li><a href="https://twitter.com/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/twitter.png" alt="twitter"></a></li>
				  <li><a href="https://www.youtube.com/user/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/youtube.png" alt="youtube"></a></li>
				  <li><a href="https://www.tokopedia.com/blog/" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/promo/assets/images/blog.png" alt="blog_tokopedia"></a></li>
			   </ul>
			</div>
		 </div>
		</footer>
		<?php endif; ?>
   <!-- End Footer -->
	<?php wp_footer(); ?>
  </body>
	<!-- 
	<?php
	echo( 'Rendered in : ' . timer_stop( 0, 3 ) . ' seconds. On ' );
	date_default_timezone_set( 'Asia/Jakarta' );
	echo date( 'Y-m-d H:i:s' );
	?>
  -->
</html>
