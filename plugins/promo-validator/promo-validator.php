<?php
/**
 * @package Promo Validator
 * @version 1.5
 */
/*
Plugin Name: Promo Validator
Plugin URI: https://www.tokopedia.com
Description: Promo Validator, will take down expired promo
Author: Tokopedia WordPress Team
Version: 1.5
Author URI: https://www.tokopedia.com
*/


// Block direct access to the file
if ( ! function_exists( 'add_action' ) ) {
	echo 'not found';
	exit;
}

// Defined Value
define( 'TKP_DIR_PROMO_VALIDATOR', plugin_dir_path( __FILE__ ) );

// Get required files..
require_once( TKP_DIR_PROMO_VALIDATOR . 'helper.php' );
require_once( TKP_DIR_PROMO_VALIDATOR . 'slack.php' );
require_once( TKP_DIR_PROMO_VALIDATOR . 'app-validator.php' );
require_once( TKP_DIR_PROMO_VALIDATOR . 'cron.php' );
