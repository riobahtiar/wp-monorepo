<?php

/**
 * Expired Promo Runner
 *
 * @return void
 * @description
 * @link
 * @author
 * @version
 * @dependencies
 */
function expired_promo_runner() {
	global $wpdb;
	global $post;

	date_default_timezone_set( 'Asia/Jakarta' );

	// Promo Berakhir Cat
	$cat_obj        = get_category_by_slug( 'promo-berakhir' );
	$id_expired_cat = $cat_obj->term_id;
	if ( false == $cat_obj ) {
		$id_expired_cat = 30;
	}

	// Filter end date only
	$date_query = array(
		array(
			'key'     => 'end_date',
			'value'   => date( 'Y-m-d' ),
			'type'    => 'date',
			'compare' => '<',
		),
	);

	// Get post args
	$selector_args = array(
		'category__not_in' => $id_expired_cat,
		'post_type'        => 'post',
		'post_status'      => array( 'publish' ),
		'post_per_page'    => -1,
		'meta_query'       => $date_query,
	);

	$the_query = new WP_Query( $selector_args );
	$num       = -1;
	if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) :
			$num++;
			$the_query->the_post();
			$post_expired  = get_the_title();
			$expired_date  = get_post_meta( get_the_ID(), 'end_date', true );
			$expired_epoch = strtotime( $expired_date );
			$today_epoch   = strtotime( date( 'Y-m-d' ) );

			// Push to Ended
			if ( $today_epoch != $expired_epoch && $today_epoch > $expired_epoch ) {
				$slack_message = sprintf( '%1$d | Promo: <a href="%2$s">%3$s</a> with end date %4$s already set ended at %5$s', $num, get_permalink( get_the_ID() ), $post_expired, $expired_date, date( 'Y-m-d h:i:sa' ) );

				// $slack_message = $num .
				// ' | Promo : <a target="_blank" href="https://www.tokopedia.com/promo/' . get_post()->post_name . '">
				// ' . $post_expired . '</a>. With end date <b>' . $expired_date .
				// '</b>. Already set ended at ' . date( 'Y-m-d h:i:sa' );

				$activity_log = 'Title : ' . $post_expired . ' | With end date ' . $expired_date .
				' | Already set ended at ' . date( 'Y-m-d h:i:sa' ) . ' | N-' . $num;

				slack_notification( $slack_message, '#banana-alert', 'Promo Bot', ':pepeshocked:' );
				echo '<br>' . $slack_message;

				send_to_activitylog( $activity_log, get_post()->ID );
				wp_set_post_categories( get_the_ID(), array( $id_expired_cat ), false );
				unset_sticky_for_category( $id_expired_cat );
			} else {
				echo '<br> Not expired post';
				send_to_activitylog( 'Expired Promo Checker Running, but no expired promos are found. | ' . date( 'Y-m-d h:i:sa' ) . ' | N-' . $num );
			}

		endwhile;
		wp_reset_postdata();

	else :
		send_to_activitylog( 'Expired Promo Checker Running, but no expired promos are found found. | ' . date( 'Y-m-d h:i:sa' ) . ' | NLOOP' );
	endif;
}



/**
 * Check if post published
 *
 * @param [type] $post_id
 * @param [type] $post
 * @return void
 * @description
 * @link
 * @author
 * @version
 * @dependencies
 */
function post_published_notification( $post_id, $post ) {
	$author         = $post->post_author;
	$name           = get_the_author_meta( 'display_name', $author );
	$title          = $post->post_title;
	$permalink      = get_permalink( $post_id );
	$post_thumbnail = get_field( 'thumbnail_image' , $post->ID );

	//check if post has post thumbnail and featured image, then publish
	if ( has_post_thumbnail( $post->ID ) && $post_thumbnail == false ) {
		$message  = sprintf( 'New promo "%s" has been published by %s' . "\n", $title, $name );
		$message .= sprintf( 'View: %s', $permalink );
		slack_notification( $message, '#banana-alert', 'New Promo Published', ':pepeshocked:' );

	} //if post doesn't has post thumbnail and featured image, then save post to draft
	else {
		wp_update_post(
			array(
				'ID'          => $post->ID,
				'post_status' => 'draft',
				'tags_input'  => 'missing-images',
			)
		);
		$message = sprintf( 'New promo "%s" created by %s was send to draft because the featured image does not set yet' . "\n", $title, $name );
		slack_notification( $message, '#banana-alert', 'New Promo Published', ':pepeshocked:' );
	}
}

//add_action( 'publish_post', 'post_published_notification', 10, 2 );
