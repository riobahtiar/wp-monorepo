<?php

/***
 * Action to run Cron
 */

if ( ! wp_next_scheduled( 'expired_promo_scheduler_hook' ) ) {
	wp_schedule_event( time(), 'hourly', 'expired_promo_scheduler_hook' );
}

add_action( 'expired_promo_scheduler_hook', 'expired_promo_runner' );

