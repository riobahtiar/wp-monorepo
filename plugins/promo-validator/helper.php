<?php

/**
 * Unset Sticky
 *
 * @param [type] $cat
 * @return void
 * @description
 * @link
 * @author riobahtiar
 * @version
 * @dependencies
 */

function unset_sticky_for_category( $cat ) {
	$sticky = get_option( 'sticky_posts' );
	$sp     = new WP_Query(
		array(
			'fields'   => 'ids',
			'post__in' => $sticky,
			'cat'      => $cat, // your category to un-sticky
		)
	);

	if ( ! empty( $sp->posts ) ) {
		$sticky = array_diff( $sticky, $sp->posts );
	}

	update_option( 'sticky_posts', $sticky );
}


/**
 * Convert Epoch to Day
 *
 * @param [type] $seconds
 * @return [string] %a days %h hours
 * @description
 * @link
 * @author
 * @version
 * @dependencies
 */
function seconds_to_date( $seconds ) {
	$dt_f = new \DateTime( '@0' );
	$dt_t = new \DateTime( "@$seconds" );
	return $dt_f->diff( $dt_t )->format( '%a days %h hours' );
}

function send_to_activitylog( $object_name = null, $object_id = null ) {
	if ( class_exists( 'AAL_API' ) ) {
					$aal_class  = new AAL_API();
					$insert_new = array(
						'action'         => 'updated',
						'object_type'    => 'Post',
						'object_subtype' => 'NakaBot Post Checker',
						'object_name'    => $object_name,
						'object_id'      => get_post()->ID,
					);
					$aal_class->insert( $insert_new );
	}
}
