<?php
/*
 * Plugin Name: TKP Consul
 * Plugin URI:  https://github.com/tokopedia
 * Description: A custom WordPress plugin to manage wordpress configuration using Consul KV, Build by Tokopedia Team.
 * Author:      Rio Bahtiar
 * Author URI:  https://github.com/riobahtiar
 * Version:     1.0
 * Text Domain: tkp-consul
 * Domain Path: /languages/
 * License:     GPL v2 or later
 */

 // Call Autoload
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/app/main.php';

// $sf = new SensioLabs\Consul\ServiceFactory();
// echo "host: ".$kv->get('service/wp/global/cdn/host', ['raw' => true]);
