<?php

/**
 * Custom Endpoint post
 * @version tkp v2.0
 * @api /wp-json/tkp/v2/posts
 * @param WP_REST_Request $request This function accepts a rest request to process data.
 *
 * Several param from mobile
 * @param user_id $user_id tokopedia user ID
 * @param os_type $os_type=2 for
 * @param device_time $device_time
 *
 *
 */
function promo_get_posts( $request ) {
	$sticky           = get_option( 'sticky_posts' );
	$expired_cat_name = get_category_by_slug( 'promo-berakhir' );
	$expired_cat_id   = $expired_cat_name->term_id;
	$cat_exclude      = array_map( 'intval', explode( ',', $request->get_param( 'categories_exclude' ) ) );

	if ( true != $request->get_param( 'show_expired' ) ) {
		array_push( $cat_exclude, $expired_cat_id );
	}

	$args = array(
		'posts_per_page'   => $request->get_param( 'per_page' ),
		'offset'           => $request->get_param( 'offset' ),
		'paged'            => $request->get_param( 'page' ),
		'category'         => array_map( 'intval', explode( ',', $request->get_param( 'categories' ) ) ),
		'category_name'    => (array) explode( ',', $request->get_param( 'categories_name' ) ),
		'category__not_in' => $cat_exclude,
		'orderby'          => $request->get_param( 'orderby' ),
		'order'            => $request->get_param( 'order' ),
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'name'             => $request->get_param( 'slug' ),
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => $request->get_param( 'post_parent' ),
		'author'           => $request->get_param( 'author' ),
		'author_name'      => $request->get_param( 'author_name' ),
		's'                => $request->get_param( 'search' ),
		'post_status'      => 'publish',
		'suppress_filters' => true,
	);

	if ( $request->get_param( 'tags_name' ) ) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'post_tag',
				'field'    => 'slug',
				'terms'    => (array) explode( ',', $request->get_param( 'tags_name' ) ),
			),
		);
	}

	/**
	 * CTA Overrider
	 */

	// Override data with $ctasrc
	if ( ! empty( $request->get_param( 'cta_src' ) ) ) {
		$cta_src = (int) sanitize_title( $request->get_param( 'cta_src' ) );
	} else {
		$cta_src = 0;
	}

	$posts_array = get_posts( $args );
	$op          = - 1;
	$posts_view  = array();
	foreach ( $posts_array as $post ) {
		$op ++;
		/**
		 * @TODO: Need convert to function in v3, for better structures
		 */

		// CTA OVD: Text
		$dm_cta = get_field( 'dm_cta', $post->ID );
		// Check if CTA Overrider exist
		$cdt = in_array( $cta_src, array_column( $dm_cta, 'dm_cta_cat' ) );
		$cta = cta_picker( $cta_src, $post->ID );

		//  CTA OVD: Link

		if ( isset( $cta ) && $cdt ) {
			$applink = $cta['applink'];
			$link    = $cta['uri'];
		} else {
			$applink = get_field( 'app_link', $post->ID );
			$link    = get_field( 'promo_link', $post->ID );
		}

		// CTA Configurator
		$cta = get_the_terms( $post->ID, 'category' );
		if ( null != $cta ) {
			// Validation If single array, show cat metas
			if ( count( $cta ) > 1 && ! $cdt ) {
				if ( get_field( 'default_cta_button', 'option' ) != null ) {
					$cta_text_val = get_field( 'default_cta_button', 'option' );
				} else {
					$cta_text_val = esc_html__( 'Cek Sekarang' );
				}
			} else {
				if ( $cta_src != 0 && $cdt ) {
					$cta_picker   = cta_picker( $cta_src, $post->ID );
					$cta_text_val = $cta_picker['text'];
				} else {
					$cta_text_val = get_field( 'cta_button', 'category_' . $cta[0]->term_id );
				}
			}
		} else {
			$cta_text_val = esc_html__( 'Cek Sekarang' );
		}

		// Multiple Promo Codes Object
		$codes      = get_field( 'promo_codes', $post->ID );
		$start_date = get_field( 'start_date', $post->ID );
		$end_date   = get_field( 'end_date', $post->ID );
		// Check data, and make it empty array if false
		if ( false == $codes || null == $codes ) {
			$codes = array();
		}

		$posts_view['posts'][ $op ]['id']        = $post->ID;
		$posts_view['posts'][ $op ]['date']      = date( 'Y-m-d\TH:i:s', rest_parse_date( $post->post_date ) );
		$posts_view['posts'][ $op ]['modified']  = date( 'Y-m-d\TH:i:s', rest_parse_date( $post->post_date_gmt ) );
		$posts_view['posts'][ $op ]['author']    = (int) $post->post_author;
		$posts_view['posts'][ $op ]['sticky']    = is_sticky( $post->ID );
		$posts_view['posts'][ $op ]['slug']      = $post->post_name;
		$posts_view['posts'][ $op ]['link']      = home_url( '/' . $post->post_name );
		$posts_view['posts'][ $op ]['link_apps'] = home_url( '/' . $post->post_name . '?flag_app=1' );
		$posts_view['posts'][ $op ]['title']     = $post->post_title;
		if ( true == $request->get_param( 'content' ) ) {
			$posts_view['posts'][ $op ]['content'] = tkp_extend_content_response( $post->post_content );
		}
		$posts_view['posts'][ $op ]['excerpt']                         = $post->post_excerpt;
		$posts_view['posts'][ $op ]['coupon']                          = get_post_meta( $post->ID, 'promo_code', true );
		$posts_view['posts'][ $op ]['multiple_coupon']['total_coupon'] = tkp_get_total_code( (object) $codes );
		$posts_view['posts'][ $op ]['multiple_coupon']['data']         = $codes;
		$posts_view['posts'][ $op ]['start_date']                      = $start_date;
		$posts_view['posts'][ $op ]['end_date']                        = $end_date;
		$posts_view['posts'][ $op ]['date_text']                       = tkp_unify_date( $start_date, $end_date );
		$posts_view['posts'][ $op ]['min_transaction']                 = get_post_meta( $post->ID, 'min_transaction', true );
		$posts_view['posts'][ $op ]['app_link']                        = $applink;
		$posts_view['posts'][ $op ]['promo_link']                      = $link;
		$posts_view['posts'][ $op ]['cta_text']                        = $cta_text_val;
		$posts_view['posts'][ $op ]['cta_src']                         = $cta_src;
		$posts_view['posts'][ $op ]['images']['thumbnail']             = wp_get_attachment_image_src( get_post_meta( $post->ID, 'thumbnail_image', true ) )[0] ?: get_post_meta( $post->ID, 'thumbnail_image', true );
		$posts_view['posts'][ $op ]['images']['banner']                = tkp_to_empty_string( get_the_post_thumbnail_url( $post->ID, 'full' ) );
		$posts_view['posts'][ $op ]['categories']                      = wp_get_post_categories( $post->ID );
		$posts_view['posts'][ $op ]['tags']                            = wp_get_post_tags( $post->ID, array( 'fields' => 'ids' ) );
	}
	// Make empty array if empty
	if ( null == $posts_view['posts'] ) {
		$posts_view['posts'] = array();
	}
	$rest_response                           = array();
	$rest_response['header']['total_data']   = $op + 1;
	$rest_response['header']['process_time'] = timer_stop( 0, 3 );
	$rest_response['data']                   = $posts_view['posts'];

	return rest_ensure_response( $rest_response );
}

/**
 * This is our callback function to return a single tkp_posts.
 *
 * @param WP_REST_Request $request This function accepts a rest request to process data.
 */
function promo_get_tkp_posts( $request ) {
	$id   = (string) $request['id'];
	$post = get_post( $id );
	if ( isset( $post ) ) {

		/**
		 * CTA Overrider
		 */

		// Override data with $ctasrc
		if ( ! empty( $request->get_param( 'cta_src' ) ) ) {
			$cta_src = (int) sanitize_title( $request->get_param( 'cta_src' ) );
		} else {
			$cta_src = 0;
		}
		/**
		 * Need convert to function in v3, for better structures
		 */
		// CTA Configurator

		// CTA OVD: Text
		$dm_cta = get_field( 'dm_cta', $post->ID );
		// Check if CTA Overrider exist
		$cdt = in_array( $cta_src, array_column( $dm_cta, 'dm_cta_cat' ) );
		$cta = cta_picker( $cta_src, $post->ID );

		//  CTA OVD: Link

		if ( isset( $cta ) && $cdt ) {
			$applink = $cta['applink'];
			$link    = $cta['uri'];
		} else {
			$applink = get_field( 'app_link', $post->ID );
			$link    = get_field( 'promo_link', $post->ID );
		}

		$cta = get_the_terms( $post->ID, 'category' );
		if ( null != $cta ) {
			// Validation If single array, show cat metas
			if ( count( $cta ) > 1 && ! $cdt ) {
				if ( get_field( 'default_cta_button', 'option' ) != null ) {
					$cta_text_val = get_field( 'default_cta_button', 'option' );
				} else {
					$cta_text_val = esc_html__( 'Cek Sekarang' );
				}
			} else {
				if ( $cta_src != 0 && $cdt ) {
					$cta_picker   = cta_picker( $cta_src, $post->ID );
					$cta_text_val = $cta_picker['text'];
				} else {
					$cta_text_val = get_field( 'cta_button', 'category_' . $cta[0]->term_id );
				}
			}
		} else {
			$cta_text_val = esc_html__( 'Cek Sekarang' );
		}

		// Multiple Promo Codes Object
		$codes      = get_field( 'promo_codes', $post->ID );
		$start_date = get_field( 'start_date', $post->ID );
		$end_date   = get_field( 'end_date', $post->ID );

		// Check data, and make it empty array if false
		if ( false == $codes || null == $codes ) {
			$codes = array();
		}

		$item['id']                              = $post->ID;
		$item['date']                            = date( 'Y-m-d\TH:i:s', rest_parse_date( $post->post_date ) );
		$item['modified']                        = date( 'Y-m-d\TH:i:s', rest_parse_date( $post->post_date_gmt ) );
		$item['author']                          = (int) $post->post_author;
		$item['sticky']                          = is_sticky( $post->ID );
		$item['slug']                            = $post->post_name;
		$item['link']                            = home_url( '/' . $post->post_name );
		$item['link_apps']                       = home_url( '/' . $post->post_name . '?flag_app=1' );
		$item['title']                           = $post->post_title;
		$item['content']                         = tkp_extend_content_response( $post->post_content );
		$item['excerpt']                         = $post->post_excerpt;
		$item['coupon']                          = get_post_meta( $post->ID, 'promo_code', true );
		$item['multiple_coupon']['total_coupon'] = tkp_get_total_code( (object) $codes );
		$item['multiple_coupon']['data']         = $codes;
		$item['start_date']                      = $start_date;
		$item['end_date']                        = $end_date;
		$item['date_text']                       = tkp_unify_date( $start_date, $end_date );
		$item['min_transaction']                 = get_post_meta( $post->ID, 'min_transaction', true );
		$item['app_link']                        = $applink;
		$item['promo_link']                      = $link;
		$item['cta_text']                        = $cta_text_val;
		$item['cta_src']                         = $cta_src;
		$item['images']['thumbnail']             = wp_get_attachment_image_src( get_post_meta( $post->ID, 'thumbnail_image', true ) )[0] ?: get_post_meta( $post->ID, 'thumbnail_image', true );
		$item['images']['banner']                = tkp_to_empty_string( get_the_post_thumbnail_url( $post->ID, 'full' ) );
		$item['categories']                      = wp_get_post_categories( $post->ID );
		$item['tags']                            = wp_get_post_tags( $post->ID, array( 'fields' => 'ids' ) );

		// Return the tkp_posts as a response.
		$rest_response                           = array();
		$rest_response['header']['total_data']   = 1;
		$rest_response['header']['process_time'] = timer_stop( 0, 3 );
		$rest_response['data']                   = $item;
		return rest_ensure_response( $rest_response );
	} else {
		// Return a WP_Error because the request tkp_posts was not found. In this case we return a 404 because the main resource was not found.
		return new WP_Error( 'rest_tkp_posts_invalid', esc_html__( 'The posts does not exist.', 'tokopedia' ), array( 'status' => 404 ) );
	}

	// If the code somehow executes to here something bad happened return a 500.
	return new WP_Error( 'rest_api_sad', esc_html__( 'Something went horribly wrong.', 'tokopedia' ), array( 'status' => 500 ) );
}

/**
 * This function is where we register our routes
 */
function promo_register_tkp_posts_routes() {
	register_rest_route(
		'tkp/v3',
		'/posts',
		array(
			'methods'  => WP_REST_Server::READABLE,
			'callback' => 'promo_get_posts',
		)
	);

	register_rest_route(
		'tkp/v3',
		'/posts/(?P<id>[\d]+)',
		array(
			'methods'  => WP_REST_Server::READABLE,
			'callback' => 'promo_get_tkp_posts',
		)
	);
}

add_action( 'rest_api_init', 'promo_register_tkp_posts_routes' );
