<?php
/**
 * @package Tokopedia
 */
/*
Plugin Name: New Promo Helper
Plugin URI: https://tokopedia.com/
Description: Promo Detail with Advanced Custom Field, Rest API's End Point, Tokopedia Login
Version: 4.0
Author: Tokopedia WordPress Team
Author URI: https://www.tokopedia.com/promo
License: GPLv2 or later
Text Domain: tokopedia
Last Change: 19 Oktober 2018

*/

// Block direct access to the file
if ( ! function_exists( 'add_action' ) ) {
	echo 'not found';
	exit;
}

// Defined Value
define( 'TOPED_PLUGIN_PROMO', plugin_dir_path( __FILE__ ) );

// Get required files..
require_once( TOPED_PLUGIN_PROMO . 'util/validation.php' );
require_once( TOPED_PLUGIN_PROMO . 'util/posts.php' );
require_once( TOPED_PLUGIN_PROMO . 'init.dynamic-cta-fields.php' );
require_once( TOPED_PLUGIN_PROMO . 'init.promo-details-fields.php' );
require_once( TOPED_PLUGIN_PROMO . 'init.custom-acf.php' );
require_once( TOPED_PLUGIN_PROMO . 'init.rest-api.php' );
require_once( TOPED_PLUGIN_PROMO . 'init.post-type.php' );
require_once( TOPED_PLUGIN_PROMO . 'init.option-page.php' );
require_once( TOPED_PLUGIN_PROMO . 'wp.login.style.php' );
require_once( TOPED_PLUGIN_PROMO . 'inc/class-wps-hide-login.php' );
require_once( TOPED_PLUGIN_PROMO . 'inc/class-wp-rest-posts-promo.php' );
require_once( TOPED_PLUGIN_PROMO . 'rest-api/tkp/v3/posts.php' );


/**
 * Function Filter Post Class
 *
 * @param [object] $args
 * @link https://developer.wordpress.org/reference/hooks/register_post_type_args/
 * @since WP 4.9.2
 * @since Plugins 3.0
 */
function extends_current_post( $args, $post_type ) {
	$custom_post_type = 'post';

	if ( $post_type !== $custom_post_type ) {
		return $args;
	}
	$args['rest_controller_class'] = 'WP_REST_Posts_Promo';

	return $args;
}

add_filter( 'register_post_type_args', 'extends_current_post', 10, 2 );
