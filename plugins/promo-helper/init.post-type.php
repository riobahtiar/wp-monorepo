<?php

// Add slider post type
add_action( 'init', 'promo_slider' );

function promo_slider() {
	register_post_type(
		'promo_slider',
		array(
			'labels'               => array(
				'name'          => __( 'Slider' ),
				'singular_name' => __( 'Slider' ),
			),
			'public'               => true,
			'has_archive'          => true,
			'supports'             => array( 'title', 'author', 'thumbnail', 'excerpt', 'comments' ),
			'register_meta_box_cb' => 'add_slider_metabox',
		)
	);
}

// Add metabox
add_action( 'add_meta_boxes', 'add_slider_metabox' );

// Custom metabox to slider
function add_slider_metabox() {
	add_meta_box( 'slider_box', 'Link Slider', 'slider_box', 'promo_slider', 'normal', 'high' );
}

// Slider box
function slider_box( $post ) {
	$value = get_post_custom( $post->ID );
	$link  = isset( $value['slider_link'] ) ? esc_attr( $value['slider_link'][0] ) : '';

	wp_nonce_field( 'slider_nonce_field', 'slider_nonce' );

	echo '<input type="text" name="slider_link" value="' . $link . '" style="width: 100%;"/>';
	echo '<p>Masukan link slider</p>';
}

// Save slider action
add_action( 'save_post', 'slider_save' );

// Save slider function
function slider_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Bail if nonce unverified
	if ( ! isset( $_POST['slider_nonce'] ) || ! wp_verify_nonce( $_POST['slider_nonce'], 'slider_nonce_field' ) ) {
		return;
	}

	// Bail if user cant edit
	if ( ! current_user_can( 'edit_post' ) ) {
		return;
	}

	if ( isset( $_POST['slider_link'] ) ) {
		update_post_meta( $post_id, 'slider_link', $_POST['slider_link'] );
	}
}
