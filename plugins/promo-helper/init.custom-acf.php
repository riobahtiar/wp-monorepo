<?php

/*
* Load App Link
*/

function acf_load_applink_list( $field ) {

	// reset choices
	$field['choices'] = array();

	$choices = get_app_link();

	// loop through array and add to field 'choices'
	if ( is_array( $choices ) ) {

		foreach ( $choices as $key => $value ) {

			$field['choices'][ $key ] = $value;

		}
	}

	// return the field
	return $field;
}
add_filter( 'acf/load_field/name=list_app_link', 'acf_load_applink_list' );

/*
* Initialize applink options
*/
function get_app_link() {
	$app_link = array(
		'no-applink' => 'No App Link',
		'home'       => 'Home',
		'product'    => 'Product',
		'shop'       => 'Shop',
		'etalase'    => 'Etalase',
		'webview'    => 'Webview',
		'digital-1'  => 'Digital - Pulsa',
		'digital-2'  => 'Digital - Paket Data',
		'digital-3'  => 'Digital - Listrik PLN',
		'digital-4'  => 'Digital - BPJS',
		'digital-5'  => 'Digital - PDAM',
		'digital-6'  => 'Digital - Voucher Game',
		'digital-7'  => 'Digital - Angsuran Kredit',
		'digital-8'  => 'Digital - TV Kabel',
		'digital-9'  => 'Digital - Pasca Bayar',
		'digital-10' => 'Digital - Telkom',
		'digital-11' => 'Digital - Tiket Kereta',
		'digital-12' => 'Digital - Donasi',
		'digital-13' => 'Digital - Streaming',
		'digital-14' => 'Digital - Gas PGN',
		'digital-15' => 'Digital - Zakat',
		'digital-16' => 'Digital - Hiburan',
		'digital-17' => 'Digital - Roaming',
		'digital-18' => 'Digital - Pajak',
		'digital-19' => 'Digital - TokoCash',
	);

	return $app_link;
}

/*
* Enqueue javascript from custom-acf.js
*/
function acf_admin_enqueue( $hook ) {
	$type  = get_post_type(); // Check current post type
	$types = array( 'post' ); // Allowed post types

	if ( ! in_array( $type, $types ) ) {
		return; // Only applies to post types in array
	}

	wp_enqueue_script( 'populate-area', plugin_dir_url( __FILE__ ) . '/assets/js/custom-acf.js' );

	wp_localize_script(
		'populate-area',
		'pa_vars',
		array(
			'pa_nonce' => wp_create_nonce( 'pa_nonce' ), // Create nonce which we later will use to verify AJAX request
		)
	);
}

add_action( 'acf/input/admin_enqueue_scripts', 'acf_admin_enqueue' );

/*
* Set default value and disable site_url field
*/
function my_acf_load_field_site_url( $field ) {

	 $site_url = get_bloginfo( 'url' );

	 $field['default_value'] = $site_url;
	 $field['value']         = $site_url;
	 $field['disabled']      = 1;

	 return $field;

}

add_filter( 'acf/load_field/name=site_url', 'my_acf_load_field_site_url' );

/*
* Add default image field
*/
// function add_default_value_to_image_field( $field ) {
//   $args = array(
//     'label' 		=> 'Default Image',
//     'instructions' 	=> 'Appears when creating a new post',
//     'type' 			=> 'image',
//     'name' 			=> 'default_value'
//   );
//   acf_render_field_setting( $field, $args );
// }

// add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field', 20);

// function enqueue_uploader_for_image_default() {
// 	$screen = get_current_screen();
// 	if ( $screen && $screen->id && ( $screen->id == 'acf-field-group' ) ) {
// 		acf_enqueue_uploader();
// 	}
// }

// function reset_default_image( $value, $post_id, $field ) {
// 	if ( ! $value ) {
// 		$value = $field['default_value'];
// 	}
// 	return $value;
// }

// add_filter( 'acf/load_value/type=image', 'reset_default_image', 10, 3 );

// function my_acf_load_field_image_thumbnail( $field ) {
// 	// $field['default_value'] = 'https://ecs7.tokopedia.net/img/blog/promo/2018/06/tokopedia404-promo-image.jpg';
// 	$img                    = get_field( 'image' );
// 	$field['value'] 		= $img;
// 	// $field['default_value'] = $img;
// 	$field['disabled']      = 1;

// 	return $field;
// }

// add_filter( 'acf/load_field/name=thumbnail_image', 'my_acf_load_field_image_thumbnail' );

// function update_image_thumbnail(  $value, $post_id, $field  ) {
// 	$img = get_field('image');
// 	$value = $img;

// 	return $value;
// }

// add_filter( 'acf/update_value/name=thumbnail_image', 'update_image_thumbnail', 10, 3 );
