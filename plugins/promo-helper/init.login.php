<?php
/**
 * @package Toped
 * Login Authentication Helper
 *
**/

// Costumize Logo Login
function my_login_logo() {
	?>
	<style type="text/css">
		#login h1 a, .login h1 a {
			background-image: url(https://ecs7.tokopedia.net/assets/images/auth.png);
			padding-bottom: 30px;
		}
	</style>
	<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// Custom Login Redirect, and unwanted get value

add_action( 'init', 'toped_disable_wp_login' );
function toped_disable_wp_login() {
	global $pagenow;
	$intrm      = ( isset( $_GET['interim-login'] ) ) ? $_GET['interim-login'] : '';
	$checkemail = ( isset( $_GET['checkemail'] ) ) ? $_GET['checkemail'] : '';
	$action     = ( isset( $_GET['action'] ) ) ? $_GET['action'] : '';
	if ( $pagenow == 'wp-login.php' && (
		( ! $action || ( $action && ! in_array( $action, array( 'logout', 'lostpassword', 'rp', 'granted', 'resetpass' ) ) ) ) ||
		( ( $checkemail && ! in_array( $checkemail, array( 'confirm' ) ) ) )
		 ) ) {
		if ( $pagenow == 'wp-login.php' && ( ! $intrm || ( ( $intrm && ! in_array( $intrm, array( 1, '1' ) ) ) ) ) ) {
			$page = get_bloginfo( 'url' );
			wp_redirect( $page );
			exit();
		} else {
			$page = get_bloginfo( 'url' );
			wp_redirect( $page . '/nest-tkpd' );
			exit();
		}
	}
}

// Show error if user failed to login user known credentials
function custom_login_failed() {
	$login_page = home_url( '/nest-tkpd/' );
	wp_redirect( $login_page . '?topedauth=failed' );
	exit;
}
add_action( 'wp_login_failed', 'custom_login_failed' );


// Redirect Logout Success
function logout_redirect() {
	$login_page = home_url( '/nest-tkpd/' );
	wp_redirect( $login_page . '?topedauth=logout-success' );
	exit;
}
add_action( 'logout_redirect', 'logout_redirect' );


// If all field empty, show error messages
function verify_user_pass( $user, $username, $password ) {
	$login_page = home_url( '/nest-tkpd/' );
	if ( $username == '' || $password == '' ) {
		wp_redirect( $login_page . '?topedauth=empty' );
		exit;
	}
}
add_filter( 'authenticate', 'verify_user_pass', 1, 3 );
