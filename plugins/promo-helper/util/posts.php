<?php

/*
* Change date to unify format
*/
if ( ! function_exists( 'tkp_unify_date' ) ) :
	function tkp_unify_date( $start_date, $end_date ) {
		$new_month = [ 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des' ];

		$end_day   = date( 'j', strtotime( $end_date ) );
		$end_month = date( 'n', strtotime( $end_date ) );
		$end_year  = date( 'Y', strtotime( $end_date ) );

		if ( null != $start_date ) {
			$start_day   = date( 'j', strtotime( $start_date ) );
			$start_month = date( 'n', strtotime( $start_date ) );
			$start_year  = date( 'Y', strtotime( $start_date ) );

			if ( $start_year == $end_year ) {
				if ( $start_month == $end_month ) {
					if ( $start_day == $end_day ) {
						return $end_day . ' ' . $new_month[ $end_month - 1 ] . ' ' . $end_year;
					}
					return $start_day . ' - ' . $end_day . ' ' . $new_month[ $start_month - 1 ] . ' ' . $start_year;
				}
				return $start_day . ' ' . $new_month[ $start_month - 1 ] . ' - ' . $end_day . ' ' . $new_month[ $end_month - 1 ] . ' ' . $start_year;
			}
			return $start_day . ' ' . $new_month[ $start_month - 1 ] . ' ' . $start_year . ' - ' . $end_day . ' ' . $new_month[ $end_month - 1 ] . ' ' . $end_year;
		} else {
			return $end_day . ' ' . $new_month[ $end_month - 1 ] . ' ' . $end_year;
		}
	}
endif;

/*
* Function to count total code
*/
if ( ! function_exists( 'tkp_get_total_code' ) ) :
	function tkp_get_total_code( $promo_codes ) {
		$total_codes = 0;

		foreach ( $promo_codes as $group_code ) {
			$total_codes += sizeof( $group_code['group_code'] );
		}

		return (int) $total_codes;
	}
endif;

/*
* Removing redirect url
* @link http://biostall.com/prevent-wordpress-redirecting-to-nearest-matching-url/
*/
remove_filter( 'template_redirect', 'redirect_canonical' );

/*
* Extend content response in rest api
* @version api: wp/v2 & tkp/v3
* @author Rio Bahtiar <hi@rio.my.id>
*/
if ( ! function_exists( 'tkp_extend_content_response' ) ) :
	function tkp_extend_content_response( $content ) {
		/**
		 * Insert style for table in the end of rest api
		 *
		 * @author Rio Bahtiar <rio.bahtiar@tokopedia.com>
		 * @api /posts & /posts/:id
		 */
		$table_style = ' <style>ol, ul, p{color: rgba(0, 0, 0, 0.7);}table{border-collapse: collapse; width: 100% !important; height: auto;}td, th{border: 1px solid #D4D4D4; padding: 5px;}a{color: #42B549 !important; text-decoration: none;}</style>';

		return apply_filters( 'the_content', $content . $table_style );
	}
endif;


// Dynamic CTA Picker
function cta_picker( $id = 0, $postid ) {
	$dm_cta = get_field( 'dm_cta', $postid );
	$rtr    = array();

	if ( 0 != $id && ! empty( $dm_cta ) ) {
		foreach ( $dm_cta as $cta ) {
			if ( $cta['dm_cta_cat'] == $id ) {
				$rtr['id']      = $cta['dm_cta_cat'];
				$rtr['text']    = $cta['dm_cta_text'];
				$rtr['uri']     = $cta['dm_cta_link'];
				$rtr['applink'] = $cta['dm_cta_app_link'];
				break;
			}
		}
	}

	return $rtr;
}
