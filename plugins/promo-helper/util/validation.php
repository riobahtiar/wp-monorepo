<?php
/**
 * Validate if Empty
 */

function tkp_to_empty_string( $data = null ) {
	$var = array( null, false, array() );
	if ( in_array( $data, $var ) ) {
		return '';
	} else {
		return $data;
	}
}

