(function($) {
	$(
		function(){
			var appLinkType   = $( '#applink-type select' );
			var appLinkPrefix = 'tokopedia://';
			var appLinkValue  = $( '#applink-value input' );
			var appLink       = '';
			var scraperUrl    = $( '#site-url input' ).val() + '/scraper';
			var promoLink     = $( '#link-promo input' );
			var etalaseField  = $( '#list-etalase select' );

			appLinkType.on(
				"change",
				function(){
					//alert( appLinkPrefix + $(this).val() );
					appLinkValue.val( '' );
					var type = $( this ).val();

					/* Home */
					if (type === 'home') {
						appLink = appLinkPrefix + $( this ).val();
						appLinkValue.val( appLink );
					}

					/* Product Page */
					if (type === 'product') {
						$.post(
							scraperUrl,
							{'url': promoLink.val(), 'option': 2},
							function(response) {
								var data = JSON.parse( response );

								if (data.success) {
									appLink = appLinkPrefix + 'product/' + data.product_id;
									appLinkValue.val( appLink );
								} else {
									alert( "Product " + data.message + ". Please input a valid promo link." );
									appLinkValue.val( "Link promo invalid" )
								}
							}
						);
					}

					/* Shop Page */
					if (type === 'shop') {
						var slug = ''; 
						var endpoint = 'https://tome.tokopedia.com/v1/shop/get_summary?domain=';

						slug = promoLink.val().replace('https://www.tokopedia.com/', '');
						if(slug.indexOf('/') >= 0) {
							slug = slug.replace('/', '');
						}

						endpoint = endpoint + slug;

						$.get(
							endpoint,
							function(result, status) {
								console.log( result );

								if ( result.data.length === 0 ) {
									alert( "Shop not found. Please input a valid promo link." );
									appLinkValue.val( "Invalid promo link" );
								} else {
									appLink = appLinkPrefix + 'shop/' + result.data[0].shop_id;
									alert( appLink )
									appLinkValue.val( appLink );
								}

							}
						);
					}

					/* Etalase */
					if (type === 'etalase') {
						etalaseField.on(
							"change",
							function(){
								etalaseField = $( this ).val();
								appLinkValue.val( etalaseField );
							}
						);
					}
					/* Webview */
					if (type === 'webview') {
						var webviewApplink = appLinkPrefix + 'webview?url=';
						var appLink        = webviewApplink + encodeURIComponent( promoLink.val() );

						appLinkValue.val( appLink );
					}

					/* Digital Page */
					if (type.indexOf( 'digital' ) !== -1) {
						var digital_id = type.slice( 8 );

						/* Tiket Kereta app link */
						if (digital_id === '11') {
							appLink = 'tokopedia://webview?url=https%3A%2F%2Ftiket.tokopedia.com%2Fkereta-api%3Fispulsa%3D1';
						} /* Other digital goods */
						else {
							appLink = appLinkPrefix + 'digital/form?category_id=' + digital_id;
						}
						appLinkValue.val( appLink );
					}
				}
			);

			promoLink.on(
				"change",
				function(){
					/* Product Page */
					if (appLinkType.val() === 'product') {
						$.post(
							scraperUrl,
							{'url': promoLink.val(), 'option': 2},
							function(response) {
								var data = JSON.parse( response );

								if (data.success) {
									appLink = appLinkPrefix + 'product/' + data.product_id;
									appLinkValue.val( appLink );
								} else {
									alert( "Product " + data.message + ". Please input a valid promo link." );
									appLinkValue.val( "Link promo invalid" )
								}
							}
						);
					}

					/* Shop Page */
					if (appLinkType.val() === 'shop') {
						var slug = ''; 
						var endpoint = 'https://tome.tokopedia.com/v1/shop/get_summary?domain=';

						slug = promoLink.val().replace('https://www.tokopedia.com/', '');
						if(slug.indexOf('/') >= 0) {
							slug = slug.replace('/', '');
						}

						endpoint = endpoint + slug;

						$.get(
							endpoint,
							function(result, status) {
								console.log( result );
								// alert(slug);

								if ( result.data.length === 0 ) {
									alert( "Shop not found. Please input a valid promo link." );
									appLinkValue.val( "Invalid promo link" );
								} else {
									appLink = appLinkPrefix + 'shop/' + result.data[0].shop_id;
									alert( appLink )
									appLinkValue.val( appLink );
								}

							}
						);
					}
				}
			);
		}
	);
})( jQuery );

(function($) {
	$(
		function(){
			var appLinkValue = $( '#applink-value input' );
			var etalaseField = $( '#list-etalase select' );

			$( "#store-slug input" ).change(				
				function() {
					var applinkEtalase = '';
					var shopSlug = $( this ).val();
					var etalase = [];
					
					if ( shopSlug !== '' ) {
						var detailShopEndpoint = 'https://tome.tokopedia.com/v1/shop/get_summary?domain=' + shopSlug;
						var shopID             = '';

						$.get(
							detailShopEndpoint,
							function(result, status) {
								console.log( result );

								if ( result.data.length == 0 ) {
									alert( "Invalid promo link" );
									// shopSlug.val("Invalid promo link");
								} else {
									shopID = result.data[0].shop_id;
									// alert(result.data[0].shop_id);
								}

								var listEtalaseEndpoint = 'https://tome.tokopedia.com/v2/shop/' + shopID + '/showcase';

								$.get(
									listEtalaseEndpoint,
									function(result, status) {
										console.log( result );

										if ( result.data.showcase.length == 0 ) {
											alert( "Invalid promo link" );
											// shopSlug.val("Invalid promo link");
										} else {
											etalase = result.data.showcase;
										}

										etalaseField.html("");
										for ( var i = 0; i < etalase.length; i++ ) {
											applinkEtalase = 'tokopedia://shop/' + shopID + '/etalase/' + etalase[i].id;
											etalaseField.append( $( '<option></option>' ).val( applinkEtalase ).html( etalase[i].name ) );
										}

									}
								);
							}
						);

					}
				}
			).change();
		}
	);
})( jQuery );
