<?php
			/**
 * WordPress Options in WP-Admin
 * @since WP 4.9.2
 * @since Plugins 3.0
 * @subpackage acf
 */

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
		array(
			'page_title' => 'APP General Settings',
			'menu_title' => 'APP Settings',
			'menu_slug'  => 'app-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
			'position'   => 5,
			'icon_url'   => 'dashicons-admin-site',
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title'  => 'WS Menu Settings',
			'menu_title'  => 'WS Menu',
			'parent_slug' => 'app-general-settings',
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title'  => 'Internal Links Settings',
			'menu_title'  => 'WS Links',
			'parent_slug' => 'app-general-settings',
		)
	);
}
