<?php
if ( function_exists( 'acf_add_local_field_group' ) ) :

	acf_add_local_field_group(
		array(
			'key'                   => 'group_5bd88a40cfbef',
			'title'                 => 'Dynamic CTA',
			'fields'                => array(
				array(
					'key'               => 'field_5bd88a4a424ad',
					'label'             => 'Dynamic CTA',
					'name'              => 'dm_cta',
					'type'              => 'repeater',
					'instructions'      => 'Notes: Jika value kosong maka akan menggunakan default value, jika terdapat kategori yang sama maka hanya akan digunakan salah satunya saja.',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'collapsed'         => '',
					'min'               => 0,
					'max'               => 0,
					'layout'            => 'row',
					'button_label'      => 'Add Condition',
					'sub_fields'        => array(
						array(
							'key'               => 'field_5bd88a77424ae',
							'label'             => 'Category',
							'name'              => 'dm_cta_cat',
							'type'              => 'taxonomy',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'taxonomy'          => 'category',
							'field_type'        => 'select',
							'allow_null'        => 0,
							'add_term'          => 0,
							'save_terms'        => 0,
							'load_terms'        => 0,
							'return_format'     => 'id',
							'multiple'          => 0,
						),
						array(
							'key'               => 'field_5bd88ac3424af',
							'label'             => 'CTA Text',
							'name'              => 'dm_cta_text',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => array(
								array(
									array(
										'field'    => 'field_5bd88a77424ae',
										'operator' => '!=empty',
									),
								),
							),
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
						array(
							'key'               => 'field_5bd88ada424b0',
							'label'             => 'CTA Link',
							'name'              => 'dm_cta_link',
							'type'              => 'url',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => array(
								array(
									array(
										'field'    => 'field_5bd88a77424ae',
										'operator' => '!=empty',
									),
								),
							),
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
						),
						array(
							'key'               => 'field_5bd88af8424b1',
							'label'             => 'APP Link',
							'name'              => 'dm_cta_app_link',
							'type'              => 'text',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => array(
								array(
									array(
										'field'    => 'field_5bd88a77424ae',
										'operator' => '!=empty',
									),
								),
							),
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'maxlength'         => '',
						),
					),
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'post',
					),
				),
				array(
					array(
						'param'    => 'post_template',
						'operator' => '==',
						'value'    => 'default',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => '',
		)
	);

endif;
