<?php
// WP REST API - v2

// Promo API Menu

function custom_api_menu() {
	$menu = get_field( 'primary_menu', 'option' );
	if ( $menu != false ) {
		// Push Content
		return rest_ensure_response( $menu );
	} else {
		// Return a WP_Error if flase
		return new WP_Error( 'rest_menu_empty', esc_html__( 'Please Contact Administrator, and ask him to fill the menu API', 'tokopedia' ), array( 'status' => 200 ) );
	}

	// If the code somehow executes to here something bad happened return a 500.
	return new WP_Error( 'rest_api_sad', esc_html__( 'Something went horribly wrong.', 'tokopedia' ), array( 'status' => 500 ) );
}

// Promo API Menu

function internal_links() {
	$menu = get_field( 'internal_links', 'option' );
	if ( $menu != false ) {
		// Push Content
		return rest_ensure_response( $menu );
	} else {
		// Return a WP_Error if flase
		return new WP_Error( 'rest_menu_empty', esc_html__( 'Please Contact Administrator, and ask him to fill the menu API', 'tkpd-promo' ), array( 'status' => 200 ) );
	}

	// If the code somehow executes to here something bad happened return a 500.
	return new WP_Error( 'rest_api_sad', esc_html__( 'Something went horribly wrong.', 'tokopedia' ), array( 'status' => 500 ) );
}

// Promo Add Field Promo Text CTA

function get_cta_text() {
	// Get cta_src val from param
	if ( isset( $_GET['cta_src'] ) ) {
		$cta_src = (int) sanitize_title( $_GET['cta_src'] );
	} else {
		$cta_src = 0;
	}
	$cta    = get_the_terms( get_the_ID(), 'category' );
	$dm_cta = get_field( 'dm_cta' );

	if ( $dm_cta != null ) {
		$cdt = in_array( $cta_src, array_column( $dm_cta, 'dm_cta_cat' ) );
	} else {
		$cdt = false;
	}

	if ( $cta != null ) {
		// Validation If single array, show cat metas
		if ( count( $cta ) > 1 && ! $cdt ) {
			if ( get_field( 'default_cta_button', 'option' ) != null ) {
				return get_field( 'default_cta_button', 'option' );
			} else {
				return esc_html__( 'Cek Sekarang' );
			}
		} else {
			if ( $cta_src != 0 && $cdt ) {
				$cta_picker = cta_picker( $cta_src, get_the_ID() );
				return esc_html__( $cta_picker['text'] );
			} else {
				return get_field( 'cta_button', 'category_' . $cta[0]->term_id );
			}
		}
	} else {
		return esc_html__( 'Cek Sekarang' );
	}

	// If the code somehow executes to here something bad happened return a 500.
	return new WP_Error( 'rest_api_sad', esc_html__( 'Something went horribly wrong.', 'tokopedia' ), array( 'status' => 500 ) );
}





// Action Manager
add_action(
	'rest_api_init',
	function () {
		register_rest_route(
			'wp/v2',
			'/hmenu',
			array(
				'methods'  => 'GET',
				'callback' => 'custom_api_menu',
			)
		);

		register_rest_route(
			'wp/v2',
			'/internal-links',
			array(
				'methods'  => 'GET',
				'callback' => 'internal_links',
			)
		);

		// registering the CTA Button Text field
		register_rest_field(
			'post',
			'cta_text',
			array(
				'get_callback' => 'get_cta_text',
				'schema'       => array(
					'description' => __( 'CTA Button Text', 'tokopedia' ),
					'type'        => 'string',
				),
			)
		);

		// registering the Promo Codes
		register_rest_field(
			'post',
			'promo_codes',
			array(
				'get_callback' => 'get_promo_codes',
				'schema'       => array(
					'description' => __( 'Promo Codes.', 'tokopedia' ),
					'type'        => 'object',
				),
			)
		);

		// Override Promo Meta
		register_rest_field(
			'post',
			'meta',
			array(
				'get_callback' => 'override_promo_meta',
				'schema'       => array(
					'description' => __( 'Promo Meta', 'tokopedia' ),
					'type'        => 'array',
				),
			)
		);

		// Add Acf for Promo Codes
		register_rest_field(
			'post',
			'acf',
			array(
				'get_callback' => 'acf_promo_codes',
				'schema'       => array(
					'description' => __( 'Promo Codes', 'tokopedia' ),
					'type'        => 'array',
				),
			)
		);
	}
);


/**
 * WordPress Rest API Promo Multiple Codes
 */

function get_promo_codes() {
	$codes = get_field( 'promo_codes' );

	// Check data, and make it empty array if false
	if ( ! empty( $codes ) ) {
		return $codes;
	} else {
		return array();
	}

	// If the code somehow executes to here something bad happened return a 500.
	return new WP_Error( 'rest_api_err', esc_html__( 'Something went horribly wrong.', 'tokopedia' ), array( 'status' => 500 ) );
}

function acf_promo_codes(){
	$codes = get_promo_codes();

	if ( empty( $codes ) ){
		$multi_state = false;
	}else{
		$multi_state = true;
	}

	return array(
		'multiple_promo_code'      => $multi_state,
		'promo_codes'        	   => $codes,
	);
}



/**
 * WordPress Rest API Override Promo Meta
 */

function override_promo_meta( $request ) {
	// Override data with $ctasrc
	if ( isset( $_GET['cta_src'] ) ) {
		$cta_src = (int) sanitize_title( $_GET['cta_src'] );
	} else {
		$cta_src = 0;
	}
	$dm_cta = get_field( 'dm_cta' );
	// Check if CTA Overrider exist
	if ( $dm_cta != null ) {
		$cdt = in_array( $cta_src, array_column( $dm_cta, 'dm_cta_cat' ) );
	} else {
		$cdt = false;
	}
	$cta = cta_picker( $cta_src, get_the_ID() );

	// Applink
	if ( isset( $cta ) && $cdt ) {
		$applink = $cta['applink'];
		$link    = $cta['uri'];
	} else {
		$applink = get_field( 'app_link' );
		$link    = get_field( 'promo_link' );
	}

	return array(
		'promo_code'      => get_field( 'promo_code' ),
		'app_link'        => $applink,
		'promo_link'      => $link,
		'min_transaction' => get_field( 'min_transaction' ),
		'thumbnail_image' => wp_get_attachment_image_src( get_field( 'thumbnail_image' ) )[0] ?: get_field( 'thumbnail_image' ),
		'start_date'      => get_field( 'start_date' ),
		'end_date'        => get_field( 'end_date' ),
		'cta_src'         => $cta_src,
	);

	// If the code somehow executes to here something bad happened return a 500.
	return new WP_Error( 'rest_api_err', esc_html__( 'Something went horribly wrong.', 'tokopedia' ), array( 'status' => 500 ) );
}
